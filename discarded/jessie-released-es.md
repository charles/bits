Title: ¡Se ha publicado Debian 8.0 Jessie!
Date: 2015-04-26 03:15
Tags: jessie
Slug: jessie-released
Author: Ana Guerrero Lopez
Translator: Daniel Cialdella
Lang: es
Status: draft

![Alt Ya se ha publicado Jessie](|static|/images/banner_jessie.png)

Hay un nuevo Alguacil en la ciudad. Y su nombre es Jessie. Estamos 
contentos de anunciar la nueva versión Debian 8.0, con nombre en clave
*Jessie*.

**¿Quieres instalarlo?**
Elige tu [dispositivo favorito](https://www.debian.org/distrib/) entre 
Blue-Ray Disc, DVDs, CDs y memoria USB. 
Luego lee la [guía de instalación](https://www.debian.org/releases/jessie/installmanual).
Para usuarios de Debian en la nube también se ofrecen [imágenes 
OpenStack pre-generadas](http://cdimage.debian.org/cdimage/openstack/current/) listas para ser usadas.

**¿Eres un usuario feliz de Debian y solo quieres actualizarte?**
¡Solo debes hacer un *apt-get dist-upgrade* desde Jessie!
Aprende cómo hacerlo leyendo [la guía de instalación](https://www.debian.org/releases/jessie/installmanual)
y las [notas de la publicación](https://www.debian.org/releases/jessie/releasenotes).

**¿Quieres celebrar la publicación?**
¡Comparte el [cartel de este blog](https://wiki.debian.org/DebianArt/Themes/Lines#Release_blog_banner.2Fbutton) en tu blog o sitio en internet!

