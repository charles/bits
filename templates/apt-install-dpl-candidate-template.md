Date: 2024-04-05 20:36
Author: $interviewer with The Debian Publicity Team
Tags: interview, dpl, vote, meetDDs
Status: draft

The Debian Project Developers will shortly vote for a new Debian Project Leader
known as the DPL.

The DPL is the official representative of representative of The Debian Project
tasked with managing the overall project, its vision, direction, and finances.

The DPL is also responsible for the selection of Delegates, defining areas of
responsibility within the project, the coordination of Developers, and making
decisions required for the project.

Our outgoing and present DPL $CurrentDPL_Name served $NUMBER terms, from $YEAR
through YEAR. $DPL__FirstName shared $Their/His/Her last [Bits from the
DPL](https: //bits.d.o/YYYY/MM/$FIXME) post to Debian recently and
$THEIR/His/Her hopes for the future of Debian.

Recently, we sat with the $NUMBER present candidates for the DPL position
asking questions to find out  who they really are in a series of interviews
about their platforms, visions for Debian, lives, and even their favorite text
editors. The interviews were conducted by $INTERVIEWER(S)NAME and made
available from video and audio transcriptions:

* $CANDIDATE [Interview]($FIXME)
* $CANDIDATE [Interview]($FIXME)
* $CANDIDATE [Interview]($FIXME)
* $CANDIDATE [Interview]($FIXME)

Voting for the position starts on $DATE_WRITTEN_OUT

We used the following tools and services:
[Turboscribe.ai](https://turboscribe.ai) for the transcription from the audio
and video files, [IRC: Oftc.net](https://www.oftc.net) for communication,
[Jitsi meet](https://meet.jit.si/) for interviews,  and [Open Broadcaster
Software (OBS)](https://obsproject.com/) for editing and video.

We are proud to present the transcripts of the interviews in edited only in a
few areas for readability.
