This repository contains all the sources needed to maintain the Debian blog
https://bits.debian.org and instructions to build the site.


## Contents

    Makefile            makefile for bits.d.o

    pelicanconf.py      configuration file for pelican

    publishconf.py      publishing configuration file for pelican

    theme-bits/         contains the blog's theme

    output/             should always be empty, it is only used to generate
                        the blog locally
    plugins/            pelican plugins

    dpl/                source files used for the internal DPL blog
                        on bits.d.o. *currently is not working, use 
                        content/YYYY/dpl-bits instead

    README.md           please do :) 

    discarded/          discarded blog posts - this should be cleared
                        every few months. ideally, if the discard was
                        timing related then delete it, if the discard was 
                        technical and resolved then create a template from the
                        discarded post and remove the discarded post

    content/            top level directory of the source files for all posts 
                        published. this is our working directory

    content/pages/      source files for the static pages

    content/extras/     source files for the favicon.ico file

    content/documents   source files for .odt, .pdf. docx, etc .... files used for a post

    content/images/     source files for any image embedded inside of a post

    content/YYYY/       source files for the blog posts by year (YYYY)	
    ../debconfYY/       source files for DebConf posts
    ../dpl-bits/        source files used for the DPL posts to the blog
    ../interviews/      source files for interviews 
    ../new-developers/  source files for the bi/monthly new developers,
                        maintainers, and contributors posts

    templates/          example source files for most of our postings, choose
                        from any to create your post or create new templates
                        here

## Formating, Credit, Use

- [Bits](https://bits.debian.org) is the offical blog of the Debian Project. The 
service is administred by the [Publicity Team](https://wiki.debian.org/Teams/Publicity)
and is mainly used for announcements, news, and official statements. Bits posts generally 
are longer in information than other Debian news/media channels such as our
[Micronews](https://micronews.debian.org). Both services push posts to various
social networks. 

- Give credit to authors, creators, and translators of work that is used in the
posts made using the author, artwork, translator, or image tags. 
 

- 80 Columns as a standard. 

## Templates

We have the following in use templates, feel free to add your own for any
repetitive posts. 

* _apt-install-dpl-candidate-template_

    current format used for the interview of DPL position candidates

* _blog-post-ideas_

    helpful ideas that you can use to create blog posts

* _new-developer-template_

    used for the bi/monthly welcome for new developers, maintainers, and contributors

*  _new-developer-sources_

    places to look to find the names and information about new members

* _dpb-template_

    format for the Debian Project Bits newsletter post

*  **_bits-post-template_**

    For all items posted the following Metadata Syntax should be 
    present in the head of the post in order for pelican to process the post.

        Title: This is my awesome news item
        Slug: this-is-my-awesome-news-item
        Date: YYYY-MM-DD HH:MM
        Author: Name Surname(s), Name Surname(s)
        Tags: tag1, tag2
        Lang: en
        Translator: Name Surname(s), Name Surname(s)
        Image: /images/openlogo-100.png
        Artist: Software in the Public Interest, Inc.
        Status: draft


## Inserting Links and Media

#### Links
As opposed to email links which are stripped and re-formatted like link[1]
link[2] we use markdown for all links so they can be displayed as Link
Text or Alt Text. 

Syntax:

        [Link Text](html link/filename) 

Use:

        [DebConf24 Logo Contest](https://lists.debian.org/debconf-discuss/2024/01/msg00000.html)

##### Shortening Links
For long links which break formatting use [deb.li](https://deb.li) a shortURL
service for Debian related usage, hosted and developed by Debian Devloper
BerndZeimetz (bzed).

deb.li is restricted by submission IP address [FAQ](https://wiki.debian.org/deb.li), in
order to use the service create the following script locally in /usr/local/bin:

        #!/bin/sh -e
        # call me 'deb.li' and use me as 'deb.li https://some/long/url', stdout is a cut-and-paste friendly URL
        echo "https://deb.li/$(ssh master.debian.org ~pollo/godebian-client/add_url "'$*'")"

Use:
    
        $ deb.li https://www.debian.org
        https://deb.li/3LLAj

#### Tags
If desired you can link a tag within the post to point readers to other topics
in the same category on the blog.

Syntax:

             [tag descriptor](|tag)}

Use:

             [artwork](|tag|artwork)

#### Media
For media consult the directory structure: 

        ../content 
        |--- $year
        |-- documents
        |-- extras
        |-- images
        |-- pages


The subdirectories under the content directory are from where all static
media is served. When including a media object inside of your post you need to
link to it using its relative file path and object name.

Syntax:

 
         ![[Alt Text]](static declaration|directory/filename)

Wen adding any image or media be sure to credit the author of the work directly
under the work displayed and _most importanly_ in the metadata *Authors:* field.
_Be especially mindful that for accesibility readers, the Alt Text is not just
useful but *very* important.
 
Use:

    ![[DebConf24 Logo Contest Winner]](|static|/images/logo_win_dc24_skorea.png)
         '_sun-seagull-sea_' by Woohee Yang

##### Uploading Media
Place your desired media in the appropriate directory for use. All that is
needed here is to add the file in your push to the repository.

## Testing locally

**Any new posts should be created using the Status: _draft_**

* Install:
    - make
    - pelican
    - ruby-mdl

* Clone this repository

		$ git clone https://salsa.debian.org/publicity-team/bits.git && cd bits

* Build the site:

		$ make html

* Serve it locally to see if it's rendered properly:

		$ make serve

* Open <http://localhost:8000/drafts/> in your browser and click on the link
for the item you created, the name will be the 'slug' that you created with
.html appended on the end

* Run markdownlint to check for any problems on the file:

		$ make check

* Once satisfied with your work or the review of your work, change the post
status to _published_. This will allow for the publishing of your post. 
You can skip this step on occasion as someone from the publicity group will
need to push the post for you. They may review, edit, or change the post prior
to publishing for syntax or readability

For a quick and dirty Markdown syntax check, you could do e.g.

		$ cmark content/2024/project/debian-turns-31.md | w3m -T text/html -dump -

Or use markdownlint (from `ruby-mdl` package) - which we use in our CI:

		$ mdl path/to/markdown/file.md

## Updating the blog in dillon.d.o

This is only valid for people in the publicity group (the group of Linux
users in Debian machines).

* Log in dillon.debian.org and get a copy of the bits repository
or update your current copy:

		$ git clone https://salsa.debian.org/publicity-team/bits.git

* Execute:

		$ cd bits
		$ make mpublish

This will remove all the files at `/srv/bits-master.debian.org/htdocs`
and will regenerate a new copy of the website and propagate any updates
to all of the static mirrors.

Check the webpage at bits.debian.org to see if the post(s) has been published, if so in the IRC
channel write "Published!"

All done. :)
