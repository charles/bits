all
# We use only H3 and H4 ocasionally for stylistic purposes
exclude_rule 'MD001'
exclude_rule 'MD002'
# God! 4 lines for identation please :-)
rule 'MD007', :indent => 4
# Line length is being triggered by pelican metadata such as title :-(
exclude_rule 'MD013'
# Pelican don't generate anchors from header title
exclude_rule 'MD024'
# We like to use !, ? and : in the end of headers :-)
rule 'MD026', :punctuation => '.,;'
# We like to use ordered lists with correct prefix (1., 2., etc)
rule 'MD029', :style => ':ordered'
# Some translations use <span lang=""> for names and abbr for acronyms
rule 'MD033', :allowed_elements => 'span, abbr'
# We used entire line emphasis to show warnings and it triggers this one
exclude_rule 'MD036'
# We already generate the title as kind of H1 header
exclude_rule 'MD041'
# We can use indented or fenced code blocks, but must follow the same style in
# the whole file
rule 'MD046', :style => ':consistent'
# mdl gets confused with the syntax for static images in pelican
exclude_rule 'MD055'
exclude_rule 'MD057'
