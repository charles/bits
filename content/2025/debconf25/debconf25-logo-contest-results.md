Title: DebConf25 Logo Contest Results
Slug: debconf25-logo-contest-results
Date: 2054-01-03 06:00
Author: Donald Norwood, Santiago Ruano Rincón
Tags: debconf25, logos, contests, debconf, artwork, france, brest
Lang: en
Image: /images/juliana-dc25-logo1-vertical.png
Artist: Juliana Camargo
Status: draft

We [asked](https://lists.debian.org/debconf-announce/2024/12/msg00000.html)
asked the community to help design the logo for the 25th
[Debian Developers Conference](https://www.debconf.org/) and the results are
in! The logo contest for [DebConf25](https://debconf25.debconf.org) received 23
submissions and 295 vote responses.

The winning logo received $XX votes, the 2nd favored logo received $XX votes,
and the 3rd most favored received $XX votes. We used the
[Condorcet](https://en.wikipedia.org/wiki/Condorcet_method) voting method,
something most should be familiar with as we use the same
[voting process](https://www.debian.org/vote/) across the entire project.
Here are the numbers behind the [votes](http://voting summary link).

Thank you to $secondplace and $thirdplace for sharing their artistic visions.

A very special Thank You to everyone who took the time to vote for our beautiful
new logo!

The DebConf25 Team is proud to
[share](https://lists.debian.org/debconf-announce/2024/12/msg00000.html) for
**preview only** the winning logo for the 25th Debian Developer Conference:

![[DebConf25 Logo Contest Winner]](|static|/images/juliana-dc25-logo1-vertical.png)

'Tower with red Debian Swirl originating from blue water', by Juliana Camargo'

Please be aware that this is a preview copy, other revisions will occur for sizing, print, and
media.

Looking forward to seeing you all at #debconf25 in #Brest, France 2025!
