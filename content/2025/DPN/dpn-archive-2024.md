Title: Debian Project News Archive Volume 1, Issue 2024
Slug: debian-project-news-archive-2024
Date: 2025-01-15 12:30
Author: DD, DC, DD, DC, Donald Norwood, DD, DC, DD
Tags: news, teams, debconf, dpn, events, sponsors, apt, memoriam, releases, archive, sources, blends, project, security, draft
Status: draft

**Debian Project News Archive** *Volume 1, Issue 2024* *Jaunary 15, 2025*

# Welcome to a very special Archive issue of the Debian Project News!

This special issue highlights some of the joys, news, events, advances, and
memorable things that our project and community experienced in the year 2024.

First let us see what some of the hard working teams and groups are working on:

# Teams

### The Publicity team

We have been very busy, we have ... ... ...

### The Debian Med Team

The Debian Med Team held their bug squashing [Advent Calendar](https://lists.debian.org/debian-med/2024/11/msg00021.html) during the month of
December, keeping with a tradition of closing one bug per day in an effort to
clean as many bugs as possible before the end of the year.
The marathon squashing event [summary](https://lists.debian.org/debian-med/2024/12/msg00032.html) shared that
122 bugs were closed which reduced open issues to under the 300 count mark.

### The $TEAM team

We have been very busy, we have ... ... ..

# Celebrations

### Debian turns 31!

We celebrated [31 Years of Debian](https://bits.debian.org/2024/08/debian-turns-31.html)
"As the expression goes, 'Time flies when you are having fun', meaning you do not normally account for the passage of time when you are distracted and enjoying yourself..."

 -- pictures and events

# Events

### DebConf24/DebCamp24

[![DebConf24 group photo - click to enlarge](|static|/images/debconf24_group_photo.jpg)](https://wiki.debian.org/DebConf/24/Photos?action=AttachFile&do=view&target=debconf24_group.jpg)

The 2024 [Debian Developers Conference](https://debconf.org) and DebCamp were held in beautiful Busan, South Korea with over 339 attendees from over 48 countries around the world. The conference hosted 108 events including Talks, Discussions, BoFs, Workshops, and many other activites.

### We had a great [DebConf24 Logo Contest](https://bits.debian.org/2024/02/debconf24-logo-contest-results.html)

The contest received 10 submissions and 354 responses.

#### MiniDebConf$x

debian, debian, lorem, debian

### MiniDebConf$x

debian, debian, lorem, debian

### BSP$location

debian, debian, lorem, debian

### BSP$location

debian, debian, lorem, debian

# Vendors, Sponsors, and new Partners

* (There are Vendors we can name, need to reach out to DSA and the partnership teams. -DN)

### Sponsors

We wish to thank all of our generous DebConf24 sponsors for their infrastructure, financial, and technical, support.
(remove or link to lower sentence -DN)

* Platinum level of sponsors were: Proxmox, Infomaniak and Wind River.

* Gold level of sponsors: Ubuntu, Freexian, the Korea Tourism Organization,
 the Busan IT Industry Promotion Agency, Microsoft, and doubleO.

* Silver level of sponsors: Roche, Two Sigma, Arm, The Bern University
 of Applied Sciences, Google, FSIJ, The Busan Tourism Organisation, the Civil
 Infrastructure Platform, Collabora, and the Matanel Foundation.

Debian acknowledges and appeciates each sponsor as a supporting element of not
only our conference but additionally toward the work of FLOSS around the world.*

### Partners

We are excited to announce and welcome Freexian into Debian Partners.

### New Ventures

* [OpenStreetMap migrated to Debian](https://bits.debian.org/2024/11/openstreetmap-on-debian.html)

We were and still are very exited to share this news with the community.
Grant Slater, the Senior Site Reliability Engineer for the OpenStreetMap Foundation sat with us for an interview on the migration, tools, and use of Debian packages.

# Diversity, FLOSS, and Outreach

#### [Debian selected seven contributors to work under mentorship on a variety of projects with us during the Google Summer of Code](https://bits.debian.org/2024/05/welcome-gsoc2024-contributors.html)

Android SDK Tools in Debian, Benchmarking Parallel Performance of Numerical MPI Packages,  Debian MobCom,  Improve support of the Rust coreutils in Debian,
Improve support of the Rust findutils in Debian, Expanding ROCm support within Debian and derivatives, and  procps: Development of System Monitoring, Statistics and Information Tools in Rust.

#### [Celebrating Women in STEM](https://bits.debian.org/2024/10/ada-lovelace-day-2024-interview-with-some-women-in-debian.html)

Ada Lovelace Day was celebrated on October 8 in 2024, and on this occasion, to celebrate and raise awareness of the contributions of women to the STEM fields we interviewed some of the women in Debian.

#### [Debian welcomes its new Outreachy interns](https://bits.debian.org/2024/11/welcome-outreachy-interns-2024.html)

Debian continues participating in Outreachy, and we're excited to announce that Debian has selected two interns for the Outreachy December 2024 - March 2025 round.

Patrick Noblet Appiah will work on Automatic Indi-3rd-party driver update, mentored by Thorsten Alteholz.

Divine Attah-Ohiemi will work on Making the Debian main website more attractive by switching to HuGo as site generator, mentored by Carsten Schoenert, Subin Siby and Thomas Lange.

# Let's talk briefly about not Breaking Debian:

[Advice For New Users On Not Breaking Their Debian System](https://wiki.debian.org/DontBreakDebian)

It is very easy to break a system using just one or two outside repositories,
ppas, or scripts. No matter how much you trust the source of the new package,
or require the newest feature, you need to be aware that if you are not rather
experienced in experimental things that is it again very easy to bork a system.

## Source.list review

##### Example of the sources.list file

    deb http://deb.debian.org/debian bookworm main
    deb-src http://deb.debian.org/debian bookworm main

    deb http://deb.debian.org/debian-security/ bookworm-security main
    deb-src http://deb.debian.org/debian-security/ bookworm-security main

    deb http://deb.debian.org/debian bookworm-updates main
    deb-src http://deb.debian.org/debian bookworm-updates main

##### Example using the repository components:

    deb http://deb.debian.org/debian bookworm main non-free-firmware
    deb-src http://deb.debian.org/debian bookworm main non-free-firmware

    deb http://deb.debian.org/debian-security/ bookworm-security main non-free-firmware
    deb-src http://deb.debian.org/debian-security/ bookworm-security main non-free-firmware

    deb http://deb.debian.org/debian bookworm-updates main non-free-firmware
    deb-src http://deb.debian.org/debian bookworm-updates main non-free-firmware

For more information and guidelines on proper configuration of the apt
source.list file please see the [Configuring Apt Sources -
Wiki](https://wiki.debian.org/SourcesList) page.

## Debian Releases

### Debian offers several rolling and experimental releases working on the developing edge of technology

#### stable [codename: (curently) bookworm]: The stable production distribution of the latest officially released distribution of Debian. This is the primary recommend release for use complete with security updates provided

#### unstable [codename: sid]: The unstable distribution where active development of Debian occurs. This distribution is primarily used by developers and package maintainers that are queued for upload to testing. Security updates for these software packages are provided by the individual package maintainers

#### testing [codename: (currently) Trixie]: The testing distribution containing software packages that are queued for the next stable distribution. This distribution offers updated and recent software versions complete with security updates provided. The testing distribution is named for the next stable distribution, presently Trixie will be the name of the next stable release

* There is a highly suggested caveat of [Best practices for using testing/sid](https://wiki.debian.org/DebianUnstable#What_are_some_best_practices_for_testing.2Fsid_users.3F) should you decide to use testing or sid. Users, testers, and hobbyists using this distribution participate along with Debian Developers across the development of Debian

#### experimental [codename: RC-Buggy]: This directory contains packages and tools that are still in the alpha testing stage of development, hence the term experimental. Not recommended even for those with experience

#### backports - This repository contains pacakges taken from the next upcoming testing and unstable distributions and recompiled for use within a current stable distribution

* (better explanation for backports anyone? -DN)

## Debian Live

debian, debian, lorem, debian
debian, debian, lorem, debian
debian, debian, lorem, debian

## Debian Pure Blends

Debian offers some specific pure blends designed to support a particular
group with an out of the box deployment.

#### [Debian Junior](https://wiki.debian.org/DebianJr)

Debian Jr is an OS that is specifically built toward children and their guides.
Children and their guides will enjoy a collection of packages focused on
younger users, their development, and journey into computer literacy.

#### [Debian Med](https://wiki.debian.org/DebianMed)

Devian Med focuses on packages associated with medicine, pre-clinical research,
and life sciences. Development is focused on medial practice, imaging, and
bioinformatics.

#### [Debian Edu](https://wiki.debian.org/DebianEdu)

Debian Edu (Skolelinux) focuses on computer solutions designed around schools'
resources and needs in most educational scenrarios, featuring many
pre-configured out-of-the-box solutions for deployment and use.

#### [Debian Science](https://wiki.debian.org/DebianScience)

Debian Science is a series of [metapackages](https://wiki.debian.org/DebianScience#metapackages)
focused around several tools for researchers and scientists.

#### [Debian Astro](https://blends.debian.org/astro/tasks/)

Debian Astronomy is built around the requirements of both professional
and hobbyist astronomers, features a large number of packages focused on
telescope control, data reduction, presentation, and education. It includes
several C/C++m Java, and Python3 packages on that task.

# Security

On the topic of updates, source lists, and blends let us not forget about
good security. Debian's Security Team releases current advisories on a daily basis.
[Security blurb and link. ]

# Inside Debian

## New Debian Developers and Maintainers

### We welcomed our newest Debian Project Members:

#### Developers

Alexandre Detiste (tchet), Amin Bandali (bandali), Jean-Pierre Giraud (jipege),
Timthy Pearson (tpearson), Carles Pina i Estany (cpina), Dave Hibberd (hibby),
Soren Stoutner (soren), Daniel Gröber (dxld), Jeremy Sowden (azazel),
Ricardo Ribalda Delgado (ribalda), Patrick Winnertz (winnie), Fabian Gruenbichler
(fabiang), Dennis van Dok (dvandok), Peter Wienemann (wiene), Quentin Lejard
(valde), Sven Geuer (sge), Taavi Väänänen (taavi), Hilmar Preusse (hille42),
Matthias Geiger (werdahias), Yogeswaran Umasankar (yogu), Carlos Henrique Lima
Melara (charles), Joenio Marques da Costa (joenio), Blair Noctis (ncts), Joachim
Bauch (fancycode), Alexander Kjäll (capitol), Jan Mojžíš (janmojzis), and
Xiao Sheng Wen (atzlinux).

#### Maintainers

Safir Secerovic, Joachim Bauch, Ananthu C V, Francesco Ballarin, Yogeswaran
Umasankar, Kienan Stewart, Juri Grabowski, Tobias Heider, Jean Charles
Delépine, Guilherme Puida Moreira, Antoine Le Gonidec, Arthur Barbosa Diniz,
Bernhard Miklautz, Felix Moessbauer, Maytham Alsudany, Aquila Macedo, David
Lamparter, Tim Theisen, Stefano Brivio, Shengqi Chen, Taihsiang Ho, Alberto
Bertogli, Alexis Murzeau, David Heilderberg, Xiyue Deng, Kathara Sasikumar,
and Philippe Swartvagher.

To find out more about our newest members or any Debian Developer, look
for them on the [Debian People list](https://nm.debian.org/public/people/).

## Debian elects a new Project Leader

Debian Developers voted to fill the role of the [Debian Project Leader](https://www.debian.org/devel/leader) (DPL).

There were 2 candidates for the position this year, both sat with the Publicity team for interviews:

* Intervew with Andreas Tille: [apt install dpl-candidate: Andreas Tille](https://bits.debian.org/2024/04/dpl-interview-AndresTille.html)

* Intervew with Sruthi Chandran: [apt install dpl-candidate: Sruthi Chandran](https://bits.debian.org/2024/04/dpl-interview-SruthiChandran.html)

The [vote](https://www.debian.org/vote/2024/vote_001)
was done via the [Condorcet method](https://en.wikipedia.org/wiki/Condorcet_method), once talled Andreas Tille was announced as the new DPL.

This election was also the stepping down of 4 time DPL Jonathan Carter who led Debian from 2020 through the end of the 2024 term.

* Jonathan Carter shares his thoughts, vision, and thanks in his last [Bits from the DPL](https://bits.debian.org/2024/04/bits-from-the-dpl-april.html) posting
 in the position.

* Andreas Tille starts the new term with his first [Bits from the DPL](https://bits.debian.org/2024/05/bits-from-the-dpl-may2024.html) posting, sharing his thoughts
 and priorities.

# The most popular packages

#### [gpgv](https://packages.debian.org/stable/gpgv)

* GNU privacy guard signature verification tool. _99,053_ installations.

&nbsp;&nbsp;&nbsp;&nbsp;gpgv is actually a stripped-down version of gpg which
is only able to check signatures. It is somewhat smaller than the fully-blown
gpg and uses a different (and simpler) way to check that the public keys used
to make the signature are valid. There are no configuration files and only a
few options are implemented.

### package

### package

### package

### package

# Once upon a time in Debian:

(Old prior examples. I propose we use a 10 year marker by showcasing one item from each month of 2004
(could also do 31 one of these if one was so inclined for 1 event for our 31 years -DN)

2014-07-31 The Technical committee choose
[libjpeg-turbo](https://lists.debian.org/debian-devel-announce/2014/08/msg00000.html)
as the default JPEG decoder.

2010-08-01
[DebConf10](https://debconf10.debconf.org/) starts à New York City, USA

# Calls for help

### The Publicity team calls for volunteers and help!

Your Publicity team is asking for help from you our readers, developers, and
interested parties to contribute to the Debian news effort. We implore you to
submit items that may be of interest to our community and also ask for your
assistance with translations of the news into (your!) other languages along
with the needed second or third set of eyes to assist in editing our work
before publishing. If you can share a small amount of your time to aid our
team which strives to keep all of us informed, we need you. Please reach out
to us via IRC on [#debian-publicity](irc://irc.debian.org/debian-publicity)
on [OFTC.net](https://oftc.net/), or our
[public mailing list](mailto:debian-publicity@lists.debian.org),
or via email at [press@debian.org](mailto:press@debian.org) for sensitive or
private inquiries.

### The $team calls for volunteers and help!

### The $team calls for volunteers and help!

### $project calls for volunteers and help!

# in memoriam

#### We remember the work and the many lives touched by our fellow Developers who have passed

#### [Jérémy Bobbio -Lunar](https://www.debian.org/News/2024/20241119)

#### [Peter De Schrijver](https://www.debian.org/News/2024/20240725)
