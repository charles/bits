Title: Nuevos desarrolladores y mantenedores de Debian (enero y febrero del 2017)
Slug: new-developers-2017-02
Date: 2017-03-08 00:30
Author: Jean-Pierre Giraud
Tags: project
Lang: es
Status: published

Los siguientes colaboradores del proyecto se convirtieron en Debian Developers
en los dos últimos meses:

* Ulrike Uhlig (ulrike)
* Hanno Wagner (wagner)
* Jose M Calhariz (calharis)
* Bastien Roucariès (rouca)

Los siguientes colaboradores del proyecto se convirtieron en Debian Maintainers
en los dos últimos meses:

* Dara Adib
* Félix Sipma
* Kunal Mehta
* Valentin Vidic
* Adrian Alves
* William Blough
* Jan Luca Naumann
* Mohanasundaram Devarajulu
* Paulo Henrique de Lima Santana
* Vincent Prat

¡Felicidades a todos!
