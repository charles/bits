Title: Nuevos desarrolladores y mantenedores de Debian (mayo y junio del 2017)
Slug: new-developers-2017-06
Date: 2017-07-02 14:30
Author: Jean-Pierre Giraud
Tags: project
Lang: es
Status: published

Los siguientes colaboradores del proyecto se convirtieron en Debian Developers
en los dos últimos meses:

* Alex Muntada (alexm)
* Ilias Tsitsimpis (iliastsi)
* Daniel Lenharo de Souza (lenharo)
* Shih-Yuan Lee (fourdollars)
* Roger Shimizu (rosh)

Los siguientes colaboradores del proyecto se convirtieron en Debian Maintainers
en los dos últimos meses:

* James Valleroy
* Ryan Tandy
* Martin Kepplinger
* Jean Baptiste Favre
* Ana Cristina Custura
* Unit 193

¡Felicidades a todos!
