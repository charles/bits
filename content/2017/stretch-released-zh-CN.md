Title: Debian 9.0 Stretch 发布了！
Date: 2017-06-18 08:25
Tags: stretch
Slug: stretch-released
Author: Ana Guerrero Lopez and Laura Arjona Reina
Status: published
Lang: zh-CN
Translator: Anthony Fok

![Alt Stretch has been released](|static|/images/banner_stretch.png)

请接受紫色橡胶玩具章鱼的拥抱吧！
我们很高兴宣布：代号为 *Stretch*[^1] 的 Debian 9.0 已经发布了！

**想安装吗？**
有蓝光光碟、DVD、CD 和 U 盘等多种[安装介质](https://www.debian.org/distrib/index.zh-cn.html)，任君选择。接着，请阅读[安装手册](https://www.debian.org/releases/stretch/installmanual)。

**已经是 Debian 的忠实用户，只需要升级？**
您可以从现有的 Debian 8 Jessie 系统升级至 Debian 9 Stretch，请阅读[发布通告](https://www.debian.org/releases/stretch/releasenotes)。

**想庆祝这次发布吗？**
那就在您的博客或网站上分享[这篇博文的 banner](https://wiki.debian.org/DebianArt/Themes/softWaves?action=AttachFile&do=view&target=wikiBanner.png) 吧！

[^1]: Stretch 就是《玩具总动员３》里的“大章鱼”。
