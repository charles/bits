Title: Nouveaux développeurs et mainteneurs de Debian (septembre et octobre 2017)
Slug: new-developers-2017-10
Date: 2017-11-02 20:30
Author: Jean-Pierre Giraud
Tags: project
Lang: fr
Status: published

Les contributeurs suivants sont devenus développeurs Debian ces deux
derniers mois :

* Allison Randal (wendar)
* Carsten Schoenert (tijuca)
* Jeremy Bicha (jbicha)
* Luca Boccassi (bluca)
* Michael Hudson-Doyle (mwhudson)
* Elana Hashman (ehashman)

et ces contributeurs ont été acceptés comme mainteneurs Debian durant la
même période :

* Ervin Hegedüs
* Tom Marble
* Lukas Schwaighofer
* Philippe Thierry

Félicitations !
