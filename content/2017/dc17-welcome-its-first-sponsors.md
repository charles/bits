Title: DebConf17 welcomes its first eighteen sponsors!
Slug: dc17-welcome-its-first-sponsors
Date: 2017-03-20 15:15
Author: Laura Arjona Reina and Tássia Camões Araújo
Tags: debconf17, debconf, sponsors
Status: published

![DebConf17 logo](|static|/images/800px-Dc17logo.png)

DebConf17 will take place in Montreal, Canada in August 2017. We are working hard
to provide fuel for hearts and minds, to make this conference once again a
fertile soil for the Debian Project flourishing. Please join us and support
this landmark in the Free Software calendar.

Eighteen companies have already committed to sponsor DebConf17! With a warm
welcome, we'd like to introduce them to you.

Our first Platinum sponsor is  [**Savoir-faire Linux**](https://www.savoirfairelinux.com/),
a Montreal-based Free/Open-Source Software company which offers Linux and
Free Software integration solutions and actively contributes to many free
software projects. *"We believe that it's an essential piece [Debian], in a
social and political way, to the freedom of users using modern technological
systems"*, said Cyrille Béraud, president of Savoir-faire Linux.

Our first Gold sponsor is [**Valve**](http://www.valvesoftware.com/),
a company developing games, social entertainment platform, and game engine technologies.
And our second Gold sponsor is [**Collabora**](https://www.collabora.com/),
which offers a comprehensive range of services to help its clients to
navigate the ever-evolving world of Open Source.

As Silver sponsors we have
[**credativ**](http://www.credativ.de/)
(a service-oriented company focusing on open-source software and also a
[Debian development partner](https://www.debian.org/partners/)),
[**Mojatatu Networks**](http://www.mojatatu.info/)
(a Canadian company developing Software Defined Networking (SDN) solutions),
the [**Bern University of Applied Sciences**](https://www.bfh.ch/)
(with over [6,600](https://www.bfh.ch/en/bfh/facts_figures.html) students enrolled,
located in the Swiss capital),
[**Microsoft**](https://www.microsoft.com/)
(an American multinational technology company),
[**Evolix**](http://evolix.ca/)
(an IT managed services and support company located in Montreal),
[**Ubuntu**](https://www.canonical.com/)
(the OS supported by Canonical)
and [**Roche**](http://www.roche.com/careers)
(a major international pharmaceutical provider and research company
dedicated to personalized healthcare).

[**ISG.EE**](http://isg.ee.ethz.ch/),
[**IBM**](https://www.ibm.com/),
[**Bluemosh**](https://bluemosh.com/),
[**Univention**](https://www.univention.com/)
and  [**Skroutz**](https://www.skroutz.gr/)
are our Bronze sponsors so far.

And finally, [**The Linux foundation**](https://www.linuxfoundation.org/),
[**Réseau Koumbit**](https://www.koumbit.org/)
and [**adte.ca**](http://www.adte.ca/)
are our supporter sponsors.

## Become a sponsor too!

Would you like to become a sponsor? Do you know of or work in a company or
organization that may consider sponsorship?

Please have a look at our
[sponsorship brochure](https://media.debconf.org/dc17/fundraising/debconf17_sponsorship_brochure_en.pdf)
(or a summarized [flyer](https://media.debconf.org/dc17/fundraising/debconf17_sponsorship_flyer_en.pdf)),
in which we outline all the details and describe the sponsor benefits.

For further details, feel free to contact us through [sponsors@debconf.org](mailto:sponsors@debconf.org), and
visit the DebConf17 website at [https://debconf17.debconf.org](https://debconf17.debconf.org).
