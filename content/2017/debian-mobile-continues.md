Title: Work on Debian for mobile devices continues
Slug: debian-mobile-continues
Date: 2017-08-17 15:40
Author: W. Martin Borgert
Tags: debian, mobile, devices, debconf17, pocketchip, pyra, zerophone
Status: published

Work on Debian for mobile devices, i.e. telephones, tablets, and
handheld computers, continues. During the recent
[DebConf17](https://debconf17.debconf.org/) in Montréal, Canada, more
than 50 people had a meeting to reconsider opportunities and
challenges for Debian on mobile devices.

A number of devices were shown at DebConf:

* [PocketCHIP](https://getchip.com/pages/pocketchip): A very small
   handheld computer with keyboard, Wi-Fi, USB, and Bluetooth, running
   Debian 8 (Jessie) or 9 (Stretch).
* [Pyra](https://pyra-handheld.com/boards/pages/pyra/): A modular
   handheld computer with a touchscreen, gaming controls, Wi-Fi,
   keyboard, multiple USB ports and SD card slots, and an optional
   modem for either Europe or the USA. It will come preinstalled with
   Debian.
* [Samsung Galaxy S Relay 4G](https://en.wikipedia.org/wiki/Samsung_Galaxy_S_Relay_4G):
   An Android smartphone featuring a physical keyboard, which can
   already run portions of Debian userspace on the Android kernel.
   Kernel upstreaming is on the way.
* [ZeroPhone](https://www.crowdsupply.com/arsenijs/zerophone): An
   open-source smartphone based on Raspberry Pi Zero, with a small
   screen, classic telephone keypad and hardware switches for
   telephony, Wi-Fi, and the microphone. It is running Debian-based
   Raspbian OS.

[![photo of Samsung, Pyra, N900, ZeroPhone, GnuK, and PocketCHIP - click to enlarge](|static|/images/debian-mobile-continues_small.jpg)](https://annex.debconf.org/debconf-share/debconf17/photos/aigarius/IMG_1262.JPG "photo of Samsung, Pyra, N900, ZeroPhone, GnuK, and PocketCHIP")

The photo (click to enlarge) shows all four devices, together with a
[Nokia N900](https://en.wikipedia.org/wiki/Nokia_N900), which was the
first Linux-based smartphone by Nokia, running Debian-based Maemo and
a completely unrelated
[Gnuk cryptographic token](https://www.fsij.org/category/gnuk.html),
which just sneaked into the setting.

If you like to participate, please

* check the [Debian Mobile wiki page](https://wiki.debian.org/Mobile),
* subscribe to the [Debian mobile mailing list](https://lists.debian.org/debian-mobile/),
* or join the `#debian-mobile` IRC chatroom at `irc.oftc.net`.
