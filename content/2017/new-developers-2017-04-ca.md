Title: Nous desenvolupadors i mantenidors de Debian (març i abril del 2017)
Slug: new-developers-2017-04
Date: 2017-05-15 12:30
Author: Jean-Pierre Giraud
Tags: project
Lang: ca
Status: published

Els següents col·laboradors del projecte han esdevingut Debian Developers
en els darrers dos mesos:

* Guilhem Moulin (guilhem)
* Lisa Baron (jeffity)
* Punit Agrawal (punit)

Els següents col·laboradors del projecte han esdevingut Debian Maintainers
en els darrers dos mesos:

* Sebastien Jodogne
* Félix Lechner
* Uli Scholler
* Aurélien Couderc
* Ondřej Kobližek
* Patricio Paez

Enhorabona a tots!
