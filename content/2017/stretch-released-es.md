Title: ¡Se ha publicado Debian 9.0 Stretch!
Date: 2017-06-18 08:25
Tags: stretch
Slug: stretch-released
Author: Ana Guerrero Lopez and Laura Arjona Reina
Status: published
Lang: es
Translator: Ana Guerrero Lopez

![Alt Ya se ha publicado Stretch](|static|/images/banner_stretch.png)

¡Déjate abrazar por Stretch, la pulpo de goma púrpura! Nos alegra anunciar
la nueva versión Debian 9.0, con nombre en clave *Stretch*.

**¿Quieres instalar Stretch?**
Elige tu [modo de instalación](https://www.debian.org/distrib/) preferido entre discos Blu-ray, DVDs,
CDs y memorias USB. No olvides leer [la guía de instalación](https://www.debian.org/releases/stretch/installmanual.es.html).

**¿Eres ya un usuario feliz de Debian y solo quieres actualizarte?**
Simplemente puedes actualizar desde tu Debian 8 Jessie, solamente recuerda
leer antes las [notas de publicación](https://www.debian.org/releases/stretch/releasenotes.es.html).

**¿Quieres celebrar la publicación de Stretch?**
Comparte [la imagen de esta publicación](https://wiki.debian.org/DebianArt/Themes/softWaves?action=AttachFile&do=view&target=wikiBanner.png) en tu blog o sitio en internet!
