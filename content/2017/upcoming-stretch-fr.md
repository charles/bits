Title: Debian 9.0 Stretch arrive !
Date: 2017-06-17 00:30
Tags: stretch
Slug: upcoming-stretch
Author: Laura Arjona Reina
Lang: fr
Translator: Jean-Pierre Giraud
Status: published

![Alt Stretch is coming on 2017-06-17](|static|/images/upcoming-stretch.png)

L'équipe de publication Debian, conjointement avec d'autres équipes, prépare
les derniers éléments nécessaires à la publication de Debian 9 Stretch.
Soyez patients ! Toute une série d'étapes est nécessaire et certaines prennent
du temps, comme la construction des images, la diffusion de la publication
dans le réseau de miroirs et la reconstruction du site de Debian pour que
« stable » pointe vers Debian 9.

Vous pouvez suivre la couverture de la publication en direct sur
[https://micronews.debian.org](https://micronews.debian.org) ou le profil
**@debian** de votre réseau social préféré ! Nous diffuserons des informations
sur ce qui est nouveau dans cette version de Debian 9, sur l'avancement du
processus durant tout ce week-end et des faits sur Debian et la vaste
communauté de contributeurs bénévoles qui rend possible son existence.
