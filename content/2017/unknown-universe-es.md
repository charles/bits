Title: Un universo paralelo desconocido usa Debian
Slug: unknown-parallel-universe-uses-debian
Date: 2017-04-01 15:30
Author: Debian Publicity Team
Tags: debian, announce
Lang: es
Translator: Laura Arjona Reina
Status: published

**Este artículo era una broma de primero de abril**

Las agencias espaciales que llevan la
[Estación Espacial Internacional (ISS)](https://en.wikipedia.org/wiki/International_Space_Station_program)
han informado que un portátil que se lanzó accidentalmente al espacio como
basura en 2013 desde la la Estación Espacial Internacional puede haber
conectado con un Universo paralelo.
[Este portátil funcionaba con Debian 6](https://phys.org/news/2013-05-international-space-station-laptop-migration.html)
y el equipo de ingeniería de la ISS pudo rastrear su viaje
a través del espacio exterior. A primeros de enero, la señal del portátil
se perdió pero se volvió a recuperar dos semanas más tarde en el mismo lugar.
El equipo de ingeniería de la ISS sospecha que el portátil pudo haber
encontrado y cruzado un
[agujero de gusano](https://es.wikipedia.org/wiki/Agujero_de_gusano) llegando
a un Universo paralelo desde donde «alguien» lo envió de vuelta más tarde.

Finalmente el portátil fue recuperado y en un primer análisis el equipo
de ingeniería de la ISS encontró que el portátil tenía un arranque dual:
una partición con la instalación de Debian realizada por ellos, y una
segunda partición con lo que parece ser un «fork» o
[derivada](https://wiki.debian.org/Derivatives/) de Debian totalmente
desconocida hasta el momento.

El equipo de ingeniería ha estado en contacto con el Proyecto Debian en
las últimas semanas y se ha formado un grupo en Debian con delegados de
los distintos equipos de Debian que han empezado a estudiar este nuevo
sistema derivado de Debian. De los resultados preliminares de esta
investigación, podemos afirmar orgullosamente que alguien (o un grupo
de seres) en un universo paralelo entienden suficientemente
los ordenadores de la Tierra, y Debian, como para:

* Clonar el sitema Debian existente en una nueva partición y proporcionar
  un arranque dual usando Grub.
* Cambiar el fondo de pantalla del [tema Spacefun](https://wiki.debian.org/DebianArt/Themes/SpaceFun)
  que había, a uno en colores del arco iris.
* Hacer «fork» a todos los paquetes para los cuales el
  [código fuente](https://sources.debian.net/) estaba disponible en el sistema
  Debian inicial, arreglar múltiples fallos en esos paquetes y proporcionar
  parches para algunos intrincados problemas de seguridad.
* Agregar diez nuevas localizaciones idiomáticas que no se corresponden
  con ninguna lengua hablada en la Tierra, con traducción completa de cuatro
  de ellas.
* Se ha encontrado na copia del
  [repositorio de la web de Debian](https://anonscm.debian.org/viewvc/webwml/),
  migrado al sistema de control de versiones git, y funcionando perfectamente,
  en la carpeta _/home/earth0/Documentos_. Este nuevo repositorio incluye
  código para mostrar las [micronoticias](https://micronews.debian.org/) de
  Debian en la página inicial y muchas otras mejoras, manteniendo el estilo de
  no necesitar JavaScript y proporcionando un buen control de las traducciones
  actualizadas/obsoletas, similar al existente en Debian.

Solo es el comienzo del trabajo para conocer mejor este nuevo Universo
y encontrar una manera de comunicar con él; todos los usuarios y contribuidores
de Debian están invitados a unirse para estudiar el sistema operativo
encontrado. Queremos preparar a nuestra Comunidad y nuestro Universo para
vivir en paz y respetuosamente con las comunidades del Universo paralelo
en en verdadero espíritu del Software Libre.

En las próximas semanas se propondrá una Resolución General para actualizar
nuestro lema a "el sistema operativo multiversal".
