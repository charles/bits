Title: Nous desenvolupadors i mantenidors de Debian (juliol i agost del 2017)
Slug: new-developers-2017-08
Date: 2017-09-01 20:30
Author: Jean-Pierre Giraud
Tags: project
Lang: ca
Status: published

Els següents col·laboradors del projecte han esdevingut Debian Developers
en els darrers dos mesos:

* Ross Gammon (rossgammon)
* Balasankar C (balasankarc)
* Roland Fehrenbacher (rfehren)
* Jonathan Cristopher Carter (jcc)

Els següents col·laboradors del projecte han esdevingut Debian Maintainers
en els darrers dos mesos:

* José Gutiérrez de la Concha
* Paolo Greppi
* Ming-ting Yao Wei
* Boyuan Yang
* Paul Hardy
* Fabian Wolff
* Moritz Schlarb
* Shengjing Zhu

Enhorabona a tots!
