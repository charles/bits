Title: Novos desenvolvedores e mantenedores Debian (julho e agosto de 2017)
Slug: new-developers-2017-08
Date: 2017-09-01 20:30
Author: Jean-Pierre Giraud
Tags: project
Lang: pt
Status: published

Os seguintes colaboradores do projeto se tornaram Desenvolvedores Debian
nos últimos dois meses:

* Ross Gammon (rossgammon)
* Balasankar C (balasankarc)
* Roland Fehrenbacher (rfehren)
* Jonathan Cristopher Carter (jcc)

Os seguintes colaboradores do projeto se tornaram Mantenedores Debian
nos últimos dois meses:

* José Gutiérrez de la Concha
* Paolo Greppi
* Ming-ting Yao Wei
* Boyuan Yang
* Paul Hardy
* Fabian Wolff
* Moritz Schlarb
* Shengjing Zhu

Parabéns a todos!
