Title: Upcoming Debian 9.0 Stretch!
Date: 2017-06-17 00:30
Tags: stretch
Slug: upcoming-stretch
Author: Laura Arjona Reina
Status: published

![Alt Stretch is coming on 2017-06-17](|static|/images/upcoming-stretch.png)

The Debian Release Team in coordination with several other teams are preparing
the last bits needed for releasing Debian 9 Stretch.
Please, be patient! Lots of steps are involved and some of them take some
time,such as building the images, propagating the release through the mirror
network, and rebuilding the Debian website so that "stable" points to
Debian 9.

Follow the live coverage of the release on
[https://micronews.debian.org](https://micronews.debian.org) or the
**@debian** profile in your favorite social network!
We'll spread the word about what's new in this version of Debian 9, how the
release process is progressing during the weekend and facts about Debian and
the wide community of volunteer contributors that make it possible.
