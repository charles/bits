Title: Những nhà Phát triển và Bảo trì Debian mới (Tháng bảy và tám 2017)
Slug: new-developers-2017-08
Date: 2017-09-01 20:30
Author: Jean-Pierre Giraud
Tags: project
Translator: Trần Ngọc Quân
Lang: vi
Status: published

Trong hai tháng qua, những người đóng góp sau đây nhận được tài khoản Phát triển Debian:

* Ross Gammon (rossgammon)
* Balasankar C (balasankarc)
* Roland Fehrenbacher (rfehren)
* Jonathan Cristopher Carter (jcc)

Trong hai tháng qua, những người đóng góp sau đây được thêm vào với tư cách là người Bảo trì Debian:

* José Gutiérrez de la Concha
* Paolo Greppi
* Ming-ting Yao Wei
* Boyuan Yang
* Paul Hardy
* Fabian Wolff
* Moritz Schlarb
* Shengjing Zhu

Xin chúc mừng!
