Title: Nuevos desarrolladores y mantenedores de Debian (julio y agosto del 2017)
Slug: new-developers-2017-08
Date: 2017-09-01 20:30
Author: Jean-Pierre Giraud
Tags: project
Lang: es
Status: published

Los siguientes colaboradores del proyecto se convirtieron en Debian Developers
en los dos últimos meses:

* Ross Gammon (rossgammon)
* Balasankar C (balasankarc)
* Roland Fehrenbacher (rfehren)
* Jonathan Cristopher Carter (jcc)

Los siguientes colaboradores del proyecto se convirtieron en Debian Maintainers
en los dos últimos meses:

* José Gutiérrez de la Concha
* Paolo Greppi
* Ming-ting Yao Wei
* Boyuan Yang
* Paul Hardy
* Fabian Wolff
* Moritz Schlarb
* Shengjing Zhu

¡Felicidades a todos!
