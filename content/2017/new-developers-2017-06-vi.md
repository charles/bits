Title: Những nhà Phát triển và Bảo trì Debian mới (Tháng năm và sáu 2017)
Slug: new-developers-2017-06
Date: 2017-07-02 14:30
Author: Jean-Pierre Giraud
Tags: project
Translator: Trần Ngọc Quân
Lang: vi
Status: published

Trong hai tháng qua, những người đóng góp sau đây nhận được tài khoản Phát triển Debian:

* Alex Muntada (alexm)
* Ilias Tsitsimpis (iliastsi)
* Daniel Lenharo de Souza (lenharo)
* Shih-Yuan Lee (fourdollars)
* Roger Shimizu (rosh)

Trong hai tháng qua, những người đóng góp sau đây được thêm vào với tư cách là người Bảo trì Debian:

* James Valleroy
* Ryan Tandy
* Martin Kepplinger
* Jean Baptiste Favre
* Ana Cristina Custura
* Unit 193

Xin chúc mừng!
