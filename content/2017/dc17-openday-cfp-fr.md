Title: Ouverture des soumissions pour la journée portes ouvertes DebConf17
Slug: dc17-openday-cfp
Date: 2017-04-18 09:00
Author: DebConf team
Tags: debconf17, debconf, openday
Status: published
Lang: fr
Translator: DebConf team

L'équipe de DebConf est à la recherche d'activités pour la journée
portes ouvertes DebConf17, une journée dédiée à des activités de type
grand public traitant de Debian et des logiciels libres.
L'événement aura lieu le 5 août 2017, soit le jour précédant
l'ouverture de DebConf17.

Toutes les personnes intéressées de près ou de loin par
le projet Debian et les logiciels libres auront une excellente
opportunité de se rencontrer et d'en apprendre plus.

# Soumettez une activité

Nous sommes à la recherche de présentations, d'activités participatives
ou de toutes autres activités à propos de Debian et des logiciels libres.
Les activités en français et en anglais sont les bienvenues.

Voici quelques idées de contenu que nous aimerions offrir
durant la journée portes ouvertes. Celle liste n'est pas exhaustive,
n'hésitez pas à proposer d'autres idées !

* Une introduction aux divers aspects du projet Debian
* Des présentations sur Debian et les logiciels libres dans l'art, l'éducation
  et la recherche
* Une initiation à la contribution aux projets de logiciels libres
* Logiciel libre et Vie privée/Surveillance
* Une introduction à la programmation et/ou au bricolage matériel
* Une présentation sur votre projet préféré en lien avec les logiciels
  libres (groupes d'utilisateurs, groupes militants, etc.)

Pour soumettre une activité, veuillez remplir le formulaire suivant :
[https://debconf17.debconf.org/talks/new/](https://debconf17.debconf.org/talks/new/)

# Devenez volontaire

Nous avons besoin de bénévoles afin que la journée portes ouvertes
DebConf17 soit un succès ! Nous sommes spécifiquement à la recherche
de personnes familières avec l'installateur Debian
pour assister à l'« installfest » de Debian, en tant que soutien
aux personnes ayant besoin d'aide pour installer Debian sur leur machine.
Si cela vous intéresse, veuillez ajouter votre nom au wiki :
[https://wiki.debconf.org/wiki/DebConf17/OpenDay#Installfest](https://wiki.debconf.org/wiki/DebConf17/OpenDay#Installfest)

# Soyez présent-e-s!

La participation aux portes ouvertes est gratuite. Aucune inscription n'est
requise.

Le programme de la journée portes ouvertes sera annoncé en juin 2017.

![DebConf17 logo](|static|/images/800px-Dc17logo.png)
