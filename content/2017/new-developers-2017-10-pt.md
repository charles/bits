Title: Novos desenvolvedores e mantenedores Debian (setembro e outubro de 2017)
Slug: new-developers-2017-10
Date: 2017-11-02 20:30
Author: Jean-Pierre Giraud
Tags: project
Lang: pt
Status: published

Os seguintes colaboradores do projeto se tornaram Desenvolvedores Debian
nos últimos dois meses:

* Allison Randal (wendar)
* Carsten Schoenert (tijuca)
* Jeremy Bicha (jbicha)
* Luca Boccassi (bluca)
* Michael Hudson-Doyle (mwhudson)
* Elana Hashman (ehashman)

Os seguintes colaboradores do projeto se tornaram Mantenedores Debian
nos últimos dois meses:

* Ervin Hegedüs
* Tom Marble
* Lukas Schwaighofer
* Philippe Thierry

Parabéns a todos!
