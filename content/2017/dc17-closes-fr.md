Title: Clôture de DebConf 17 à Montréal et annonce des dates de Debconf 18
Date: 2017-08-12 23:59
Tags: debconf17, announce, debconf18, debconf
Slug: debconf17-closes
Author: Laura Arjona Reina
Status: published
Lang: fr
Translator: Jean-Pierre Giraud

[![Photo de groupe de DebConf17 - cliquez pour agrandir](|static|/images/debconf17_group_photo_small.jpg)](https://annex.debconf.org/debconf-share/debconf17/photos/aigarius/debcond17%20group%20photo.jpg)

Samedi 12 août 2017, le conférence annuelle des développeurs et
contributeurs Debian s'est achevée.
Avec plus de 405 participants venus du monde entier
et 169 événements, dont 89 communications, 61 séances de discussion ou
sessions spécialisées, 6 ateliers et 13 autres activités,
[DebConf17](https://debconf17.debconf.org) a été saluée comme un
succès.

Parmi les points forts, on retiendra le DebCamp avec 117 participants, la
[journée porte ouverte](https://debconf17.debconf.org/news/2017-08-05-open-day/),
où des activités intéressant un plus large public ont
été proposées, les communications des orateurs invités
([Deb Nicholson](https://debconf17.debconf.org/talks/183/),
[Matthew Garrett](https://debconf17.debconf.org/talks/177/)
et [Katheryn Sutter](https://debconf17.debconf.org/talks/134/)),
les traditionnelles Brèves du chef de projet Debian, les présentations
éclair et les démonstrations en direct et l'annonce de la DebConf de l'an
prochain ([DebConf18](https://wiki.debconf.org/wiki/DebConf18) à Hsinchu,
Taïwan).

Le [programme](https://debconf17.debconf.org/schedule/)
a été mis à jour quotidiennement, intégrant 32 nouvelles activités
ponctuelles, programmées par les participants durant toute la conférence.

Pour tous ceux qui n'ont pu participer à Debconf, les sessions et les
communications ont été enregistrées et diffusées en direct, et les vidéos
sont disponibles sur le
[site internet des réunions Debian](http://meetings-archive.debian.net/pub/debian-meetings/2017/debconf17).
Dans beaucoup de sessions, la participation à distance a aussi été facilitée
par l'utilisation d'IRC ou d'un éditeur de texte collaboratif.

Le site web de [DebConf17](https://debconf17.debconf.org/)
restera actif à fin d'archive et continuera à offrir des liens vers les
présentations et vidéos des communications et des événements.

L'an prochain, [DebConf18](https://wiki.debconf.org/wiki/DebConf18) se tiendra
à Hsinchu, Taïwan, du 29 juillet 2018 au 5 août 2018. Ce sera la
première DebConf organisée en Asie. Pour les journées précédant la DebConf,
les organisateurs locaux mettront à nouveau en place un DebCamp (du 21 au
27 juillet), une session de travail intense pour améliorer la distribution,
et organisera une journée porte ouverte le 28 juillet 2018, destinée au
grand public.

DebConf s'est engagée à offrir un environnement sûr et accueillant pour
tous les participants. Voir le
[Code de conduite de DebConf](http://debconf.org/codeofconduct.shtml)
et le [Code de conduite de Debian](https://www.debian.org/code_of_conduct)
pour en savoir plus à ce sujet.

Debian remercie les nombreux [parrains](https://debconf17.debconf.org/sponsors/)
pour leur engagement dans leur soutien à DebConf 17, et en particulier nos
parrains platine
[**Savoir-Faire Linux**](https://www.savoirfairelinux.com/),
[**Hewlett Packard Enterprise**](http://www.hpe.com/engage/opensource),
et [**Google**](https://google.com/).

### À propos de Savoir-faire Linux

[Savoir-faire Linux](https://www.savoirfairelinux.com/)
est une entreprise de logiciels libres et à code source ouvert, basée à
Montréal, avec des bureaux à Québec, Toronto, Paris et Lyon. Elle fournit
des solutions d'intégration pour Linux et le logiciel libre afin d'offrir
performance, flexibilité et indépendance à ses clients. Cette entreprise
contribue activement à de nombreux projets de logiciels libres et fournit
des miroirs pour Debian, Ubuntu, Linux et d'autres projets.

### À propos de Hewlett Packard Enterprise

[Hewlett Packard Enterprise (HPE)](http://www.hpe.com/engage/opensource)
est l'une des plus grandes entreprises informatiques au monde et fournit
une large gamme de produits et de services, tels que des serveurs, des
équipements de stockage, des équipements pour le réseau, du conseil et
support, des logiciels et des services de financement.

HPE est aussi un partenaire de développement de Debian et fournit du
matériel pour le développement de portages, pour des miroirs et d'autres
services de Debian.

### À propos de Google

[Google](https://www.google.com)
est une des plus grandes entreprises technologique du monde qui fournit une
large gamme de services relatifs à Internet et de produits tels que des
technologies de publicité en ligne, des outils de recherche, de
l'informatique dans les nuages, des logiciels et du matériel.

Google soutient Debian en parrainant les DebConf depuis plus de dix ans,
parrain or depuis DebConf 12 et parrain platine pour DebConf 17.
