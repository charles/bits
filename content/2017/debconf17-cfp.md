Title: DebConf17: Call for Proposals
Date: 2017-02-08 20:50
Tags: debconf, debconf17, cfp
Slug: 7-cfp
Author: Gunnar Wolf
Status: published

The DebConf Content team would like to Call for Proposals for
[the DebConf17 conference], to be held in Montreal, Canada, from August 6
through August 12, 2017.

[the DebConf17 conference]: https://debconf17.debconf.org/

You can find this Call for Proposals in its latest form at: [https://debconf17.debconf.org/cfp]

[https://debconf17.debconf.org/cfp]: https://debconf17.debconf.org/cfp/

Please refer to this URL for updates on the present information.

## Submitting an Event

[Submit an event proposal] and describe your plan. Please
note, events are not limited to traditional presentations or informal
sessions (BoFs). We welcome submissions of tutorials, performances, art
installations, debates, or any other format of event that you think
would be beneficial to the Debian community.

[Submit an event proposal]: https://debconf17.debconf.org/talks/new/

Please include a short title, suitable for a compact schedule, and an
engaging description of the event. You should use the field "Notes" to
provide us information such as additional speakers, scheduling
restrictions, or any special requirements we should consider for your
event.

Regular sessions may either be 20 or 45 minutes long (including time for
questions), other kinds of sessions (like workshops) could have
different durations. Please choose the most suitable duration for your
event and explain any special requests.

You will need to create an account on the site, to submit a talk. We'd
encourage Debian account holders (e.g. DDs) to use [Debian SSO] when
creating an account. But this isn't required for everybody, you can
sign up with an e-mail address and password.

[Debian SSO]: https://wiki.debian.org/DebianSingleSignOn

## Timeline

The first batch of accepted proposals will be announced in April. If
you depend on having your proposal accepted in order to attend the
conference, please submit it as soon as possible so that it can be
considered during this first evaluation period.

All proposals must be submitted before Sunday 4 June 2017 to be
evaluated for the official schedule.

## Topics and Tracks

Though we invite proposals on any Debian or FLOSS related subject, we
have some broad topics on which we encourage people to submit proposals,
including:

- Blends
- Debian in Science
- Cloud and containers
- Social context
- Packaging, policy and infrastructure
- Embedded
- Systems administration, automation and orchestration
- Security

You are welcome to either suggest more tracks, or become a coordinator
for any of  them; please refer to the [Content Tracks wiki page] for more
information on that.

[Content Tracks wiki page]: https://wiki.debconf.org/wiki/DebConf17/ContentTracks

## Code of Conduct

Our event is covered by a Code of Conduct designed to ensure everyone's
safety and comfort. The code applies to all attendees, including
speakers and the content of their presentations. For more information,
[please see the Code on the Web], and do not hesitate to contact us
at [<content@debconf.org>] if you have any questions or are unsure about
certain content you'd like to present.

[please see the Code on the Web]: https://debconf.org/codeofconduct.shtml
[content@debconf.org]: mailto:content@debconf.org

## Video Coverage

Providing video of sessions amplifies DebConf achievements and is one of
the [conference goals]. Unless speakers opt-out, official events will
be streamed live over the Internet to promote remote participation.
Recordings will be published later under the [DebConf license], as
well as presentation slides and papers whenever available.

[conference goals]: https://debconf.org/goals.shtml
[DebConf license]: http://meetings-archive.debian.net/pub/debian-meetings/LICENSE

DebConf would not be possible without the generous support of all our
sponsors, especially our Platinum Sponsor [Savoir-Faire Linux].
DebConf17 is still accepting sponsors; if you are interested, or think
you know of others who would be willing to help, [please get in touch]!

[Savoir-Faire Linux]: https://www.savoirfairelinux.com/en/
[please get in touch]: https://debconf17.debconf.org/sponsors/become-a-sponsor/

In case of any questions, or if you wanted to bounce some ideas off us
first, please do not hesitate to reach out to us at
[<content@debconf.org>].

[<content@debconf.org>]: mailto:content@debconf.org

We hope to see you in Montreal!

The DebConf team
