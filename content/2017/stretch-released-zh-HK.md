Title: Debian 9.0 Stretch 發佈了！
Date: 2017-06-18 08:25
Tags: stretch
Slug: stretch-released
Author: Ana Guerrero Lopez and Laura Arjona Reina
Status: published
Lang: zh-HK
Translator: Anthony Fok

![Alt Stretch has been released](|static|/images/banner_stretch.png)

請接受紫色橡膠玩具八爪魚的擁抱吧！
我們很高興宣佈：代號為 *Stretch*[^1] 的 Debian 9.0 已經發佈了！

**想安裝嗎？**
有藍光光碟、DVD、CD 和 USB 手指等多種[安裝媒介](https://www.debian.org/distrib/index.zh-hk.html)，任君選擇。接著，請閱讀[安裝手冊](https://www.debian.org/releases/stretch/installmanual)。

**已經是 Debian 的忠實用戶，只需要升級？**
閣下可以從現有的 Debian 8 Jessie 系統升級至 Debian 9 Stretch，請閱讀[發佈通告](https://www.debian.org/releases/stretch/releasenotes)。

**想慶祝這次發佈嗎？**
那就在閣下的網誌或網站上分享[這篇網誌的 banner](https://wiki.debian.org/DebianArt/Themes/softWaves?action=AttachFile&do=view&target=wikiBanner.png) 吧！

[^1]: Stretch 即《反斗奇兵３》裏的「八爪仔」。
