Title: New Debian Developers and Maintainers (September and October 2017)
Slug: new-developers-2017-10
Date: 2017-11-02 20:30
Author: Jean-Pierre Giraud
Tags: project
Status: published

The following contributors got their Debian Developer accounts in the last two
months:

* Allison Randal (wendar)
* Carsten Schoenert (tijuca)
* Jeremy Bicha (jbicha)
* Luca Boccassi (bluca)
* Michael Hudson-Doyle (mwhudson)
* Elana Hashman (ehashman)

The following contributors were added as Debian Maintainers in the last two
months:

* Ervin Hegedüs
* Tom Marble
* Lukas Schwaighofer
* Philippe Thierry

Congratulations!
