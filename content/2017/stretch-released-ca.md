Title: S'ha llançat Debian 9.0 Stretch!
Date: 2017-06-18 08:25
Tags: stretch
Slug: stretch-released
Author: Ana Guerrero Lopez and Laura Arjona Reina
Status: published
Lang: ca
Translator: Guillem Jover

![Alt S'ha llançat Stretch](|static|/images/banner_stretch.png)

Deixeu-vos abraçar per Stretch, el pop de joguina porpra! Ens complau
anunciar el llançament de Debian 9.0, amb nom en clau *Stretch*.

**Voleu instal·lar-la?**
Escolliu el vostre [mitjà d'instal·lació](https://www.debian.org/distrib/)
preferit d'entre Blu-ray, DVD, CD o memòries USB. I llegiu el
[manual d'instal·lació](https://www.debian.org/releases/stretch/installmanual).

**Sou ja usuaris satisfets i només voleu actualitzar?**
Podeu actualitzar fàcilment des de la vostra instal·lació de Debian 8 Jessie,
llegiu les [notes de llançament](https://www.debian.org/releases/stretch/releasenotes).

**Voleu celebrar el llançament?**
Compartiu la [pancarta d'aquest bloc](https://wiki.debian.org/DebianArt/Themes/softWaves?action=AttachFile&do=view&target=wikiBanner.png)
al vostre bloc o lloc web!
