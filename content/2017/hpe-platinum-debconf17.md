Title: Hewlett Packard Enterprise Platinum Sponsor of DebConf17
Slug: hpe-platinum-debconf17
Date: 2017-06-23 16:15
Author: Laura Arjona Reina
Tags: debconf17, debconf, sponsors, HPE
Status: published

[![HPElogo](|static|/images/hpe.png)](http://www.hpe.com/engage/opensource)

We are very pleased to announce that [**Hewlett Packard Enterprise (HPE)**](http://www.hpe.com/engage/opensource)
has committed to support [DebConf17](https://debconf17.debconf.org) as a **Platinum sponsor**.

*"Hewlett Packard Enterprise is excited to support Debian's annual developer
conference again this year"*, said Steve Geary, Senior Director R&D at
Hewlett Packard Enterprise. *"As Platinum sponsors and member of the Debian community,
HPE is committed to supporting Debconf. The conference, community and  open
distribution are foundational to the development of The Machine research
program  and will our bring our Memory Driven Computing agenda to life."*

HPE is one of the largest computer companies in the
world, providing a wide range of products and services, such as servers, storage,
networking, consulting and support, software, and financial services.

HPE is also a development partner of Debian,
and provides hardware for port development, Debian mirrors, and other Debian services
(hardware donations are listed in the [Debian machines](https://db.debian.org/machines.cgi) page).

With this additional commitment as Platinum Sponsor,
HPE contributes to make possible our annual conference,
and directly supports the progress of Debian and Free Software
helping to strengthen the community that continues to collaborate on
Debian projects throughout the rest of the year.

Thank you very much Hewlett Packard Enterprise, for your support of DebConf17!

## Become a sponsor too!

DebConf17 is still accepting sponsors.
Interested companies and organizations may contact the DebConf team
through [sponsors@debconf.org](mailto:sponsors@debconf.org), and
visit the DebConf17 website at [https://debconf17.debconf.org](https://debconf17.debconf.org).
