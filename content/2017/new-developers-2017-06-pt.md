Title: Novos desenvolvedores e mantenedores Debian (maio e junho de 2017)
Slug: new-developers-2017-06
Date: 2017-07-02 14:30
Author: Jean-Pierre Giraud
Tags: project
Lang: pt
Status: published

Os seguintes colaboradores do projeto se tornaram Desenvolvedores Debian
nos últimos dois meses:

* Alex Muntada (alexm)
* Ilias Tsitsimpis (iliastsi)
* Daniel Lenharo de Souza (lenharo)
* Shih-Yuan Lee (fourdollars)
* Roger Shimizu (rosh)

Os seguintes colaboradores do projeto se tornaram Mantenedores Debian
nos últimos dois meses:

* James Valleroy
* Ryan Tandy
* Martin Kepplinger
* Jean Baptiste Favre
* Ana Cristina Custura
* Unit 193

Parabéns a todos!
