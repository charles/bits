Title: Chamada de propostas para o Dia Livre (Open Day) na DebConf17
Slug: dc17-openday-cfp
Date: 2017-04-18 09:00
Author: DebConf team
Tags: debconf17, debconf, openday
Status: published
Lang: PT-br
Translator: Paulo Henrique de Lima Santana

O time da DebConf gostaria de anunciar a chamada de propostas para o Dia Livre
(Open Day) na DebConf17, um dia inteiro dedicado a sessões sobre Debian e
Software Livre, e destinado ao público em geral. O Dia Livre antecederá a
DebConf17 e será realizado em Montreal, Canadá, no dia 05 de agosto de 2017.

O Dia Livre na DebConf será uma grande oportunidade para usuários,
desenvolvedores, e pessoas simplesmente curiosas sobre o nosso trabalho,
conhecerem e aprenderem sobre o Projeto Debian, Software Livre em geral e
tópicos relacionados.

# Submeta sua proposta

Nós estamos recebendo as submissões de oficinas, apresentações ou qualquer
outra atividade que envolva Debian e Software Livre.
São aceitas atividades em inglês e francês.

Aqui estão algumas ideias de conteúdos que nós adoraríamos oferecer durante o
Dia Livre. Essa lista não é definitiva, sinta-se livre para propor outras
ideias!

* Uma introdução aos vários aspectos do Projeto Debian
* Palestras sobre Debian e Software Livre nas artes, educação e/ou pesquisas
* Um passo a passo sobre como contribuir com projetos de Software Livre
* Software Livre & Privacidade/Vigilância
* Uma introdução a programação e/ou manuseio de hardware
* Uma oficina sobre a sua parte favorita do Software Livre
* Uma apresentação sobre o seu projeto relacionado a Software Livre favorito
  (grupo de usuários, grupo de militantes, etc)

Para submeter a sua proposta, por favor preencha o formulário em
[https://debconf17.debconf.org/talks/new/](https://debconf17.debconf.org/talks/new/)

# Voluntários

Nós precisamos de voluntários para ajudar a garantir que o Dia Livre será um
sucesso! Nós estamos procurando especialmente pessoas acostumadas
com o instalador do Debian para participarem do install fest Debian,
como suporte para as pessoas que procuram ajuda para instalar o Debian em suas
máquinas.

Se você está interessado, por favor adicione o seu nome na nossa wiki:
[https://wiki.debconf.org/wiki/DebConf17/OpenDay#Installfest](https://wiki.debconf.org/wiki/DebConf17/OpenDay#Installfest)

# Participação

A participação no Dia Livre é gratuita e não é obrigatório nenhuma inscrição.

A programação do Dia Livre será anunciada em junho de 2017.

![DebConf17 logo](|static|/images/800px-Dc17logo.png)
