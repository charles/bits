Title: Nuevos desarrolladores y mantenedores de Debian (septiembre y octubre del 2017)
Slug: new-developers-2017-10
Date: 2017-11-02 20:30
Author: Jean-Pierre Giraud
Tags: project
Lang: es
Status: published

Los siguientes colaboradores del proyecto se convirtieron en Debian Developers
en los dos últimos meses:

* Allison Randal (wendar)
* Carsten Schoenert (tijuca)
* Jeremy Bicha (jbicha)
* Luca Boccassi (bluca)
* Michael Hudson-Doyle (mwhudson)
* Elana Hashman (ehashman)

Los siguientes colaboradores del proyecto se convirtieron en Debian Maintainers
en los dos últimos meses:

* Ervin Hegedüs
* Tom Marble
* Lukas Schwaighofer
* Philippe Thierry

¡Felicidades a todos!
