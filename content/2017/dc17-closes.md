Title: DebConf17 closes in Montreal and DebConf18 dates announced
Date: 2017-08-12 23:59
Tags: debconf17, announce, debconf18, debconf
Slug: debconf17-closes
Author: Laura Arjona Reina
Status: published

[![DebConf17 group photo - click to enlarge](|static|/images/debconf17_group_photo_small.jpg)](https://annex.debconf.org/debconf-share/debconf17/photos/aigarius/debcond17%20group%20photo.jpg)

Today, Saturday 12 August 2017, the annual Debian Developers
and Contributors Conference came to a close.
With over 405 people attending from all over the world,
and 169 events including 89 talks, 61 discussion sessions or BoFs,
6 workshops and 13 other activities,
[DebConf17](https://debconf17.debconf.org) has been hailed as a success.

Highlights included DebCamp with 117 participants,
the [Open Day](https://debconf17.debconf.org/news/2017-08-05-open-day/),
where events of interest to a broader audience were offered,
talks from invited speakers
([Deb Nicholson](https://debconf17.debconf.org/talks/183/),
[Matthew Garrett](https://debconf17.debconf.org/talks/177/)
and [Katheryn Sutter](https://debconf17.debconf.org/talks/134/)),
the traditional Bits from the DPL, lightning talks and live demos
and the announcement of next year's DebConf
([DebConf18](https://wiki.debconf.org/wiki/DebConf18) in Hsinchu, Taiwan).

The [schedule](https://debconf17.debconf.org/schedule/)
has been updated every day, including 32 ad-hoc new activities, planned
by attendees during the whole conference.

For those not able to attend, talks and sessions were recorded and live
streamed, and videos are being made available at the
[Debian meetings archive website](http://meetings-archive.debian.net/pub/debian-meetings/2017/debconf17).
Many sessions also facilitated remote participation via IRC or a collaborative
pad.

The [DebConf17](https://debconf17.debconf.org/) website
will remain active for archive purposes, and will continue to offer
links to the presentations and videos of talks and events.

Next year, [DebConf18](https://wiki.debconf.org/wiki/DebConf18) will be held
in Hsinchu, Taiwan, from 29 July 2018 until 5 August 2018. It will be the
first DebConf held in Asia. For the days before DebConf the local organisers
will again set up DebCamp (21 July - 27 July), a session for some intense
work on improving the distribution, and organise the Open Day on 28 July
2018, aimed at the general public.

DebConf is committed to a safe and welcome environment for all participants.
See the [DebConf Code of Conduct](http://debconf.org/codeofconduct.shtml)
and the [Debian Code of Conduct](https://www.debian.org/code_of_conduct)
for more details on this.

Debian thanks the commitment of numerous
[sponsors](https://debconf17.debconf.org/sponsors/)
to support DebConf17, particularly our Platinum Sponsors
[**Savoir-Faire Linux**](https://www.savoirfairelinux.com/),
[**Hewlett Packard Enterprise**](http://www.hpe.com/engage/opensource),
and [**Google**](https://google.com/).

### About Savoir-faire Linux

[Savoir-faire Linux](https://www.savoirfairelinux.com/)
is a Montreal-based Free/Open-Source Software company
with offices in Quebec City, Toronto, Paris and Lyon. It offers Linux and
Free Software integration solutions in order to provide performance,
flexibility and independence for its clients. The company actively contributes
to many free software projects, and provides mirrors of Debian, Ubuntu, Linux
and others.

### About Hewlett Packard Enterprise

[Hewlett Packard Enterprise (HPE)](http://www.hpe.com/engage/opensource)
is one of the largest computer companies in the world, providing a wide range
of products and services, such as servers, storage, networking, consulting and
support, software, and financial services.

HPE is also a development partner of Debian, and provides hardware for port
development, Debian mirrors, and other Debian services.

### About Google

[Google](https://www.google.com)
is one of the largest technology companies in the world, providing a wide
range of Internet-related services and products as online advertising
 technologies, search, cloud computing, software, and hardware.

Google has been supporting Debian by sponsoring DebConf since more than
ten years, at gold level since DebConf12, and at platinum level for this
DebConf17.
