Title: Những nhà Phát triển và Bảo trì Debian mới (Tháng chín và mười 2017)
Slug: new-developers-2017-10
Date: 2017-11-02 20:30
Author: Jean-Pierre Giraud
Tags: project
Translator: Trần Ngọc Quân
Lang: vi
Status: published

Trong hai tháng qua, những người đóng góp sau đây nhận được tài khoản Phát triển Debian:

* Allison Randal (wendar)
* Carsten Schoenert (tijuca)
* Jeremy Bicha (jbicha)
* Luca Boccassi (bluca)
* Michael Hudson-Doyle (mwhudson)
* Elana Hashman (ehashman)

Trong hai tháng qua, những người đóng góp sau đây được thêm vào với tư cách là người Bảo trì Debian:

* Ervin Hegedüs
* Tom Marble
* Lukas Schwaighofer
* Philippe Thierry

Xin chúc mừng!
