Title: Le travail sur Debian pour les mobiles continue
Slug: debian-mobile-continues
Date: 2017-08-17 15:40
Author: W. Martin Borgert
Tags: debian, mobile, devices, debconf17, pocketchip, pyra, zerophone
Status: published
Lang: fr
Translator: Jean-Pierre Giraud

Le travail sur Debian pour les appareils mobiles, c'est à dire les
téléphones, les tablettes et les ordinateurs de poche se poursuit.
Durant la dernière
[DebConf 17](https://debconf17.debconf.org/) à Montréal, Canada, plus de
50  personnes se sont réunies pour réévaluer les perspectives et
les enjeux pour Debian sur les appareils mobiles.

Un certain nombre d'appareils ont été montrés à la DebConf :

* [PocketCHIP](https://getchip.com/pages/pocketchip) : un très petit
   ordinateur de poche avec clavier, Wi-Fi, USB et Bluetooth, fonctionnant
   avec Debian 8 (Jessie) ou 9 (Stretch) ;
* [Pyra](https://pyra-handheld.com/boards/pages/pyra/) : un ordinateur
   de poche modulaire avec écran tactile, commandes de jeu, Wi-Fi,
   clavier, plusieurs ports USB et emplacements pour carte SD et
   un modem optionnel pour soit l'Europe soit les USA. Il est fournit avec
   Debian préinstallé ;
* [Samsung Galaxy S Relay 4G](https://en.wikipedia.org/wiki/Samsung_Galaxy_S_Relay_4G) :
   un smartphone Android avec un clavier physique qui peut déjà exécuter
   des parties de l'espace utilisateur de Debian sur le noyau Android.
   Le mise à niveau du noyau est en cours ;
* [ZeroPhone](https://www.crowdsupply.com/arsenijs/zerophone) : un
   smartphone libre basé sur Raspberry Pi Zero, avec un petit écran
   un clavier de téléphone classique et des commutateurs physiques pour
   le téléphone, le Wi-Fi et le microphone. Il fonctionne avec le système
   d'exploitation Raspbian basé sur Debian.

[![photo des Samsung, Pyra, N900, ZeroPhone, GnuK et PocketCHIP — cliquer pour agrandir](|static|/images/debian-mobile-continues_small.jpg)](https://annex.debconf.org/debconf-share/debconf17/photos/aigarius/IMG_1262.JPG "photo of Samsung, Pyra, N900, ZeroPhone, GnuK, and PocketCHIP")

La photo (cliquer pour agrandir) montre ces quatre appareils avec un
[Nokia N900](https://en.wikipedia.org/wiki/Nokia_N900), qui a été le
premier smartphone de Nokia basé sur Linux, fonctionnant avec Maemo basé
sur Debian, et un [jeton chiffré Gnuk](https://www.fsij.org/category/gnuk.html)
totalement sans rapport, qui est juste glissé au milieu.

Si vous souhaitez participer, veuillez

* consulter la [page Debian Mobile du wiki](https://wiki.debian.org/Mobile),
* vous abonner à la [liste de diffusion Debian mobile](https://lists.debian.org/debian-mobile/),
* ou rejoindre le canal IRC #debian-mobile sur irc.oftc.net.
