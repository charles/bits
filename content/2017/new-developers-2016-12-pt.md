Title: Novos desenvolvedores e mantenedores Debian (novembro e dezembro de 2016)
Slug: new-developers-2016-12
Date: 2017-01-09 00:30
Author: Jean-Pierre Giraud
Tags: project
Lang: pt
Status: published

Os seguintes colaboradores do projeto se tornaram Desenvolvedores Debian
nos últimos dois meses:

* Karen M Sandler (karen)
* Sebastien Badia (sbadia)
* Christos Trochalakis (ctrochalakis)
* Adrian Bunk (bunk)
* Michael Lustfield (mtecknology)
* James Clarke (jrtc27)
* Sean Whitton (spwhitton)
* Jerome Georges Benoit (calculus)
* Daniel Lange (dlange)
* Christoph Biedl (cbiedl)
* Gustavo Panizzo (gefa)
* Gert Wollny (gewo)
* Benjamin Barenblat (bbaren)
* Giovani Augusto Ferreira (giovani)
* Mechtilde Stehmann (mechtilde)
* Christopher Stuart Hoskin (mans0954)

Os seguintes colaboradores do projeto se tornaram Mantenedores Debian
nos últimos dois meses:

* Dmitry Bogatov
* Dominik George
* Gordon Ball
* Sruthi Chandran
* Michael Shuler
* Filip Pytloun
* Mario Anthony Limonciello
* Julien Puydt
* Nicholas D Steeves
* Raoul Snyman

Parabéns a todos!
