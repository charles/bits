Title: Nya Debianutvecklare och Debian Maintainers (Januari och Februari 2023)
Slug: new-developers-2023-02
Date: 2023-03-22 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: sv
Translator: Andreas Rönnquist
Status: published

Följande bidragslämnare fick sina Debianutvecklarkonton under de senaste två
månaderna:

* Sahil Dhiman (sahil)
* Jakub Ružička (jru)

Följande bidragslämnare adderades som Debian Maintainers under de senaste två
månaderna:

* Josenilson Ferreira da Silva
* Ileana Dumitrescu
* Douglas Kosovic
* Israel Galadima
* Timothy Pearson
* Blake Lee
* Vasyl Gello
* Joachim Zobel
* Amin Bandali

Grattis!
