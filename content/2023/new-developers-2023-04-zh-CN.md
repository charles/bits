Title: 新的 Debian 开发者和维护者 (2023年3月 至 4月)
Slug: new-developers-2023-04
Date: 2023-05-25 12:00
Author: Jean-Pierre Giraud
Tags: project
Lang: zh-CN
Translator: Boyuan Yang
Status: published

下列贡献者在过去的两个月中获得了他们的 Debian 开发者帐号：

* James Lu (jlu)
* Hugh McMaster (hmc)
* Agathe Porte (gagath)

下列贡献者在过去的两个月中被添加为 Debian 维护者：

* Soren Stoutner
* Matthijs Kooijman
* Vinay Keshava
* Jarrah Gosbell
* Carlos Henrique Lima Melara
* Cordell Bloor

祝贺他们！
