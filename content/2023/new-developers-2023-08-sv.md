Title: Nya Debianutvecklare och Debian Maintainers (Juli och Augusti 2023)
Slug: new-developers-2023-08
Date: 2023-09-27 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: sv
Translator: Andreas Rönnquist
Status: published

Följande bidragslämnare fick sina Debianutvecklarkonton under de senaste två
månaderna:

* Marius Gripsgard (mariogrip)
* Mohammed Bilal (rmb)
* Lukas Märdian (slyon)
* Robin Gustafsson (rgson)
* David da Silva Polverari (polverari)
* Emmanuel Arias (eamanu)

Följande bidragslämnare adderades som Debian Maintainers under de senaste två
månaderna:

* Aymeric Agon-Rambosson
* Blair Noctis
* Lena Voytek
* Philippe Coval
* John Scott

Grattis!
