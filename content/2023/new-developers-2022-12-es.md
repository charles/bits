Title: Nuevos desarrolladores y mantenedores de Debian (noviembre y diciembre del 2022)
Slug: new-developers-2022-12
Date: 2023-01-24 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: es
Status: published

Los siguientes colaboradores del proyecto se convirtieron en Debian Developers
en los dos últimos meses:

* Dennis Braun (snd)
* Raúl Benencia (rul)

Los siguientes colaboradores del proyecto se convirtieron en Debian Maintainers
en los dos últimos meses:

* Gioele Barabucci
* Agathe Porte
* Braulio Henrique Marques Souto
* Matthias Geiger
* Alper Nebi Yasak
* Fabian Grünbichler
* Lance Lin

¡Felicidades a todos!
