Title: नए डेबियन डेवलपर्स और मेंटेनर्स (सितंबर और अक्टूबर 2023)
Slug: new-developers-2023-10
Date: 2023-11-30 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: hi-IN
Translator: Yashraj Moghe
Status: published

पिछले दो महीनों में निम्नलिखित सहायकों को उनका डेबियन डेवलपर अकाउंट मिला:

* François Mazen (mzf)
* Andrew Ruthven (puck)
* Christopher Obbard (obbardc)
* Salvo Tomaselli (ltworf)

और निम्नलिखित को डेबियन मेंटेनर का स्थान दिया गया हैं:

* Bo YU
* Athos Coimbra Ribeiro
* Marc Leeman
* Filip Strömbäck

इन्हें बधाईयाँ!
