Title: Uppmaning om bud för DebConf24
Date: 2023-10-31 13:30
Tags: debconf, debconf24, debian
Lang: sv
Slug: debconf24-call-for-bids
Author: Debian DebConf Committe
Translator: Andreas Rönnquist
Status: published

På grund av det aktuella läget i Israel, som skulle vara värd för DebConf24,
har DebConf-kommittén beslutat att förnya sin efterfrågan om anbud för värdskap
för DebConf24 på annan plats.

DebConf-kommittén vill uttrycka sin uppriktiga uppskattning för DebConf-gruppen
i Israel och arbetet som dom har utfört under flera år. Med tanke på osäkerheten
rörande situationen beklagar vi att det med största sannolikhet inte kommer
att vara möjligt att hålla DebConf i Israel.

När vi efterfrågar bidrag för nya värdplatser ber vi dig att granska och
förstå detaljerna och kraven för en anbudsinlämning för att vara värd för
[Debians utvecklarkonferens](https://www.debconf.org).

Vänligen granska
[mallen för ett DebConf-bud](https://wiki.debian.org/DebConf/General/Handbook/Bids/LocationCheckList)
för riktlinjer för hur man lägger ett korrekt bud.

För att lägga ett anbud, vänligen skapa lämplig sida (sidor) under
[DebConf Wiki Bids](https://wiki.debian.org/DebConf/24/Bids), och lägg till den
till "Bids"-avsnittet på huvudsidan
[DebConf 24](https://wiki.debian.org/DebConf/24).

_Det är ont om tid för att fatta ett beslut. Vi behöver anbud senast i slutet
av november för att kunna fatta ett beslut i slutet av året._

När din inlämning är klar, vänligen skicka oss ett meddelande på
[debconf-team@lists.debian.org](mailto:debconf-team@lists.debian.org)
för att låta oss veta att ditt anbud är redo för granskning.

Vi föreslår också att du besöker vårt IRC-chattrum
[#debconf-team](https://www.oftc.net).

Med tanke på denna korta deadline förstår vi att buden inte kommer att vara
lika fullständiga som de brukar vara. Gör så gott du kan med den tid som finns.

Bud kommer att utvärderas enligt
[prioritetslistan](https://wiki.debian.org/DebConf/General/Handbook/Bids/PriorityList).

Du kan komma i kontakt med DebConf-gruppen via e-post till
[debconf-team@lists.debian.org](mailto:debconf-team@lists.debian.org), eller
via IRC-kanalen #debconf-team på OFTC eller via vår
[Matrixkanal](https://matrix.to/#/#debconf-team:matrix.debian.social).

Tack,

Debians DebConf-kommitté
