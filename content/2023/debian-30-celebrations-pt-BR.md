Title: Celebrações e comentários do DebianDay
Slug: debian-30-celebrations
Date: 2023-09-09 11:00
Author: Donald Norwood, Paulo Henrique de Lima Santana
Tags:  debian30, debian, project, anniversary, DebianDay
Artist: Credited
Translator: Paulo Henrique de Lima Santana (phls)
Lang: pt-BR
Status: published

**O Debian celebra os 30 anos!**

Comemoramos nosso [aniversário](https://wiki.debian.org/DebianDay) este ano, e
nos divertimos muito com os(as) novos(as) amigos(as), novos(as) membros(as)
recebidos na comunidade e no mundo.

Coletamos alguns comentários, vídeos e discussões por toda a internet e algumas
fotos de alguns dos eventos do
[DebianDay2023](https://wiki.debian.org/DebianDay/2023). Esperamos que você
tenha aproveitado esse(s) dia(s) tanto quanto nós!

[![Maqsuel Maqson](|static|/images/debian-30y-MMaqson-photo_2023-08-17_19-43-17thumb.png)](|static|/images/debian-30y-MMaqson-photo_2023-08-17_19-43-17.png)
***<p style="text-align: center;">"Debian 30 anos de inteligência coletiva" - Maqsuel Maqson</p>***

# Brasil

[![Thiago Pezzo](|static|/images/debian-30y-TPezzo-Brazi-photo_2023-08-16-thumb.png)](|static|/images/debian-30y-TPezzo-Brazi-photo_2023-08-16.png)
***<p style="text-align: center;">Pouso Alegre, Brasil</p>***

[![Daniel Pimentel](|static|/images/debian-30y-DPimentel-Brazil-photo_2023_08_26thumb.jpg)](|static|/images/debian-30y-DPimentel-Brazil-photo_2023_08_26.jpg)
***<p style="text-align: center;">Maceió, Brasil</p>***

[![Daniel Lenharo](|static|/images/debian-30y-DLenharo-Brazil-photo_2023_08_19thumb.png)](|static|/images/debian-30y-DLenharo-Brazil-photo_2023_08_19.png)
***<p style="text-align: center;">Curitiba, Brasil</p>***

[![Daniel Lenharo](|static|/images/debian-30y-Dlenharo-photo_2023-08-17_16-52-19thumb.png)](|static|/images/debian-30y-Dlenharo-photo_2023-08-17_16-52-19thumb.png)
***<p style="text-align: center;">O bolo está aqui. :) </p>***

[![phls](|static|/images/debian-30y-brasil-phls-photo_2023-08-17_16-51-35.png)](|static|/images/debian-30y-brasil-phls-photo_2023-08-17_16-51-35thumb.png)
***<p style="text-align; center;">Os desenvolvedores Debian honorários: [Buzz](https://wiki.debian.org/DebianBuzz), [Jessie](https://wiki.debian.org/DebianJessie), e [Woody](https://wiki.debian.org/DebianWoody) dão boas-vindas a esta incrível festa.</p>***

[![Carlos Melara](|static|/images/debian-30y-CMelara-Brazi-photo_2023-08-22_19-11-29thumb.png)](|static|/images/debian-30y-CMelara-Brazi-photo_2023-08-22_19-11-29thumb.png)
***<p style="text-align; center;">Sao Carlos, Sao Paulo, Brasil</p>***

[![Carlos Melara](|static|/images/debian-30y-CMelara-Brazi-photo_2023-08-22_19-11-29thumb.png)](|static|/images/debian-30y-CMelara-Brazi-photo_2023-08-22_19-11-29thumb.png)
***<p style="text-align; center;">Adesivos, panfletos, e notebooks, que incrível!</p>***

[![phls](|static|/images/debian-30y-belo-horizontel-phls_2023-08-12thumb.png)](|static|/images/debian-30y-belo-horizontel-phls_2023-08-12thumb.png)
***<p style="text-align; center;">Belo Horizonte, Brasil</p>***

[![sergiosacj](|static|/images/debian-30y-sergiosacj-brazil-photo_2023-08-22_19-11-29thumb.png)](|static|/images/debian-30y-sergiosacj-brazil-photo_2023-08-22_19-11-29thumb.png)
***<p style="text-align; center;">Brasília, Brasil</p>***

[![sergiosacj](|static|/images/debian-30y-sergiosacj-brazil-photo_2023-08-22_19-12-33thumb.png)](|static|/images/debian-30y-sergiosacj-brazil-photo_2023-08-22_19-12-33thumb.png)
***<p style="text-align; center;">Brasília, Brasil</p>***

# México

[![Jathan](|static|/images/debian-30y-jathan-photo_2023-08-27_10-36-41thumb.png)](|static|/images/debian-30y-jathan-photo_2023-08-27_10-36-41thumb.png)
***<p style="text-align; center;">30 anos!</p>***

[![Jathan](|static|/images/debian-30y-jathan-photo_2023-08-27_10-39-41thumb.png)](|static|/images/debian-30y-jathan-photo_2023-08-27_10-39-41thumb.png)
***<p style="text-align; center;">Uma selfie rápida</p>***

[![Jathan](|static|/images/debian-30y-jathan-photo_2023-08-27_10-40-02thumb.png)](|static|/images/debian-30y-jathan-photo_2023-08-27_10-40-02thumb.png)
***<p style="text-align; center;">Não incentivamos bebidas em cima de computadores, mas esta é aceitável para nós.</p>***

# Alemanha

[![h01ger](|static|/images/debian-30y-h01ger-2-photo_2023-08-17_16-58-46thumb.png)](|static|/images/debian-30y-h01ger-2-photo_2023-08-17_16-58-46thumb.png)
***<p style="text-align: center;">30 anos de amor</p>***

[![h01ger](|static|/images/debian-30y-h01ger-3-photo_2023-08-17_16-58-42thumb.png)](|static|/images/debian-30y-h01ger-3-photo_2023-08-17_16-58-42thumb.png)
***<p style="text-align: center;">A equipe alemã também procura este cachorro que pagou a conta da festa e depois saiu misteriosamente.</p>***

[![h01ger](|static|/images/debian-30y-h01ger-photo_2023-08-17_16-57-35thumb.png)](|static|/images/debian-30y-h01ger-photo_2023-08-17_16-57-35thumb.png)
***<p style="text-align: center;">Levamos a festa para o ar-livre</p>***

[![Stefano Rivera](|static|/images/debian-30y-SRivera-photo_2023-08-17_16-56-52thumb.png)](|static|/images/debian-30y-SRivera-photo_2023-08-17_16-56-52thumb.png)
***<p style="text-align: center;">Levamos a festa de volta para dentro do CCCamp</p>***

# Bélgica

[![Stefano Rivera](|static|/images/debian-30y-SRivera-photo_2023-08-20_01-17-18.png)](|static|/images/debian-30y-SRivera-photo_2023-08-20_01-17-18.png)
***<p style="text-align: center;">Bolo e diversidade na Bélgica</p>***

# El Salvador

[![Gato Barato Canelón Pulgosky](|static|/images/debian-30y-neomish-photo_2023-08-17_16-55-54thumb.png)](|static|/images/debian-30y-neomish-photo_2023-08-17_16-55-54thumb.png)
***<p style="text-align: center;">Comidas e amizades em El Salvador</p>***

# África do Sul

[![highvoltage](|static|/images/debian-30y-southafrica-highvoltage.photo_2023-08-17_16-47-07-1thumb.png)](|static|/images/debian-30y-southafrica-highvoltage.photo_2023-08-17_16-47-07-1thumb.png)
***<p style="text-align: center;">O Debian também é muito delicioso!</p>***

[![highvoltage](|static|/images/debian-30y-southafrica-highvoltage.photo_2023-08-17_16-49-57-2thumb.png)](|static|/images/debian-30y-southafrica-highvoltage.photo_2023-08-17_16-49-57-2thumb.png)
***<p style="text-align: center;">Todos sorrindo esperando por um pedaço de bolo</p>***

**Relatos**

[Debian Day 30 anos em Maceió - Brasil](https://debianbrasil.org.br/blog/debianday-30-anos-maceio-report/)

[Debian Day 30 anos em São Carlos - Brasil](https://debianbrasil.org.br/blog/debianday-30-anos-sao-carlos-report/)

[Debian Day 30 anos em Pouso Alegre - Brasil](https://debianbrasil.org.br/blog/debianday-30-anos-ifsuldeminas-report/)

[Debian Day 30 anos em Belo Horizonte - Brasil](https://debianbrasil.org.br/blog/debianday-30-anos-belo-horizonte-report/)

[Debian Day 30 anos em Curitiba - Brasil](https://debianbrasil.org.br/blog/debianday-30-anos-curitiba-report/)

[Debian Day 30 anos em Brasília - Brasil](https://debianbrasil.org.br/blog/debianday-30-anos-brasilia-report/)

[Debian Day 30 anos online no Brasil](https://debianbrasil.org.br/blog/debianday-30-anos-online-report/)

**Artigos & blogs**

[Feliz Debian Day - completando 30 anos de força](https://www.gamingonlinux.com/2023/08/happy-debian-day-going-30-years-strong/) - Liam Dawe

[O Debian faz 30 anos, feliz aniversário!](https://9to5linux.com/debian-turns-30-years-old-happy-birthday) -  Marius Nestor

[30 anos de estabilidade, segurança e liberdade: celebrando o aniversário do Debian](https://linuxiac.com/30-years-debian/) - Bobby Borisov

[Feliz 30o. aniversário, Debian!](https://www.geekersdigest.com/30-years-debian-linux/) - Claudio Kuenzier

[O Debian tem 30 anos e o Sgt Pepper tem pelo menos noventa e poucos anos](https://fossforce.com/2023/08/debian-is-30-and-sgt-pepper-is-at-least-ninetysomething/) - Christine Hall

[O Debian faz 30!](https://lwn.net/Articles/941744/) -Corbet

[Trinta anos de Debian!](https://vazaha.blog/en/22/thirty-years-of-debian) - Lennart Hengstmengel

[O Debian marca três décadas como "O Sistema Operacional Universal"](https://itwire.com/business-it-news/open-source/debian-marks-three-decades-as-universal-operating-system.html) - Sam Varghese

[O Debian Linux celebra o marco de 30 anos](https://www.linuxcapable.com/debian-linux-celebrates-30-years-milestone/) - Joshua James

[30 anos depois, o Debian está no coração das distros Linux de maior sucesso do mundo](https://www.theregister.com/2023/08/17/debian_turns_30/) - Liam Proven

[Relembrando 30 anos do Debian](https://hackaday.com/2023/08/21/looking-back-on-30-years-of-debian/) - Maya Posch

[Felicidades aos 30 anos de Debian: uma jornada de excelência em Código Aberto](https://debugpointnews.com/debian-30/) - arindam

**Discussões e mídias sociais**

[O Debian celebra 30 anos - Fonte: News YCombinator](https://news.ycombinator.com/item?id=37156529)

[Lançamento da nova versão do Linux, que estou chamando de Debian ... Fonte: News YCombinator](https://news.ycombinator.com/item?id=37147617)

[Comentário: Congrats @debian !!! Happy Birthday! Thank you for becoming a
cornerstone of the #opensource world. ￼Here's to decades of collaboration,
stability & #software #freedom](https://twitter.com/openSUSE/status/1691743754103795962) -openSUSELinux via X (anteriormente Twitter)

[Comentário: Today we #celebrate the 30th birthday of #Debian, one of the largest
and most important cornerstones of the #opensourcecommunity. For this we
would like to thank you very much and wish you the best for the next 30 years!
Source: X (Formerly Twitter](https://twitter.com/TUXEDOComputers/status/1691730101593907533) -TUXEDOComputers via X (anteriormente Twitter)

[Feliz Debian Day! - Fonte: Reddit.com](https://www.reddit.com/r/debian/comments/15s3lvz/happy_debian_day/)

_Video_ [A história do Debian | O início - Fonte: Linux User Space](https://www.youtube.com/watch?v=ASKFv8wu0IM)

[O Debian celebra 30 anos - Fonte: Lobste.rs](https://lobste.rs/s/bqcmxg/debian_celebrates_30_years)

_Video_ [O Debian aos 30 e sem mais distros! - LWDW388 - Fonte: LinuxGameCast](https://linuxgamecast.com/2023/08/debian-at-30-and-no-more-distro-hopping-lwdw388/)

[O Debian celebra 30 anos! - Fonte: Debian User Forums](https://forums.debian.net/viewtopic.php?t=155696)

[O Debian celebra 30 anos! - Fonte: Linux.org](https://www.linux.org/threads/debian-celebrates-30-years.46370/post-202667)
