Title: Những nhà Phát triển và Bảo trì Debian mới (Tháng giêng và hai 2023)
Slug: new-developers-2023-02
Date: 2023-03-22 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: vi
Translator: Trần Ngọc Quân
Status: published

Trong hai tháng qua, những người đóng góp sau đây nhận được tài khoản Phát triển Debian:

* Sahil Dhiman (sahil)
* Jakub Ružička (jru)

Trong hai tháng qua, những người đóng góp sau đây được thêm vào với tư cách là người Bảo trì Debian:

* Josenilson Ferreira da Silva
* Ileana Dumitrescu
* Douglas Kosovic
* Israel Galadima
* Timothy Pearson
* Blake Lee
* Vasyl Gello
* Joachim Zobel
* Amin Bandali

Xin chúc mừng!
