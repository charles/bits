Title: Appel à candidature pour DebConf24
Date: 2023-10-31 13:30
Tags: debconf, debconf24, debian
Lang: fr
Slug: debconf24-call-for-bids
Author: Debian DebConf Committe
Translator: Jean-Pierre Giraud
Status: published

Du fait de la situation actuelle en Israël qui devait accueillir la DebConf24,
le comité DebConf a décidé de refaire un appel à candidature pour l'accueil
de la DebConf24 dans un autre endroit.

Le comité DebConf souhaiterait adresser ses sincères remerciements à l'équipe
israélienne de la DebConf pour le travail qu'elle a réalisé depuis plusieurs
années. Cependant, eu égard à l'incertitude de la situation, le comité regrette
qui soit fort probable qu'il ne soit pas possible de tenir la DebConf en Israël.

Alors que nous sollicitons des propositions pour un nouveau lieu d'accueil,
nous vous prions de bien vouloir examiner et comprendre les détails des
exigences d'une candidature pour être l'hôte de la
[Conférence des développeurs Debian](https://www.debconf.org).

Consultez le [modèle de candidature pour une DebConf](https://wiki.debian.org/DebConf/General/Handbook/Bids/LocationCheckList)
où vous trouverez des directives pour la soumission d'une bonne candidature.

Pour soumettre votre candidature, veuillez créer la ou les pages appropriées
dans [DebConf Wiki Bids](https://wiki.debian.org/DebConf/24/Bids) et ajoutez la
dans la section « Bids » de la page principale
[DebConf 24](https://wiki.debian.org/DebConf/24).

_Il ne reste pas beaucoup de temps pour prendre la décision. Les candidatures
doivent être soumises avant la fin du mois de novembre pour que la décision
soit prise à la fin de l'année._

Dès que votre dossier de candidature sera achevé, envoyez une notification à
[debconf-team@lists.debian.org](mailto:debconf-team@lists.debian.org) pour que
nous sachions que votre proposition de candidature est prête à être examinée.

Nous vous conseillons aussi passer du temps sur notre espace de discussion sur
IRC [#debconf-team](https://www.oftc.net).

Étant donné le délai très court, nous comprendrons que les propositions ne
soient pas aussi abouties que d'habitude. Faites du mieux que vous pourrez dans
le temps disponible.

Les candidatures seront examinées selon la
[liste des priorités](https://wiki.debian.org/DebConf/General/Handbook/Bids/PriorityList).

Vous pouvez contacter l'équipe DebConf par courriel sur la liste de diffusion
[debconf-team@lists.debian.org](mailto:debconf-team@lists.debian.org) ou sur
le canal IRC #debconf-team d'OFTC ou sur notre
[canal Matrix](https://matrix.to/#/#debconf-team:matrix.debian.social).

Nous vous remercions par avance,

Le comité DebConf de Debian
