Title: नए डेबियन डेवलपर्स और मेंटेनर्स (नवंबर और दिसंबर 2022)
Slug: new-developers-2022-12
Date: 2023-01-24 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: hi-IN
Translator: Yashraj Moghe
Status: published

पिछले दो महीनों में निम्नलिखित सहायकों को उनका डेबियन डेवलपर अकाउंट मिला:

* Dennis Braun (snd)
* Raúl Benencia (rul)

और निम्नलिखित को डेबियन मेंटेनर का स्थान दिया गया हैं:

* Gioele Barabucci
* Agathe Porte
* Braulio Henrique Marques Souto
* Matthias Geiger
* Alper Nebi Yasak
* Fabian Grünbichler
* Lance Lin

इन्हें बधाईयाँ!
