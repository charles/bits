Title: DebConf23 closes in Kochi and DebConf24 announced
Date: 2023-09-18 16:30
Tags: debconf23, debconf24, announce, debconf
Slug: debconf23-closes
Author: Jean-Pierre Giraud and Donald Norwood
Artist: Aigars Mahinovs
Image: /images/debconf23_group_small.jpg
Status: published

[![DebConf23 group photo - click to enlarge](|static|/images/debconf23_group_small.jpg)](https://wiki.debian.org/DebConf/23/Photos?action=AttachFile&do=view&target=debconf23_group.jpg)

On Sunday 17 September 2023, the annual Debian Developers and Contributors
Conference came to a close.

Over 474 attendees representing 35 countries from around the world came
together for a combined 89 events made up of Talks, Discussons, Birds of a
Feather (BoF) gatherings, workshops, and activities in support of furthering
our distribution, learning from our mentors and peers, building our community,
and having a bit of fun.

The conference was preceded by the annual
[DebCamp](https://wiki.debian.org/DebCamp) hacking session held September 3d
through September 9th where Debian Developers and Contributors convened to
focus on their Individual Debian related projects or work in team sprints
geared toward in-person collaboration in developing Debian.

In particular this year Sprints took place to advance development in
Mobian/Debian, Reproducible Builds, and Python in Debian. This year also
featured a BootCamp that was held for newcomers staged by a team of
dedicated mentors who shared hands-on experience in Debian and offered a
deeper understanding of how to work in and contribute to the community.

The actual Debian Developers Conference started on Sunday 10 September 2023.

In addition to the traditional 'Bits from the DPL' talk, the continuous
key-signing party, lightning talks and the announcement of next year's
DebConf4, there were several update sessions shared by internal projects and
teams.

Many of the hosted discussion sessions were presented by our technical
teams who highlighted the work and focus of the Long Term Support (LTS),
Android tools, Debian Derivatives, Debian Installer, Debian Image, and the
Debian Science teams. The Python, Perl, and Ruby programming language teams
also shared updates on their work and efforts.

Two of the larger local Debian communities, Debian Brasil and Debian India
shared how their respective collaborations in Debian moved the project
forward and how they attracted new members and opportunities both in
Debian, F/OSS, and the sciences with their HowTos of demonstrated community
engagement.

The [schedule](https://debconf23.debconf.org/schedule/)
was updated each day with planned and ad-hoc activities introduced by
attendees over the course of the conference. Several activities that were
unable to be held in past years due to the Global COVID-19 Pandemic were
celebrated as they returned to the conference's schedule: a job fair, the
open-mic and poetry night, the traditional Cheese and Wine party, the group
photos and the Day Trips.

For those who were not able to attend, most of the talks and sessions were
videoed for live room streams with the recorded videos to be made available
later through the
[Debian meetings archive website](https://meetings-archive.debian.net/pub/debian-meetings/2023/DebConf23/).
Almost all of the sessions facilitated remote participation via IRC
messaging apps or online collaborative text documents which allowed remote
attendees to 'be in the room' to ask questions or share comments with the
speaker or assembled audience.

DebConf23 saw over 4.3 TiB of data streamed, 55 hours of scheduled talks,
23 network access points, 11 network switches, 75 kb of equipment imported,
400 meters of gaffer tape used, 1,463 viewed streaming hours, 461 T-shirts,
35 country Geoip viewers, 5 day trips, and an average of 169 meals planned
per day.

All of these events, activies, conversations, and streams coupled with our
love, interest, and participation in Debian annd F/OSS certainly made this
conference an overall success both here in Kochi, India and On-line around
the world.

The [DebConf23 website](https://debconf23.debconf.org/)
will remain active for archival purposes and will continue to offer
links to the presentations and videos of talks and events.

Next year, [DebConf24](https://wiki.debian.org/DebConf/24) will be held
in Haifa, Israel. As tradition follows before the next DebConf the local
organizers in Israel will start the conference activites with DebCamp with
particular focus on individual and team work towards improving the
distribution.

DebConf is committed to a safe and welcome environment for all
participants. See the
[web page about the Code of Conduct in DebConf23 website](https://debconf23.debconf.org/about/coc/)
for more details on this.

Debian thanks the commitment of numerous [sponsors](https://debconf23.debconf.org/sponsors/)
to support DebConf23, particularly our Platinum Sponsors:
[**Infomaniak**](https://www.infomaniak.com),
[**Proxmox**](https://www.proxmox.com/),
and [**Siemens**](https://www.siemens.com/).

We also wish to thank our Video and Infrastructure teams, the DebConf23 and
DebConf commitiees, our host nation of India, and each and every person who
helped contribute to this event and to Debian overall.

Thank you all for your work in helping Debian continue to be "The Universal
Operating System".

See you next year!

### About Debian

The Debian Project was founded in 1993 by Ian Murdock to be a truly free
community project. Since then the project has grown to be one of the
largest and most influential open source projects. Thousands of
volunteers from all over the world work together to create and maintain
Debian software. Available in 70 languages, and supporting a huge range
of computer types, Debian calls itself the _universal operating system_.

### About DebConf

DebConf is the Debian Project's developer conference. In addition to a
full schedule of technical, social and policy talks, DebConf provides an
opportunity for developers, contributors and other interested people to
meet in person and work together more closely. It has taken place
annually since 2000 in locations as varied as Scotland, Argentina, and
Bosnia and Herzegovina. More information about DebConf is available from
[https://debconf.org/](https://debconf.org).

### About Infomaniak

[**Infomaniak**](https://www.infomaniak.com) is a key player in the
European cloud market and the leading developer of Web technologies in
Switzerland. It aims to be an independent European alternative to the web
giants and is committed to an ethical and sustainable Web that respects
privacy and creates local jobs. Infomaniak develops cloud solutions (IaaS,
PaaS, VPS), productivity tools for online collaboration and video and radio
streaming services.

### About Proxmox

[**Proxmox**](https://www.proxmox.com/) develops powerful, yet easy-to-use
open-source server software. The product portfolio from Proxmox, including
server virtualization, backup, and email security, helps companies of any
size, sector, or industry to simplify their IT infrastructures. The Proxmox
solutions are based on the great Debian platform, and we are happy that we
can give back to the community by sponsoring DebConf23.

### About Siemens

[**Siemens**](https://www.siemens.com/) is technology company focused on
industry, infrastructure and transport. From resource-efficient factories,
resilient supply chains, smarter buildings and grids, to cleaner and more
comfortable transportation, and advanced healthcare, the company creates
technology with purpose adding real value for customers. By combining the
real and the digital worlds, Siemens empowers its customers to transform
their industries and markets, helping them to enhance the everyday of
billions of people.

### Contact Information

For further information, please visit the DebConf23 web page at
[https://debconf23.debconf.org/](https://debconf23.debconf.org/) or send
mail to <press@debian.org>.
