Title: Debian fête ses 30 ans !
Slug: debian-turns-30
Date: 2023-08-1 11:00
Author: Jean-Pierre Giraud, Donald Norwood, Grzegorz Szymaszek, Debian Publicity Team
Tags: debian, project, anniversary, DebianDay
Artist: Jeff Maier and Laure Sivadier
Translator: Jean-Pierre Giraud
Lang: fr
Status: Published

[![Debian a trente ans par Jeff Maier et Laure Sivadier](|static|/images/logo-debian-30-years-FR.png)](|static|/images/logo-debian-30-years-FR.png)

Il y a plus de trente ans, le regretté Ian Murdock
[écrivait](https://wiki.debian.org/DebianHistory?action=AttachFile&do=get&target=Debian-announcement-1993.txt)
au groupe de nouvelles comp.os.linux.development pour annoncer l'achèvement
d'une publication Linux flambant neuve qu'il baptisait « The Debian Linux
Release » (La publication Debian de Linux).

Il avait construit la publication à la main, à partir de rien pour ainsi dire.
Ian exposait les lignes directrices sur comment cette nouvelle publication
devait fonctionner, quelle approche devait être adoptée en matière de taille, de
quelle façon la mettre à niveau et les procédures d'installation ; et tout cela
avec une prise en considération majeure pour les utilisateurs sans connexion
Internet.

Inconscient d'avoir déclenché un mouvement dans la communauté encore naissante
de l'informatique libre et à code source ouvert (FLOSS), Ian travaillait et
retravaillait sans relâche sur Debian. La publication, désormais aidée par des
volontaires issus du groupe de nouvelles et du monde entier, a grandi et
continue à croître comme un des principaux et des plus anciens systèmes
d'exploitation LIBRES qui existent encore aujourd'hui.

Debian est essentiellement constituée d'utilisateurs, de contributeurs, de
développeurs et de parrains, mais, et c'est le plus important, de
***personnes***. Le dynamisme de Ian et ses objectifs demeurent inscrits
dans le cœur de Debian, ils demeurent dans tout notre travail, ils demeurent
dans l'esprit et dans les mains des utilisateurs du « Système d'exploitation
universel ».

Le projet Debian est fier et heureux de partager cet anniversaire, pas
exclusivement entre nous, mais aussi de partager cet événement avec tout le
monde, alors que nous nous réunissons pour célébrer une communauté remarquable
qui collabore, évolue et continue à faire la différence, pas seulement dans son
travail, mais aussi dans le monde entier.

Debian est présente dans les systèmes en grappe, les centres de données, les
ordinateurs de bureau, les systèmes embarqués, l'Internet des objets, les
ordinateurs portables, les serveurs, et elle peut éventuellement faire
fonctionner le serveur web et l'appareil sur lequel vous lisez cet article, et
il est même possible de la trouver dans un
[vaisseau spatial](https://www.zdnet.com/article/to-the-space-station-and-beyond-with-linux/).

Pour revenir sur terre, Debian apporte tout son soutien à des projets pour en
faciliter l'accès : [Debian EDU/Skolelinux](https://blends.debian.org/edu/), un
système d'exploitation à usage éducatif pour les écoles et les communautés,
[Debian Science](https://wiki.debian.org/DebianScience) pour des logiciels
scientifiques libres, aussi bien pour des domaines bien établis que pour des
domaines innovants,
[Debian Hamradio](https://www.debian.org/blends/hamradio/about)
pour les radioamateurs,
[Debian-Accessibility](https://www.debian.org/devel/debian-accessibility/);
un projet centré sur la conception d'un système d'exploitation pour répondre
aux besoins des personnes handicapées et
[Debian Astro](https://blends.debian.org/astro/) dont l'objectif est d'aider
les astronomes professionnels et amateurs.

Debian fait tout son possible pour donner, atteindre, inclure, guider, partager
et enseigner au moyen de stages à travers plusieurs programmes internes et
externes comme le « Google Summer of Code », « Outreachy » ou le « Open Source
Promotion Plan ».

Rien de tout cela n'aurait pu être possible sans la quantité de soutiens,
d'attention et de contributions de ce qui a débuté comme un projet bénévole et
le demeure encore. Nous le fêtons avec toutes celles et tous ceux qui ont
contribué à façonner Debian durant toutes ces années et pour l'avenir.

Aujourd'hui c'est certain, nous célébrons tous, les trente ans de Debian, mais
sachez qu'en même temps, Debian partage cette célébration avec vous tous.

Dans les prochains jours, des fêtes anniversaires sont prévues en Afrique du
Sud, Allemagne (CCcamp), Autriche, Belgique, Bolivie, Brésil, Bulgarie, France,
Inde, Iran, Portugal, République Tchèque, Serbie et Turquie.

Venez nous rejoindre !

Recherchez, assistez ou organisez votre propre
[fête anniversaire de la journée Debian 2023](https://wiki.debian.org/DebianDay/2023).

À bientôt !

Merci à tous, merci beaucoup.

Bien Amicalement,

Le projet Debian
