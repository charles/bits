Title: Nuevos desarrolladores y mantenedores de Debian (marzo y abril del 2023)
Slug: new-developers-2023-04
Date: 2023-05-25 12:00
Author: Jean-Pierre Giraud
Tags: project
Lang: es
Status: published

Los siguientes colaboradores del proyecto se convirtieron en Debian Developers
en los dos últimos meses:

* James Lu (jlu)
* Hugh McMaster (hmc)
* Agathe Porte (gagath)

Los siguientes colaboradores del proyecto se convirtieron en Debian Maintainers
en los dos últimos meses:

* Soren Stoutner
* Matthijs Kooijman
* Vinay Keshava
* Jarrah Gosbell
* Carlos Henrique Lima Melara
* Cordell Bloor

¡Felicidades a todos!
