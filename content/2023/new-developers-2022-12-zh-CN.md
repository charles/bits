Title: 新的 Debian 开发者和维护者 (2022年11月 至 年12月)
Slug: new-developers-2022-12
Date: 2023-01-24 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: zh-CN
Translator: Boyuan Yang
Status: published

下列贡献者在过去的两个月中获得了他们的 Debian 开发者帐号：

* Dennis Braun (snd)
* Raúl Benencia (rul)

下列贡献者在过去的两个月中被添加为 Debian 维护者：

* Gioele Barabucci
* Agathe Porte
* Braulio Henrique Marques Souto
* Matthias Geiger
* Alper Nebi Yasak
* Fabian Grünbichler
* Lance Lin

祝贺他们！
