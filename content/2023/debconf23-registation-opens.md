Title: Registration and the Call for Proposals for DebConf23 are now open!
Slug: debconf23-registation-opens
Date: 2023-06-12 18:17
Author: Sahil Dhiman
Tags: debconf23, debconf
Status: published

For DebConf23, we're pleased to announce opening of registration and call for
proposal. Following is the info text -

---
Registration and the Call for Proposals for DebConf23 are now open. The 24th
edition of the Debian annual conference will be held from *September 10th to
September 17th, 2023, in Infopark, Kochi, India.* The main conference will be
preceded by DebCamp, which will take place from September 3rd to
September 9th, 2023.

The registration form can be accessed by creating an account on the
[DebConf23 website](https://debconf23.debconf.org/) and clicking on "register"
in the profile section. The number of attendees is capped at 300 this year.
All registrations will be reviewed by bursary team, and completing the
registration form does not guarantee attendance.

As always, basic registration for DebConf is free of charge for attendees. If
you are attending the conference in a professional capacity or as a
representative of your company, we kindly ask that you consider registering in
one of our paid categories to help cover the costs of organizing the
conference and to support subsidizing other community members.

The last day to register with guaranteed swag is 5th August.

We also encourage eligible individuals to apply for a diversity bursary. Travel,
food, and accommodation bursaries are available. More details can be found on
the [bursary info page](https://debconf23.debconf.org/about/bursaries/).

The last day to apply for a bursary is 1st July. Applicants should receive
feedback on their bursary application by 16th July.

The call for proposals for talks, discussions and other activities is also open.
To submit a proposal you need to create an account on the website, and then use
the "Submit Talk" button in the profile section. The last day to submit and
have your proposal be considered for the main conference schedule, with video
coverage guaranteed, is 13th August.

DebConf23 is also accepting sponsors. Interested companies and organizations
may contact the DebConf team through sponsors@debconf.org or visit the
[DebConf23 website](https://debconf23.debconf.org/sponsors/become-a-sponsor/).
