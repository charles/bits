Title: Debian Celebrates 30 years!
Slug: debian-turns-30
Date: 2023-08-16 11:00
Author: Jean-Pierre Giraud, Donald Norwood, Grzegorz Szymaszek, Debian Publicity Team
Tags: debian, project, anniversary, DebianDay
Artist: Jeff Maier
Status: published

[![Debian 30 years by Jeff Maier](|static|/images/logo-debian-30-years-600x600.png)](|static|/images/logo-debian-30-years.png)

Over 30 years ago the late Ian Murdock
[wrote](https://wiki.debian.org/DebianHistory?action=AttachFile&do=get&target=Debian-announcement-1993.txt)
to the comp.os.linux.development newsgroup about the completion of a brand-new
Linux release which he named "The Debian Linux Release".

He built the release by hand, from scratch, so to speak. Ian laid out
guidelines for how this new release would work, what approach the release
would take regarding its size, manner of upgrades, installation procedures; and
with great care of consideration for users without Internet connection.

Unaware that he had sparked a movement in the fledgling F/OSS community, Ian
worked on and continued to work on Debian. The release, now aided by volunteers
from the newsgroup and around the world, grew and continues to grow as one of
the largest and oldest FREE operating systems that still exist today.

Debian at its core is comprised of Users, Contributors, Developers, and
Sponsors, but most importantly, ***People***. Ians drive and focus remains
embedded in the core of Debian, it remains in all of our work, it remains in
the minds and hands of the users of _**The Universal Operating System**_.

The Debian Project is proud and happy to share our anniversary not exclusively
unto ourselves, instead we share this moment with everyone, as we come together
in celebration of a resounding community that works together, effects change,
and continues to make a difference, not just in our work but around the world.

Debian is present in cluster systems, datacenters, desktop computers, embedded
systems, IoT devices, laptops, servers, it may possibly be powering the web
server and device you are reading this article on, and it can also be found in
[Spacecraft](https://www.zdnet.com/article/to-the-space-station-and-beyond-with-linux/).

Closer to earth, Debian fully supports projects for accessibility:
[Debian Edu/Skolelinux](https://blends.debian.org/edu/) - an operating system
designed for educational use in schools and communities,
[Debian Science](https://wiki.debian.org/DebianScience) - providing free
scientific software across many established and emerging fields,
[Debian Hamradio](https://www.debian.org/blends/hamradio/about) - for amateur
radio enthusiasts,
[Debian-Accessibility](https://www.debian.org/devel/debian-accessibility/) - a
project focused on the design of an operating system suited to fit the
requirements of people with disabilites, and
[Debian Astro](https://blends.debian.org/astro/) - focused on supporting
professional and hobbyist astronomers.

Debian strives to give, reach, embrace, mentor, share, and teach with
internships through many programs internally and externally such as the Google
Summer of Code, Outreachy, and the Open Source Promotion Plan.

None of this could be possible without the vast amount of support, care, and
contributions from what started as and is still an all volunteer project.
We celebrate with each and every one who has helped shape Debian over all of
these years and toward the future.

Today we all certainly celebrate 30 years of Debian, but know that Debian
celebrates with each and every one of you all at the same time.

Over the next few days Celebration parties are planned to take place in
Austria, Belgium, Bolivia, Brazil, Bulgaria, Czech Republic, France, Germany
(CCCcamp), India, Iran, Portugal, Serbia, South Africa, and Turkey.

You are of course, invited to join us!

Check out, attend, or form your very own
[DebianDay 2023 Event](https://wiki.debian.org/DebianDay/2023).

See you then!

Thank you, thank you all so very much.

With Love,

The Debian Project
