Title: New Debian Developers and Maintainers (March and April 2023)
Slug: new-developers-2023-04
Date: 2023-05-25 12:00
Author: Jean-Pierre Giraud
Tags: project
Lang: en
Status: published

The following contributors got their Debian Developer accounts in the last two
months:

* James Lu (jlu)
* Hugh McMaster (hmc)
* Agathe Porte (gagath)

The following contributors were added as Debian Maintainers in the last two
months:

* Soren Stoutner
* Matthijs Kooijman
* Vinay Keshava
* Jarrah Gosbell
* Carlos Henrique Lima Melara
* Cordell Bloor

Congratulations!
