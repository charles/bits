Title: New Debian Developers and Maintainers (September and October 2023)
Slug: new-developers-2023-10
Date: 2023-11-30 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: en
Status: published

The following contributors got their Debian Developer accounts in the last two
months:

* François Mazen (mzf)
* Andrew Ruthven (puck)
* Christopher Obbard (obbardc)
* Salvo Tomaselli (ltworf)

The following contributors were added as Debian Maintainers in the last two
months:

* Bo YU
* Athos Coimbra Ribeiro
* Marc Leeman
* Filip Strömbäck

Congratulations!
