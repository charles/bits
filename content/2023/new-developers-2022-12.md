Title: New Debian Developers and Maintainers (November and December 2022)
Slug: new-developers-2022-12
Date: 2023-01-24 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: en
Status: published

The following contributors got their Debian Developer accounts in the last two
months:

* Dennis Braun (snd)
* Raúl Benencia (rul)

The following contributors were added as Debian Maintainers in the last two
months:

* Gioele Barabucci
* Agathe Porte
* Braulio Henrique Marques Souto
* Matthias Geiger
* Alper Nebi Yasak
* Fabian Grünbichler
* Lance Lin

Congratulations!
