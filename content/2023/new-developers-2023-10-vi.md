Title: Những nhà Phát triển và Bảo trì Debian mới (Tháng chín và mười 2023)
Slug: new-developers-2023-10
Date: 2023-11-30 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: vi
Translator: Trần Ngọc Quân
Status: published

Trong hai tháng qua, những người đóng góp sau đây nhận được tài khoản Phát triển Debian:

* François Mazen (mzf)
* Andrew Ruthven (puck)
* Christopher Obbard (obbardc)
* Salvo Tomaselli (ltworf)

Trong hai tháng qua, những người đóng góp sau đây được thêm vào với tư cách là người Bảo trì Debian:

* Bo YU
* Athos Coimbra Ribeiro
* Marc Leeman
* Filip Strömbäck

Xin chúc mừng!
