Title: DebianDay Celebrations and comments
Slug: debian-30-celebrations
Date: 2023-09-09 11:00
Author: Donald Norwood, Paulo Henrique de Lima Santana
Tags: debian30, debian, project, anniversary, DebianDay
Artist: Credited
Status: published

**Debian Celebrates 30 years!**

We celebrated our [birthday](https://wiki.debian.org/DebianDay) this year and
we had a great time with new friends, new members welcomed to the community,
and the world.

We have collected a few comments, videos, and discussions from
around the Internet, and some images from some of the
[DebianDay2023](https://wiki.debian.org/DebianDay/2023) events. We hope that
you enjoyed the day(s) as much as we did!

[![Maqsuel Maqson](|static|/images/debian-30y-MMaqson-photo_2023-08-17_19-43-17thumb.png)](|static|/images/debian-30y-MMaqson-photo_2023-08-17_19-43-17.png)
***<p style="text-align: center;">"Debian 30 years of collective intelligence" -Maqsuel Maqson</p>***

# Brazil

[![Thiago Pezzo](|static|/images/debian-30y-TPezzo-Brazi-photo_2023-08-16-thumb.png)](|static|/images/debian-30y-TPezzo-Brazi-photo_2023-08-16.png)
***<p style="text-align: center;">Pouso Alegre, Brazil</p>***

[![Daniel Pimentel](|static|/images/debian-30y-DPimentel-Brazil-photo_2023_08_26thumb.jpg)](|static|/images/debian-30y-DPimentel-Brazil-photo_2023_08_26.jpg)
***<p style="text-align: center;">Maceió, Brazil</p>***

[![Daniel Lenharo](|static|/images/debian-30y-DLenharo-Brazil-photo_2023_08_19thumb.png)](|static|/images/debian-30y-DLenharo-Brazil-photo_2023_08_19.png)
***<p style="text-align: center;">Curitiba, Brazil</p>***

[![Daniel Lenharo](|static|/images/debian-30y-Dlenharo-photo_2023-08-17_16-52-19thumb.png)](|static|/images/debian-30y-Dlenharo-photo_2023-08-17_16-52-19thumb.png)
***<p style="text-align: center;">The cake is there. :) </p>***

[![phls](|static|/images/debian-30y-brasil-phls-photo_2023-08-17_16-51-35.png)](|static|/images/debian-30y-brasil-phls-photo_2023-08-17_16-51-35thumb.png)
***<p style="text-align; center;">Honorary Debian Developers: [Buzz](https://wiki.debian.org/DebianBuzz), [Jessie](https://wiki.debian.org/DebianJessie), and [Woody](https://wiki.debian.org/DebianWoody) welcome guests to this amazing party.</p>***

[![Carlos Melara](|static|/images/debian-30y-CMelara-Brazi-photo_2023-08-22_19-11-29thumb.png)](|static|/images/debian-30y-CMelara-Brazi-photo_2023-08-22_19-11-29thumb.png)
***<p style="text-align; center;">Sao Carlos, state of Sao Paulo, Brazil</p>***

[![Carlos Melara](|static|/images/debian-30y-CMelara-Brazil-photo_2023-08-20_01-15-04thumb.png)](|static|/images/debian-30y-CMelara-Brazil-photo_2023-08-20_01-15-04thumb.png)
***<p style="text-align; center;">Stickers, and Fliers, and Laptops, oh my!</p>***

[![phls](|static|/images/debian-30y-belo-horizontel-phls_2023-08-12thumb.png)](|static|/images/debian-30y-belo-horizontel-phls_2023-08-12thumb.png)
***<p style="text-align; center;">Belo Horizonte, Brazil</p>***

[![sergiosacj](|static|/images/debian-30y-sergiosacj-brazil-photo_2023-08-22_19-11-29thumb.png)](|static|/images/debian-30y-sergiosacj-brazil-photo_2023-08-22_19-11-29thumb.png)
***<p style="text-align; center;">Brasília, Brazil</p>***

[![sergiosacj](|static|/images/debian-30y-sergiosacj-brazil-photo_2023-08-22_19-12-33thumb.png)](|static|/images/debian-30y-sergiosacj-brazil-photo_2023-08-22_19-12-33thumb.png)
***<p style="text-align; center;">Brasília, Brazil</p>***

# Mexico

[![Jathan](|static|/images/debian-30y-jathan-photo_2023-08-27_10-36-41thumb.png)](|static|/images/debian-30y-jathan-photo_2023-08-27_10-36-41thumb.png)
***<p style="text-align; center;">30 años!</p>***

[![Jathan](|static|/images/debian-30y-jathan-photo_2023-08-27_10-39-41thumb.png)](|static|/images/debian-30y-jathan-photo_2023-08-27_10-39-41thumb.png)
***<p style="text-align; center;">A quick Selfie</p>***

[![Jathan](|static|/images/debian-30y-jathan-photo_2023-08-27_10-40-02thumb.png)](|static|/images/debian-30y-jathan-photo_2023-08-27_10-40-02thumb.png)
***<p style="text-align; center;">We do not encourage beverages on computing hardware, but this one is okay by us.</p>***

# Germany

[![h01ger](|static|/images/debian-30y-h01ger-2-photo_2023-08-17_16-58-46thumb.png)](|static|/images/debian-30y-h01ger-2-photo_2023-08-17_16-58-46thumb.png)
***<p style="text-align: center;">30 years of love</p>***

[![h01ger](|static|/images/debian-30y-h01ger-3-photo_2023-08-17_16-58-42thumb.png)](|static|/images/debian-30y-h01ger-3-photo_2023-08-17_16-58-42thumb.png)
***<p style="text-align: center;">The German Delegation is also looking for this dog who footed the bill for the party, then left mysteriously.</p>***

[![h01ger](|static|/images/debian-30y-h01ger-photo_2023-08-17_16-57-35thumb.png)](|static|/images/debian-30y-h01ger-photo_2023-08-17_16-57-35thumb.png)
***<p style="text-align: center;">We took the party outside</p>***

[![Stefano Rivera](|static|/images/debian-30y-SRivera-photo_2023-08-17_16-56-52thumb.png)](|static|/images/debian-30y-SRivera-photo_2023-08-17_16-56-52thumb.png)
***<p style="text-align: center;">We brought the party back inside at CCCamp</p>***

# Belgium

[![Stefano Rivera](|static|/images/debian-30y-SRivera-photo_2023-08-20_01-17-18.png)](|static|/images/debian-30y-SRivera-photo_2023-08-20_01-17-18.png)
***<p style="text-align: center;">Cake and Diversity in Belgium</p>***

# El Salvador

[![Gato Barato Canelón Pulgosky](|static|/images/debian-30y-neomish-photo_2023-08-17_16-55-54thumb.png)](|static|/images/debian-30y-neomish-photo_2023-08-17_16-55-54thumb.png)
***<p style="text-align: center;">Food and Fellowship in El Salvador</p>***

# South Africa

[![highvoltage](|static|/images/debian-30y-southafrica-highvoltage.photo_2023-08-17_16-47-07-1thumb.png)](|static|/images/debian-30y-southafrica-highvoltage.photo_2023-08-17_16-47-07-1thumb.png)
***<p style="text-align: center;">Debian is also very delicious!</p>***

[![highvoltage](|static|/images/debian-30y-southafrica-highvoltage.photo_2023-08-17_16-49-57-2thumb.png)](|static|/images/debian-30y-southafrica-highvoltage.photo_2023-08-17_16-49-57-2thumb.png)
***<p style="text-align: center;">All smiles waiting to eat the cake</p>***

**Reports**

[Debian Day 30 years in Maceió - Brazil](https://debianbrasil.org.br/blog/debianday-30-anos-maceio-report/)

[Debian Day 30 years in São Carlos - Brazil](https://debianbrasil.org.br/blog/debianday-30-anos-sao-carlos-report/)

[Debian Day 30 years in Pouso Alegre - Brazil](https://debianbrasil.org.br/blog/debianday-30-anos-ifsuldeminas-report/)

[Debian Day 30 years in Belo Horizonte - Brazil](https://debianbrasil.org.br/blog/debianday-30-anos-belo-horizonte-report/)

[Debian Day 30 years in Curitiba - Brazil](https://debianbrasil.org.br/blog/debianday-30-anos-curitiba-report/)

[Debian Day 30 years in Brasília - Brazil](https://debianbrasil.org.br/blog/debianday-30-anos-brasilia-report/)

[Debian Day 30 years online in Brazil](https://debianbrasil.org.br/blog/debianday-30-anos-online-report/)

**Articles & Blogs**

[Happy Debian Day - going 30 years strong](https://www.gamingonlinux.com/2023/08/happy-debian-day-going-30-years-strong/) - Liam Dawe

[Debian Turns 30 Years Old, Happy Birthday!](https://9to5linux.com/debian-turns-30-years-old-happy-birthday) -  Marius Nestor

[30 Years of Stability, Security, and Freedom: Celebrating Debian’s Birthday](https://linuxiac.com/30-years-debian/) - Bobby Borisov

[Happy 30th Birthday, Debian!](https://www.geekersdigest.com/30-years-debian-linux/) - Claudio Kuenzier

[Debian is 30 and Sgt Pepper Is at Least Ninetysomething](https://fossforce.com/2023/08/debian-is-30-and-sgt-pepper-is-at-least-ninetysomething/) - Christine Hall

[Debian turns 30!](https://lwn.net/Articles/941744/) -Corbet

[Thirty years of Debian!](https://vazaha.blog/en/22/thirty-years-of-debian) - Lennart Hengstmengel

[Debian marks three decades as 'Universal Operating System'](https://itwire.com/business-it-news/open-source/debian-marks-three-decades-as-universal-operating-system.html) - Sam Varghese

[Debian Linux Celebrates 30 Years Milestone](https://www.linuxcapable.com/debian-linux-celebrates-30-years-milestone/) - Joshua James

[30 years on, Debian is at the heart of the world's most successful Linux distros](https://www.theregister.com/2023/08/17/debian_turns_30/) - Liam Proven

[Looking Back on 30 Years of Debian](https://hackaday.com/2023/08/21/looking-back-on-30-years-of-debian/) - Maya Posch

[Cheers to 30 Years of Debian: A Journey of Open Source Excellence](https://debugpointnews.com/debian-30/) - arindam

**Discussions and Social Media**

[Debian Celebrates 30 Years - Source: News YCombinator](https://news.ycombinator.com/item?id=37156529)

[Brand-new Linux release, which I'm calling the Debian ... Source: News YCombinator](https://news.ycombinator.com/item?id=37147617)

[Comment: Congrats @debian !!! Happy Birthday! Thank you for becoming a
cornerstone of the #opensource world. ￼Here's to decades of collaboration,
stability & #software #freedom](https://twitter.com/openSUSE/status/1691743754103795962) -openSUSELinux via X (formerly Twitter)

[Comment: Today we #celebrate the 30th birthday of #Debian, one of the largest
and most important cornerstones of the #opensourcecommunity. For this we
would like to thank you very much and wish you the best for the next 30 years!
Source: X (Formerly Twitter](https://twitter.com/TUXEDOComputers/status/1691730101593907533) -TUXEDOComputers via X (formerly Twitter)

[Happy Debian Day! - Source: Reddit.com](https://www.reddit.com/r/debian/comments/15s3lvz/happy_debian_day/)

_Video_ [The History of Debian | The Beginning - Source: Linux User Space](https://www.youtube.com/watch?v=ASKFv8wu0IM)

[Debian Celebrates 30 years -Source: Lobste.rs](https://lobste.rs/s/bqcmxg/debian_celebrates_30_years)

_Video_ [Debian At 30 and No More Distro Hopping! - LWDW388 - Source: LinuxGameCast](https://linuxgamecast.com/2023/08/debian-at-30-and-no-more-distro-hopping-lwdw388/)

[Debian Celebrates 30 years! - Source: Debian User Forums](https://forums.debian.net/viewtopic.php?t=155696)

[Debian Celebrates 30 years! - Source: Linux.org](https://www.linux.org/threads/debian-celebrates-30-years.46370/post-202667)
