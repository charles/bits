Title: Novos(as) desenvolvedores(as) e mantenedores(as) Debian (julho e agosto de 2023)
Slug: new-developers-2023-08
Date: 2023-09-27 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: pt
Status: published

Os(As) seguintes colaboradores(as) do projeto se tornaram Desenvolvedores(as)
Debian nos últimos dois meses:

* Marius Gripsgard (mariogrip)
* Mohammed Bilal (rmb)
* Lukas Märdian (slyon)
* Robin Gustafsson (rgson)
* David da Silva Polverari (polverari)
* Emmanuel Arias (eamanu)

Os(As) seguintes colaboradores(as) do projeto se tornaram Mantenedores(as)
Debian nos últimos dois meses:

* Aymeric Agon-Rambosson
* Blair Noctis
* Lena Voytek
* Philippe Coval
* John Scott

Parabéns a todos!
