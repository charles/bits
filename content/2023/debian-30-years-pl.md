Title: Debian świętuje 30 lat!
Slug: debian-turns-30
Date: 2023-08-16 11:00
Author: Jean-Pierre Giraud, Donald Norwood, Grzegorz Szymaszek, Debian Publicity Team
Tags: debian, project, anniversary, DebianDay
Artist: Jeff Maier
Lang: pl
Translator: Grzegorz Szymaszek
Status: published

[![30 lat Debiana autorstwa Jeffa Maiera](|static|/images/logo-debian-30-years-600x600.png)](|static|/images/logo-debian-30-years.png)

Ponad 30 lat temu świętej pamięci Ian Murdock
[napisał](https://wiki.debian.org/DebianHistory?action=AttachFile&do=get&target=Debian-announcement-1993.txt)
na grupę dyskusyjną comp.os.linux.development o&nbsp;przygotowaniu zupełnie
nowego wydania Linuksa, które nazwał &bdquo;<span lang="en">The Debian Linux
Release</span>&rdquo;.

Zbudował to wydanie, w&nbsp;pewnym sensie, ręcznie, od zera.
Ian wyłożył wytyczne co do sposobu działania nowego wydania, podejścia do
kwestii rozmiaru, sposobu aktualizacji i&nbsp;instalacji, ze szczególnym
uwzględnieniem użytkowników bez dostępu do internetu.

Nieświadom, że zapoczątkował ruch w&nbsp;powstającej społeczności wolnego
i&nbsp;otwartego oprogramowania, Ian pracował i&nbsp;pracował nad Debianem.
To wydanie, już wspierane przez wolontariuszy z&nbsp;grupy dyskusyjnej
i&nbsp;całego świata, rosło i&nbsp;nadal rośnie jako jeden z&nbsp;największych
i&nbsp;najstarszych **wolnych** systemów operacyjnych, które nadal istnieją.

Debian u&nbsp;podstaw składa się z&nbsp;Użytkowników, Kontrybutorów,
Deweloperów i&nbsp;Sponsorów, ale przede wszystkim **Ludzi**.
Wkład i&nbsp;cele Iana pozostają podstawą Debiana, pozostają w&nbsp;całej naszej
pracy, pozostają w&nbsp;umysłach i&nbsp;rękach użytkowników **uniwersalnego
systemu operacyjnego**.

Projekt Debian jest dumny i&nbsp;szczęśliwy mogąc dzielić się naszym świętem nie
tylko wśród nas, zamiast tego dzielimy się tą chwilą z&nbsp;każdym, ponieważ
przyszliśmy razem świętować aktywną społeczność, która pracuje razem, dokonuje
zmian i&nbsp;ulepszeń, nie tylko w&nbsp;naszej pracy, ale
i&nbsp;w&nbsp;otaczającym nas świecie.

Debian jest obecny w&nbsp;systemach klastrowych, centrach danych, komputerach
osobistych, systemach wbudowanych, urządzeniach Internetu Rzeczy, laptopach,
serwerach; być może działa w&nbsp;serwerze WWW i&nbsp;urządzeniu, na którym
czytasz ten artykuł.
Można go także znaleźć
w&nbsp;[przestrzeni kosmicznej](https://www.zdnet.com/article/to-the-space-station-and-beyond-with-linux/).

Bliżej Ziemi, Debian w&nbsp;pełni wspiera projekty dla szerokiego grona
odbiorców:
[Debian Edu/Skolelinux](https://blends.debian.org/edu/)&nbsp;&mdash; system
operacyjny zaprojektowany do używania w&nbsp;celach edukacyjnych w&nbsp;szkołach
i&nbsp;różnych społecznościach,
[Debian Science](https://wiki.debian.org/DebianScience)&nbsp;&mdash; dostarcza
wolne oprogramowanie naukowe do wielu istniejących i&nbsp;rodzących się gałęzi
nauki,
[Debian Hamradio](https://www.debian.org/blends/hamradio/about)&nbsp;&mdash; dla
miłośników krótkofalarstwa,
[Debian-Accessibility](https://www.debian.org/devel/debian-accessibility/)&nbsp;&mdash;
projekt mający na celu przygotowanie systemu operacyjnego dopasowanego do
potrzeb osób z&nbsp;niepełnosprawnością,
oraz [Debian Astro](https://blends.debian.org/astro/)&nbsp;&mdash; skupiony na
wspieraniu zarówno zawodowych, jak i&nbsp;amatorskich astronomów.

Debian stara się dawać, docierać, przyjmować, udostępniać i&nbsp;uczyć ze
stażystami w&nbsp;ramach wielu wewnętrznych i&nbsp;zewnętrznych programów,
takich jak Google Summer of Code, Outreachy, czy Open Source Promotion Plan.

To wszystko nie byłoby możliwe bez obszernego wsparcia, opieki i&nbsp;wkładu
z&nbsp;tego, co zaczęło się jako projekt wolontariuszy, i&nbsp;nadal nim jest.
Świętujemy z&nbsp;każdym, kto pomógł ukształtować Debiana przez te wszystkie
lata i&nbsp;w&nbsp;przyszłości.

Dziś świętujemy 30 lat Debiana, ale wiedzcie, że Debian świętuje z&nbsp;każdym
z&nbsp;Was w&nbsp;tym samym czasie.

W&nbsp;najbliższych dniach planowane jest świętowanie na przyjęciach, które
odbędą się w&nbsp;Austrii, Belgii, Boliwii, Brazylii, Bułgarii, Czechach,
Francji, Indiach, Iranie, Niemczech (CCCcamp), Południowej Afryce, Portugalii,
Serbii i&nbsp;Turcji.
Zachęcamy oczywiście do dołączenia do nas!
Sprawdź, weź udział, lub zorganizuj własny
[DebianDay 2023](https://wiki.debian.org/DebianDay/2023).
Do zobaczenia!

Dziękujemy, serdecznie dziękujemy Wam wszystkim.

Z&nbsp;miłością,  
Projekt Debian
