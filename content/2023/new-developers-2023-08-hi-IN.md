Title: नए डेबियन डेवलपर्स और मेंटेनर्स (जुलाई और अगस्त 2023)
Slug: new-developers-2023-08
Date: 2023-09-27 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: hi-IN
Translator: Yashraj Moghe
Status: published

पिछले दो महीनों में निम्नलिखित सहायकों को उनका डेबियन डेवलपर अकाउंट मिला:

* Marius Gripsgard (mariogrip)
* Mohammed Bilal (rmb)
* Lukas Märdian (slyon)
* Robin Gustafsson (rgson)
* David da Silva Polverari (polverari)
* Emmanuel Arias (eamanu)

और निम्नलिखित को डेबियन मेंटेनर का स्थान दिया गया हैं:

* Aymeric Agon-Rambosson
* Blair Noctis
* Lena Voytek
* Philippe Coval
* John Scott

इन्हें बधाईयाँ!
