Title: Novos(as) desenvolvedores(as) e mantenedores(as) Debian (novembro e dezembro de 2022)
Slug: new-developers-2022-12
Date: 2023-01-24 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: pt
Status: published

Os(As) seguintes colaboradores(as) do projeto se tornaram Desenvolvedores(as)
Debian nos últimos dois meses:

* Dennis Braun (snd)
* Raúl Benencia (rul)

Os(As) seguintes colaboradores(as) do projeto se tornaram Mantenedores(as)
Debian nos últimos dois meses:

* Gioele Barabucci
* Agathe Porte
* Braulio Henrique Marques Souto
* Matthias Geiger
* Alper Nebi Yasak
* Fabian Grünbichler
* Lance Lin

Parabéns a todos!
