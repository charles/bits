Title: 新的 Debian 开发者和维护者 (2023年7月 至 8月)
Slug: new-developers-2023-08
Date: 2023-09-27 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: zh-CN
Translator: Boyuan Yang
Status: published

下列贡献者在过去的两个月中获得了他们的 Debian 开发者帐号：

* Marius Gripsgard (mariogrip)
* Mohammed Bilal (rmb)
* Lukas Märdian (slyon)
* Robin Gustafsson (rgson)
* David da Silva Polverari (polverari)
* Emmanuel Arias (eamanu)

下列贡献者在过去的两个月中被添加为 Debian 维护者：

* Aymeric Agon-Rambosson
* Blair Noctis
* Lena Voytek
* Philippe Coval
* John Scott

祝贺他们！
