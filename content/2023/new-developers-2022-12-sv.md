Title: Nya Debianutvecklare och Debian Maintainers (November och December 2022)
Slug: new-developers-2022-12
Date: 2023-01-24 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: sv
Translator: Andreas Rönnquist
Status: published

Följande bidragslämnare fick sina Debianutvecklarkonton under de senaste två
månaderna:

* Dennis Braun (snd)
* Raúl Benencia (rul)

Följande bidragslämnare adderades som Debian Maintainers under de senaste två
månaderna:

* Gioele Barabucci
* Agathe Porte
* Braulio Henrique Marques Souto
* Matthias Geiger
* Alper Nebi Yasak
* Fabian Grünbichler
* Lance Lin

Grattis!
