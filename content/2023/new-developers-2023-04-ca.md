Title: Nous desenvolupadors i mantenidors de Debian (març i abril del 2023)
Slug: new-developers-2023-04
Date: 2023-05-25 12:00
Author: Jean-Pierre Giraud
Tags: project
Lang: ca
Status: published

Els següents col·laboradors del projecte han esdevingut Debian Developers en
els darrers dos mesos:

* James Lu (jlu)
* Hugh McMaster (hmc)
* Agathe Porte (gagath)

Els següents col·laboradors del projecte han esdevingut Debian Maintainers en
els darrers dos mesos:

* Soren Stoutner
* Matthijs Kooijman
* Vinay Keshava
* Jarrah Gosbell
* Carlos Henrique Lima Melara
* Cordell Bloor

Enhorabona a tots!
