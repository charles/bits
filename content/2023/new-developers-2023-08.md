Title: New Debian Developers and Maintainers (July and August 2023)
Slug: new-developers-2023-08
Date: 2023-09-27 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: en
Status: published

The following contributors got their Debian Developer accounts in the last two
months:

* Marius Gripsgard (mariogrip)
* Mohammed Bilal (rmb)
* Lukas Märdian (slyon)
* Robin Gustafsson (rgson)
* David da Silva Polverari (polverari)
* Emmanuel Arias (eamanu)

The following contributors were added as Debian Maintainers in the last two
months:

* Aymeric Agon-Rambosson
* Blair Noctis
* Lena Voytek
* Philippe Coval
* John Scott

Congratulations!
