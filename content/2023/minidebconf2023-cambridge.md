Title: Debian Events: MiniDebConfCambridge-2023
Slug: minidebconf2023-cambridge
Date: 2023-11-18 11:00
Author: The Debian Publicity Team
Tags: minidebconf, debianevents, sponsors, cambridge
Lang: en
Image: /images/Miniconf-logo-200x248.png
Status: published

![MiniConfLogo](|static|/images/Miniconf-logo-200x248.png)

Next week the #MiniDebConfCambridge takes place in Cambridge, UK. This event
will run from Thursday 23 to Sunday 26 November 2023.

The 4 days of the MiniDebConf include a
[Mini-DebCamp](https://wiki.debian.org/DebCamp) and of course the main
Conference talks, BoFs, meets, and Sprints.

We give thanks to our partners and sponsors for this event

[Arm](https://www.arm.com) - Building the Future of Computing

[Codethink](https://www.codethink.co.uk) - Open Source System Software Experts

[pexip](https://www.pexip.com) - Powering video everywhere

Please see the
[MiniDebConfCambridge](https://wiki.debian.org/DebianEvents/gb/2023/MiniDebConfCambridge)
page more for information regarding Travel documentation, Accomodation, Meal
planning, the full conference schedule, and yes, even parking.

We hope to see you there!
