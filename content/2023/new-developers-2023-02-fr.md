Title: Nouveaux développeurs et mainteneurs de Debian (janvier et février 2023)
Slug: new-developers-2023-02
Date: 2023-03-22 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: fr
Status: published

Les contributeurs suivants sont devenus développeurs Debian ces deux derniers
mois :

* Sahil Dhiman (sahil)
* Jakub Ružička (jru)

et ces contributeurs ont été acceptés comme mainteneurs Debian durant la même
période :

* Josenilson Ferreira da Silva
* Ileana Dumitrescu
* Douglas Kosovic
* Israel Galadima
* Timothy Pearson
* Blake Lee
* Vasyl Gello
* Joachim Zobel
* Amin Bandali

Félicitations !
