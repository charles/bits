Title: Nouveaux développeurs et mainteneurs de Debian (mars et avril 2019)
Slug: new-developers-2019-04
Date: 2019-05-11 16:35
Author: Jean-Pierre Giraud
Tags: project
Lang: fr
Status: published

Les contributeurs suivants sont devenus développeurs Debian ces deux derniers
mois :

* Jean-Baptiste Favre (jbfavre)
* Andrius Merkys (merkys)

et ces contributeurs ont été acceptés comme mainteneurs Debian durant la même
période :

* Christian Ehrhardt
* Aniol Marti
* Utkarsh Gupta
* Nicolas Schier
* Stewart Ferguson
* Hilmar Preusse

Félicitations !
