Title: Apresentando o novo DPL, pergunte-lhe qualquer coisa!
Date: 2019-04-26 19:20
Tags: dpl,debian-meeting
Slug: ask-dpl-anything
Author: Jonathan Carter
Lang: pt-BR
Translator: Daniel Pimentel, Rafael Fontenelle, Paulo Henrique de Lima Santana (phls)
Status: published

Temos um novo DPL! Em 21 de abril de 2019, Sam Hartman iniciou seu mandato
como o novo Líder do Projeto Debian (DPL).

Junte-se a nós no canal
[#debian-meeting](https://webchat.oftc.net/?channels=#debian-meeting) do
[OFTC](https://www.oftc.net/) na rede IRC em 10 de maio de 2019 às 10:00 (UTC)
para uma apresentação do novo DPL, e também para ter a chance de perguntar
qualquer coisa a ele.

Seu apelido no IRC deve ser registrado nesse canal. Consulte e
[registre sua conta](https://www.oftc.net/Services/) na seção do site oftc para
maiores informações sobre como registrar seu apelido.

Planejamos ter muito mais sessões no IRC futuramente.

Você pode sempre consultar a
[página wiki debian-meeting](https://wiki.debian.org/IRC/debian-meeting)
para as últimas informações e cronograma atualizado.
