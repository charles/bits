Title: New Debian Developers and Maintainers (November and December 2018)
Slug: new-developers-2018-12
Date: 2019-01-08 14:00
Author: Jean-Pierre Giraud
Tags: project
Lang: en
Status: published

The following contributors got their Debian Developer accounts in the last two
months:

* Abhijith PA (abhijith)
* Philippe Thierry (philou)
* Kai-Chung Yan (seamlik)
* Simon Quigley (tsimonq2)
* Daniele Tricoli (eriol)
* Molly de Blanc (mollydb)

The following contributors were added as Debian Maintainers in the last two
months:

* Nicolas Mora
* Wolfgang Silbermayr
* Marcos Fouces
* kpcyrd
* Scott Martin Leggett

Congratulations!
