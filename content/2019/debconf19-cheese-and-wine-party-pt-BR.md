Title: DebConf19 Cheese and Wine Party
Slug: debconf19-cheese-and-wine-party
Date: 2019-07-02 14:30
Author: Adriana Cássia da Costa
Tags: debconf19, debconf, cheese, wine, party
Lang: pt-BR
Translator: Adriana Cássia da Costa
Image: /images/800px-Debconf19-horizontal.png
Status: published

Em menos de um mês estaremos em Curitiba para começar a [DebCamp][debcamp]
e a [DebConf19][debconf19] \o/

Esta será a 15a edição do Queijos e Vinhos. O primeiro Queijos e Vinhos foi
improvisado em Helsinki durante a DebConf5 na "Sala Francesa". Agora o Queijos
e Vinhos é uma tradição na DebConf.

O evento é muito simples: traga algo comestível do seu país ou região. Nós
gostamos de queijos e vinhos, mas nós gostamos de coisas surpreendentes que
as pessoas trazem das regiões do mundo ou do Brasil. Então, você pode trazer
também bebidas não alcoólicas ou comidas típicas que você gostaria de
compartilhar. Se você não trouxer nada, sinta-se livre para participar: nossa
prioridade são os participantes e queijos de graça.

Nós precisamos organizar uma grande festa. Uma parte importante é o
planejamento - Nós queremos saber o que você vai trazer, para preparar os
adesivos de identificação e outras coisas.

Então, por favor acesse a nossa [página wiki][wikipage] e adicione o que você vai trazer

Se você não tiver tempo para comprar antes de viajar, nós listamos alguns
lugares onde você pode comprar queijos e vinhos em Curitiba.

Tem mais informações sobre o Queijos e Vinhos, o que você pode trazer,
queijo vegano, regulamentos alfandegários do Brasil e bebidas não
alcoólicas no [nosso site][cheese].

O Queijos e Vinhos será no dia 22 de julho (segunda-feira) após às 19h30min.

Estamos ansiosos(as) para ver todos(as) vocês aqui!

![DebConf19 logo](|static|/images/800px-Debconf19-horizontal.png)

[debcamp]: https://wiki.debian.org/DebConf/19/DebCamp
[debconf19]: https://debconf19.debconf.org
[wikipage]: https://wiki.debian.org/DebConf/19/CheeseWine#What_will_be_available_during_the_party.3F
[cheese]: https://debconf19.debconf.org/about/cheese-and-wine-party
