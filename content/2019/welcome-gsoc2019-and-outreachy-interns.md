Title: Debian welcomes its GSoC 2019 and Outreachy interns
Slug: welcome-gsoc2019-and-outreachy-interns
Date: 2019-05-31 14:15
Author: znoteer
Artist: Google, Outreachy
Tags: gsoc, google, outreachy, announce, development, diversity, software, code, projects, android, continuous integration
Image: /images/outreachy-logo-300x61.png
Status: published

![GSoC logo](|static|/images/gsoc.jpg)

![Outreachy logo](|static|/images/outreachy-logo-300x61.png)

We're excited to announce that Debian has selected seven interns to work with
us during the next months: two people for [Outreachy][1], and five for the
[Google Summer of Code][2].

[1]: https://www.outreachy.org/alums/
[2]: https://summerofcode.withgoogle.com/organizations/5566947593289728/

Here is the list of projects and the interns who will work on them:

[Android SDK Tools in Debian](https://wiki.debian.org/AndroidTools)

* [Saif Abdul Cassim](https://salsa.debian.org/m36-guest)
* Katerina

[Package Loomio for Debian](https://wiki.debian.org/SummerOfCode2019/ApprovedProjects/PackageLoomioForDebian)

* [utkarsh2102](https://nm.debian.org/person/utkarsh2102)

[Debian Cloud Image Finder](https://wiki.debian.org/SummerOfCode2019/ApprovedProjects/DebianCloudImageFinder)

* [Arthur Diniz](https://salsa.debian.org/arthurbdiniz-guest)

[Debian Patch Porting System](https://wiki.debian.org/SummerOfCode2019/ApprovedProjects/DebianPatchPorting)

* [Jaskaran Singh](https://salsa.debian.org/jaskaransingh-guest)

[Continuous Integration](https://wiki.debian.org/SummerOfCode2019/ApprovedProjects/DebianContinuousIntegration)

* [Saira Hussain](https://salsa.debian.org/sh0213-guest)
* [Candy Tsai](https://stringpiggy.hpd.io/)

Congratulations and welcome to all the interns!

The Google Summer of Code and Outreachy programs are possible in Debian thanks
to the efforts of Debian developers and contributors that dedicate
part of their free time to mentor interns and outreach tasks.

Join us and help extend Debian! You can follow the interns weekly reports on
the [debian-outreach mailing-list][debian-outreach-ml], chat with us on
[our IRC channel][debian-outreach-irc] or on each project's team mailing lists.

[debian-outreach-ml]: https://lists.debian.org/debian-outreach/ (debian-outreach AT lists.debian.org)
[debian-outreach-irc]: irc://irc.debian.org/#debian-outreach (#debian-outreach on irc.debian.org)
