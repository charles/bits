Title: Debian is welcoming applicants for Outreachy and GSoC 2019
Date: 2019-03-28 12:15
Tags: gsoc, google, outreachy, announce, development, diversity, software, code, projects
Slug: call-for-applicants-outreachy-gsoc-2019
Author: Laura Arjona Reina and Lesley Mitchell
Status: published

Debian is [dedicated](https://www.debian.org/intro/diversity) to increasing the
diversity of contributors to the project and improving the inclusivity of the
project.
We strongly believe working towards these goals provides benefits both for
people from backgrounds that are currently under-represented in free software,
and for the wider movement, by increasing the range of skills, experiences and
viewpoints contributing to it.

As part of this outreach effort, Debian is participating in the next round
of [Outreachy](https://wiki.debian.org/Outreachy/Round17).

**The application period for the May 2019 to August 2019 round has been extended
until April 2**, and Debian offers the following projects:

* [Continuous Integration for biological applications inside Debian](https://wiki.debian.org/SummerOfCode2019/ApprovedProjects/CIforDebianMed)
* [Debian Continuous Integration: user experience improvements](https://wiki.debian.org/SummerOfCode2019/ApprovedProjects/DebianContinuousIntegration)
* [Reproducible Builds](https://wiki.debian.org/SummerOfCode2019/ApprovedProjects/ReproducibleBuilds)

Outreachy invites applicants who are women (both cis and trans),
trans men, and genderqueer people to apply. Anyone who faces systemic bias or
discrimination in the technology industry of their country is also invited to
apply.

Don't wait up! You can learn more details on how to submit your application or
get help in our
[wiki page for Outreachy](https://wiki.debian.org/Outreachy/Round17)
and the [Outreachy website](https://outreachy.org).

Debian is also participating in the
[Google Summer of Code (GSoC)](https://wiki.debian.org/SummerOfCode2019)
with eight [projects](https://wiki.debian.org/SummerOfCode2019/Projects),
and the **student application period is open until April 9**.

You can learn more details on how to submit your GSoC application or
get help for in our
[wiki page for GSoC](https://wiki.debian.org/SummerOfCode2019)
and the
[Google Summer of Code website](https://summerofcode.withgoogle.com/).

We encourage people who are elegible for Outreachy and GSoC to submit their
application to both programs.
