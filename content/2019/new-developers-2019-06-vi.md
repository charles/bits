Title: Những nhà Phát triển và Bảo trì Debian mới (Tháng năm và sáu 2019)
Slug: new-developers-2019-06
Date: 2019-08-03 10:00
Author: Jean-Pierre Giraud
Tags: project
Lang: vi
Translator: Trần Ngọc Quân
Status: published

Trong hai tháng qua, những người đóng góp sau đây nhận được tài khoản Phát triển Debian:

* Jean-Philippe Mengual (jpmengual)
* Taowa Munene-Tardif (taowa)
* Georg Faerber (georg)
* Kyle Robbertze (paddatrapper)
* Andy Li (andyli)
* Michal Arbet (kevko)
* Sruthi Chandran (srud)
* Alban Vidal (zordhak)
* Denis Briand (denis)
* Jakob Haufe (sur5r)

Trong hai tháng qua, những người đóng góp sau đây được thêm vào với tư cách là người Bảo trì Debian:

* Bobby de Vos
* Jongmin Kim
* Bastian Germann
* Francesco Poli

Xin chúc mừng!
