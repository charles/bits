Title: Nouveaux développeurs et mainteneurs de Debian (novembre et décembre 2018)
Slug: new-developers-2018-12
Date: 2019-01-08 14:00
Author: Jean-Pierre Giraud
Tags: project
Lang: fr
Status: published

Les contributeurs suivants sont devenus développeurs Debian ces deux derniers
mois :

* Abhijith PA (abhijith)
* Philippe Thierry (philou)
* Kai-Chung Yan (seamlik)
* Simon Quigley (tsimonq2)
* Daniele Tricoli (eriol)
* Molly de Blanc (mollydb)

et ces contributeurs ont été acceptés comme mainteneurs Debian durant la même
période :

* Nicolas Mora
* Wolfgang Silbermayr
* Marcos Fouces
* kpcyrd
* Scott Martin Leggett

Félicitations !
