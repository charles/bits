Title: Upcoming Debian 10 "buster"!
Date: 2019-07-05 08:00
Tags: buster
Slug: upcoming-buster
Author: Laura Arjona Reina, Jean-Pierre Giraud and Thomas Vincent
Image: /images/upcoming-buster.png
Status: published

[![Alt Buster is coming on 2019-07-06](|static|/images/upcoming-buster.png)](https://deb.li/buster)

The Debian Release Team in coordination with several other teams are
[preparing](https://lists.debian.org/debian-devel-announce/2019/06/msg00003.html)
the last bits needed for releasing Debian 10 "buster" on Saturday 6 July 2019.
Please, be patient! Lots of steps are involved and some of them take some time,
such as building the images, propagating the release through the mirror
network, and rebuilding the Debian website so that "stable" points to
Debian 10.

If you are considering create some artwork on the occasion of buster release,
feel free to send us links to your creations to the (publicly archived)
[debian-publicity mailing list](mailto:debian-publicity@lists.debian.org), so
that we can disseminate them throughout our community.

Follow the live coverage of the release on
[https://micronews.debian.org](https://micronews.debian.org) or the
**@debian** profile in your favorite social network!
We'll spread the word about what's new in this version of Debian 10, how the
release process is progressing during the weekend and facts about Debian and
the wide community of volunteer contributors that make it possible.

If you want to celebrate the release of Debian 10 buster, join one of the
[many release parties](https://wiki.debian.org/ReleasePartyBuster) or consider
organizing one in your city!
Celebration will also happen online on the
[Debian Party Line](https://wiki.debian.org/ReleasePartyBuster#Debian_Party_Line).
