Title: Nya Debianutvecklare och Debian Maintainers (Juli och Augusti 2019)
Slug: new-developers-2019-08
Date: 2019-09-17 17:30
Author: Jean-Pierre Giraud
Tags: project
Lang: sv
Translator: Andreas Rönnquist
Status: published

Följande bidragslämnare fick sina Debianutvecklarkonton under de senaste två
månaderna:

* Keng-Yu Lin (kengyu)
* Judit Foglszinger (urbec)

Följande bidragslämnare adderades som Debian Maintainers under de senaste två
månaderna:

* Hans van Kranenburg
* Scarlett Moore

Grattis!
