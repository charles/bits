Title: Nuevos desarrolladores y mantenedores de Debian (noviembre y diciembre del 2018)
Slug: new-developers-2018-12
Date: 2019-01-08 14:00
Author: Jean-Pierre Giraud
Tags: project
Lang: es
Status: published

Los siguientes colaboradores del proyecto se convirtieron en Debian Developers
en los dos últimos meses:

* Abhijith PA (abhijith)
* Philippe Thierry (philou)
* Kai-Chung Yan (seamlik)
* Simon Quigley (tsimonq2)
* Daniele Tricoli (eriol)
* Molly de Blanc (mollydb)

Los siguientes colaboradores del proyecto se convirtieron en Debian Maintainers
en los dos últimos meses:

* Nicolas Mora
* Wolfgang Silbermayr
* Marcos Fouces
* kpcyrd
* Scott Martin Leggett

¡Felicidades a todos!
