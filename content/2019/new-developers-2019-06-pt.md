Title: Novos desenvolvedores e mantenedores Debian (maio e junho de 2019)
Slug: new-developers-2019-06
Date: 2019-08-03 10:00
Author: Jean-Pierre Giraud
Tags: project
Lang: pt
Status: published

Os seguintes colaboradores do projeto se tornaram Desenvolvedores Debian nos
últimos dois meses:

* Jean-Philippe Mengual (jpmengual)
* Taowa Munene-Tardif (taowa)
* Georg Faerber (georg)
* Kyle Robbertze (paddatrapper)
* Andy Li (andyli)
* Michal Arbet (kevko)
* Sruthi Chandran (srud)
* Alban Vidal (zordhak)
* Denis Briand (denis)
* Jakob Haufe (sur5r)

Os seguintes colaboradores do projeto se tornaram Mantenedores Debian nos
últimos dois meses:

* Bobby de Vos
* Jongmin Kim
* Bastian Germann
* Francesco Poli

Parabéns a todos!
