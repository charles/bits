Title: Nous desenvolupadors i mantenidors de Debian (gener i febrer del 2019)
Slug: new-developers-2019-02
Date: 2019-03-12 13:00
Author: Jean-Pierre Giraud
Tags: project
Lang: ca
Status: published

Els següents col·laboradors del projecte han esdevingut Debian Developers en
els darrers dos mesos:

* Paulo Henrique de Lima Santana (phls)
* Unit 193 (unit193)
* Marcio de Souza Oliveira (marciosouza)
* Ross Vandegrift (rvandegrift)

Els següents col·laboradors del projecte han esdevingut Debian Maintainers
en els darrers dos mesos:

* Romain Perier
* Felix Yan

Enhorabona a tots!
