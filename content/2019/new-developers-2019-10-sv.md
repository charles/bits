Title: Nya Debianutvecklare och Debian Maintainers (September och Oktober 2019)
Slug: new-developers-2019-10
Date: 2019-11-26 15:00
Author: Jean-Pierre Giraud
Tags: project
Lang: sv
Translator: Andreas Rönnquist
Status: published

Följande bidragslämnare fick sina Debianutvecklarkonton under de senaste två
månaderna:

* Teus Benschop (teusbenschop)
* Nick Morrott (nickm)
* Ondřej Kobližek (kobla)
* Clément Hermann (nodens)
* Gordon Ball (chronitis)

Följande bidragslämnare adderades som Debian Maintainers under de senaste två
månaderna:

* Nikos Tsipinakis
* Joan Lledó
* Baptiste Beauplat
* Jianfeng Li

Grattis!
