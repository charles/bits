Title: Debian Web Team Sprint 2019
Slug: debian-web-team-sprint-2019
Date: 2019-04-16 09:20
Author: Laura Arjona Reina
Artist: Laura Arjona Reina
Tags: web, sprint
Image: /images/web_team_sprint_2019.jpg
Status: published

The [Debian Web team](https://wiki.debian.org/Teams/Webmaster) held a
[sprint](https://wiki.debian.org/Sprints/2019/DebianWebTeam) for the
first time, in Madrid (Spain) from March 15th to March 17th, 2019.

We discussed the status of the Debian website in general, review several
important pages/sections and agreed on many things how to improve them.

For the sections we want to reorganise (mainly the homepage and a new
section "download" which will group our current "CD" and "distrib"
sections), we have designed this workflow:

* Create branches in the webwml repo,
* Agreed on the new or modified content (started already during the
  sprint), and work on them further after the sprint
* Review a lot of related open bugs to see if we can address them with
  the new content (done during the sprint)
* Create bug reports for the things that cannot be solved quickly to
  keep them tracked (started during the sprint)
* We agreed we should get further help from web designers/information
  architects (pending)
* Once the English version is more or less settled, call for
  translations on the branch (pending)
* If we have English and the main translations ready, merging into the
  master branch (pending)
* We will try to have at least the homepage and the download section
  ready for the Buster release.

We also agreed that the press delegates should decide what new News
entry is worth to be posted in the homepage instead of showing the last
6 entries.

For some other pages or areas (e.g. doc/books, misc/merchandise, /users)
we found that the content is outdated and the team can not maintain it,
we agreed in issuing a call for help (request for adoption) and if we
cannot find volunteers for those pages/areas, we'll remove the content
or move it to wiki.debian.org at the end of 2019.

We have agreed that we'll need to reduce the size (number of pages) of
the website (*see some numbers about statistics at bottom) so it's more
sustainable to keep the whole website up-to-date (content wise), so
we'll remove some pages having content already covered in other pages,
having content that currently is easy discoverable with a web search
engine, can be maintained better in the wiki, etc.

We have talked a bit about certain other aspects  like point release
workflow, the build time of the website, team memberships and
governance. In general the sprint has shown that for most of the
discussed topics the migration to git as VCS and the existence of Salsa
is a huge step forward for the usability and attractiveness for
contributors of the webwml repository.

The core webteam is happy that the sprint has also attracted new people
to jump in and which are also members of the webteam now. We welcome
Thomas Lange and Carsten Schoenert in our team!

Finally, we have passed time together to socialize and knowing each
other better, and got very motivated to continue working on the web.

![Group photo of the participants in the Web Team Sprint](|static|/images/web_team_sprint_2019.jpg)
Left to right: Rhonda D'Vine, Laura Arjona Reina, Thomas Lange, Carsten
Schoenert, Steve McIntyre

A more
[detailed report](https://lists.debian.org/debian-www/2019/04/msg00091.html)
has been sent to the debian-www mailing list.

The participants would like to thank all donors to the Debian project who
helped to cover a large part of our expenses.
