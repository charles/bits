Title: New Debian Developers and Maintainers (July and August 2019)
Slug: new-developers-2019-08
Date: 2019-09-17 17:30
Author: Jean-Pierre Giraud
Tags: project
Lang: en
Status: published

The following contributors got their Debian Developer accounts in the last two months:

* Keng-Yu Lin (kengyu)
* Judit Foglszinger (urbec)

The following contributors were added as Debian Maintainers in the last two months:

* Hans van Kranenburg
* Scarlett Moore

Congratulations!
