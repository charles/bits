Title: Nuevos desarrolladores y mantenedores de Debian (septiembre y octubre del 2019)
Slug: new-developers-2019-10
Date: 2019-11-26 15:00
Author: Jean-Pierre Giraud
Tags: project
Lang: es
Status: published

Los siguientes colaboradores del proyecto se convirtieron en Debian Developers
en los dos últimos meses:

* Teus Benschop (teusbenschop)
* Nick Morrott (nickm)
* Ondřej Kobližek (kobla)
* Clément Hermann (nodens)
* Gordon Ball (chronitis)

Los siguientes colaboradores del proyecto se convirtieron en Debian Maintainers
en los dos últimos meses:

* Nikos Tsipinakis
* Joan Lledó
* Baptiste Beauplat
* Jianfeng Li

¡Felicidades a todos!
