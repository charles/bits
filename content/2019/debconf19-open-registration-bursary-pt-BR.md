Title: DebConf19 inscrições abertas!
Date: 2019-03-20 20:30
Tags: debconf, debconf19
Slug: debconf19-open-registration-bursary
Translator: Sergio Durigan Junior, Paulo Santana
Artist: DebConf19 Design Team
Lang: pt-BR
Image: /images/800px-Debconf19-horizontal-open-registration.png
Status: published

![DebConf19 banner open registration](|static|/images/800px-Debconf19-horizontal-open-registration.png)

Estão abertas as inscrições para a
[DebConf19](https://debconf19.debconf.org) **que acontecerá de 21 a 28 de julho
de 2019 no Campus central da Universidade Tecnológica Federal do Paraná -
UTFPR, em Curitiba - Brasil**. O evento será precedido pela DebCamp, de 14 a
19 de julho, e pelo [Open Day](https://debconf19.debconf.org/schedule/openday/)
no dia 20 de julho.

A [DebConf](https://www.debconf.org) é um evento aberto a todos(as), não
importando como você se identifica ou como os(a) outros(as) o(a) percebem.
Queremos aumentar a visibilidade da nossa diversidade e trabalhar para a
inclusão no Projeto Debian, incentivando a participação desde pessoas que
estão iniciando a sua jornada no Debian até desenvolvedores(as) experientes ou
contribuidores(as) ativos(as) do Debian, em diferentes áreas como
empacotamento, tradução, documentação, design, testes, derivados
especializados, suporte e muitos outras. Em outras palavras, todos(as) são
bem-vindos(as)!

Para se inscrever no evento, você deverá fazer login [no sistema de
registro](https://debconf19.debconf.org/register/)
e preencher o formulário.
Você poderá editar e atualizar as suas informações a qualquer momento. No
entanto, para ajudar os organizadores a ter uma estimativa melhor de quantas
pessoas realmente virão para evento, nós agradecemos se você puder acessar o
sistema e confirmar (ou cancelar) a sua participação na Conferência assim que
souber se será mesmo possível vir. **O último dia para confirmar ou cancelar a
sua inscrição é 14 de junho de 2019 às 20:59:59 (horário de Brasília)**. Se
você não confirmar ou se inscrever após essa data, você pode vir para
a DebConf19, mas nós não poderemos garantir a disponibilidade de hospedagem,
de alimentação e do kit de participante (camiseta, bolsa, etc.).

Para mais informações sobre a inscrição, acesse:
[Informações sobre a inscrição](https://debconf19.debconf.org/pt-br/inscricao/)

## Bolsa para passagens, hospedagem e alimentação

Com o objetivo de promover a participação de mais contribuidores(as) na
DebConf, o Projeto Debian destina uma parte dos recursos financeiros obtidos
com os patrocínios do evento para pagar bolsas (passagens, hospedagem e/ou
alimentação) para os(as) participantes que solicitarem esse apoio/patrocínio
no momento da sua inscrição.

Como os recursos financeiros são limitados, nós avaliaremos os pedidos e
decidiremos quem receberão as bolsas. As bolsas são destinadas:

* Para contribuidores(as) ativos(as) do Debian.
* Para promoção de diversidade: novos(as) participantes no Debian e/ou na
  DebConf, especialmente de grupos sub-representados na área de tecnologia.

Palestrar, organizar uma atividade ou ajudar durante a DebConf19 conta pontos
para ganhar a bolsa, então por favor mencione essas iniciativas no seu pedido.
Os planos para a DebCamp podem ser inseridos no link habitual:
[Página dos Sprints no wiki do Debian](https://wiki.debian.org/Sprints).

Para mais informações sobre as bolsas, acesse:
[Candidatando-se para uma Bolsa para da DebConf](https://debconf19.debconf.org/pt-br/bolsas/)

**Atenção:** as inscrições para a DebConf19 ficarão abertas até a data do
da Conferência, mas a **solicitação das bolsas deverá ser realizada pelas
pessoas interessadas usando o formulário de inscrição até o dia 15 de abril de
2019 às 20:59:59 (horário de Brasília).**
Esse prazo é necessário para que os organizadores tenham tempo
suficiente para analisar os pedidos, e para que os aplicantes
selecionados possam se preparar para a Conferência.

Para realizar a sua inscrição, com ou sem pedido de bolsa, acesse:
[https://debconf19.debconf.org/register](https://debconf19.debconf.org/register)

A DebConf não seria possível sem o generoso suporte de todos os
patrocinadores, especialmente dos nossos Patrocinadores
Platinum [**Infomaniak**](https://www.infomaniak.com)
e [**Google**](https://www.google.com).  A DebConf19 ainda está
aceitando patrocinadores; se você está interessado, ou acha que
conhece outras pessoas que poderiam
ajudar,
[por favor, entre em contato](https://debconf19.debconf.org/sponsors/become-a-sponsor/)!
