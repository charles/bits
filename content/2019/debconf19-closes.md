Title: DebConf19 closes in Curitiba and DebConf20 dates announced
Date: 2019-07-27 23:40
Tags: debconf19, announce, debconf20, debconf
Slug: debconf19-closes
Author: Laura Arjona Reina and Donald Norwood
Artist: Aigars Mahinovs
Image: /images/debconf19_group_small.jpg
Status: published

[![DebConf19 group photo - click to enlarge](|static|/images/debconf19_group_small.jpg)](https://wiki.debian.org/DebConf/19/Photos)

Today, Saturday 27 July 2019, the annual Debian Developers and Contributors
Conference came to a close. Hosting more than 380 attendees from 50 different
countries over a combined 145 event talks, discussion sessions,
Birds of a Feather (BoF) gatherings, workshops, and activities,
[DebConf19](https://debconf19.debconf.org) was a large success.

The conference was preceded by the annual DebCamp held 14 July to 19 July
which focused on individual work and team sprints for in-person collaboration
toward developing Debian and host to a 3-day packaging workshop
where new contributors were able to start on Debian packaging.

The [Open Day](https://debconf19.debconf.org/news/2019-07-20-open-day/)
held on July 20, with over 250 attendees, enjoyed presentations
and workshops of interest to the wider audience, a Job Fair with booths from
several of the DebConf19 sponsors and a Debian install fest.

The actual Debian Developers Conference started on Sunday 21 July 2019.
Together with plenaries such as the the traditional 'Bits from the DPL',
lightning talks, live demos and the announcement of next year's DebConf
([DebConf20](https://wiki.debian.org/DebConf/20) in Haifa, Israel),
there were several sessions related to the recent release of Debian 10 buster
and some of its new features, as well as news updates on several projects and
internal Debian teams, discussion sessions (BoFs) from the language, ports,
infrastructure, and  community teams, along with many other events of interest
regarding Debian and free software.

The [schedule](https://debconf19.debconf.org/schedule/)
was updated each day with planned and ad-hoc activities introduced by attendees
over the course of the entire conference.

For those who were not able to attend, most of the talks and sessions were
recorded for live streams with videos made, available through the
[Debian meetings archive website](https://meetings-archive.debian.net/pub/debian-meetings/2019/DebConf19/).
Almost all of the sessions facilitated remote participation via IRC messaging
apps or online collaborative text documents.

The [DebConf19](https://debconf19.debconf.org/) website
will remain active for archival purposes and will continue to offer
links to the presentations and videos of talks and events.

Next year,
[DebConf20](https://wiki.debian.org/DebConf/20) will be held in Haifa,
Israel, from 23 August to 29 August 2020.
As tradition follows before the next DebConf the local organizers in Israel
will start the conference activites with DebCamp (16 August to 22 August),
with particular focus on individual and team work toward improving the
distribution.

DebConf is committed to a safe and welcome environment for all participants.
During the conference, several teams (Front Desk, Welcome team and
Anti-Harassment team) are available to help so both on-site and remote
participants get their best experience in the conference, and find solutions
to any issue that may arise. See the
[web page about the Code of Conduct in DebConf19 website](https://debconf19.debconf.org/about/coc/)
for more details on this.

Debian thanks the commitment of numerous
[sponsors](https://debconf19.debconf.org/sponsors/) to support DebConf19,
particularly our Platinum Sponsors:
[**Infomaniak**](https://www.infomaniak.com),
[**Google**](https://google.com/)
and [**Lenovo**](https://www.lenovo.com).

### About Debian

The Debian Project was founded in 1993 by Ian Murdock to be a truly
free community project. Since then the project has grown to be one of
the largest and most influential open source projects.  Thousands of
volunteers from all over the world work together to create and
maintain Debian software. Available in 70 languages, and
supporting a huge range of computer types, Debian calls itself the
_universal operating system_.

### About DebConf

DebConf is the Debian Project's developer conference. In addition to a
full schedule of technical, social and policy talks, DebConf provides an
opportunity for developers, contributors and other interested people to
meet in person and work together more closely. It has taken place
annually since 2000 in locations as varied as Scotland, Argentina, and
Bosnia and Herzegovina. More information about DebConf is available from
[https://debconf.org/](https://debconf.org).

### About Infomaniak

[**Infomaniak**](https://www.infomaniak.com) is Switzerland's largest
web-hosting company, also offering backup and storage services, solutions for
event organizers, live-streaming and video on demand services.
It wholly owns its datacenters and all elements critical
to the functioning of the services and products provided by the company
(both software and hardware).

### About Google

[**Google**](https://google.com/) is one of the largest technology companies
in the world, providing a wide range of Internet-related services and products
such as online advertising technologies, search, cloud computing, software,
and hardware.

Google has been supporting Debian by sponsoring DebConf for more than
ten years, and is also a Debian partner sponsoring parts
of [Salsa](https://salsa.debian.org)'s continuous integration infrastructure
within Google Cloud Platform.

### About Lenovo

As a global technology leader manufacturing a wide portfolio of connected
products, including smartphones, tablets, PCs and workstations as well as
AR/VR devices, smart home/office and data center solutions,
[**Lenovo**](https://www.lenovo.com) understands how critical open systems
and platforms are to a connected world.

### Contact Information

For further information, please visit the DebConf19 web page at
[https://debconf19.debconf.org/](https://debconf19.debconf.org/)
or send mail to <press@debian.org>.
