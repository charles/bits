Title: Novos desenvolvedores e mantenedores Debian (novembro e dezembro de 2018)
Slug: new-developers-2018-12
Date: 2019-01-08 14:00
Author: Jean-Pierre Giraud
Tags: project
Lang: pt
Status: published

Os seguintes colaboradores do projeto se tornaram Desenvolvedores Debian nos
últimos dois meses:

* Abhijith PA (abhijith)
* Philippe Thierry (philou)
* Kai-Chung Yan (seamlik)
* Simon Quigley (tsimonq2)
* Daniele Tricoli (eriol)
* Molly de Blanc (mollydb)

Os seguintes colaboradores do projeto se tornaram Mantenedores Debian nos
últimos dois meses:

* Nicolas Mora
* Wolfgang Silbermayr
* Marcos Fouces
* kpcyrd
* Scott Martin Leggett

Parabéns a todos!
