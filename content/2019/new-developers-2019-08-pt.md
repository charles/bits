Title: Novos desenvolvedores e mantenedores Debian (julho e agosto de 2019)
Slug: new-developers-2019-08
Date: 2019-09-17 17:30
Author: Jean-Pierre Giraud
Tags: project
Lang: pt
Status: published

Os seguintes colaboradores do projeto se tornaram Desenvolvedores Debian nos
últimos dois meses:

* Keng-Yu Lin (kengyu)
* Judit Foglszinger (urbec)

Os seguintes colaboradores do projeto se tornaram Mantenedores Debian nos
últimos dois meses:

* Hans van Kranenburg
* Scarlett Moore

Parabéns a todos!
