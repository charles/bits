Title: 新的 Debian 开发者和维护者 (2019年1月 至 2月)
Slug: new-developers-2019-02
Date: 2019-03-12 13:00
Author: Jean-Pierre Giraud
Tags: project
Lang: zh-CN
Translator: Boyuan Yang
Status: published

下列贡献者在过去的两个月中获得了他们的 Debian 开发者帐号：

* Paulo Henrique de Lima Santana (phls)
* Unit 193 (unit193)
* Marcio de Souza Oliveira (marciosouza)
* Ross Vandegrift (rvandegrift)

下列贡献者在过去的两个月中被添加为 Debian 维护者：

* Romain Perier
* Felix Yan

祝贺他们！
