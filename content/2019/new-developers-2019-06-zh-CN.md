Title: 新的 Debian 开发者和维护者 (2019年5月 至 6月)
Slug: new-developers-2019-06
Date: 2019-08-03 10:00
Author: Jean-Pierre Giraud
Tags: project
Lang: zh-CN
Translator: Boyuan Yang
Status: published

下列贡献者在过去的两个月中获得了他们的 Debian 开发者帐号：

* Jean-Philippe Mengual (jpmengual)
* Taowa Munene-Tardif (taowa)
* Georg Faerber (georg)
* Kyle Robbertze (paddatrapper)
* Andy Li (andyli)
* Michal Arbet (kevko)
* Sruthi Chandran (srud)
* Alban Vidal (zordhak)
* Denis Briand (denis)
* Jakob Haufe (sur5r)

下列贡献者在过去的两个月中被添加为 Debian 维护者：

* Bobby de Vos
* Jongmin Kim
* Bastian Germann
* Francesco Poli

祝贺他们！
