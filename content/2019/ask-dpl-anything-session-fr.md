Title: Présentation du nouveau chef du projet Debian, posez lui toutes vos questions !
Date: 2019-04-26 19:20
Tags: dpl,debian-meeting
Slug: ask-dpl-anything
Author: Jonathan Carter
Lang: fr
Translator: Jean-Pierre Giraud
Status: published

Nous avons un nouveau chef du projet Debian ! Le 21 avril 2019, Sam Hartman a
débuté son mandat comme chef du projet Debian.

Rejoignez-nous sur le canal IRC
[#debian-meeting](https://webchat.oftc.net/?channels=#debian-meeting)
sur le réseau IRC [OFTC](https://www.oftc.net/) le 10 mai prochain
à 10 h 00 UTC pour une présentation du nouveau chef du projet et aussi pour
avoir l'occasion de lui poser toutes vos questions.

Votre pseudonyme IRC doit être enregistré pour pouvoir rejoindre le canal.
Consultez le paragraphe [Register your account](https://www.oftc.net/Services/)
du site web d'OFTC pour avoir plus d'information sur la manière d'enregistrer
votre pseudonyme.

Nous projetons d'avoir à l'avenir beaucoup d'autres sessions IRC à l'échelle du
projet.

Vous pouvez toujours consulter
la [page wiki de debian-meeting](https://wiki.debian.org/IRC/debian-meeting)
pour les dernières informations et un programme actualisé.
