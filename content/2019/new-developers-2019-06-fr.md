Title: Nouveaux développeurs et mainteneurs de Debian (mai et juin 2019)
Slug: new-developers-2019-06
Date: 2019-08-03 10:00
Author: Jean-Pierre Giraud
Tags: project
Lang: fr
Status: published

Les contributeurs suivants sont devenus développeurs Debian ces deux derniers
mois :

* Jean-Philippe Mengual (jpmengual)
* Taowa Munene-Tardif (taowa)
* Georg Faerber (georg)
* Kyle Robbertze (paddatrapper)
* Andy Li (andyli)
* Michal Arbet (kevko)
* Sruthi Chandran (srud)
* Alban Vidal (zordhak)
* Denis Briand (denis)
* Jakob Haufe (sur5r)

et ces contributeurs ont été acceptés comme mainteneurs Debian durant la même
période :

* Bobby de Vos
* Jongmin Kim
* Bastian Germann
* Francesco Poli

Félicitations !
