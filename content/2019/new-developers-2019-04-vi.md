Title: Những nhà Phát triển và Bảo trì Debian mới (Tháng ba và tư 2019)
Slug: new-developers-2019-04
Date: 2019-05-11 16:35
Author: Jean-Pierre Giraud
Tags: project
Lang: vi
Translator: Trần Ngọc Quân
Status: published

Trong hai tháng qua, những người đóng góp sau đây nhận được tài khoản Phát triển Debian:

* Jean-Baptiste Favre (jbfavre)
* Andrius Merkys (merkys)

Trong hai tháng qua, những người đóng góp sau đây được thêm vào với tư cách là người Bảo trì Debian:

* Christian Ehrhardt
* Aniol Marti
* Utkarsh Gupta
* Nicolas Schier
* Stewart Ferguson
* Hilmar Preusse

Xin chúc mừng!
