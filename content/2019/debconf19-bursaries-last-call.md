Title: Bursary applications for DebConf19 are closing in less than 72 hours!
Slug: debconf19-bursaries-last-call
Date: 2019-04-13 10:15
Author: Laura Arjona Reina, Paulo Santana
Tags: debconf19, debconf
Image: /images/800px-Debconf19-horizontal-open-registration.png
Status: published

If you intend to apply for a [DebConf19](https://debconf19.debconf.org/)
bursary and have not yet done so, please proceed as soon as possible!

Bursary applications for DebConf19 will be accepted until April 15th at 23:59 UTC.
Applications submitted after this deadline will not be considered.

You can apply for a bursary when you
[register](https://debconf19.debconf.org/register) for the conference.

Remember that giving a talk or organising an event is considered towards your
bursary; if you have a submission to make, submit it even if it is only
sketched-out. You will be able to detail it later. DebCamp plans can be
entered in the usual
[Sprints page at the Debian wiki](https://wiki.debian.org/Sprints).

**Please make sure to double-check your accommodation choices (dates and venue)**.
Details about accommodation arrangements can be found on the
[accommodation page](https://debconf19.debconf.org/about/accommodation/).

See you in [Curitiba](https://debconf19.debconf.org/about/curitiba/)!

![DebConf19 banner open registration](|static|/images/800px-Debconf19-horizontal-open-registration.png)
