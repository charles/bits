Title: Novos desenvolvedores e mantenedores Debian (março e abril de 2019)
Slug: new-developers-2019-04
Date: 2019-05-11 16:35
Author: Jean-Pierre Giraud
Tags: project
Lang: pt
Status: published

Os seguintes colaboradores do projeto se tornaram Desenvolvedores Debian nos
últimos dois meses:

* Jean-Baptiste Favre (jbfavre)
* Andrius Merkys (merkys)

Os seguintes colaboradores do projeto se tornaram Mantenedores Debian nos
últimos dois meses:

* Christian Ehrhardt
* Aniol Marti
* Utkarsh Gupta
* Nicolas Schier
* Stewart Ferguson
* Hilmar Preusse

Parabéns a todos!
