Title: Những nhà Phát triển và Bảo trì Debian mới (Tháng chín và mười 2019)
Slug: new-developers-2019-10
Date: 2019-11-26 15:00
Author: Jean-Pierre Giraud
Tags: project
Lang: vi
Translator: Trần Ngọc Quân
Status: published

Trong hai tháng qua, những người đóng góp sau đây nhận được tài khoản Phát triển Debian:

* Teus Benschop (teusbenschop)
* Nick Morrott (nickm)
* Ondřej Kobližek (kobla)
* Clément Hermann (nodens)
* Gordon Ball (chronitis)

Trong hai tháng qua, những người đóng góp sau đây được thêm vào với tư cách là người Bảo trì Debian:

* Nikos Tsipinakis
* Joan Lledó
* Baptiste Beauplat
* Jianfeng Li

Xin chúc mừng!
