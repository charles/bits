Title: Diversity and inclusion in Debian: small actions and large impacts
Slug: diversity-and-inclusion
Date: 2019-06-29 00:40
Author: Laura Arjona Reina and Rhonda D'Vine
Artist: Valessio Brito
Tags: diversity
Image: /images/diversity-2019.png
Status: published

The Debian Project always has and always will welcome contributions from people
who are willing to work on a constructive level with each other,
without discrimination.

The [Diversity Statement](https://www.debian.org/intro/diversity)
and the [Code of Conduct](https://www.debian.org/code_of_conduct) are
genuinely important parts of our community, and over recent years some other
things have been done to make it clear that they aren't just words.

One of those things is the creation of the Debian Diversity Team: it was
[announced in April 2019](https://lists.debian.org/debian-devel-announce/2019/04/msg00005.html),
although it had already been working for several months before as a welcoming
space for, and a way of increasing visibility of, underrepresented groups
within the Debian project.

During DebConf19 in Curitiba there will be a dedicated Diversity and Welcoming
Team. It will consist of people from the Debian community to offer a contact
point when you feel lost or uneasy. The DebConf team is also in contact
with a local LGBTIQA+ support group for exchange of safety concerns and
information with respect to Brazil in general.

Today Debian also recognizes the impact LGBTIQA+ people have had in the world
and within the Debian project, joining the worldwide Pride celebrations. We
show it by changing our logo for this time to the Debian Diversity logo,
and encourage all Debian members and contributors to show their support of a
diverse and inclusive community.

![Debian Diversity logo](|static|/images/diversity-2019.png)
