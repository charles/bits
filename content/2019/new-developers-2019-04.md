Title: New Debian Developers and Maintainers (March and April 2019)
Slug: new-developers-2019-04
Date: 2019-05-11 16:35
Author: Jean-Pierre Giraud
Tags: project
Lang: en
Status: published

The following contributors got their Debian Developer accounts in the last two
months:

* Jean-Baptiste Favre (jbfavre)
* Andrius Merkys (merkys)

The following contributors were added as Debian Maintainers in the last two
months:

* Christian Ehrhardt
* Aniol Marti
* Utkarsh Gupta
* Nicolas Schier
* Stewart Ferguson
* Hilmar Preusse

Congratulations!
