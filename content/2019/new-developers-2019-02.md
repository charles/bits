Title: New Debian Developers and Maintainers (January and February 2019)
Slug: new-developers-2019-02
Date: 2019-03-12 13:00
Author: Jean-Pierre Giraud
Tags: project
Lang: en
Status: published

The following contributors got their Debian Developer accounts in the last two
months:

* Paulo Henrique de Lima Santana (phls)
* Unit 193 (unit193)
* Marcio de Souza Oliveira (marciosouza)
* Ross Vandegrift (rvandegrift)

The following contributors were added as Debian Maintainers in the last two
months:

* Romain Perier
* Felix Yan

Congratulations!
