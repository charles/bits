Title: Nous desenvolupadors i mantenidors de Debian (maig i juny del 2019)
Slug: new-developers-2019-06
Date: 2019-08-03 10:00
Author: Jean-Pierre Giraud
Tags: project
Lang: ca
Status: published

Els següents col·laboradors del projecte han esdevingut Debian Developers en
els darrers dos mesos:

* Jean-Philippe Mengual (jpmengual)
* Taowa Munene-Tardif (taowa)
* Georg Faerber (georg)
* Kyle Robbertze (paddatrapper)
* Andy Li (andyli)
* Michal Arbet (kevko)
* Sruthi Chandran (srud)
* Alban Vidal (zordhak)
* Denis Briand (denis)
* Jakob Haufe (sur5r)

Els següents col·laboradors del projecte han esdevingut Debian Maintainers en
els darrers dos mesos:

* Bobby de Vos
* Jongmin Kim
* Bastian Germann
* Francesco Poli

Enhorabona a tots!
