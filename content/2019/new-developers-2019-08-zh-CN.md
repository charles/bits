Title: 新的 Debian 开发者和维护者 (2019年7月 至 8月)
Slug: new-developers-2019-08
Date: 2019-09-17 17:30
Author: Jean-Pierre Giraud
Tags: project
Lang: zh-CN
Translator: Boyuan Yang
Status: published

下列贡献者在过去的两个月中获得了他们的 Debian 开发者帐号：

* Keng-Yu Lin (kengyu)
* Judit Foglszinger (urbec)

下列贡献者在过去的两个月中被添加为 Debian 维护者：

* Hans van Kranenburg
* Scarlett Moore

祝贺他们！
