Title: Nya Debianutvecklare och Debian Maintainers (Januari och Februari 2019)
Slug: new-developers-2019-02
Date: 2019-03-12 13:00
Author: Jean-Pierre Giraud
Tags: project
Lang: sv
Translator: Andreas Rönnquist
Status: published

Följande bidragslämnare fick sina Debianutvecklarkonton under de senaste två
månaderna:

* Paulo Henrique de Lima Santana (phls)
* Unit 193 (unit193)
* Marcio de Souza Oliveira (marciosouza)
* Ross Vandegrift (rvandegrift)

Följande bidragslämnare adderades som Debian Maintainers under de senaste två
månaderna:

* Romain Perier
* Felix Yan

Grattis!
