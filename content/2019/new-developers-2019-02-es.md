Title: Nuevos desarrolladores y mantenedores de Debian (enero y febrero del 2019)
Slug: new-developers-2019-02
Date: 2019-03-12 13:00
Author: Jean-Pierre Giraud
Tags: project
Lang: es
Status: published

Los siguientes colaboradores del proyecto se convirtieron en Debian Developers
en los dos últimos meses:

* Paulo Henrique de Lima Santana (phls)
* Unit 193 (unit193)
* Marcio de Souza Oliveira (marciosouza)
* Ross Vandegrift (rvandegrift)

Los siguientes colaboradores del proyecto se convirtieron en Debian Maintainers
en los dos últimos meses:

* Romain Perier
* Felix Yan

¡Felicidades a todos!
