Title: DebConf19 invites you to Debian Open Day at the Federal University of Technology - Paraná (UTFPR), in Curitiba
Slug: debconf19-open-day
Date: 2019-07-20 18:30
Author: Laura Arjona Reina
Tags: debconf, debconf19, debian
Image: /images/800px-Debconf19-horizontal.png
Status: published

![DebConf19 logo](|static|/images/800px-Debconf19-horizontal.png)

[DebConf](https://debconf19.debconf.org), the annual conference for Debian
contributors and users interested in improving
the [Debian operating system](https://www.debian.org), will be held in
Federal University of Technology - Paraná (UTFPR) in Curitiba, Brazil,
from July 21 to 28, 2019.
The conference is preceded by DebCamp from July 14 to 19,
and the DebConf19 Open Day on July 20.

The Open Day, Saturday, 20 July, is targeted at the general public.
Events of interest to a wider audience will be offered, ranging from topics
specific to Debian to the greater Free Software community and maker movement.

The event is a perfect opportunity for interested users to meet the Debian
community, for Debian to broaden its community, and for the DebConf sponsors
to increase their visibility.

Less purely technical than the main conference schedule,
the events on Open Day will cover a large range of topics from social
and cultural issues to workshops and introductions to Debian.

The [detailed schedule of the Open Day's events](https://debconf19.debconf.org/schedule/?day=2019-07-20)
includes events in English and Portuguese. Some of the talks are:

* "The metaverse, gaming and the metabolism of cities" by Bernelle Verster
* "O Projeto Debian quer você!" by Paulo Henrique de Lima Santana
* "Protecting Your Web Privacy with Free Software" by Pedro Barcha
* "Bastidores Debian - Entenda como a distribuição funciona" by Joao Eriberto
  Mota Filho
* "Caninos Loucos: a plataforma nacional de Single Board Computers para IoT"
  by geonnave
* "Debian na vida de uma Operadora de Telecom" by Marcelo Gondim
* "Who's afraid of Spectre and Meltdown?" by Alexandre Oliva
* "New to DebConf BoF" by Rhonda D'Vine

During the Open Day, there will also be a Job Fair with booths from our
several of our sponsors, a workshop about the Git version control system and a
Debian installfest, for attendees who would like to get help installing Debian
on their machines.

Everyone is welcome to attend. As the rest of the conference, attendance is
free of charge, but registration in the
[DebConf19 website](https://debconf19.debconf.org/) is highly recommended.

The full schedule for the Open Day's events and the rest of the conference
is at
[https://debconf19.debconf.org/schedule](https://debconf19.debconf.org/schedule)
and the video streaming will be available at the
[DebConf19 website](https://debconf19.debconf.org)

DebConf is committed to a safe and welcome environment for all participants.
See the [DebConf Code of Conduct](https://debconf.org/codeofconduct.shtml)
and the [Debian Code of Conduct](https://www.debian.org/code_of_conduct)
for more details on this.

Debian thanks the numerous [sponsors](https://debconf19.debconf.org/sponsors/)
for their commitment to DebConf19, particularly its Platinum Sponsors:
[Infomaniak](https://www.infomaniak.com),
[Google](https://google.com/)
and [Lenovo](https://www.lenovo.com).
