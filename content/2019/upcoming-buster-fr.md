Title: Debian 10 Buster arrive !
Date: 2019-07-05 08:00
Tags: buster
Slug: upcoming-buster
Author: Laura Arjona Reina, Jean-Pierre Giraud et Thomas Vincent
Lang: fr
Translator: Jean-Pierre Giraud
Image: /images/upcoming-buster.png
Status: published

![Alt Buster arrive le 6 juillet 2019](|static|/images/upcoming-buster.png)

L'équipe de publication Debian, conjointement avec d'autres équipes,
[prépare](https://lists.debian.org/debian-devel-announce/2019/06/msg00003.html)
les derniers éléments nécessaires à la publication de Debian 10 Buster le
samedi 6 juillet 2019. Soyez patients ! Toute une série d'étapes est nécessaire
et certaines prennent du temps, comme la construction des images, la diffusion
de la publication dans le réseau de miroirs et la reconstruction du site de
Debian pour que « stable » pointe vers Debian 10.

Si vous envisagez de créer des illustrations à l'occasion de la publication de
Buster, n'hésitez pas à envoyer un lien vers vos créations à la
[liste de diffusion debian-publicity](mailto:debian-publicity@lists.debian.org)
(archivée publiquement), de manière à ce que nous puissions les diffuser à
toute la communauté.

Vous pouvez suivre la couverture de la publication en direct sur
[https://micronews.debian.org](https://micronews.debian.org) ou le profil
**@debian** de votre réseau social préféré ! Nous diffuserons des informations
sur ce qui est nouveau dans cette version de Debian 10, sur l'avancement du
processus durant tout ce week-end et des faits sur Debian et la vaste
communauté de contributeurs bénévoles qui rend possible son existence.

Si vous voulez célébrer la publication de Debian 10 Buster, vous pouvez vous
joindre à une des
[nombreuses fêtes organisées](https://wiki.debian.org/ReleasePartyBuster) ou en
organiser une dans votre ville ! Il y aura aussi une célébration en ligne sur
[Debian Party Line](https://wiki.debian.org/ReleasePartyBuster#Debian_Party_Line).
