Title: Nous desenvolupadors i mantenidors de Debian (gener i febrer del 2018)
Slug: new-developers-2018-02
Date: 2018-03-04 08:30
Author: Jean-Pierre Giraud
Tags: project
Lang: ca
Status: published

Els següents col·laboradors del projecte han esdevingut Debian Developers
en els darrers dos mesos:

* Alexandre Mestiashvili (mestia)
* Tomasz Rybak (serpent)
* Louis-Philippe Véronneau (pollo)

Els següents col·laboradors del projecte han esdevingut Debian Maintainers
en els darrers dos mesos:

* Teus Benschop
* Kyle John Robbertze
* Maarten van Gompel
* Dennis van Dok
* Innocent De Marchi
* David Rabel

Enhorabona a tots!
