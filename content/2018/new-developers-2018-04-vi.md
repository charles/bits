Title: Những nhà Phát triển và Bảo trì Debian mới (Tháng ba và tư 2018)
Slug: new-developers-2018-04
Date: 2018-05-02 22:03
Author: Jean-Pierre Giraud
Tags: project
Lang: vi
Translator: Trần Ngọc Quân
Status: published

Trong hai tháng qua, những người đóng góp sau đây nhận được tài khoản Phát triển Debian:

* Andreas Boll (aboll)
* Dominik George (natureshadow)
* Julien Puydt (jpuydt)
* Sergio Durigan Junior (sergiodj)
* Robie Basak (rbasak)
* Elena Grandi (valhalla)
* Peter Pentchev (roam)
* Samuel Henrique (samueloph)

Trong hai tháng qua, những người đóng góp sau đây được thêm vào với tư cách là người Bảo trì Debian:

* Andy Li
* Alexandre Rossi
* David Mohammed
* Tim Lunn
* Rebecca Natalie Palmer
* Andrea Bolognani
* Toke Høiland-Jørgensen
* Gabriel F. T. Gomes
* Bjorn Anders Dolk
* Geoffroy Youri Berret
* Dmitry Eremin-Solenikov

Xin chúc mừng!
