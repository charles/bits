Title: Nous desenvolupadors i mantenidors de Debian (setembre i octubre del 2018)
Slug: new-developers-2018-10
Date: 2018-11-08 14:00
Author: Jean-Pierre Giraud
Tags: project
Lang: ca
Translator:
Status: published

Els següents col·laboradors del projecte han esdevingut Debian Developersen
els darrers dos mesos:

* Joseph Herlant (aerostitch)
* Aurélien Couderc (coucouf)
* Dylan Aïssi (daissi)
* Kunal Mehta (legoktm)
* Ming-ting Yao Wei (mwei)
* Nicolas Braud-Santoni (nicoo)
* Pierre-Elliott Bécue (peb)
* Stephen Gelman (ssgelm)
* Daniel Echeverry (epsilon)
* Dmitry Bogatov (kaction)

Els següents col·laboradors del projecte han esdevingut Debian Maintainers
en els darrers dos mesos:

* Sagar Ippalpalli
* Kurt Kremitzki
* Michal Arbet
* Peter Wienemann
* Alexis Bienvenüe
* Gard Spreemann

Enhorabona a tots!
