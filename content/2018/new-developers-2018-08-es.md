Title: Nuevos desarrolladores y mantenedores de Debian (julio y agosto del 2018)
Slug: new-developers-2018-08
Date: 2018-09-09 17:00
Author: Jean-Pierre Giraud
Tags: project
Lang: es
Translator: Jean-Pierre Giraud y Laura Arjona Reina
Status: published

Los siguientes colaboradores del proyecto se convirtieron en Debian Developers
en los dos últimos meses:

* William Blough (bblough)
* Shengjing Zhu (zhsj)
* Boyuan Yang (byang)
* Thomas Koch (thk)
* Xavier Guimard (yadd)
* Valentin Vidic (vvidic)
* Mo Zhou (lumin)
* Ruben Undheim (rubund)
* Damiel Baumann (daniel)

Los siguientes colaboradores del proyecto se convirtieron en Debian Maintainers
en los dos últimos meses:

* Phil Morrell
* Raúl Benencia
* Brian T. Smith
* Iñaki Martin Malerba
* Hayashi Kentaro
* Arnaud Rebillout

¡Felicidades a todos!
