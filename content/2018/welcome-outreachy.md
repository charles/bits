Title: Debian welcomes its Outreachy interns
Slug: welcome-outreachy-interns-2017-2018
Date: 2018-02-02 08:30
Author: Laura Arjona Reina
Tags: gsoc, google, announce, development, diversity, software, code, projects, osmocom, mobile, calendar
Status: published

We'd like to welcome our three Outreachy interns
for this round, lasting from December 2017 to March 2018.

[Juliana Oliveira](https://medium.com/@jwnx)
is working on
[reproducible builds for Debian and free software](https://wiki.debian.org/Outreachy/Round15/Projects#Outreachy.2FRound15.2FProjects.2FReproducibleBuildsOfDebian.Reproducible_builds_for_Debian_and_free_software).

[Kira Obrezkova](https://kira385881773.wordpress.com/)
is working on
[bringing open-source mobile technologies to a new level with Debian (Osmocom)](https://wiki.debian.org/Outreachy/Round15/Projects#Outreachy.2FRound15.2FProjects.2FDebianOsmoCom.Debian_MobCom).

[Renata D'Avila](https://rsip22.github.io/blog/)
is working on
[a calendar database of social events and conferences for free software developers](https://wiki.debian.org/Outreachy/Round15/Projects#Outreachy.2FRound15.2FProjects.2FSocialEventAndConferenceCalendars.A_calendar_database_of_social_events_and_conferences).

Congratulations, Juliana, Kira and Renata!

From [the official website](https://www.outreachy.org/): *Outreachy provides three-month internships
for people from groups traditionally underrepresented in tech.
Interns work remotely with mentors from Free and Open Source Software (FOSS) communities
on projects ranging from programming, user experience, documentation,
illustration and graphical design, to data science.*

The Outreachy programme is possible in Debian thanks
to the efforts of Debian developers and contributors who dedicate
their free time to mentor students and outreach tasks, and
the [Software Freedom Conservancy](https://sfconservancy.org/)'s administrative support,
as well as the continued support of Debian's donors, who provide funding
for the internships.

Debian will also participate this summer in the next round for Outreachy,
and is currently applying as mentoring organisation for
the [Google Summer of Code 2018](https://wiki.debian.org/SummerOfCode2018) programme.
Have a look at the [projects wiki page](https://wiki.debian.org/SummerOfCode2018/Projects)
and contact the Debian Outreach Team [mailing list](https://lists.debian.org/debian-outreach/)
to join as a mentor or welcome applicants into the Outreachy or GSoC programme.

Join us and help extend Debian!
