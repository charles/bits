Title: Novos desenvolvedores e mantenedores Debian (julho e agosto de 2018)
Slug: new-developers-2018-08
Date: 2018-09-09 17:00
Author: Jean-Pierre Giraud
Tags: project
Lang: pt
Translator:
Status: published

Os seguintes colaboradores do projeto se tornaram Desenvolvedores Debian nos
últimos dois meses:

* William Blough (bblough)
* Shengjing Zhu (zhsj)
* Boyuan Yang (byang)
* Thomas Koch (thk)
* Xavier Guimard (yadd)
* Valentin Vidic (vvidic)
* Mo Zhou (lumin)
* Ruben Undheim (rubund)
* Damiel Baumann (daniel)

Os seguintes colaboradores do projeto se tornaram Mantenedores Debian nos
últimos dois meses:

* Phil Morrell
* Raúl Benencia
* Brian T. Smith
* Iñaki Martin Malerba
* Hayashi Kentaro
* Arnaud Rebillout

Parabéns a todos!
