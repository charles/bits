Title: Debian welcomes its GSoC 2018 and Outreachy interns
Slug: welcome-gsoc2018-and-outreachy-interns
Date: 2018-05-30 20:45
Author: Laura Arjona Reina
Tags: gsoc, google, announce, development, diversity, software, code, projects, clang, firefox, thunderbird, SSO, Caldav, Kali, QA
Status: published

![GSoC logo](|static|/images/gsoc.jpg)

![Outreachy logo](|static|/images/outreachy-logo-300x61.png)

We're excited to announce that Debian has selected twenty-six interns to work with us
during the next months: one person for [Outreachy][1], and twenty-five for the [Google Summer of Code][2].

[1]: https://www.outreachy.org/alums/
[2]: https://summerofcode.withgoogle.com/organizations/5166394929315840/?sp-page=3#!

Here is the list of projects and the interns who will work on them:

[A calendar database of social events and conferences](https://wiki.debian.org/SocialEventAndConferenceCalendars)

* [Doğukan Çelik](https://www.dogukancelik.com)

[Android SDK Tools in Debian](https://wiki.debian.org/AndroidTools)

* [Saif Abdul Cassim](https://salsa.debian.org/m36-guest) (GSoC 2018 Android SDK tools in Debian)
* [Chandramouli Rajagopalan](https://salsa.debian.org/r_chandramouli-guest) (Packaging and Updating Android SDK Tools)
* [Umang Parmar](https://salsa.debian.org/darkLord-guest) (Android SDK Tools in Debian)

[Automatic builds with clang using OBS](https://wiki.debian.org/SummerOfCode2018/Projects/ClangRebuild)

* [Athos Ribeiro](https://athoscr.me/blog/gsoc2018-1/)

[Automatic Packages for Everything](https://wiki.debian.org/SummerOfCode2018/Projects/AutomaticPackagesForEverything)

* [Alexandre Viau](https://salsa.debian.org/aviau)

[Click To Dial Popup Window for the Linux Desktop](https://wiki.debian.org/SummerOfCode2018/Projects/ClickToDialPopupWindowForLinuxDesktop)

* [Diellza Shabani](https://wiki.debian.org/DiellzaShabani)
* [Sanjay Prajapat](https://salsa.debian.org/sanjaypra555-guest/)
* [Vishal Gupta](https://salsa.debian.org/comfortablydumb-guest/)

[Design and implementation of a Debian SSO solution](https://wiki.debian.org/SummerOfCode2018/Projects/NewDebianSSO)

* [Birger Schacht](https://bisco.org/tags/gsoc18/)

[EasyGnuPG Improvements](https://wiki.debian.org/SummerOfCode2018/Projects/EasyGnuPG)

* [Divesh Uttamchandani](https://wiki.debian.org/DiveshUttamchandani)

[Extracting data from PDF invoices and bills for financial accounting](https://wiki.debian.org/SummerOfCode2018/Projects/ExtractingDataFromPDFInvoicesAndBills)

* [Harshit Joshi](https://wiki.debian.org/HarshitJoshi)

[Firefox and Thunderbird plugin for free software habits](https://wiki.debian.org/SummerOfCode2018/Projects/FirefoxAndThunderbirdPluginFreeSoftwareHabits)

* [Enkelena Haxhija](https://wiki.debian.org/EnkelenaHaxhija)

[GUI app for EasyGnuPG](https://wiki.debian.org/SummerOfCode2018/Projects/EasyGnuPG)

* [Yugesh Ajit Kothari](https://github.com/yugeshk)

[Improving Distro Tracker to better support Debian teams](https://wiki.debian.org/SummerOfCode2018/Projects/ImprovingDistro-trackerToBetterSupportDebianTeams)

* [Arthur Del Esposte](https://wiki.debian.org/SummerOfCode2018/StudentApplications/ArthurEsposte)

[Kanban Board for Debian Bug Tracker and CalDAV servers](https://wiki.debian.org/SummerOfCode2018/Projects/KanbanBoardForDebianBugTrackerAndCalDAVServer)

* [Chikirou Massiwayne](https://wiki.debian.org/SummerOfCode2018/StudentApplications/ChikirouMassiwayne)

[OwnMailbox Improvements](https://wiki.debian.org/SummerOfCode2018/Projects/OwnMailbox)

* [Georgios Pipilis](https://github.com/giorgos314)

[P2P Network Boot with BitTorrent](https://wiki.debian.org/SummerOfCode2018/Projects/BootTorrent)

* [Shreyansh Khajanchi](https://github.com/shreyanshk)

[PGP Clean Room Live CD](https://wiki.debian.org/SummerOfCode2018/Projects/PGPCleanRoomKeyManagement)

* [Jacob Adams](https://wiki.debian.org/JacobAdams)

[Port Kali Packages to Debian](https://wiki.debian.org/SummerOfCode2018/Projects/PortKaliPackages)

* [Samuel Henrique](https://salsa.debian.org/samueloph)

[Quality assurance for biological applications inside Debian](https://wiki.debian.org/SummerOfCode2018/Projects/QualityAssuranceForBiologicalApplicationsInsideDebian)

* [Liubov Chuprikova](https://salsa.debian.org/liubovch-guest)

[Reverse Engineering Radiator Bluetooth Thermovalves](https://wiki.debian.org/SummerOfCode2018/Projects/RadiatorThermovalveReverseEngineering)

* [Sergio Alberti](https://gitlab.com/sergioalberti)

[Virtual LTSP Server](https://wiki.debian.org/SummerOfCode2018/Projects/VirtualLtspServer)

* [Deepanshu Gajbhiye](https://github.com/d78ui98)

[Wizard/GUI helping students/interns apply and get started](https://wiki.debian.org/NewContributorWizard)

* [Elena Gjevukaj](https://wiki.debian.org/ElenaGjevukaj)
* [Minkush Jain](https://wiki.debian.org/MinkushJain)
* [Shashank Kumar](https://wiki.debian.org/ShashankKumar)

Congratulations and welcome to all of them!

The Google Summer of Code and Outreachy programs are possible in Debian thanks
to the efforts of Debian developers and contributors that dedicate
part of their free time to mentor interns and outreach tasks.

Join us and help extend Debian! You can follow the interns weekly reports on the
[debian-outreach mailing-list][debian-outreach-ml], chat with us on
[our IRC channel][debian-outreach-irc] or on each project's team mailing lists.

[debian-outreach-ml]: https://lists.debian.org/debian-outreach/ (debian-outreach AT lists.debian.org)
[debian-outreach-irc]: irc://irc.debian.org/#debian-outreach (#debian-outreach on irc.debian.org)
