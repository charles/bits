Title: Novos desenvolvedores e mantenedores Debian (janeiro e fevereiro de 2018)
Slug: new-developers-2018-02
Date: 2018-03-04 08:30
Author: Jean-Pierre Giraud
Tags: project
Lang: pt
Status: published

Os seguintes colaboradores do projeto se tornaram Desenvolvedores Debian
nos últimos dois meses:

* Alexandre Mestiashvili (mestia)
* Tomasz Rybak (serpent)
* Louis-Philippe Véronneau (pollo)

Os seguintes colaboradores do projeto se tornaram Mantenedores Debian
nos últimos dois meses:

* Teus Benschop
* Kyle John Robbertze
* Maarten van Gompel
* Dennis van Dok
* Innocent De Marchi
* David Rabel

Parabéns a todos!
