Title: Nous desenvolupadors i mantenidors de Debian (maig i juny del 2018)
Slug: new-developers-2018-06
Date: 2018-07-30 19:45
Author: Jean-Pierre Giraud
Tags: project
Lang: ca
Translator:
Status: published

Els següents col·laboradors del projecte han esdevingut Debian Maintainers en
els darrers dos mesos:

* Andre Bianchi
* Simon Quigley
* Andrius Merkys
* Tong Sun
* James Lu
* Raphaël Halimi
* Paul Seyfert
* Dustin Kirkland
* Yanhao Mo
* Paride Legovini

Enhorabona a tots!

Es necessiten gestors de sol·licituds per ajudar els col·laboradors a
aconseguir els seus comptes de Desenvolupador de Debian. Per a més detalls,
consulteu la
[crida de la Recepció de Nous Membres](https://lists.debian.org/debian-devel-announce/2018/06/msg00007.html).
