Title: 新的 Debian 开发者和维护者 (2018年7月 至 8月)
Slug: new-developers-2018-08
Date: 2018-09-09 17:00
Author: Jean-Pierre Giraud
Tags: project
Lang: zh-CN
Translator: Boyuan Yang
Status: published

下列贡献者在过去的两个月中获得了他们的 Debian 开发者帐号：

* William Blough (bblough)
* Shengjing Zhu (zhsj)
* Boyuan Yang (byang)
* Thomas Koch (thk)
* Xavier Guimard (yadd)
* Valentin Vidic (vvidic)
* Mo Zhou (lumin)
* Ruben Undheim (rubund)
* Damiel Baumann (daniel)

下列贡献者在过去的两个月中被添加为 Debian 维护者：

* Phil Morrell
* Raúl Benencia
* Brian T. Smith
* Iñaki Martin Malerba
* Hayashi Kentaro
* Arnaud Rebillout

祝贺他们！
