Title: Những nhà Phát triển và Bảo trì Debian mới (Tháng chín và mười 2018)
Slug: new-developers-2018-10
Date: 2018-11-08 14:00
Author: Jean-Pierre Giraud
Tags: project
Lang: vi
Translator: Trần Ngọc Quân
Status: published

Trong hai tháng qua, những người đóng góp sau đây nhận được tài khoản Phát triển Debian:

* Joseph Herlant (aerostitch)
* Aurélien Couderc (coucouf)
* Dylan Aïssi (daissi)
* Kunal Mehta (legoktm)
* Ming-ting Yao Wei (mwei)
* Nicolas Braud-Santoni (nicoo)
* Pierre-Elliott Bécue (peb)
* Stephen Gelman (ssgelm)
* Daniel Echeverry (epsilon)
* Dmitry Bogatov (kaction)

Trong hai tháng qua, những người đóng góp sau đây được thêm vào với tư cách là người Bảo trì Debian:

* Sagar Ippalpalli
* Kurt Kremitzki
* Michal Arbet
* Peter Wienemann
* Alexis Bienvenüe
* Gard Spreemann

Xin chúc mừng!
