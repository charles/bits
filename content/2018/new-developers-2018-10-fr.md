Title: Nouveaux développeurs et mainteneurs de Debian (septembre et octobre 2018)
Slug: new-developers-2018-10
Date: 2018-11-08 14:00
Author: Jean-Pierre Giraud
Tags: project
Lang: fr
Translator:
Status: published

Les contributeurs suivants sont devenus développeurs Debian ces deux derniers
mois :

* Joseph Herlant (aerostitch)
* Aurélien Couderc (coucouf)
* Dylan Aïssi (daissi)
* Kunal Mehta (legoktm)
* Ming-ting Yao Wei (mwei)
* Nicolas Braud-Santoni (nicoo)
* Pierre-Elliott Bécue (peb)
* Stephen Gelman (ssgelm)
* Daniel Echeverry (epsilon)
* Dmitry Bogatov (kaction)

et ces contributeurs ont été acceptés comme mainteneurs Debian durant la même
période :

* Sagar Ippalpalli
* Kurt Kremitzki
* Michal Arbet
* Peter Wienemann
* Alexis Bienvenüe
* Gard Spreemann

Félicitations !
