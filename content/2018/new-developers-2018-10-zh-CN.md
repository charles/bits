Title: 新的 Debian 开发者和维护者（2018年9月至2018年10月）
Slug: new-developers-2018-10
Date: 2018-11-08 14:00
Author: Jean-Pierre Giraud
Tags: project
Lang: zh-CN
Translator: Boyuan Yang
Status: published

下列贡献者在过去的两个月中获得了他们的 Debian 开发者帐号：

* Joseph Herlant (aerostitch)
* Aurélien Couderc (coucouf)
* Dylan Aïssi (daissi)
* Kunal Mehta (legoktm)
* Ming-ting Yao Wei (mwei)
* Nicolas Braud-Santoni (nicoo)
* Pierre-Elliott Bécue (peb)
* Stephen Gelman (ssgelm)
* Daniel Echeverry (epsilon)
* Dmitry Bogatov (kaction)

下列贡献者在过去的两个月中被添加为 Debian 维护者：

* Sagar Ippalpalli
* Kurt Kremitzki
* Michal Arbet
* Peter Wienemann
* Alexis Bienvenüe
* Gard Spreemann

祝贺他们！
