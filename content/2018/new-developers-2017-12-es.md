Title: Nuevos desarrolladores y mantenedores de Debian (noviembre y diciembre del 2017)
Slug: new-developers-2017-12
Date: 2018-01-01 20:30
Author: Jean-Pierre Giraud
Tags: project
Lang: es
Status: published

Los siguientes colaboradores del proyecto se convirtieron en Debian Developers
en los dos últimos meses:

* Ben Armstrong (synrg)
* Frédéric Bonnard (frediz)
* Jerome Charaoui (lavamind)
* Michael Jeanson (mjeanson)
* Jim Meyering (meyering)
* Christopher Knadle (krait)

Los siguientes colaboradores del proyecto se convirtieron en Debian Maintainers
en los dos últimos meses:

* Chris West
* Mark Lee Garrett
* Pierre-Elliott Bécue
* Sebastian Humenda
* Stefan Schörghofer
* Stephen Gelman
* Georg Faerber
* Nico Schlömer

¡Felicidades a todos!
