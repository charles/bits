Title: Bursary applications for DebConf18 are closing in 48 hours!
Slug: dc18-bursaries
Date: 2018-04-12 12:30
Author: Laura Arjona Reina
Tags: debconf18, debconf
Status: published

If you intend to apply for a DebConf18 bursary and
have not yet done so, please proceed as soon as possible!

Bursary applications for DebConf18 will be accepted until April 13th at
23:59 UTC. Applications submitted after this deadline will not be considered.

You can apply for a bursary when you [register](https://debconf18.debconf.org/register)
for the conference.

Remember that giving a talk or organising an event is considered towards your bursary;
if you have a submission to make, submit it even if it is only sketched-out.
You will be able to detail it later. DebCamp plans can be entered in the usual
[Sprints page at the Debian wiki](https://wiki.debian.org/Sprints).

Please make sure to double-check your accommodation choices (dates and venue).
Details about accommodation arrangements can be found on the
[wiki](https://wiki.debconf.org/wiki/DebConf18/Accommodation).

See you in Hsinchu!

![DebConf18 logo](|static|/images/DebConf18_Horizontal_Logo_600_240.png)
