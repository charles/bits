Title: Nous desenvolupadors i mantenidors de Debian (juliol i agost del 2018)
Slug: new-developers-2018-08
Date: 2018-09-09 17:00
Author: Jean-Pierre Giraud
Tags: project
Lang: ca
Translator:
Status: published

Els següents col·laboradors del projecte han esdevingut Debian Developersen els
darrers dos mesos:

* William Blough (bblough)
* Shengjing Zhu (zhsj)
* Boyuan Yang (byang)
* Thomas Koch (thk)
* Xavier Guimard (yadd)
* Valentin Vidic (vvidic)
* Mo Zhou (lumin)
* Ruben Undheim (rubund)
* Damiel Baumann (daniel)

Els següents col·laboradors del projecte han esdevingut Debian Maintainers en
els darrers dos mesos:

* Phil Morrell
* Raúl Benencia
* Brian T. Smith
* Iñaki Martin Malerba
* Hayashi Kentaro
* Arnaud Rebillout

Enhorabona a tots!
