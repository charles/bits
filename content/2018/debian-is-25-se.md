Title: 25 år och mer
Slug: debian-is-25
Date: 2018-08-16 8:50
Author: Ana Guerrero Lopez
Artist: Angelo Rosa
Tags: debian, project, anniversary, DebianDay
Lang: se
Translator: Andreas Rönnquist
Status: published

![Debian är 25 år av Angelo Rosa](|static|/images/debian25years.png)

När den avlidne Ian Murdock för 25 år sedan i comp.os.linux.development
tillkännagav *"det nära förestående fullbordandet av en helt ny Linux-version,
[...] Debian Linux Release"*, hade ingen förväntat att "Debian Linux Release" skulle bli
det som idag är känt som Debianprojektet, ett av de största och mest
inflytelserika mjukvaruprojekten. Den primära produkten är Debian, ett
fritt operativsystem (OS) för din dator, såväl som för många andra system
som förbättrar ditt liv. Från den inre verksamheten i din lokala
flygplats till din bils underhållningssystem, och från molnservrar som
värd för dina favoritwebbplatser till IoT-enheterna som kommunicerar med dem,
kan Debian driva alltihop.

Idag är Debianprojektet en stor och blomstrande organisation med otaliga
självorganiserade grupper sammansatta av volontärer. Medan det ofta ser
kaotiskt utifrån upprätthålls projektet av sina två huvudorganisationsdokument:
[Debians sociala kontrakt], som tillhandahåller en vision om att förbättra
samhället och [Debians riktlinjer för fri programvara], som ger en indikation
om vilken programvara som anses vara användbar. De kompletteras av [projektets
konstitution] som fastställer projektstrukturen och [uppförandekoden], som anger
tonen för interaktioner inom projektet.

Varje dag under de senaste 25 åren har människor skickat felrapporter och patchar,
laddat upp paket, uppdaterat översättningar, skapat illustrationer, organiserat
evenemang rörande Debian, uppdaterat webbplatsen, lärt andra hur man använder
Debian och skapat hundratals derivat.

**För ytterligare 25 år - och förhoppningsvis många, många fler!**

[Debians sociala kontrakt]: https://www.debian.org/social_contract
[Debians riktlinjer för fri programvara]: https://www.debian.org/social_contract#guidelines
[projektets konstitution]: https://www.debian.org/devel/constitution
[uppförandekoden]: https://www.debian.org/code_of_conduct
