Title: Nous desenvolupadors i mantenidors de Debian (març i abril del 2018)
Slug: new-developers-2018-04
Date: 2018-05-02 22:03
Author: Jean-Pierre Giraud
Tags: project
Lang: ca
Translator:
Status: published

Els següents col·laboradors del projecte han esdevingut Debian Developersen
els darrers dos mesos:

* Andreas Boll (aboll)
* Dominik George (natureshadow)
* Julien Puydt (jpuydt)
* Sergio Durigan Junior (sergiodj)
* Robie Basak (rbasak)
* Elena Grandi (valhalla)
* Peter Pentchev (roam)
* Samuel Henrique (samueloph)

Els següents col·laboradors del projecte han esdevingut Debian Maintainers
en els darrers dos mesos:

* Andy Li
* Alexandre Rossi
* David Mohammed
* Tim Lunn
* Rebecca Natalie Palmer
* Andrea Bolognani
* Toke Høiland-Jørgensen
* Gabriel F. T. Gomes
* Bjorn Anders Dolk
* Geoffroy Youri Berret
* Dmitry Eremin-Solenikov

Enhorabona a tots!
