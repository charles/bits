Title: Nya Debianutvecklare och Debian Maintainers (September och Oktober 2018)
Slug: new-developers-2018-10
Date: 2018-11-08 14:00
Author: Jean-Pierre Giraud
Tags: project
Lang: sv
Translator:
Status: published

Följande bidragslämnare fick sina Debianutvecklarkonton under de senaste två
månaderna:

* Joseph Herlant (aerostitch)
* Aurélien Couderc (coucouf)
* Dylan Aïssi (daissi)
* Kunal Mehta (legoktm)
* Ming-ting Yao Wei (mwei)
* Nicolas Braud-Santoni (nicoo)
* Pierre-Elliott Bécue (peb)
* Stephen Gelman (ssgelm)
* Daniel Echeverry (epsilon)
* Dmitry Bogatov (kaction)

Följande bidragslämnare adderades som Debian Maintainers under de senaste två
månaderna:

* Sagar Ippalpalli
* Kurt Kremitzki
* Michal Arbet
* Peter Wienemann
* Alexis Bienvenüe
* Gard Spreemann

Grattis!
