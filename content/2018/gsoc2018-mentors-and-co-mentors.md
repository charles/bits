Title: Mentors and co-mentors for Debian's Google Summer of Code 2018
Slug: gsoc-2018-mentors-and-co-mentors
Date: 2018-01-23 00:50
Author: Daniel Pocock and Laura Arjona Reina
Tags: gsoc, google, announce, development, diversity, software, code, projects
Status: published

![GSoC logo](|static|/images/gsoc.jpg)

Debian is applying as a mentoring organization for the
[Google Summer of Code 2018](https://wiki.debian.org/SummerOfCode2018),
an internship program open to university students aged 18 and up.

Debian already has a wide range of [projects](https://wiki.debian.org/SummerOfCode2018/Projects)
listed but it is not too
late to add more or to improve the existing proposals.
Google will start reviewing the ideas page over the
next two weeks and students will start looking at it in mid-February.

Please join us and help extending Debian!
You can consider listing a potential project for interns or listing your
name as a possible co-mentor for one of the existing projects on
[Debian's Google Summer of Code wiki page](https://wiki.debian.org/SummerOfCode2018).

At this stage, mentors are not obliged to commit to accepting an intern
but it is important for potential mentors to be listed to get the
process started.  You will have the opportunity to review student
applications in March and April and give the administrators a definite
decision if you wish to proceed in early April.

Mentors, co-mentors and other volunteers can follow an intern through
the entire process or simply volunteer for one phase of the program,
such as helping recruit students in a local university or helping test
the work completed by a student at the end of the summer.

Participating in GSoC has many benefits for Debian and the wider free
software community.  If you have questions, please come and ask us on
IRC #debian-outreach or the [debian-outreach mailing list](https://lists.debian.org/debian-outreach/).
