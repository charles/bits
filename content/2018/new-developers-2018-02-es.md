Title: Nuevos desarrolladores y mantenedores de Debian (enero y febrero del 2018)
Slug: new-developers-2018-02
Date: 2018-03-04 08:30
Author: Jean-Pierre Giraud
Tags: project
Lang: es
Status: published

Los siguientes colaboradores del proyecto se convirtieron en Debian Developers
en los dos últimos meses:

* Alexandre Mestiashvili (mestia)
* Tomasz Rybak (serpent)
* Louis-Philippe Véronneau (pollo)

Los siguientes colaboradores del proyecto se convirtieron en Debian Maintainers
en los dos últimos meses:

* Teus Benschop
* Kyle John Robbertze
* Maarten van Gompel
* Dennis van Dok
* Innocent De Marchi
* David Rabel

¡Felicidades a todos!
