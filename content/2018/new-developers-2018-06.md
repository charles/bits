Title: New Debian Developers and Maintainers (May and June 2018)
Slug: new-developers-2018-06
Date: 2018-07-30 19:45
Author: Jean-Pierre Giraud
Tags: project
Lang: en
Translator:
Status: published

The following contributors were added as Debian Maintainers in the last two
months:

* Andre Bianchi
* Simon Quigley
* Andrius Merkys
* Tong Sun
* James Lu
* Raphaël Halimi
* Paul Seyfert
* Dustin Kirkland
* Yanhao Mo
* Paride Legovini

Congratulations!

Application managers are needed to help contributors get their Debian Developer
accounts. See the
[call from New Member Front Desk](https://lists.debian.org/debian-devel-announce/2018/06/msg00007.html)
for details.
