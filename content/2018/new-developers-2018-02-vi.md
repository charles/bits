Title: Những nhà Phát triển và Bảo trì Debian mới (Tháng giêng và hai 2018)
Slug: new-developers-2018-02
Date: 2018-03-04 08:30
Author: Jean-Pierre Giraud
Tags: project
Translator: Trần Ngọc Quân
Lang: vi
Status: published

Trong hai tháng qua, những người đóng góp sau đây nhận được tài khoản Phát triển Debian:

* Alexandre Mestiashvili (mestia)
* Tomasz Rybak (serpent)
* Louis-Philippe Véronneau (pollo)

Trong hai tháng qua, những người đóng góp sau đây được thêm vào với tư cách là người Bảo trì Debian:

* Teus Benschop
* Kyle John Robbertze
* Maarten van Gompel
* Dennis van Dok
* Innocent De Marchi
* David Rabel

Xin chúc mừng!
