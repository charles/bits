Title: Những nhà Phát triển và Bảo trì Debian mới (Tháng mời một và mười hai 2017)
Slug: new-developers-2017-12
Date: 2018-01-01 20:30
Author: Jean-Pierre Giraud
Tags: project
Translator: Trần Ngọc Quân
Lang: vi
Status: published

Trong hai tháng qua, những người đóng góp sau đây nhận được tài khoản Phát triển Debian:

* Ben Armstrong (synrg)
* Frédéric Bonnard (frediz)
* Jerome Charaoui (lavamind)
* Michael Jeanson (mjeanson)
* Jim Meyering (meyering)
* Christopher Knadle (krait)

Trong hai tháng qua, những người đóng góp sau đây được thêm vào với tư cách là người Bảo trì Debian:

* Chris West
* Mark Lee Garrett
* Pierre-Elliott Bécue
* Sebastian Humenda
* Stefan Schörghofer
* Stephen Gelman
* Georg Faerber
* Nico Schlömer

Xin chúc mừng!
