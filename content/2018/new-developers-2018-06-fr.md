Title: Nouveaux développeurs et mainteneurs de Debian (mai et juin 2018)
Slug: new-developers-2018-06
Date: 2018-07-30 19:45
Author: Jean-Pierre Giraud
Tags: project
Lang: fr
Translator:
Status: published

Les contributeurs suivant ont été acceptés comme mainteneurs Debian ces deux
derniers mois :

* Andre Bianchi
* Simon Quigley
* Andrius Merkys
* Tong Sun
* James Lu
* Raphaël Halimi
* Paul Seyfert
* Dustin Kirkland
* Yanhao Mo
* Paride Legovini

Félicitations !

Plus de responsables de candidature («Application Managers») sont nécessaires
pour aider les collaborateurs du projet à devenir développeurs Debian.
Des détails dans
[l'appel du secrétariat des nouveau membres](https://lists.debian.org/debian-devel-announce/2018/06/msg00007.html).
