Title: Những nhà Phát triển và Bảo trì Debian mới (Tháng năm và sáu 2018)
Slug: new-developers-2018-06
Date: 2018-07-30 19:45
Author: Jean-Pierre Giraud
Tags: project
Lang: vi
Translator: Trần Ngọc Quân
Status: published

Trong hai tháng qua, những người đóng góp sau đây được thêm vào với tư cách là người Bảo trì Debian:

* Andre Bianchi
* Simon Quigley
* Andrius Merkys
* Tong Sun
* James Lu
* Raphaël Halimi
* Paul Seyfert
* Dustin Kirkland
* Yanhao Mo
* Paride Legovini

Xin chúc mừng!

Các ứng dụng quản lý đơn đăng ký là cần thiết để giúp những người đóng góp nhận được các tài khoản Phát triển Debian của họ.
Xem [thư kêu gọi từ Bộ Phận Tiếp Nhận Thành Viên Mới](https://lists.debian.org/debian-devel-announce/2018/06/msg00007.html)
để biết thêm chi tiết.
