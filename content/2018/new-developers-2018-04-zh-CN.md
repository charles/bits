Title: 新的 Debian 开发者和维护者（2018年3月至4月）
Slug: new-developers-2018-04
Date: 2018-05-02 22:03
Author: Jean-Pierre Giraud
Tags: project
Lang: zh-CN
Translator: Boyuan Yang
Status: published

下列贡献者在过去的两个月中获得了他们的 Debian 开发者帐号：

* Andreas Boll (aboll)
* Dominik George (natureshadow)
* Julien Puydt (jpuydt)
* Sergio Durigan Junior (sergiodj)
* Robie Basak (rbasak)
* Elena Grandi (valhalla)
* Peter Pentchev (roam)
* Samuel Henrique (samueloph)

下列贡献者在过去的两个月中被添加为 Debian 维护者：

* Andy Li
* Alexandre Rossi
* David Mohammed
* Tim Lunn
* Rebecca Natalie Palmer
* Andrea Bolognani
* Toke Høiland-Jørgensen
* Gabriel F. T. Gomes
* Bjorn Anders Dolk
* Geoffroy Youri Berret
* Dmitry Eremin-Solenikov

祝贺他们！
