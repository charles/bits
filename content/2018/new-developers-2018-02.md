Title: New Debian Developers and Maintainers (January and February 2018)
Slug: new-developers-2018-02
Date: 2018-03-04 08:30
Author: Jean-Pierre Giraud
Tags: project
Status: published

The following contributors got their Debian Developer accounts in the last two
months:

* Alexandre Mestiashvili (mestia)
* Tomasz Rybak (serpent)
* Louis-Philippe Véronneau (pollo)

The following contributors were added as Debian Maintainers in the last two
months:

* Teus Benschop
* Kyle John Robbertze
* Maarten van Gompel
* Dennis van Dok
* Innocent De Marchi
* David Rabel

Congratulations!
