Title: Debian Cloud Sprint 2018
Slug: debian-cloud-sprint-2018
Date: 2018-12-11 12:30
Author: Tomasz Rybak
Artist:
Tags: cloud, sprint
Status: published

The [Debian Cloud team](https://wiki.debian.org/Teams/Cloud) held a
[sprint](https://wiki.debian.org/Sprints/2018/DebianCloudOct2018) for the
third time, hosted by Amazon at its Seattle offices from October 8th to
October 10th, 2018.

We discussed the status of images on various platforms, especially in
light of moving to [FAI](https://fai-project.org/) as the only method for
building images on all the cloud platforms.  The next topic was building and
testing workflows, including the use of Debian machines for building, testing,
storing, and publishing built images. This was partially caused by the move of
all [repositories to Salsa](https://salsa.debian.org/cloud-team), which allows
for better management of code changes, especially reviewing new code.

Recently  we have made progress supporting cloud usage cases; grub
and kernel optimised for cloud images help with reducing boot time and
required memory footprint.  There is also growing interest in non-x86
images, and FAI can now build such images.

Discussion of support for LTS images, which started at the sprint, has now
moved to the
[debian-cloud mailing list](https://lists.debian.org/debian-cloud/)).
We also discussed providing many image variants, which
requires a more advanced and automated workflow, especially regarding
testing.  Further discussion touched upon providing newer kernels and software
like cloud-init from backports.  As interest in using
secure boot is increasing, we might cooperate with other team and use work on
UEFI to provide images signed boot loader and kernel.

Another topic of discussion was the management of accounts used by Debian to
build and publish Debian images.  [SPI](https://spi-inc.org/) will create and
manage such accounts for Debian, including user accounts (synchronised
with Debian accounts).  Buster images should be published using those
new accounts.  Our Cloud Team delegation proposal (prepared by Luca
Fillipozzi) was
[accepted](https://lists.debian.org/debian-cloud/2018/10/msg00035.html)
by the Debian Project Leader. Sprint minutes are
[available](https://lists.debian.org/debian-cloud/2018/11/msg00008.html),
including a summary and a list of action items for individual members.

![Group photo of the participants in the Cloud Team Sprint](|static|/images/cloud_sprint_2018.jpeg)
