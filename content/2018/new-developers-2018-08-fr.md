Title: Nouveaux développeurs et mainteneurs de Debian (juillet et août 2018)
Slug: new-developers-2018-08
Date: 2018-09-09 17:00
Author: Jean-Pierre Giraud
Tags: project
Lang: fr
Translator: Jean-Pierre Giraud
Status: published

Les contributeurs suivants sont devenus développeurs Debian ces deux
derniers mois :

* William Blough (bblough)
* Shengjing Zhu (zhsj)
* Boyuan Yang (byang)
* Thomas Koch (thk)
* Xavier Guimard (yadd)
* Valentin Vidic (vvidic)
* Mo Zhou (lumin)
* Ruben Undheim (rubund)
* Damiel Baumann (daniel)

et ces contributeurs ont été acceptés comme mainteneurs Debian durant la
même période :

* Phil Morrell
* Raúl Benencia
* Brian T. Smith
* Iñaki Martin Malerba
* Hayashi Kentaro
* Arnaud Rebillout

Félicitations !
