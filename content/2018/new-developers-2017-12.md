Title: New Debian Developers and Maintainers (November and December 2017)
Slug: new-developers-2017-12
Date: 2018-01-01 20:30
Author: Jean-Pierre Giraud
Tags: project
Status: published

The following contributors got their Debian Developer accounts in the last two
months:

* Ben Armstrong (synrg)
* Frédéric Bonnard (frediz)
* Jerome Charaoui (lavamind)
* Michael Jeanson (mjeanson)
* Jim Meyering (meyering)
* Christopher Knadle (krait)

The following contributors were added as Debian Maintainers in the last two
months:

* Chris West
* Mark Lee Garrett
* Pierre-Elliott Bécue
* Sebastian Humenda
* Stefan Schörghofer
* Stephen Gelman
* Georg Faerber
* Nico Schlömer

Congratulations!
