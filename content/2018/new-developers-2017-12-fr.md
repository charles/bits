Title: Nouveaux développeurs et mainteneurs de Debian (novembre et décembre 2017)
Slug: new-developers-2017-12
Date: 2018-01-01 20:30
Author: Jean-Pierre Giraud
Tags: project
Lang: fr
Status: published

Les contributeurs suivants sont devenus développeurs Debian ces deux
derniers mois :

* Ben Armstrong (synrg)
* Frédéric Bonnard (frediz)
* Jerome Charaoui (lavamind)
* Michael Jeanson (mjeanson)
* Jim Meyering (meyering)
* Christopher Knadle (krait)

et ces contributeurs ont été acceptés comme mainteneurs Debian durant la
même période :

* Chris West
* Mark Lee Garrett
* Pierre-Elliott Bécue
* Sebastian Humenda
* Stefan Schörghofer
* Stephen Gelman
* Georg Faerber
* Nico Schlömer

Félicitations !
