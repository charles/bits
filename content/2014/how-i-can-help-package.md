Title: skyrocketing how-can-i-help popcon count
Date: 2014-02-10 21:00
Tags: contributing
Slug: how-i-can-help-package
Author: Ana Guerrero Lopez
Status: published

**This is a repost from [Stefano Zacchiroli's post](http://upsilon.cc/~zack/blog/posts/2014/02/apt-get_install_how-can-i-help/)**

[**how-can-i-help**](http://packages.debian.org/sid/how-can-i-help) by
[Lucas Nussbaum](http://www.lucas-nussbaum.net/blog/) is one of the best things
that happened in the area of **attracting contributions to Debian** in quite a
while. It can be used both as a standalone tool to list opportunities for
contributing to Debian which are related to your installed packages, and as an
APT hook (which is also the default configuration) that at each upgrade will
inform you of *new* contribution opportunities.

how-can-i-help is **great for newbies** who are looking for ways to give back
to Debian which are a good match for their skills: among other things,
how-can-i-help shows
[bugs tagged "gift"](https://wiki.debian.org/qa.debian.org/GiftTag) related to
packages you use.

how-can-i-help is also great for **experienced developers**, as it allows them
to find out, in a timely manner, that packages they use are in dire need of
help: RC bugs, pending removals, adoptions needed, requests for sponsor, etc.
*(As highly unscientific evidence: I've noticed a rather quick turnover
of RFA/O/ITA bugs on packages installed on my machine. I suspect how-can-i-help
is somehow responsible for that, due to the fact that it increases awareness of
ongoing package issues directly with the people *using* them.)*

So, if you haven't yet, please `apt-get install how-can-i-help` RIGHT NOW.

I daresay that we should aim at **installing how-can-i-help by default** on all
Debian machines, but that might be an ambitious initial goal. In the meantime
I'll settle for making how-can-i-help's **popcon count** skyrocket. As of
today, it looks like this:

[![Alt how-can-i-help popularity contest graph 10/02/2014](|static|/images/500x-how-can-i-help-popcon-20141002.png)](http://qa.debian.org/popcon.php?package=how-can-i-help)

which is definitely too low for my taste. Please **spread the word** about
how-can-i-help. And let's see what we can collectively do to that graph.

how-can-i-help is just a tiny teeny helper, but I'm convinced it can do wonders
in liberating dormant contributions to the Debian Project.
