Title: DebConf15 welcomes its first nine sponsors!
Slug: dc15-welcome-its-first-sponsors
Date: 2014-11-13 14:35
Author: Laura Arjona Reina
Tags: debconf15, debconf, sponsors
Status: published

DebConf15 will take place in Heidelberg, Germany in August 2015. We strive to
provide an intense working environment and enable good progress for Debian and
for Free Software in general. We extend an invitation to everyone to join us
and to support this event. As a volunteer-run non-profit conference, we depend
on our sponsors.

Nine companies have already committed to sponsor DebConf15! Let's introduce
them:

Our first Gold sponsor is [**credativ**](http://www.credativ.de), a
service-oriented company focusing on open-source software, and also a [Debian
development partner](https://www.debian.org/partners/).

Our second Gold sponsor is [**sipgate**](http://www.sipgate.de/), a Voice over IP
service provider based in Germany that also operates in the United Kingdom
([sipgate site in English](http://www.live.sipgate.co.uk/)).

[**Google**](http://google.com) (the search engine and advertising company),
[**Farsight Security, Inc.**](http://farsightsecurity.com/) (developers of
real-time passive DNS solutions), [**Martin Alfke / Buero
2.0**](http://martin-alfke.de/) (Linux & UNIX Consultant and Trainer,
LPIC-2/Puppet Certified Professional) and [**Ubuntu**](http://www.ubuntu.com) (the
OS supported by Canonical) are our four Silver sponsors.

And last but not least, [**Logilab**](http://www.logilab.fr/),
[**Netways**](http://netways.de/) and [**Hetzner**](https://www.hetzner.de/en/) have
agreed to support us as Bronze-level.

## Become a sponsor too!

Would you like to become a sponsor? Do you know of or work in a company or
organization that may consider sponsorship?

Please have a look at our [sponsorship
brochure](http://media.debconf.org/dc15/fundraising/debconf15_sponsorship_brochure.pdf)
(also available [in
German](http://media.debconf.org/dc15/fundraising/debconf15_sponsoren-broschuere.pdf)),
in which we outline all the details and describe the sponsor benefits. For
instance, sponsors have the option to
reach out to Debian contributors, derivative developers, upstream authors and
other
community members during a Job Fair and through postings on our job wall, and
to show-case their Free Software involvement by staffing a booth on the Open
Weekend.
In addition, sponsors are able to distribute marketing materials in the
attendee bags. And it goes without saying that we honour your sponsorship
with visibility of your logo in the conference's videos, on our website, on
printed materials, and banners.

The [final report of
DebConf14](http://media.debconf.org/dc14/report/DebConf14_final_report.en.pdf) is also available, illustrating the broad
spectrum, quality, and enthusiasm of the community at work, and providing
detailed information about the different outcomes that last conference brought
up (talks, participants, social events, impact in the Debian project and the
free software scene, and much more).

For further details, feel free to contact us through [sponsors@debconf.org](mailto:sponsors@debconf.org), and
visit the DebConf15 website at [http://debconf15.debconf.org](http://debconf15.debconf.org).
