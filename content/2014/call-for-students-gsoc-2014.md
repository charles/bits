Title: Call for participants in the Google Summer of Code for Debian
Date: 2014-02-25 11:15
Tags:  gsoc, google, announce, development, diversity, software, code, projects
Slug: call-for-students-gsoc-2014
Author: Nicolas Dandrimont
Status: published

![GSoC 2014 logo](|static|/images/gsoc-2014-logo.jpg)

The [Google Summer of Code][gsoc-home]
is a program that allows post-secondary students aged 18 and older to
earn a stipend writing code for Free and Open Source Software projects
during the summer.

Debian has just been accepted as a mentoring organization for this
year's program! We're looking for students and mentors to make this
GSoC in Debian the best ever!

[Eligible students][eligibility], now is the time to take a look at our
[project ideas list][project-ideas], engage with the mentors for the
projects you find interesting, and start working on
[your application][student-applications]!  For more information, please
read the [FAQ][google-faq] and the [Program Timeline][google-timeline]
on Google's website.

Mentors for prospective projects can still submit proposals on the
[project ideas list][project-ideas]. You also need to send an email to
the mailing list linked below to present your project in a few
words. Feel also free to propose yourself as a co-mentor for one of the
listed projects, more help is always welcome!

If you are interested, we encourage you to come and chat with us on irc
([#debian-soc][debian-soc-irc] on irc.oftc.net), or to send an email to
the [SoC coordination mailing-list][debian-soc-ml]
([subscribe][debian-soc-ml-sub]).  Most of the Debian-specific GSoC
information can be found on [our wiki pages][debian-soc-home], but don't
be afraid to ask us directly on irc or via email.

We're looking forward to work with an amazing team of students and
mentors again this summer!

[gsoc-home]: http://www.google-melange.com/gsoc/homepage/google/gsoc2014
[eligibility]: http://www.google-melange.com/gsoc/document/show/gsoc_program/google/gsoc2014/help_page#2._Whos_eligible_to_participate_as_a
[project-ideas]: http://wiki.debian.org/SummerOfCode2014/Projects
[student-applications]: http://wiki.debian.org/SummerOfCode2014/StudentApplications
[google-faq]: http://www.google-melange.com/gsoc/document/show/gsoc_program/google/gsoc2014/help_page
[google-timeline]: http://www.google-melange.com/gsoc/document/show/gsoc_program/google/gsoc2014/help_page#2._What_is_the_program_timeline
[debian-soc-irc]: irc://irc.oftc.net/debian-soc
[debian-soc-ml]: mailto:soc-coordination@lists.alioth.debian.org
[debian-soc-ml-sub]: http://lists.alioth.debian.org/cgi-bin/mailman/listinfo/soc-coordination
[debian-soc-home]: https://wiki.debian.org/SummerOfCode2014
