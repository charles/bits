Title: Debian turns 21!
Date: 2014-08-16 11:45
Tags: birthday, debian
Slug: 21-birthday-debian
Author: Ana Guerrero Lopez and Valessio Brito
Status: published

Today is Debian's 21st anniversary.
[Plenty of cities are celebrating Debian Day](https://wiki.debian.org/DebianDay/2014).
If you are not close to any of those cities, there's still time for you to
organize a little celebration!

![Debian 21](|static|/images/debian21.png)

Happy 21st birthday Debian!
