Title: DebConf14 - schedule available
Date: 2014-08-03 23:25
Tags: debconf14, announce
Slug: debconf14-schedule-available
Author: Ana Guerrero Lopez
Status: published

Debconf14 will be held in three weeks in Portland, OR, USA and we're happy to
announce that the schedule is already available. Of course, it is still
possible for some minor changes to happen!

DebConf will open on Saturday, August 23 with the [Welcome talk][wt] followed
by two highlighted talks:

* [Debian in the Dark Ages of Free Software][talk-zack] by
  *Stefano Zacchiroli*, former Debian Project Leader. Stefano will speak about
  the achievements realized by Free Software communities in the past years,
  andhow now, despite the visible success, this freedom is being threatened by
  the current technology trends, and how can Debian help to preserve the so
  well deserved freedom.

* [Weapons of the Geek][talk-biella] by *Biella Coleman*, cultural
  anthropologist, who researches, writes, and teaches on computer hackers and
  digital activism will share with us part of her research, explaining how
  online communities can have a big impact on world politics today.

There will also be also a plethora of social events, such as our traditional
[cheese and wine party][caw], our [group photo][gp] and our [day trip][dt].

The complete schedule can be found at:
[https://summit.debconf.org/debconf14/](https://summit.debconf.org/debconf14/)

DebConf talks will be broadcast live on the Internet when possible, and videos
of the talks will be published on the web along with the presentation slides.

[talk-zack]:https://summit.debconf.org/debconf14/meeting/108/debian-in-the-dark-ages-of-free-software/
[talk-biella]:https://summit.debconf.org/debconf14/meeting/122/weapons-of-the-geek/
[wt]:https://summit.debconf.org/debconf14/meeting/32/welcome-talk/
[caw]:https://summit.debconf.org/debconf14/meeting/34/cheese-and-wine-party/
[gp]:https://summit.debconf.org/debconf14/meeting/36/group-photo/
[dt]:https://summit.debconf.org/debconf14/meeting/35/daytrip/
