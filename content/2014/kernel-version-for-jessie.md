Title: Jessie will ship Linux 3.16
Date: 2014-07-30 23:10
Tags: jessie, kernel, announce
Slug: kernel-version-for-jessie
Author: Ana Guerrero Lopez
Status: published

The [Debian Linux kernel team](https://wiki.debian.org/DebianKernel) has
discussed and chosen the kernel version to use as a basis for
[Debian 8 'jessie'](https://www.debian.org/releases/jessie/).

This will be Linux 3.16, due to be released in early August.  Release
candidates for Linux 3.16 are already packaged and available in the
experimental suite.

If you maintain a package that is closely bound to the kernel version -
a kernel module or a userland application that depends on an unstable
API - please ensure that it is compatible with Linux 3.16 prior to the
freeze date (**5th November, 2014**).  Incompatible packages are very likely
to be removed from testing and not included in 'jessie'.

1. **My kernel module package doesn't build on 3.16 and upstream is not
  interested in supporting this version.  What can I do?**
  The kernel team might be able to help you with forward-porting, but also try
  [Linux Kernel Newbies](http://kernelnewbies.org) or the mailing list(s) for
  the relevant kernel subsystem(s).

2. **There's an important new kernel feature that ought to go into jessie,
  but it won't be in 3.16.  Can you still add it?**
  Maybe - sometimes this is easy and sometimes it's too disruptive to
  the rest of the kernel.  Please [contact the team on the debian-kernel
  mailinglist](https://lists.debian.org/debian-kernel/) or by opening a
  wishlist bug.

3. **Will Linux 3.16 get long term support from upstream?**
  The Linux 3.16-stable branch will **not** be maintained as a longterm
  branch at [kernel.org](http://kernel.org).  However, the
  [Ubuntu kernel team](https://wiki.ubuntu.com/KernelTeam) will continue to
  maintain that branch, following the same rules for acceptance and
  review, until around **April 2016**.  Ben Hutchings is planning to continue
  maintenance from then until the end of regular support for 'jessie'.
