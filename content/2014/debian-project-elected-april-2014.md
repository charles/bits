Title: Debian Project elects Javier Merino Cacho as Project Leader
Date: 2014-04-01 12:25
Tags: dpl, vote
Slug: debian-project-elected-april-2014
Author: Francesca Ciceri and Ana Guerrero Lopez
Status: published

**This post was an April Fools' Day joke.**

![Alt Red Nose DPL](|static|/images/red_nose_dpl.png)

In accordance with its constitution, the Debian Project has just elected
Javier Merino Cacho as Debian Project Leader. More than 80% of voters put
him as their first choice (or equal first) on their ballot papers.

Javier's large majority over his opponents shows how his inspiring vision for
the future of the Debian project is largely shared by the other developers.
[Lucas Nussbaum](https://www.debian.org/vote/2014/platforms/lucas) and [Neil
McGovern](https://www.debian.org/vote/2014/platforms/neilm) also gained a lot
of support from Debian project members, both coming many votes ahead of the
None of the above ballot choice.

Javier has been a Debian Developer since February 2012 and, among other
packages, works on keeping the mercurial package under control, as mercury is
very poisonous for trouts.

After it was announced that he had won this year's election, Javier said: *I'm
flattered by the trust that Debian members have put in me. One of the main
points in my platform is to remove the "Debian is old and boring" image.  In
order to change that, my first action as DPL is to encourage all Debian
Project Members to wear a clown red nose in public.*

Among others, [the main points from his
platform](http://zoo.zouish.org/vote/vicho.en.html) are mainly
related to improve the communication style in mailing lists through an
innovative filter called *aponygisator*, to make Debian less "old and
boring", as well as solve technical issues among developers with barehanded
fights. Betting on the fights will be not only allowed but encouraged for
fundraising reasons.

Javier also contemplated the use of misleading talk titles such as *The use of
cannabis in contemporary ages: a practical approach* and *Real Madrid vs
Barcelona* to lure new users and contributors to Debian events.

Javier's platform was collaboratively written by a team of communication
experts and high profile Debian contributors during the last DebConf. It has
since evolved thanks to the help of many other contributors.
