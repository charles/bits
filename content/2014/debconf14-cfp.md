Title: DebConf14 - Call for talks, BoFs and events
Date: 2014-06-08 22:30
Tags: debconf14, cfp
Slug: debconf14-cfp
Author: Ana Guerrero Lopez
Status: published

The Debian project is excited to announce that we are now accepting
presentations, discussion sessions and tutorials for our [DebConf14][dc14-web]
conference which will take place in **Portland State University, Oregon, USA**
from 23 to 31 August.

## Submitting an event

To submit an event, first [register as an attendee][dc14-reg] for DebConf14 in
the conference management system. If you have any doubts or have problems with
the registration process please check the [Registration FAQ][dc14-reg-faq].

After registering, go to [the event submission page][dc14-sub-event], or click
on the *Create an event* option from the management system. Describe your
submission in the web form. The most common event types are *Lecture* or
*Open Discussion (BoF)*. Please include a short title (to make it easy to
produce a compact schedule) and an engaging description of the event.

## Tracks

We will organize some talks into thematic tracks. If you have a proposal for a
DebConf track, such as *"Debian ARM"*, *"Debian Infrastructure"*, or
*"Community Outreach"* please contact
[talks@debconf.org](mailto:talks@debconf.org).

If you would like to be a track coordinator, please volunteer on the given
mail address.

## Format of the events

A regular session will be 45 minutes long, including time for questions.
There will be a 15 minute breaks between events.

Submissions are not limited to traditional talks: you could propose a
performance, art installation, debate, or anything else. If you have any
specific requirements for your event, please send an email to
[talks@debconf.org](mailto:talks@debconf.org) with the details of
your requirements and be sure to mention your event title in the subject.

## Deadline

While we ask speakers to submit their events **before the deadline of 7 July
2014, 23:59:59 UTC**, late submissions will continue to be accepted for
scheduling until the end of DebConf. All attendees will have an opportunity to
schedule ad-hoc events during DebConf itself if we have space for them. Very
promising late submissions may be considered for inclusion in the main
conference. Note that ad-hoc events have a much lower chance of video
archiving, and streaming, so if you want these services it's better to get
your submissions in early.

DebConf official events will be broadcast live on the Internet when possible,
and videos of the talks will be published on the web along with the
presentation slides and papers.

For private communication regarding your talk, or for more general ideas,
or questions about the event and talks, please
[mail us](mailto:talks@debconf.org)

We hope to you see you and share some good times with you this year in Portland
during DebConf14!

[dc14-web]: http://debconf14.debconf.org/
[dc14-reg]: http://debconf14.debconf.org/registration.xhtml
[dc14-sub-event]: https://summit.debconf.org/debconf14/propose_meeting/
[dc14-reg-faq]: https://wiki.debconf.org/wiki/DebConf14/RegistrationFAQ
