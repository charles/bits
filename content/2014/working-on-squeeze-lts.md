Title: Working in a possible LTS for Debian Squeeze
Date: 2014-03-20 11:15
Tags: squeeze, security
Slug: working-on-squeeze-lts
Author: Ana Guerrero Lopez
Status: published

The Debian Security Team announced in the
[bits from their last meeting](https://lists.debian.org/debian-devel-announce/2014/03/msg00004.html)
that they're considering to ask for the addition of a new suite to provide long
term support (LTS) for Squeeze.

Quoting their announcement:

* It needs to be pointed out that for this effort to be sustainable
  actual contributions by interested parties are required. squeeze-lts
  is not something that will magically fall from the sky. If you're
  dependent/interested in extended security support you should make an
  effort to contribute, either by contributing on your own or by
  paying a Debian developer/consultant to contribute for you.
  The security team itself is driving the effort, NOT doing it. Some team
  members will contribute to it individually, however.

* Anyone interested in contributing, please get in touch with
  [team@security.debian.org](mailto:team@security.debian.org). We'll setup
  an initial coordination list with all interested parties. All policies /
  exact work will be sorted out there.

This means this is still not something settled and **if you or your company is
insterested in joining this effort, you should contact the security team
ASAP**.  So they can see if there is enough people and resources to launch the
LTS.
