Title: Debian Project Leader elections 2015
Date: 2015-03-12 11:00
Tags: dpl, vote
Slug: dpl-elections-2015
Author: Ana Guerrero Lopez
Status: published

It's that time of year again for the Debian Project: [the elections of its
Project Leader](https://www.debian.org/vote/2015/vote_001)!  Starting on April
1st, and during the following two weeks, the Debian Developers will vote to
choose the person who will guide the project for one year. The results will
be published on April 15th and the term for new the project leader will
start on April 17th, 2015.

Lucas Nussbaum who has held the office for the last two years won't be seeking
reelection this year and Debian Developers will have to choose between three
candidates:

* [Mehdi Dogguy](https://www.debian.org/vote/2015/platforms/mehdi)
* [Gergely Nagy](https://www.debian.org/vote/2015/platforms/algernon)
* [Neil McGovern](https://www.debian.org/vote/2015/platforms/neilm)

Gergely Nagy and Neil McGovern previously ran for DPL in past years;
it's the first run for Mehdi Dogguy.

The campaigning period started today and will last until March 31st. The
candidates are expected to engage in debates and discussions on the
[debian-vote mailing list](http://lists.debian.org/debian-vote/) where they'll
reply to questions from users and contributors.
