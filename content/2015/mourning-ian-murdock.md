Title: Debian mourns the passing of Ian Murdock
Date: 2015-12-30 20:15:00
Tags: ian murdock, in memoriam
Slug: mourning-ian-murdock
Author: Ana Guerrero Lopez, Donald Norwood and Paul Tagliamonte
Status: published

![Ian Murdock](|static|/images/ianmurdock.jpg)

With a heavy heart Debian mourns the passing of Ian Murdock, stalwart
proponent of Free Open Source Software, Father, Son, and the 'ian' in Debian.

Ian started the Debian project in August of 1993, releasing the first versions
of Debian later that same year. Debian would go on to become the world's
Universal Operating System, running on everything from embedded devices to
the space station.

Ian's sharp focus was on creating a Distribution and community culture that
did the right thing, be it ethically, or technically. Releases went out when
they were ready, and the project's staunch stance on Software Freedom are the
gold standards in the Free and Open Source world.

Ian's devotion to the right thing guided his work, both in Debian and in the
subsequent years, always working towards the best possible future.

Ian's dream has lived on, the Debian community remains incredibly active, with
thousands of developers working untold hours to bring the world a reliable and
secure operating system.

The thoughts of the Debian Community are with Ian's family in this hard time.

His family has asked for privacy during  this difficult time and we very much
wish to respect that. Within our Debian and the larger Linux community
condolences may be sent to  in-memoriam-ian@debian.org where they will be kept
and archived.
