Title: Nuevos desarrolladores y mantenedores de Debian (septiembre y octubre del 2015)
Slug: new-developers-2015-10
Date: 2015-11-11 22:35
Author: Jean-Pierre Giraud
Tags: project
Lang: es
Status: published

Los siguientes colaboradores del proyecto se convirtieron en Debian Developers
en los dos últimos meses:

* ChangZhuo Chen (czchen)
* Eugene Zhukov (eugene)
* Hugo Lefeuvre (hle)
* Milan Kupcevic (milan)
* Timo Weingärtner (tiwe)
* Uwe Kleine-König (ukleinek)
* Bernhard Schmidt (berni)
* Stein Magnus Jodal (jodal)
* Prach Pongpanich (prach)
* Markus Koschany (apo)
* Andy Simpkins (rattustrattus)

Los siguientes colaboradores del proyecto se convirtieron en Debian Maintainers
en los dos últimos meses:

* Miguel A. Colón Vélez
* Afif Elghraoui
* Bastien Roucariès
* Carsten Schoenert
* Tomasz Nitecki
* Christoph Ulrich Scholler
* Mechtilde Stehmann
* Alexandre Viau
* Daniele Tricoli
* Russell Sim
* Benda Xu
* Andrew Kelley
* Ivan Udovichenko
* Shih-Yuan Lee
* Edward Betts
* Punit Agrawal
* Andreas Boll
* Dave Hibberd
* Alexandre Detiste
* Marcio de Souza Oliveira
* Andrew Ayer
* Alf Gaida

¡Felicidades a todos!
