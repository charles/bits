Title: Debian 深切哀悼 Ian Murdock 的離世
Date: 2015-12-30 20:15:00
Tags: ian murdock, in memoriam, 悼念
Lang: zh-TW
Slug: mourning-ian-murdock
Author: Ana Guerrero Lopez, Donald Norwood 和 Paul Tagliamonte
Translator: 聞其詳 (bootingman), Anthony Fok, Anthony Wong
Status: published

![Ian Murdock](|static|/images/ianmurdock.jpg)

懷著一顆沉重的心，Debian 同仁深切哀悼剛剛離我們而去的 Ian Murdock，一位為自由開放原始碼軟體而矢志不渝的創導者，同時也身為人父、身為人子，更是 Debian 中的 “ian”。

Ian 在 1993 年 8 月創立了「Debian 計劃」，並在同年釋出了 Debian 的多個初始版本。從此，Debian 逐步成為了流行世界、讓大眾受惠的通用作業系統；無論是嵌入式裝置還是國際太空站，我們都能看到它的身影。

Ian 所專注的，是要創造一種「擇善固執」、堅持「做正確的事」的發行版和社群文化；在道德上如此，在技術上亦然。Debian 每个版本「必先準備就緒才䆁出」的力臻完美，以及 Debian 捍衛「軟體自由」的堅定立場，都是自由及開放原始碼軟體世界的黃金標準。

Ian 的「擇善固執」，這份對「正確的事」的堅持和奉獻，一直引導著他的工作，不管是在 Debian 還是在後來的年月裡，一直伴隨他朝著最好的未來而奮鬥。

Ian 的夢想繼續發揚光大，Debian 社群依舊非常活躍，成千上萬的開發人員奉獻數不盡的日日夜夜，帶給世界一個安全可靠的作業系統。

在這個傷痛的時刻，Debian 社群眾人謹此向 Ian 的家人寄以哀思和慰問；願我們的思緒陪伴著他們，一起共度難關。

他的家人也請求大家，在這段艱難的時期，留給他們一些隱私空間，我們對此非常尊重，同時也號召大家尊重 Ian 家人的意願。各位來自 Debian 社群以及廣大 Linux 社群的朋友，請將你們的慰問寄送到 <span style="white-space: nowrap;">[in-memoriam-ian@debian.org](mailto:in-memoriam-ian@debian.org)，所有唁函將被永遠保存。

該郵箱有效期至 2016 年 1 月底。其後，「Debian 計劃」將會把唁函轉交給 Ian 的家屬保管。如果得到 Ian 家屬的同意，唁函內容將於今年稍後公開。

&nbsp;

----

> 中文版原譯者：聞其詳 (bootingman)；黃彦邦 (Anthony Wong)
>
> * [〈Debian 社区发表悼念声明，缅怀创始人 Ian Murdock〉](http://www.linuxstory.org/debian-mourns-the-passing-of-ian-murdock/)——[聞其詳 (bootingman)](http://www.bootingman.org/about/) 譯
> * [〈Debian 哀悼 Ian Murdock 離世〉](http://blog.anthonywong.net/2016/01/08/debian-%E5%93%80%E6%82%BC-ian-murdock-%E9%9B%A2%E4%B8%96/)——[黃彦邦 (Anthony Wong)](http://blog.anthonywong.net/) 譯
>
> 合併及修訂：Debian 中文小組
