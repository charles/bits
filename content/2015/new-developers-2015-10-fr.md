Title: Nouveaux développeurs et mainteneurs de Debian (septembre et octobre 2015)
Slug: new-developers-2015-10
Date: 2015-11-11 22:35
Author: Jean-Pierre Giraud
Tags: project
Lang: fr
Status: published

Les contributeurs suivants sont devenus développeurs Debian ces deux
derniers mois :

* ChangZhuo Chen (czchen)
* Eugene Zhukov (eugene)
* Hugo Lefeuvre (hle)
* Milan Kupcevic (milan)
* Timo Weingärtner (tiwe)
* Uwe Kleine-König (ukleinek)
* Bernhard Schmidt (berni)
* Stein Magnus Jodal (jodal)
* Prach Pongpanich (prach)
* Markus Koschany (apo)
* Andy Simpkins (rattustrattus)

et ces contributeurs ont été acceptés comme mainteneurs Debian durant la
même période :

* Miguel A. Colón Vélez
* Afif Elghraoui
* Bastien Roucariès
* Carsten Schoenert
* Tomasz Nitecki
* Christoph Ulrich Scholler
* Mechtilde Stehmann
* Alexandre Viau
* Daniele Tricoli
* Russell Sim
* Benda Xu
* Andrew Kelley
* Ivan Udovichenko
* Shih-Yuan Lee
* Edward Betts
* Punit Agrawal
* Andreas Boll
* Dave Hibberd
* Alexandre Detiste
* Marcio de Souza Oliveira
* Andrew Ayer
* Alf Gaida

Félicitations !
