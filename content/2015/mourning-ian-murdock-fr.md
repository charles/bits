Title: Debian pleure la disparition de Ian Murdock
Date: 2015-12-30 20:15:00
Tags: ian murdock, in memoriam
Slug: mourning-ian-murdock
Author: Ana Guerrero Lopez, Donald Norwood et Paul Tagliamonte
Translator: Jean-Pierre Giraud
Lang: fr
Status: published

![Ian Murdock](|static|/images/ianmurdock.jpg)

C'est avec une grande tristesse que Debian déplore la disparition de Ian
Murdock, ardent défenseur du logiciel libre, un père, un fils, et le « ian »
de Debian.

Ian a lancé le projet Debian en août 1993, publiant les premières versions de
Debian plus tard la même année. Debian allait devenir le système
d'exploitation universel à travers le monde entier, fonctionnant sur toutes
les machines, depuis les systèmes embarqués à la station spatiale.

Ian s'était concentré sur la création d'une distribution et d'une culture de
communauté qui prendrait les bonnes décisions aussi bien du point de vue
éthique que du point de vue technique. Publier seulement lorsque c'est prêt
et une position ferme du projet sur la liberté du logiciel sont les règles
d'or dans le monde du logiciel libre et à source ouvert.

L'attachement à bien faire de Ian a guidé son travail aussi bien dans Debian
que les années suivantes, œuvrant toujours pour le meilleur avenir possible.

Le rêve de Ian se perpétue, la communauté Debian demeure incroyablement
active, avec des milliers de développeurs travaillant un nombre incalculable
d'heures pour offrir au monde un système d'exploitation stable et sûr.

Les pensées de la communauté Debian accompagnent la famille de Ian dans ces
moments pénibles.

Sa famille a souhaité la plus grande discrétion pendant ces moments difficiles
et nous respectons cette volonté. Dans notre communauté Debian et plus
largement dans la communauté Linux, les condoléances peuvent être envoyées à
in-memoriam-ian@debian.org, où elles seront conservées et archivées.
