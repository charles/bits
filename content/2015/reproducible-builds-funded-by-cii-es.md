Title: Las compilaciones reproducibles obtienen fondos de la «Core Infrastructure Initiative»
Slug: reproducible-builds-funded-by-cii
Date: 2015-06-23 14:00
Author: Ana Guerrero Lopez
Translator: Daniel Cialdella
Tags: debian, cii, reproducible builds
Lang: es
Status: published

La «Core Infrastructre Initiative» («Iniciativa de Infraestructura Central»)
[anunció][lf-announce] hoy que
dará soporte a dos desarrolladores Debian, Holger Levsen y Jérémy
Bobbio, con 200.000 dólares estadounidenses para avanzar en su trabajo
en Debian en compilaciones reproducibles y colaborar más estrechamente con
otras distribuciones como Fedora, Ubuntu, OpenWrt para que se
beneficien de su esfuerzo.

[lf-announce]:http://www.linuxfoundation.org/news-media/announcements/2015/06/linux-foundation-s-core-infrastructure-initiative-funds-three-new

La [«Core Infrastructure Initiative» (CII)][cii-link] se creó en 2014
para fortalecer la seguridad en proyectos clave de software libre.
La iniciativa está compuesta por más de 20 empresas y gestionada
por la Fundación Linux.

[cii-link]: http://www.linuxfoundation.org/programs/core-infrastructure-initiative

La iniciativa [compilaciones reproducibles][rb] trata de lograr que todos
puedan reproducir bit a bit cada paquete binario de una fuente dada,
permitiendo que cualquiera verifique de manera independiente que un
programa binario coincide con el programa fuente del que se dice que
deriva. Como ejemplo, esto permite que los usuarios de Debian construyan los
paquetes y obtengan exactamente el mismo paquete que el provisto por
los repositorios Debian.

[rb]:https://wiki.debian.org/ReproducibleBuilds
