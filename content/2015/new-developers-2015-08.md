Title: New Debian Developers and Maintainers (July and August 2015)
Slug: new-developers-2015-08
Date: 2015-09-01 13:45
Author: Ana Guerrero López
Tags: project
Status: published

The following contributors got their Debian Developer accounts in the last two
months:

* Gianfranco Costamagna (locutusofborg)
* Graham Inggs (ginggs)
* Ximin Luo (infinity0)
* Christian Kastner (ckk)
* Tianon Gravi (tianon)
* Iain R. Learmonth (irl)
* Laura Arjona Reina (larjona)

The following contributors were added as Debian Maintainers in the last two
months:

* Senthil Kumaran
* Riley Baird
* Robie Basak
* Alex Muntada
* Johan Van de Wauw
* Benjamin Barenblat
* Paul Novotny
* Jose Luis Rivero
* Chris Knadle
* Lennart Weller

Congratulations!
