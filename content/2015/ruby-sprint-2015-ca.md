Title: Esprint del 2015 de l'equip Debian Ruby
Slug: ruby-sprint-2015
Date: 2015-05-12 00:01
Author: Antonio Terceiro
Translator: Adrià García-Alzórriz, Lluís Gili
Tags: ruby, sprint, paris, irill
Lang: ca
Status: published

L'equip de [Ruby de Debian][d-ruby] va fer el seu primer esprint el 2014.
L'experiència va ser molt positiva i es va decidir fer-lo de nou
el 2015. El passat abril, l'equip es va trobar un cop més a les
oficines d'[IRILL][irill] a París, França.

[d-ruby]:https://wiki.debian.org/Teams/Ruby
[irill]:http://www.irill.org/

Els participants van treballar per millorar la qualitat dels paquets
de Ruby a Debian, incloent-hi la resolució d'errors crítics i de
seguretat, millorant-ne les metadades i l'empaquetament de codi, i
classificant els errors de les proves fetes al servei d' [Integració
Contínua de Debian][d-ci].

[d-ci]:https://ci.debian.net/

L'esprint també va servir per tal que l'equip preparés la
infraestructura per la futura versió 9 de Debian:

- l'ajudant d'empaquetament `gem2deb` per millorar la generació
  semiautomàtica de paquets font des d'uns paquets de Ruby compatibles
  amb les [gemmes de Ruby][rubygems].

[rubygems]:https://rubygems.org/

- també van haver-hi forces per preparar el canvi a Ruby 2.2, l'última
  versió estable del llenguatge Ruby, el qual es va publicar després
  que la recopilació de Debian testing es congelés per passar a la
  versió 8 de Debian.

![Foto de grup dels participants a l'esprint. D'esquerra a dreta:
Christian Hofstaedtler, Tomasz Nitecki, Sebastien Badia i Antonio
Terceiro](|static|/images/ruby-sprint-2015.jpg)

D'esquerra a dreta: Christian Hofstaedtler, Tomasz Nitecki, Sebastien
Badia i Antonio Terceiro.

Van [publicar][report] un informe complert amb els detalls tècnics a
les llistes de Debian relacionades.

[report]:https://lists.debian.org/debian-ruby/2015/05/msg00024.html
