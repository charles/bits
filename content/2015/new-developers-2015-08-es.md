Title: Nuevos desarrolladores y mantenedores de Debian (julio y agosto del 2015)
Slug: new-developers-2015-08
Date: 2015-09-01 13:45
Author: Ana Guerrero López
Tags: project
Lang: es
Status: published

Los siguientes colaboradores del proyecto se convirtieron en Debian Developers
en los dos últimos meses:

* Gianfranco Costamagna (locutusofborg)
* Graham Inggs (ginggs)
* Ximin Luo (infinity0)
* Christian Kastner (ckk)
* Tianon Gravi (tianon)
* Iain R. Learmonth (irl)
* Laura Arjona Reina (larjona)

Los siguientes colaboradores del proyecto se convirtieron en Debian Maintainers
en los dos últimos meses:

* Senthil Kumaran
* Riley Baird
* Robie Basak
* Alex Muntada
* Johan Van de Wauw
* Benjamin Barenblat
* Paul Novotny
* Jose Luis Rivero
* Chris Knadle
* Lennart Weller

¡Felicidades a todos!
