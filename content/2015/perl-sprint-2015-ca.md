Title: Esprint de Debian del 2015
Slug: perl-sprint-2015
Date: 2015-07-13 21:00
Author: Alex Muntada
Translator: Adrià García-Alzórriz, Lluís Gili
Tags: perl, sprint, barcelona
Lang: ca
Status: published

L'equip de [Perl de Debian][d-perl] va fer el seu [primer
esprint][sprint] al maig i va ser un èxit: 7 membres es van trobar a
Barcelona el cap de setmana del 22 al 24 de maig per començar el
desenvolupament per Stretch i treballar en tasques de QA al més de 3000
paquets que manté l'equip.

[d-perl]:https://wiki.debian.org/Teams/DebianPerlGroup
[sprint]:https://wiki.debian.org/Sprints/2015/DebianPerlSprint

Tot i que els participants van gaudir d'un temps meravellós i de molt
menjar, també van van fer una bona quantitat de feina:

* Es van tractar 53 bugs, entre filtrats i treballats, i es van
  acceptar 31 pujades.
* Es va discutir sobre l'actual pràctica de gestió de pegats (`quilt`)
  i es van mostrar possibles alternatives (git-debcherry i git-dpm).
* Es van fer millores a les «Debian Perl Tools» (`dpt`) i es va
  discutir sobre com seguir la pista i l'històric de les noves pujades de
  git i les etiquetes.
* Es van revisar i posar al dia les directives de l'equip,
  documentació i tasques recurrents.
* Es va preparar la versió de Perl 5.22 i es van discutir els plans per
  `src:perl` a Stretch.
* Es van revisar les llistes blanques d'`autopkgtest`, afegir nous
  paquets, i es van discutir les notificacions a IRC de KGB.
* Es van revisar migracions excepcionals.
* Es van la possibilitat de reproduir incidències amb `POD_MAN_DATE`.

S'ha publicat l'[informe complert][report] a les llistes de Debian relacionades.

[report]:https://lists.debian.org/debian-perl/2015/07/msg00009.html

Els participants voldríem agrair al [Departament d'Arquitectura de
Computadors][upc-dac] de la Universitat Politècnica de Catalunya per
allotjar-nos, així com també a tots els donants al projecte Debian els
quals han ajudat a cobrir la major part de les nostres despeses.

[upc-dac]:http://www.ac.upc.edu/
