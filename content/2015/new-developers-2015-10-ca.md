Title: Nous desenvolupadors i mantenidors de Debian (setembre i octubre del 2015)
Slug: new-developers-2015-10
Date: 2015-11-11 22:35
Author: Jean-Pierre Giraud
Translator: Adrià García-Alzórriz
Tags: project
Lang: ca
Status: published

Els següents col·laboradors del projecte han esdevingut Debian Developers
en els darrers dos mesos:

* ChangZhuo Chen (czchen)
* Eugene Zhukov (eugene)
* Hugo Lefeuvre (hle)
* Milan Kupcevic (milan)
* Timo Weingärtner (tiwe)
* Uwe Kleine-König (ukleinek)
* Bernhard Schmidt (berni)
* Stein Magnus Jodal (jodal)
* Prach Pongpanich (prach)
* Markus Koschany (apo)
* Andy Simpkins (rattustrattus)

Els següents col·laboradors del projecte han esdevingut Debian Maintainers
en els darrers dos mesos:

* Miguel A. Colón Vélez
* Afif Elghraoui
* Bastien Roucariès
* Carsten Schoenert
* Tomasz Nitecki
* Christoph Ulrich Scholler
* Mechtilde Stehmann
* Alexandre Viau
* Daniele Tricoli
* Russell Sim
* Benda Xu
* Andrew Kelley
* Ivan Udovichenko
* Shih-Yuan Lee
* Edward Betts
* Punit Agrawal
* Andreas Boll
* Dave Hibberd
* Alexandre Detiste
* Marcio de Souza Oliveira
* Andrew Ayer
* Alf Gaida

Enhorabona a tots!
