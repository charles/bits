Title: S'ha publicat Debian 8.0 Jessie!
Date: 2015-04-26 03:15
Tags: jessie
Slug: jessie-released
Author: Ana Guerrero Lopez
Translator: Adrià García-Alzórriz, Innocent De Marchi, Lluís Gili
Lang: ca
Status: published

![Alt S'ha publicat Jessie](|static|/images/banner_jessie.png)

Hi ha un nou *sheriff* a la ciutat. I el seu nom és Jessie. Estem
contents d'anunciar la publicació de Debian 8.0, amb nom en clau
*Jessie*.

**Voleu instal·lar-la?**
Escolliu el vostre [medi d'instal·lació](https://www.debian.org/distrib/)
d'entre discos Blu-ray, DVDs, CDs i memòries USB. Després llegiu el
[manual d'instal·lació](https://www.debian.org/releases/jessie/installmanual).
Per als usuaris del núvol, Debian també els ofereix
[imatges preconstruïdes per a OpenStack](http://cdimage.debian.org/cdimage/openstack/current/)
llestes per al seu ús.

**Ja sou un feliç usuari de Debian i només voleu actualitzar-vos?**
Només heu de fer un *apt-get dist-upgrade* des de Jessie!
Trobeu com llegint la [guia d'instal·lació](https://www.debian.org/releases/jessie/installmanual)
i les [notes de la versió](https://www.debian.org/releases/jessie/releasenotes).

**Voleu celebrar la publicació?**
Compartiu el [banner des d'aquest blog](https://wiki.debian.org/DebianArt/Themes/Lines#Release_blog_banner.2Fbutton) al vostre lloc o racó d'Internet!
