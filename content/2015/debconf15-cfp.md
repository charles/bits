Title: DebConf15: Call for Proposals
Date: 2015-03-12 22:20
Tags: debconf15, cfp
Slug: debconf15-cfp
Author: Ana Guerrero Lopez
Status: published

The DebConf Content team is pleased to announce the Call for Proposals for the
DebConf15 conference, to be held in **Heidelberg, Germany** from the 15th
through the 22nd of August, 2015.

## Submitting an Event

In order to submit an event, you must be
[registered as an attendee of DebConf15][dc15-reg].If you have any questions
about the registration process, please check the related
[information on the conference website][dc15-reginfo].

Once registered, go to *"Propose an event"* and describe your proposal. Please
note, events are not limited to traditional presentations or informal sessions
(BoFs). We welcome submissions of tutorials, performances, art installations,
debates, or any other format of event that you think would be beneficial to the
Debian community.

Please include a short title, suitable for a compact schedule, and an engaging
description of the event. You should use the field *"Notes for Content Team"*
to provide us information such as additional speakers, scheduling restrictions,
or any special requirements we should consider for your event.

Regular sessions may either be 20 or 45 minutes long (including time for
questions) and will be followed by a 10 or 15 minutes break, respectively.
Other kinds of sessions (like workshops) could have different durations.
Please make sure to choose the most suitable duration for your event and
justify any special requests.

## Timeline

The first batch of accepted proposals will be announced in May. If you depend
on having your proposal accepted in order to attend the conference, please
submit it as soon as possible so that it can be considered during this first
evaluation period.

All proposals must be submitted before **June 15th, 2015** to be evaluated for
the official schedule.

## Topics and Tracks

Though we invite proposals on any Debian or FLOSS related subject, we will have
some broad topics arranged as tracks for which we encourage people to submit
proposals. The currently proposed list is:

* Debian Packaging, Policy, and Infrastructure
* Security, Safety, and Hacking
* Debian System Administration, Automation and Orchestration
* Containers and Cloud Computing with Debian
* Debian Success Stories
* Debian in the Social, Ethical, Legal, and Political Context
* Blends, Subprojects, Derivatives, and Projects using Debian
* Embedded Debian and Hardware-Level Systems

If you have ideas for further tracks, or would like to volunteer as a track
coordinator, please contact [content@debconf.org](mailto:content@debconf.org).
In order for a track to take place during DebConf15, we must have received a
sufficient amount of proposals on that specific theme. Track coordinators will
play an important role in inviting people to submit events.

## Video Coverage

Providing video of sessions amplifies DebConf achievements and is one of the
[conference goals][dc15-goals]. Unless speakers opt-out, official events will
be streamed live over the Internet to promote remote participation. Recordings
will be published later under the [DebConf license][dc15-video-license], as
well as presentation slides and papers whenever available.

## Contact and Thanks to Sponsors

DebConf would not be possible without the generous support of all our sponsors,
especially our platinum sponsor [HP][HP]. DebConf15 is still accepting
sponsors; if you are interested, please [get in touch][dc15-sponsoring]!

You are welcome to contact the Content Team with any concerns about your event,
or with any ideas or questions ambout DebConf events in general. You can reach
us at [content@debconf.org](mailto:content@debconf.org).

We hope to see you all in Heidelberg!

[dc15-reg]: https://summit.debconf.org/debconf15/registration/
[dc15-reginfo]: http://debconf15.debconf.org/registration.xhtml
[dc15-goals]: http://debconf.org/goals.shtml
[dc15-video-license]: http://meetings-archive.debian.net/pub/debian-meetings/LICENSE
[dc15-sponsoring]: http://debconf15.debconf.org/sponsors.xhtml
[HP]: http://www.hp.com
