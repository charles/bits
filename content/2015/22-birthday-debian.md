Title: Debian turns 22!
Date: 2015-08-16 23:59
Tags: birthday, debian
Slug: 22-birthday-debian
Author: Ana Guerrero Lopez and Valessio Brito
Status: published

Sorry for posting so late, we're very busy at
[DebConf15](http://debconf15.debconf.org/)!

![Debian 22](|static|/images/debian22.jpg)

Happy 22nd birthday Debian!
