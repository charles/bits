Title: apt install dpl-candidate: Gergely Nagy
Slug: dpl-interview-algernon
Date: 2015-03-14 20:35
Author: Zlatan Todorić
Tags: interviews, dpl, vote, meetDDs
Status: published

**0. Who are you and what's your history with Debian Project?**

I'm a little mouse behind a keyboard, going by the nickname
"algernon". I used to be a lot of things: a [flaming youth][mbk:youth],
an [application manager][mbk:am], [package maintainer][mbk:maint],
upstream, [ftp-assistant][mbk:ftpassist], a student, a
[mentor][mbk:gsoc], a hacker. In the end, however, I am but a simple,
albeit [sometimes crazy][mbk:crazy] person.

[mbk:youth]: https://lists.debian.org/debian-newmaint/2001/10/msg00021.html
[mbk:am]: https://nm.debian.org/public/person/algernon
[mbk:maint]: https://qa.debian.org/developer.php?login=algernon@madhouse-project.org
[mbk:ftpassist]: https://ftp-master.debian.org/#ftpteam
[mbk:gsoc]: https://www.google-melange.com/gsoc/project/details/google/gsoc2013/tichy/5818821692620800
[mbk:crazy]: https://github.com/algernon/HumpY

I did a number of things within Debian - mostly small and marginal
things, mind you. With a little break, I've been here for over a decade,
and am planning to stay for at least another.

**1. What's your most proud moment as Debian Developer?**

At last year's LinuxTag, I was wandering around a stand where they sold
Raspberry Pis (with cases and other accessories). I had a nice chat with
one of the staffers there, inquired about the price (including the case,
of course), and a few other things. He asked a few things back: what
I'll be using it for, and so on. After it turned out that I'm a Debian
Developer, and syslog-ng hacker, he went to the back, and emerged a few
minutes later with a boxed up Pi, and gave it to me as a gift, for
working on Debian.

This was an incredibly touching moment, in many, many ways.

**2. In your opinion what is the strongest part of Debian Project?**

That's hard to say, to be honest. There are a good number of things
Debian is incredibly strong at, and it would be hard to arbitrarily pick
one. Quality, responsibility, safety, predictability are all areas we
are very good at. But those are the qualities of the OS. As a project,
we are remarkably well organised, given the volunteer & distributed
nature of the project.

**3. And what is the weakest part of Debian Project?**

While we can resolve and work with technical issues in a reasonable
manner, the project as a whole is rather lacking in all other areas. To
grow beyond being the creators of the Universal OS, we, as a project,
need to pursue goals beyond the OS.

Being part of GSoC and Outreachy are great steps forward. But we still
have a lot of internal issues that need to be resolved. Areas such as
innovation, team work, where we're in dire need of improvement.

**4. How do you intend to resolve the weakest part?**

As explained in my platform, my primary goal is to remove
roadblocks. The DPL can do very little alone, his time and powers are
better spent on enabling those who have the required skills and desires,
to pursue those.

**5. DPL term lasts for one year - what would you challenge during that
term and what have you learned from previous DPL's?**

The most valuable thing I learned from past DPLs is that the
expectations are sky-high, yet, a significant portion of what the DPL
does is very different than what I imagined in past years.

I'd like to challenge the status quo of the DPL being a nearly full-time
job.

**6. What motivates you to  work in Debian and run for DPL?**

I'm in it for the fame and glory, of course! And because my Tamagotchi
told me to.

But on a more serious tone, my main motivation to work on Debian is
because contributing makes me happy. It satisfies my hunger for doing
useful work. Debian is - in my opinion - the perfect platform to give
back to the wider Free Software community. Similarly, my motivation to
run for DPL is to allow Debian to be a stronger member of that greater
