Title: ¡Debian cumple 22 años!
Date: 2015-08-16 23:59
Tags: birthday, debian
Slug: 22-birthday-debian
Author: Ana Guerrero Lopez y Valessio Brito
Translator: Adrià García-Alzórriz
Lang: es
Status: published

Disculpen el retraso, ¡estamos muy ocupados en la
[DebConf15](http://debconf15.debconf.org/)!

![Debian 22](|static|/images/debian22.jpg)

¡Feliz 22º aniversario Debian!
