Title: ¡Software Freedom Conservancy necesita tu ayuda!
Slug: conservancy-fundraising-campaign
Date: 2015-12-04 00:30
Author: Mehdi Dogguy
Translator: Ana Guerrero López
Lang: es
Tags: sfc, conservancy, fundraising, contributing, donation
Status: published

[![Logo de Software Freedom Conservancy](|static|/images/conservancy-logo.png)](https://sfconservancy.org/)

«*[Software Freedom Conservancy](https://sfconservancy.org/) contribuye
a promover, mejorar, desarrollar y defender proyectos de software
libre y código abierto (FLOSS).*», así es como Software Freedom Conservancy
se define a sí misma. Organizaciones como Conservancy permiten a los
desarrolladores de software enfocarse en lo que se les da mejor mientras
Conservancy se preocupa de hacer cumplir el copyleft, de las cuestiones
legales y proporcionar [numerosos servicios](https://sfconservancy.org/members/services/).

El pasado mes de agosto, Debian y Conservancy
[anunciaron](http://sfconservancy.org/news/2015/aug/17/debian/)
un acuerdo de colaboración y formaron el proyecto de agregación
de «copyright», donde entre otras cosas, Conservancy podrá ser titular
de los derechos de autor para algunas obras de Debian y asegurar el cumplimiento
del copyleft para que estas obras permanezcan como software libre.

Hace poco, Conservancy ha lanzado una gran campaña de recaudación
de fondos y necesita más donantes individuales con el fin de
financiarse de una manera más sostenible e independiente.
Esto permitirá a Conservancy continuar con sus esfuerzos para convencer
a más compañías de cumplir con las licencias de software libre
como la GPL y tomar acciones legales cuando el diálogo no ha funcionado.
Conservancy necesita tu ayuda ahora más que nunca.

Muchos desarrolladores y colaboradores de Debian ya se han
convertido en
[donantes de Conservancy](https://sfconservancy.org/sponsors#supporters).
Por favor, ¡considera subscribirte como donante en
[https://sfconservancy.org/supporter/](https://sfconservancy.org/supporter/)!
