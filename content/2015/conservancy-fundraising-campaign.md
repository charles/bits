Title: Software Freedom Conservancy needs your support!
Slug: conservancy-fundraising-campaign
Date: 2015-12-04 00:30
Author: Mehdi Dogguy
Tags: sfc, conservancy, fundraising, contributing, donation
Status: published

[![Software Freedom Conservancy Logo](|static|/images/conservancy-logo.png)](https://sfconservancy.org/)

"*[Software Freedom Conservancy](https://sfconservancy.org/) helps
promote, improve, develop, and defend Free, Libre, and Open Source
Software (FLOSS) projects.  Conservancy provides a non-profit home and
infrastructure for FLOSS projects.*", that is how Software Freedom
Conservancy defines itself. Organizations like Conservancy allow free
software developers to focus on what they do the best by doing
copyleft enforcement, taking care of legal aspects and provide [many
services](https://sfconservancy.org/members/services/) to its project
members.

Last August, Debian and Conservancy
[announced](http://sfconservancy.org/news/2015/aug/17/debian/) a
partnership and formed the Copyright Aggregation Project where, among
other things, Conservancy will be able to hold copyrights for some
Debian works and ensure compliance with copyleft so that those works
remain in free software.

Recently, Conservancy launched a major fundraising campaign and needs
more individual supporters to gain more sustainable and independent
funding. This will allow the Conservancy to continue its efforts
towards convincing more companies to comply with free software
licenses such as the GPL and take legal actions when dialogue turns
out to be unsuccessful. Conservancy needs your support now, more than
ever!

Many Debian Developers and Contributors have already become
[Conservancy
supporters](https://sfconservancy.org/sponsors#supporters). Please
consider signing up as a supporter on
[https://sfconservancy.org/supporter/](https://sfconservancy.org/supporter/)!
