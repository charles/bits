Title: Nous desenvolupadors i mantenidors de Debian (juliol i agost del 2015)
Slug: new-developers-2015-08
Date: 2015-09-01 13:45
Author: Ana Guerrero López
Translator: Adrià García-Alzórriz
Tags: project
Lang: ca
Status: published

Els següents col·laboradors del projecte han esdevingut Debian Developers
en els darrers dos mesos:

* Gianfranco Costamagna (locutusofborg)
* Graham Inggs (ginggs)
* Ximin Luo (infinity0)
* Christian Kastner (ckk)
* Tianon Gravi (tianon)
* Iain R. Learmonth (irl)
* Laura Arjona Reina (larjona)

Els següents col·laboradors del projecte han esdevingut Debian Maintainers
en els darrers dos mesos:

* Senthil Kumaran
* Riley Baird
* Robie Basak
* Alex Muntada
* Johan Van de Wauw
* Benjamin Barenblat
* Paul Novotny
* Jose Luis Rivero
* Chris Knadle
* Lennart Weller

Enhorabona a tots!
