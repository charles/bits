Title: DebConf14 to be held in Portland, Oregon, USA
Date: 2013-04-08 00:01
Tags: debconf14, announce
Slug: debconf14-in-portland-usa
Author: Patty Langasek
Status: published

We are pleased to announce the 15th annual Debian Conference (DebConf14) is to
be held in Portland, Oregon, USA in August 2014, with specific dates yet to be
announced.

Portland is an open source hotspot in the Pacific Northwest of the US.  It is a
technologically savvy community, home to Intel and the adopted home of Linus
Torvalds.  The city plays host to many Free Software conferences including
[OSCON](http://www.oscon.com), and is where [Linux
Plumbers](http://www.linuxplumbersconf.org) originated.

The local team has been involved in mulitple DebConfs in the past, and is
excited to bring their experience and ideas to fruition in a city
well-positioned to host such a prestigious event.
