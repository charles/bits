Title: Martin Michlmayr gets the O'Reilly Open Source Award
Date: 2013-07-30 14:00
Tags: announce, award
Slug: tbm_gets_open_source_award
Author: Ana Guerrero Lopez
Status: published

Longtime Debian Developer [Martin Michlmayr was named as one of 6 winners of
the 2013 O’Reilly Open Source
Awards](http://www.oscon.com/oscon2013/public/schedule/detail/29956). This
Award recognize individual contributors who have demonstrated exceptional
leadership, creativity, and collaboration in the development of Open Source
Software.

Martin received the award for his investment in Debian where he served as
Debian Project Leader for two terms between 2003 and 2005.

[![Alt Martin Michlmayr gets the O'Reilly Open Source
Award](|static|/images/flickr_oreilly_9374340924.jpg)](http://www.flickr.com/photos/oreillyconf/9374340924/in/photostream/)

Congratulations tbm!
