Title: Debian joins Free & Open Source Software Outreach Program for Women
Date: 2013-04-08 08:01
Tags: gsoc, gnome, OPW, announce, development, diversity, software, code, projects, women
Slug: joining-opw
Author:  Ana Guerrero
Status: published

The GNOME Foundation started the [Free & Open Source Software Outreach
Program for Women, OPW](http://live.gnome.org/OutreachProgramForWomen),
in 2010. In the January-April 2013 round, many other FOSS organizations
joined the program. We are happy to announce that Debian will also join
in the next round from June-September and we'll offer one internship.

You can find more details about Debian's participation in the program
at [Debian OPW page](http://wiki.debian.org/OutreachProgramForWomen).

## Call for mentors and projects

OPW allows applicants to work on any kind of project, including coding,
design, marketing, web development... The [Debian Google Summer of Code
projects](http://wiki.debian.org/SummerOfCode2013/Projects) will be offered
also as possible projects for OPW, but GSoC only allows coding projects. If you
have any idea of a non-coding project and you want to mentor it, please contact
us in the [soc-coordination mailing
list](http://lists.alioth.debian.org/mailman/listinfo/soc-coordination) adding
[OPW] in subject.

OPW works in the same way as GSoC except Google doesn't play a part here.
The same advice that is provided for GSoC mentors works for OPW mentors.

## Call for participants

The main goal of this program is to increase the number of women in
FOSS, so all women who are not yet a Debian Developer or a Debian
Maintainer are encouraged to apply. There are no age restrictions and
applicants don't need to be a student.

If you want to apply, you must follow three steps:

1. Choose a project [from this
   list](http://wiki.debian.org/OutreachProgramForWomen). There are two lists,
   one for GSoC and another with non-coding tasks that can be only offered by
   the OPW. Those lists may change and add or remove more projects in the next
   few weeks.

2. Make a small contribution to Debian. Projects will add a task the
   applicant must complete as part of the pre-selection process. If no task
   is provided, you are welcome to ask the mentors of the project. You can
   also make a different extra task of the one listed to show your skills
   and interest.

3. Create a page in the Debian wiki with your application. You can do so under
   pseudonym, but in that case, please give us information about yourself
   privately by email to the coordinators listed in the [Debian OPW
   page](http://wiki.debian.org/OutreachProgramForWomen)!
