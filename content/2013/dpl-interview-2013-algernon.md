Title: Debian Project Leader elections 2013: interview with Gergely Nagy
Date: 2013-03-27 18:50
Tags: interviews, dpl, vote, meetDDs
Slug: dpl-interview-2013-algernon
Author: Francesca Ciceri
Status: published

We have asked Gergely Nagy, one of the three candidates for
[DPL elections 2013](|filename|candidates-for-dpl-2013.md),
to tell our readers about himself and his ideas for the Debian Project.

You can also read the interviews to the other two candidates:
[Lucas Nussbaum](|filename|dpl-interview-2013-lucas.md)
and [Moray Allan](|filename|dpl-interview-2013-moray.md).

----

**Please tell us a little about yourself.**

I was born in Hungary, a little bit over three decades ago, as a son of
a biochemist and a pharmacist, who gave me the name Gergely Nagy
(however, online - and offline too by now - I'm mostly known by my
nickname, algernon).

I went on to study human arts (hungarian grammar & literature, in
particular), and to support this passion, I work as a software engineer,
one who gets paid to work on free software. As such, I'm in a fortunate
situation where my hobby supports my passion, and my hobby aligns well
with my Debian work too.

**What do you do in Debian and how did you started contributing?**

At the moment, apart from maintaining a few packages, I'm doing a few
other, mostly invisible things, like reassigning misfiled bugs so they
don't end up being forgotten; or review newly uploaded packages before
they enter the archive, making sure we are allowed to distribute them,
and that their quality is up to our standards. I used to do quite a lot
of other things, but I chose to spend the past year mostly invisible,
learning.

I started contributing by packaging an editor I was using at the time,
but quickly ended up adopting another package - things escalated from
there quickly.

**Why did you decide to run as DPL?**

There were two reasons that motivated me to run: one is that I believe I
can bring something new to the table, that I can help Debian expand in
new directions. The other reason is that I'm always on the lookout for
new ways to contribute back to Debian, and being the project leader is a
position where I believe I could contribute most at this point in time.

**Three keywords to summarise your platform.**

Non-technical contributors.

**What are the biggest challenges that you envision for Debian in the future?**

The biggest challenge is growing up, to become more than a group of
computer geeks creating an amazing distribution. To become a community
of a wide variety of people, where both computer geeks and art geeks
feel equally at home. Yet, at the same time, where we as a project, keep
our focus straight, and be the champions of Free Software.

We just need to realize that there's much more to Free Software than
the software itself.

**What are, in your opinion, the areas of the project more in need of
technical and/or social improvements?**

I believe that while we do have many areas where we could use technical
improvements, we are reasonably safe there, because we do have very
skilled technical people to help us solve these problems. We can make
our tools better, we can develop our infrastructure better to aid us
even more - and so on and so forth. While we need work on many areas,
we're on the right track there.

However, when it comes to social issues, we're at a loss. We have
serious trouble keeping certain topics civilised on mailing lists, we
have trouble attracting women, and we have trouble reaching people who
are not naturally exposed to Debian (or Free Software). We could really
use a more diverse community, but that requires us to overcome quite a
lot of social roadblocks, so to say. Outreach is one particular area
where we need much more technical and social improvements.

**Why should people vote for you?**

People should vote me, because they found my platform, my answers on
debian-vote@, and my ideas and goals convincing and worthy to
pursue. People should vote me, because they trust I'll be able to serve
the project well.

**Name three tools you couldn't stay without.**

Emacs, git and a pencil. Because with these three, I can pretty much do
anything.

**What keep you motivated to work in Debian?**

The community. Over the years, I had the good fortune to meet with a lot
of people I hold in high esteem, whose enthusiasm and motivation I found
inspiring. For any other common goals Debian and I may share, in the
end, it is the people within Debian that keep me motivated.

**Are there any other fields where you call yourself a geek, besides computers?**

I'm not quite there yet, but I'm working hard on becoming a human arts
geek, or at least a geek of the hungarian language.
