Title: Call for participants in Debian for the Google Summer of Code
Date: 2013-04-09 21:30
Tags: announce, gsoc
Slug: call-for-students-gsoc-2013
Author: Nicolas Dandrimont
Status: published

The [Google Summer of
Code](http://www.google-melange.com/gsoc/homepage/google/gsoc2013) is a program
that allows post-secondary students aged 18 and older to earn a stipend writing
code for Free and Open Source Software projects during the summer.

For the eighth time, Debian has just been accepted as a mentoring organization
for this year's program. If you're an [eligible
student](http://www.google-melange.com/gsoc/document/show/gsoc_program/google/gsoc2013/help_page#2._Whos_eligible_to_participate_as_a),
now is the time to take a look at our [project ideas
list](http://wiki.debian.org/SummerOfCode2013/Projects), engage with the
mentors for the projects you find interesting, and start working on [your
application](http://wiki.debian.org/SummerOfCode2013/StudentApplicationTemplate)!
Please read the
[FAQ](http://www.google-melange.com/gsoc/document/show/gsoc_program/google/gsoc2013/help_page)
and the [Program
Timeline](http://www.google-melange.com/gsoc/document/show/gsoc_program/google/gsoc2013/help_page#2._What_is_the_program_timeline)
on Google's website.

If you are interested, we encourage you to come and chat with us on irc
([#debian-soc](irc://irc.oftc.net/debian-soc) on irc.oftc.net), or to send an
email to the [SoC coordination
mailing-list](mailto:soc-coordination@lists.alioth.debian.org). Most of the
GSoC related information in Debian can be found on [our wiki
pages](http://wiki.debian.org/SummerOfCode2013), but feel free to ask us
directly on irc or via email.

We're looking forward to work with amazing students again this summer!
