Title: Remove unofficial debian-multimedia.org repository from your sources
Date: 2013-06-14 00:10
Tags: announce
Slug: remove-debian-multimedia
Author: Publicity team
Status: published

The **unofficial** third party repository Debian Multimedia stopped using the
domain debian-multimedia.org some months ago. The domain expired and it is now
registered again by someone unknown to Debian. (If we're wrong on this point,
please sent us an email so we can take over the domain! ;) )

This means that **the repository is no longer safe to use, and you should
remove the related entries from your sources.list file**.

After all, the need of an external repository for multimedia related packages
has been greatly reduced with the release of Wheezy, [which features many new
and updated codecs and multimedia
players](https://www.debian.org/releases/wheezy/amd64/release-notes/ch-whats-new.en.html#multimedia).

Not sure if you're using the debian-multimedia repository? You can easily check
it by running:

`grep -i debian-multimedia.org /etc/apt/sources.list /etc/apt/sources.list.d/*`

If you can see debian-multimedia.org line in output, you should remove all the
lines including it.
