Title: Debian Project Leader elections 2013: interview with Lucas Nussbaum
Date: 2013-03-27 18:52
Tags: interviews, dpl, vote, meetDDs
Slug: dpl-interview-2013-lucas
Author: Francesca Ciceri
Status: published

We have asked Lucas Nussbaum, one of the three candidates for
[DPL elections 2013](|filename|candidates-for-dpl-2013.md),
to tell our readers about himself and his ideas for the Debian Project.

You can also read the interviews to the other two candidates:
[Gergely Nagy](|filename|dpl-interview-2013-algernon.md)
and [Moray Allan](|filename|dpl-interview-2013-moray.md).

----

**Please tell us a little about yourself.**

Hi! I'm a 31 years old french computer geek. In my day job, I'm an assistant
professor (Maître de Conférences) of Computer Science at Université de
Lorraine.

**What do you do in Debian and how did you started contributing?**

Like many, I started contributing to Debian by creating and maintaining
packages for my own software, in the Ruby team.
Then, I discovered that, even if it's not so obvious from the outside, there
are a lot of areas in Debian that could use more contributors. So I just
started to contribute to more and more things.

There's a list of things I did in Debian in my [platform](https://www.debian.org/vote/2013/platforms/lucas).
What I have been doing recently is:

- rebuild all packages in Debian on a regular basis in order to identify
  packages that can no longer be built, and file bugs accordingly. In order to
  do that efficiently, I use cluster and cloud resources ([more info](http://www.lucas-nussbaum.net/blog/?p=718))

- develop and maintain [Ultimate Debian Database](http://udd.debian.org), a
  data aggregator that collects data in most Debian services so that it is
  possible to expose it in interesting ways (e.g. find release-critical bugs
  affecting popular packages).

- write and maintain a [Debian Packaging tutorial](https://www.debian.org/doc/devel-manuals#packaging-tutorial),
  ([packaging-tutorial package](http://packages.debian.org/sid/packaging-tutorial)),
  to provide an easy-to-read introduction to packaging in Debian.

**Why did you decide to run as DPL?**

Two main reasons:

- Most of my Debian contributions aim at addressing problems at the
  distribution scale (cross-distro collaboration, Quality Assurance,
  data-mining). Being DPL is a great way to contribute to Debian at this
  level.

- the DPL campaign is a great time in Debian where we discuss the project's
  problems, politics and visions. Being a candidate is in itself a way to
  contribute to Debian (though it would be better if we had those discussions
  outside DPL campaigns too).

**Three keywords to summarise your platform.**

(re-)make Debian the center of the Free Software ecosystem; foster innovation
inside Debian; reduce barriers to contributions

**What are the biggest challenges that you envision for Debian in the future?**

I often have the impression that the project is losing momentum,
positive energy, and slowing down. It feels like we are living on the benefits
of the past. A lot of very cool things happen in the Debian ecosystem, but
very often outside the Debian project (in derivative distributions).

Debian should aim at reinforcing its position in the center of the Free
Software ecosystem: it should be the main active intermediary between upstream
projects and final users. To achieve that, we need to reinforce the visibility
and the impact of Debian. This is extremely important because the values we
fight for as a project are often neglected by our derivatives.

**What are, in your opinion, the areas of the project more in need
of technical and/or social improvements?**

Fostering innovation inside Debian: we should be more welcoming towards
innovation and experiments inside the project. Often, we merely tolerate them,
and bureaucracy makes them hard and slow to conduct. As a result, people tends
to innovate outside the Debian project.

Making it easier to contribute to Debian: we compete with more and more
projects to attract contributors. While we are already quite good at welcoming
new contributors with good documentation and mentoring (much better than
people usually think), there's still a lot of room for improvement.

**Why should people vote for you?**

A great thing in Debian's voting system is that you don't vote "for" or
"against" a specific candidate. Instead, due to our use of the
[Condorcet method](http://en.wikipedia.org/wiki/Condorcet_method), you
rank candidates (and also indicate those who you consider suitable for the
role by ranking a virtual "None of the above" candidate).

Why am I a good candidate? My previous contributions to Debian show that I
have a pretty good understanding of the inner workings of the project, and
that I have a track record of managing projects successfully inside Debian.
I think that those are two required qualities for a DPL.

**Name three tools you couldn't stay without.**

vim, mutt, ssh.

**What keep you motivated to work in Debian?**

Debian is a fantastic project from a technical point of view (focus on
technical excellence, lots of interesting challenges), but also from a social
point of view: the Debian community is a great community where I have lots of
good friends. Also, what's great when you contribute to Debian is that your
work has a real impact, and that you see people using stuff you worked on
everywhere.

**Are there any other fields where you call yourself a geek, besides computers?**

I'm not sure this really qualifies as "besides computers", but I've gotten
very interested in the OpenStreetMap project lately. I very much enjoy
exploring unmapped areas on a mountain bike. It feels like being Christopher
Columbus or Marco Polo, but 20 minutes from home. ;) The OpenStreetMap and
Debian projects also share many values, such as a great attention to quality and
details.
