Title: Debian GNU/Hurd 2013 released!
Date: 2013-05-22 22:50
Tags: announce, hurd
Slug: debian-gnu-hurd-wheezy
Author: Ana Guerrero
Status: published

It is with huge pleasure that the Debian GNU/Hurd team announces the **release
of Debian GNU/Hurd 2013**. This is a snapshot of Debian "sid" at the time of
the Debian "wheezy" release (May 2013), so it is mostly based on the same
sources. It is **not** an official Debian release, but it is an official Debian
GNU/Hurd port release.

The installation ISO images can be downloaded from [Debian
Ports](http://ftp.ports.debian.org/debian-ports-cd/hurd-i386/current/) in the
usual three Debian flavors: NETINST, CD, DVD. Besides the friendly Debian
installer, a pre-installed disk image is also available, making it even easier
to try Debian GNU/Hurd.

Debian GNU/Hurd is currently available for the i386 architecture with more than
10.000 software packages available (more than 75% of the Debian archive, and
more to come!).

Please make sure to read the [configuration
information](https://www.debian.org/ports/hurd/hurd-install), the
[FAQ](https://www.gnu.org/software/hurd/faq.html), and the [translator
primer](https://www.gnu.org/software/hurd/hurd/documentation/translator_primer.html)
to get a grasp of the great features of GNU/Hurd.

Due to the very small number of developers, our progress of the project has not
been as fast as other successful operating systems, but we believe to have
reached [a very decent
state](https://www.gnu.org/software/hurd/hurd/status.html), even with our
limited resources.

We would like to thank all the people who have worked on GNU/Hurd [over the
past decades](https://www.gnu.org/software/hurd/history.html). There were not
many people at any given time (and still not many people today, please
[join](https://www.gnu.org/software/hurd/contributing.html)!), but in the end a
lot of people have contributed one way or another. **Thanks everybody!**

*This article appeared originally at [GNU Hurd
news](https://www.gnu.org/software/hurd/news/2013-05-debian_gnu_hurd_2013.html)
and in [News about Debian
GNU/Hurd](https://www.debian.org/ports/hurd/hurd-news.en.html)*.
