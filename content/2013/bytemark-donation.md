Title: Improvements in Debian's core infrastructure
Date: 2013-04-04 10:00
Tags: announce, donation
Slug: bytemark-donation
Author: Ana Guerrero
Status: published

Thanks to a generous donation by [Bytemark Hosting][bytemark], Debian
started deploying machines for its core infrastructure services in a
new data center in York, UK.

This hardware and hosting donation will allow the [Debian Systems
Administration (DSA) team][dsa] to distribute Debian's core services across a
greater number of geographically diverse locations, and improve, in
particular, the fault-tolerance and availability of end-user facing services.
Additionally, the storage component of this donation will dramatically reduce
the storage challenges that Debian currently faces.

The hardware provided by [Bytemark Hosting][bytemark] consists of a
fully-populated [HP C7000 BladeSystem
chassis](http://h18004.www1.hp.com/products/blades/components/enclosures/c-class/c7000/)
containing 16 server blades:

* 12 BL495cG5 blades with 2x Opteron 2347 and 64GB RAM each
* 4 BL465cG7 blades with 2x Opteron 6100 series and 128GB RAM each

and several HP Modular Storage Arrays:

* 3 MSA2012sa
* 6 MSA2000 expansion shelves

with 108 drive bays in total, mostly 500GB SATA drives, some 2TB, some 600GB
15kRPM SAS, providing a total of 57 TB.

57 TB today could host roughly 80 times the current [Debian
archive](https://www.debian.org/mirror/size) or 3 times the [Debian
Snapshot](http://snapshot.debian.org/) archive. But remember both archives are
constantly growing!

[debian]: https://www.debian.org "Debian - The Universal Operating System"
[bytemark]: http://bytemark.co.uk "Bytemark Hosting"
[dsa]: http://dsa.debian.org "Debian Systems Administration (DSA) team"
