Title: Release date for Wheezy announced
Date: 2013-04-24 21:04
Tags: wheezy
Slug: release-date
Author: Francesca Ciceri
Status: published

![Alt Wheezy is coming](|static|/images/gotwheezy.jpg)

Neil McGovern, on behalf of the Debian Release Team, announced
the target date of the weekend of 4th/5th May for the release
of Debian 7.0 "Wheezy".

Now it's time to organize some
[Wheezy release parties](http://wiki.debian.org/ReleasePartyWheezy)
 to celebrate the event and show all your Debian love!
