Title: DPL election is over, congratulations Lucas Nussbaum!
Date: 2013-04-14 12:25
Tags: dpl
Slug: results-dpl-election-2013
Author: Ana Guerrero
Status: published

The Debian Project Leader election has concluded and the winner is Lucas
Nussbaum. Of a total of 988 developers, 390 developers voted using the
[Condorcet method](http://en.wikipedia.org/wiki/Condorcet_method).

More information about the result is available in the [Debian Project Leader
Elections 2013 page](https://www.debian.org/vote/2013/vote_001).

The new term for the project leader will start on April 17th and expire on
April 17th 2014.
