Title: all Debian source are belong to us
Date: 2013-07-02 18:50
Tags: debian, mirrors, announce, infrastructure, technical, sources
Slug: introducing_sources.debian.net
Author:  Ana Guerrero
Status: published

**This is a verbatim repost from [Stefano Zacchiroli's
post](http://upsilon.cc/~zack/blog/posts/2013/07/introducing_sources.debian.net/)**

**TL;DR**: go to **<http://sources.debian.net>** and enjoy.

----

[**Debsources**](https://salsa.debian.org/qa/debsources) is a
new toy I've been working on at [IRILL](http://www.irill.org) together with
[Matthieu Caneill](http://matthieu-blog.fr/). In essence, debsources is a
simple web application that allows to **publish an unpacked Debian source
mirror on the Web**.

You can deploy Debsources where you please, but there is a main instance at
**<http://sources.debian.net>** (`sources.d.n` for short) that you will
probably find interesting. `sources.d.n` follows closely the Debian archive in
two ways:

1. it is updated 4 times a day to reflect the content of the Debian archive
2. it contains sources coming from official Debian suites: the usual ones
   (from oldstable to experimental), `*-updates` (ex volatile),
   `*-proposed-updates`, and `*-backports` (from Wheezy on)

Via `sources.d.n` you can therefore browse the content of Debian source
packages with usual code viewing features like **syntax highlighting**. More
interestingly, you can **search through** the source code (of unstable only,
though) via integration with <http://codesearch.debian.net>. You can also use
`sources.d.n` programmatically to
[query available versions](http://sources.debian.net/doc/api/) or
[**link to specific lines**](http://sources.debian.net/doc/url/), with the
possibility of adding contextual **pop-up messages**
([example](http://sources.debian.net/src/cowsay/3.03%2Bdfsg1-4/cowsay?hl=22:28&msg=22:Cowsay:See?%20Cowsay%20variables%20are%20declared%20here.#L22)).

In fact, you might have stumbled upon `sources.d.n` already in the past few
days, via other popular Debian services where it has already been integrated.
In particular: `codesearch.d.n` now defaults to show results via `sources.d.n`,
and the [PTS](http://packages.qa.debian.org/) has grown new "browse source
code" hyperlinks that point to it. If you've ideas of other Debian services
where `sources.d.n` should be integrated, please let me know.

I find Debsources and `sources.d.n` already quite useful but, as it often
happens, there is still a lot
**TODO**.
Obviously, it is all Free Software (released under GNU AGPLv3). Do not hesitate
to report new bugs and, better, to submit patches for the outstanding ones.

## Acknowledgements

* [Matthieu Caneill](http://matthieu-blog.fr/) is the main developer of
  Debsources web front-end; `sources.d.n` wouldn't exist without him.
* others have already contributed patches to integrate `sources.d.n` with other
  services, in particular:
    * many thanks to Michael Stapelberg (for `codesearch.d.n` integration), and
    * Paul Wise (for PTS integration).
* a full list of
  [contributors](https://salsa.debian.org/qa/debsources/blob/master/AUTHORS)
  is available and eagerly waiting for new additions
* [IRILL](http://www.irill.org) has kindly provided sponsoring for Matthieu's
  initial development work on Debsources, and offered both the server and
  hosting facilities that power `sources.d.n`

**PS** in case you were wondering: at present `sources.d.n` requires **~381
GB** of disk space to hold all uncompressed source packages, plus ~83 GB for
the local (compressed) source mirror
