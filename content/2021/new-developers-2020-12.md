Title: New Debian Maintainers (November and December 2020)
Slug: new-developers-2020-12
Date: 2021-01-22 18:00
Author: Jean-Pierre Giraud
Tags: project
Lang: en
Status: published

The following contributors were added as Debian Maintainers in the last two
months:

* Timo Röhling
* Fabio Augusto De Muzio Tobich
* Arun Kumar Pariyar
* Francis Murtagh
* William Desportes
* Robin Gustafsson
* Nicholas Guriev

Congratulations!
