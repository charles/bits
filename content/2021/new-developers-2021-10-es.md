Title: Nuevos desarrolladores y mantenedores de Debian (septiembre y octubre del 2021)
Slug: new-developers-2021-10
Date: 2021-11-19 13:00
Author: Jean-Pierre Giraud
Tags: project
Lang: es
Status: published

Los siguientes colaboradores del proyecto se convirtieron en Debian Developers
en los dos últimos meses:

* Bastian Germann (bage)
* Gürkan Myczko (tar)

Los siguientes colaboradores del proyecto se convirtieron en Debian Maintainers
en los dos últimos meses:

* Clay Stan
* Daniel Milde
* David da Silva Polverari
* Sunday Cletus Nkwuda
* Ma Aiguo
* Sakirnth Nagarasa

¡Felicidades a todos!
