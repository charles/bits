Title: Novos(as) desenvolvedores(as) e mantenedores(as) Debian (setembro e outubro de 2021)
Slug: new-developers-2021-10
Date: 2021-11-19 13:00
Author: Jean-Pierre Giraud
Tags: project
Lang: pt
Status: published

Os(As) seguintes colaboradores(as) do projeto se tornaram Desenvolvedores(as)
Debian nos últimos dois meses:

* Bastian Germann (bage)
* Gürkan Myczko (tar)

Os(As) seguintes colaboradores(as) do projeto se tornaram Mantenedores(as)
Debian nos últimos dois meses:

* Clay Stan
* Daniel Milde
* David da Silva Polverari
* Sunday Cletus Nkwuda
* Ma Aiguo
* Sakirnth Nagarasa

Parabéns a todos!
