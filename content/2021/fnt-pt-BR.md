Title: 2.000 fontes para o Debian
Slug: 2000-fonts-debian
Date: 2021-12-17 18:45
Author: Gürkan Myczko
Artist: Gürkan Myczko
Tags: fonts, typography, artwork
Translator: Thiago Pezzo (tico), Paulo Henrique de Lima Santana (phls)
Lang: pt-BR
Status: published

![fnt](|static|/images/fnt-poster.png)

O Debian vem com toneladas de fontes para todos os propósitos,
você pode facilmente listá-las (quase) todas com: `apt-cache search ^fonts-`.

No entanto, às vezes elas não estão em sua versão mais recente ou, como usuário(a), você gostaria
de obter acesso a novas fontes que ainda não estão presentes no Debian estável (stable).

Com a ferramenta [`fnt`](https://github.com/alexmyczko/fnt) você pode visualizar e instalar facilmente
fontes do Debian sid e do Google Web Fonts (cerca de 2.000 fontes que são compatíveis com DSFG).
Qualquer usuário(a) pode usar a ferramenta para instalar fontes apenas para o(a) próprio(a) usuário(a), ou
o(a) administrador(a) do sistema pode executá-lo como root para instalar as fontes em todo o sistema.

O [pacote `fnt`](https://packages.debian.org/bookworm/fnt) já está no Bookworm,
então se você usa o Debian teste (testing) pode obter, testar e usar muitas fontes que estão a caminho
de serem empacotadas no Debian:

[ITP #973779](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=973779)   `fnt install scheherazadenew`  
[RFP #944140](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=944140)   `fnt install arsenal`  
[RFP #757249](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=757249)   `fnt install ekmukta`  
[RFP #724629](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=724629)   `fnt install firasans`  
[RFP #766211](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=766211)   `fnt install orbitron`  
[RFP #803690](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=803690)   `fnt install pompiere`  
[RFP #754784](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=754784)   `fnt install raleway`  
[RFP #827735](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=827735)   `fnt install reemkufi`  
[RFP #986999](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=986999)   `fnt install redhat{display,mono,text}`  

Se quiser saber mais, dê uma olhada na página wiki sobre fontes
([https://wiki.debian.org/Fonts](https://wiki.debian.org/Fonts)),
e se quiser contribuir ou manter fontes no Debian,
não hesite em se juntar à [Equipe de Fontes](https://wiki.debian.org/Teams/pkg-fonts)!
