Title: Nya Debianutvecklare och Debian Maintainers (Juli och Augusti 2021)
Slug: new-developers-2021-08
Date: 2021-09-13 17:00
Author: Jean-Pierre Giraud
Tags: project
Lang: sv
Translator: Andreas Rönnquist
Status: published

Följande bidragslämnare fick sina Debianutvecklarkonton under de senaste två
månaderna:

* Aloïs Micard (creekorful)
* Sophie Brun (sophieb)

Följande bidragslämnare adderades som Debian Maintainers under de senaste två
månaderna:

* Douglas Andrew Torrance
* Marcel Fourné
* Marcos Talau
* Sebastian Geiger

Grattis!
