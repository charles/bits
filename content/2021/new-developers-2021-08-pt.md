Title: Novos(as) desenvolvedores(as) e mantenedores(as) Debian (julho e agosto de 2021)
Slug: new-developers-2021-08
Date: 2021-09-13 17:00
Author: Jean-Pierre Giraud
Tags: project
Lang: pt
Status: published

Os(As) seguintes colaboradores(as) do projeto se tornaram Desenvolvedores(as)
Debian nos últimos dois meses:

* Aloïs Micard (creekorful)
* Sophie Brun (sophieb)

Os(As) seguintes colaboradores(as) do projeto se tornaram Mantenedores(as)Debian nos últimos dois meses:

* Douglas Andrew Torrance
* Marcel Fourné
* Marcos Talau
* Sebastian Geiger

Parabéns a todos!
