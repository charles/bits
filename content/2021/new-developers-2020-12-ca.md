Title: Nous mantenidors de Debian (novembre i desembre del 2020)
Slug: new-developers-2020-12
Date: 2021-01-22 18:00
Author: Jean-Pierre Giraud
Tags: project
Lang: ca
Status: published

Els següents col·laboradors del projecte han esdevingut Debian Maintainers en
els darrers dos mesos:

* Timo Röhling
* Fabio Augusto De Muzio Tobich
* Arun Kumar Pariyar
* Francis Murtagh
* William Desportes
* Robin Gustafsson
* Nicholas Guriev

Enhorabona a tots!
