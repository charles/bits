Title: Nouveaux développeurs et mainteneurs de Debian (juillet et août 2021)
Slug: new-developers-2021-08
Date: 2021-09-13 17:00
Author: Jean-Pierre Giraud
Tags: project
Lang: fr
Status: published

Les contributeurs suivants sont devenus développeurs Debian ces deux derniers
mois :

* Aloïs Micard (creekorful)
* Sophie Brun (sophieb)

et ces contributeurs ont été acceptés comme mainteneurs Debian durant la même
période :

* Douglas Andrew Torrance
* Marcel Fourné
* Marcos Talau
* Sebastian Geiger

Félicitations !
