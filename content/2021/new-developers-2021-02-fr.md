Title: Nouveaux développeurs et mainteneurs de Debian (janvier et février 2021)
Slug: new-developers-2021-02
Date: 2021-03-23 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: fr
Status: published

Les contributeurs suivants sont devenus développeurs Debian ces deux derniers
mois :

* Nicholas D. Steeves (sten)
* Nilesh Patra (nilesh)
* David Suárez Rodríguez (deiv)
* Pierre Gruet (pgt)

et ces contributeurs ont été acceptés comme mainteneurs Debian durant la même
période :

* Antonio Valentino
* Boian Nikolaev Bonev
* Filip Hroch
* Maarten L. Hekkelman
* Xialei Qin
* Xiang Gao

Félicitations !
