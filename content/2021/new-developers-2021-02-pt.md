Title: Novos(as) desenvolvedores(as) e mantenedores(as) Debian (janeiro e fevereiro de 2021)
Slug: new-developers-2021-02
Date: 2021-03-23 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: pt
Status: published

Os(As) seguintes colaboradores(as) do projeto se tornaram Desenvolvedores(as)
Debian nos últimos dois meses:

* Nicholas D. Steeves (sten)
* Nilesh Patra (nilesh)
* David Suárez Rodríguez (deiv)
* Pierre Gruet (pgt)

Os(As) seguintes colaboradores(as) do projeto se tornaram Mantenedores(as)
Debian nos últimos dois meses:

* Antonio Valentino
* Boian Nikolaev Bonev
* Filip Hroch
* Maarten L. Hekkelman
* Xialei Qin
* Xiang Gao

Parabéns a todos(as)!
