Title: Nuevos desarrolladores y mantenedores de Debian (marzo y abril del 2021)
Slug: new-developers-2021-04
Date: 2021-05-13 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: es
Status: published

Los siguientes colaboradores del proyecto se convirtieron en Debian Developers
en los dos últimos meses:

* Jeroen Ploemen (jcfp)
* Mark Hindley (leepen)
* Scarlett Moore (sgmoore)
* Baptiste Beauplat (lyknode)

Los siguientes colaboradores del proyecto se convirtieron en Debian Maintainers
en los dos últimos meses:

* Gunnar Ingemar Hjalmarsson
* Stephan Lachnit

¡Felicidades a todos!
