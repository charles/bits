Title: Dia Eu amo Software Livre 2021: mostre o seu amor pelo software livre
Slug: ilovefs-2021
Date: 2021-02-14 11:01
Author: Donald Norwood
Translator: Thiago Pezzo (tico), Paulo Santana (phls)
Lang: pt-BR
Tags: contributing, debian, free software, FSFE
Status: published

[![ILoveFS banner](|static|/images/ilovefs-banner-medium-en.png)][ilovefs-link]

Neste 14 de fevereiro, o Debian se junta à
[Free Software Foundation Europa][fsfe-link] para celebrar o
[dia "I Love Free Software"][ilovefs-link] (Eu amo Software Livre). Neste dia,
valorizamos e aplaudimos todas e todos que contribuem para as muitas áreas do
Software Livre.

O Debian manda todo o seu amor e um gigante "Obrigado" para criadores(as) e
mantenedores(as), upstream e downstream, fornecedores(as) de hospedagem,
parceiros(as) e, claro, para todos(as) os(as) desenvolvedores(as) e
contribuidores(as) Debian.

Obrigado por tudo que fazem ao tornar o Debian o verdadeiro Sistema Operacional
Universal e por fazer e manter o Software Livre livre!

Mostre o seu amor e valorize o Software Livre espalhando a
mensagem e o agradecimento ao redor do mundo. Se você compartilhar nas
mídias sociais, use a hashtag: #ilovefs.

[ilovefs-link]: https://fsfe.org/activities/ilovefs/index.en.html
[fsfe-link]: https://fsfe.org
