Title: Élection du responsable du projet Debian 2021, Jonathan Carter réélu.
Slug: 2021dpl-election
Date: 2021-04-18 10:00
Author: Donald Norwood
Tags: dpl
Lang: fr
Translator: Jean-Pierre Giraud
Status: published

La période de vote et le décompte des votes pour l'élection du responsable
du projet Debian viennent de s'achever et Jonathan Carter a remporté
l'élection !

455 des 1 018 développeurs ont voté en utilisant la
[méthode Condorcet](https://fr.wikipedia.org/wiki/M%C3%A9thode_de_Condorcet).

Plus d'informations sur les résultats du vote sont disponibles sur la page
[Élection du responsable du projet Debian 2021](https://www.debian.org/vote/2021/vote_001).

Un grand merci à Jonathan Carter et Sruthi Chandran pour leur campagne et
aux développeurs Debian pour leur vote.
