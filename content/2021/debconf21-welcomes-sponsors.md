Title: DebConf21 welcomes its sponsors!
Slug: debconf21-welcomes-sponsors
Date: 2021-08-25 19:15
Author: Laura Arjona Reina, Jean-Pierre Giraud
Tags: debconf21, debconf, sponsors
Status: published

![DebConf21 logo](|static|/images/dc21-logo-horizontal-diversity.png)

DebConf21 is taking place online, from 24 August to 28 August 2021.
It is the 22nd Debian conference, and organizers and participants are working
hard together at creating interesting and fruitful events.

We would like to warmly welcome the 19 sponsors of DebConf21, and introduce
them to you.

We have five Platinum sponsors.

Our first Platinum sponsor is [**Lenovo**](https://www.lenovo.com).
As a global technology leader manufacturing a wide portfolio of connected
products, including smartphones, tablets, PCs and workstations as well as
AR/VR devices, smart home/office and data center solutions, Lenovo
understands how critical open systems and platforms are to a connected world.

Our next Platinum sponsor is [**Infomaniak**](https://www.infomaniak.com/en).
Infomaniak is Switzerland's largest web-hosting company,
also offering backup and storage services, solutions for event organizers,
live-streaming and video on demand services.
It wholly owns its datacenters and all elements critical
to the functioning of the services and products provided by the company
(both software and hardware).

[**Roche**](https://code4life.roche.com/) is our third Platinum sponsor.
Roche is a major international pharmaceutical provider and research company
dedicated to personalized healthcare. More than 100,000 employees worldwide
work towards solving some of the greatest challenges for humanity using
science and technology. Roche is strongly involved in publicly funded
collaborative research projects with other industrial and academic partners
and has supported DebConf since 2017.

[**Amazon Web Services (AWS)**](https://aws.amazon.com) is our fourth Platinum
sponsor.
Amazon Web Services is one of the world's
most comprehensive and broadly adopted cloud platform,
offering over 175 fully featured services from data centers globally
(in 77 Availability Zones within 24 geographic regions).
AWS customers include the fastest-growing startups, largest enterprises
and leading government agencies.

[**Google**](https://google.com) is our fifth Platinum sponsor.
Google is one of the largest technology companies in the
world, providing a wide range of Internet-related services and products such
as online advertising technologies, search, cloud computing, software, and
hardware. Google has been supporting Debian by sponsoring DebConf for more than
ten years, and is also a Debian partner sponsoring parts
of [Salsa](https://salsa.debian.org)'s continuous integration infrastructure
within Google Cloud Platform.

Our Gold sponsor is the [**Matanel Foundation**](http://www.matanel.org).
The Matanel Foundation operates in Israel,
as its first concern is to preserve the cohesion of a society and a nation
plagued by divisions. The Matanel Foundation also works in Europe, in Africa
and in South America.

Our Silver sponsors are:
[**arm**](https://www.arm.com/): the World’s Best SoC Design Portfolio,
Arm powered solutions have been supporting innovation for more than 30 years
and are deployed in over 160 billion chips to date,
[**Hudson-Trading**](https://www.hudsonrivertrading.com/),
a company researching and developing automated trading algorithms
using advanced mathematical techniques,
[**Ubuntu**](https://www.canonical.com/)
the Operating System delivered by Canonical,
[**Globo**](https://www.globo.com/),
the largest media conglomerate in Brazil, founded in Rio de Janeiro in 1925
and distributing high-quality content across multiple platforms,
[**Two Sigma**](https://www.twosigma.com/),
rigorous inquiry, data analysis, and invention to help solve the toughest
challenges across financial services
and
[**GitLab**](https://about.gitlab.com/solutions/open-source/),
an open source end-to-end software development platform with built-in version
control, issue tracking, code review, CI/CD, and more.

Bronze sponsors:
[**Univention**](https://www.univention.com/),
[**Gandi.net**](https://www.gandi.net/en),
[**daskeyboard**](https://www.daskeyboard.com/),
[**InterFace AG**](https://www.interface-ag.com/) and
[**credativ**](https://www.credativ.com/).

And finally, our Supporter level sponsor, [**ISG.EE**](https://isg.ee.ethz.ch/).

Thanks to all our sponsors for their support!
Their contributions make it possible for a large number
of Debian contributors from all over the globe to work together,
help and learn from each other in DebConf21.

## Participating in DebConf21 online

The 22nd Debian Conference is being held online, due to COVID-19,
from August 24 to 28, 2021. Talks, discussions, panels and other activities
run from 12:00 to 02:00 UTC.
Visit the DebConf21 website at
[https://debconf21.debconf.org](https://debconf21.debconf.org)
to learn about the complete schedule, watch the live streaming and join the
different communication channels for participating in the conference.
