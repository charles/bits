Title: Nya Debianutvecklare och Debian Maintainers (Maj och Juni 2021)
Slug: new-developers-2021-06
Date: 2021-07-22 15:30
Author: Jean-Pierre Giraud
Tags: project
Lang: sv
Translator: Andreas Rönnquist
Status: published

Följande bidragslämnare fick sina Debianutvecklarkonton under de senaste två
månaderna:

* Timo Röhling (roehling)
* Patrick Franz (deltaone)
* Christian Ehrhardt (paelzer)
* Fabio Augusto De Muzio Tobich (ftobich)
* Taowa (taowa)
* Félix Sipma (felix)
* Étienne Mollier (emollier)
* Daniel Swarbrick (dswarbrick)
* Hanno Wagner (wagner)

Följande bidragslämnare adderades som Debian Maintainers under de senaste två
månaderna:

* Evangelos Ribeiro Tzaras
* Hugh McMaster

Grattis!
