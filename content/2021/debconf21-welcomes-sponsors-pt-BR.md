Title: A DebConf21 dá boas-vindas a seus(suas) patrocinadores(as)!
Slug: debconf21-welcomes-sponsors
Date: 2021-08-25 19:15
Author: Laura Arjona Reina, Jean-Pierre Giraud
Tags: debconf21, debconf, sponsors
Status: published
Lang: pt-BR
Translator: Thiago Pezzo (Tico)

![DebConf21 logo](|static|/images/dc21-logo-horizontal-diversity.png)

A DebConf21 está acontecendo on-line de 24 a 28 de agosto de 2021.
Esta é a 22a. conferência Debian, e organizadores(as) e participantes estão
trabalhando arduamente e unidos(as) para criar eventos interessantes e
frutíferos.

Gostaríamos de calorosamente dar boas-vindas aos(às) 19 patrocinadores(as) da
DebConf21 e apresentá-los(as) a você.

Temos cinco patrocinadores(as) Platinum.

Nossa primeira patrocinadora Platinum é a [**Lenovo**](https://www.lenovo.com).
Como uma líder global em tecnologia, fabricando um amplo portfólio de produtos
conectados, incluindo smartphones, tablets, PCs e workstations, bem como
dispositivos de realidade aumentada/virtual, casas/escritórios inteligentes e
soluções para datacenters, a Lenovo entende quão críticos são os sistemas
abertos e as plataformas para um mundo conectado.

Nossa próxima patrocinadora Platinum é a
[**Infomaniak**](https://www.infomaniak.com/en).
A Infomaniak é a maior empresa de hospedagem web da Suíça,
que também oferece serviços de backup e de armazenamento, soluções
para organizadores(as) de eventos, serviços de transmissão em tempo real e vídeo
sob demanda.
Todos os datacenters e elementos críticos para o funcionamento dos serviços e
produtos oferecidos pela empresa (tanto software quanto hardware) são de
propriedade absoluta da Infomaniak.

A [**Roche**](https://code4life.roche.com/) é nossa terceira patrocinadora
Platinum. A Roche é um grande fornecedor farmacêutico internacional e companhia
de pesquisa, dedicada aos cuidados personalizados com a saúde. Mais de 100.000 funcionários(as) ao redor do mundo trabalham para resolver alguns dos maiores
desafios da humanidade usando ciência e tecnologia. A Roche está envolvida
fortemente em projetos de pesquisa colaborativos financiados publicamente com
outros(as) parceiros(as) industriais e acadêmicos(as), e apoia a DebConf
desde 2017.

A [**Amazon Web Services (AWS)**](https://aws.amazon.com) é nossa quarta
patrocinadora Platinum.
A Amazon Web Services é uma das plataformas
em nuvem mais abrangentes e largamente adotadas no mundo,
oferecendo mais de 175 serviços repletos de funcionalidades via datacenters
globais (em 77 zonas de disponibilidade dentro de 24 regiões geográficas).
Alguns(mas) consumidores(as) da AWS incluem as startups de mais rápido
crescimento, as maiores empresas e as agências governamentais de liderança.

A [**Google**](https://google.com) é nossa terceira patrocinadora Platinum.
A Google é uma das maiores empresas de tecnologia do mundo, fornecendo uma ampla
gama de serviços e produtos relacionados à Internet como tecnologias de
publicidade on-line, pesquisa, computação em nuvem, software e hardware.
A Google tem apoiado o Debian através do patrocínio da DebConf por mais de
dez anos, e é também uma parceira Debian patrocinando partes da infraestrutura
de integração contínua do [Salsa](https://salsa.debian.org) dentro da plataforma
em nuvem da Google.

Nossa patrocinadora Golden é a [**Matanel Foundation**](http://www.matanel.org).
A Matanel Foundation opera em Israel,
e sua primeira preocupação é preservar a coesão da sociedade e da nação
assoladas por divisões. A Matanel Foundation também atua na Europa, na África
e na América do Sul.

Nossos(as) patrocinadores(as) Silver são:
[**arm**](https://www.arm.com/): o melhor portfólio de design SoC do mundo,
as soluções Arm apoiam a inovação por mais de 30 anos
e foram implementadas em mais de 160 bilhões de chips até o momento,
[**Hudson-Trading**](https://www.hudsonrivertrading.com/),
uma companhia que pesquisa e desenvolve algoritmos de comércio automatizados
usando técnicas matemáticas avançadas,
[**Ubuntu**](https://www.canonical.com/)
o sistema operacional fornecido pela Canonical,
[**Globo**](https://www.globo.com/),
o maior conglomerado de mídia do Brasil, fundado no Rio de Janeiro em 1925
e que distribui conteúdo de alta qualidade em diferentes plataformas,
[**Two Sigma**](https://www.twosigma.com/),
pesquisa rigorosa, análise de dados e invenção para auxiliar na resolução dos
desafios mais difíceis dos serviços financeiros
e
[**GitLab**](https://about.gitlab.com/solutions/open-source/),
uma plataforma de desenvolvimento de software completa e de código aberto com
controle de versão embutida, monitoramento de problemas, revisão de código,
CI/CD e muitas outras coisas.

Patrocinadores(as) Bronze:
[**Univention**](https://www.univention.com/),
[**Gandi.net**](https://www.gandi.net/en),
[**daskeyboard**](https://www.daskeyboard.com/),
[**InterFace AG**](https://www.interface-ag.com/) e
[**credativ**](https://www.credativ.com/).

E finalmente, nossa patrocinadora Supporter,
[**ISG.EE**](https://isg.ee.ethz.ch/).

Obrigado(a) a todos(as) os(as) nossos(as) patrocinadores(as) pelo apoio!
Suas contribuições tornaram possível para um grande quantidade
de contribuidores(as) Debian de todo o mundo trabalharem juntos(as),
mutuamente ajudando e aprendendo na DebConf21.

## Participe da DebConf21 on-line

A 22a. Conferência Debian está ocorrendo on-line devido ao COVID-19,
de 24 a 28 de agosto de 2021. Palestras, debates, painéis e outras atividades
acontecem das 12h às 2h UTC (12h às 23h no horário de Brasília)
Visite o site web da DebConf21 em
[https://debconf21.debconf.org](https://debconf21.debconf.org)
para conhecer a programação completa, assistir às lives e juntar-se aos
diferentes canais de comunicação para participar da conferência.
