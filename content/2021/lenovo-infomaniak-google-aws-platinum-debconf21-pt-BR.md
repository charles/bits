Title: Lenovo, Infomaniak, Roche, Amazon Web Services (AWS) e Google, patrocinadores(as) Platinum da DebConf21
Slug: lenovo-infomaniak-roche-aws-google-platinum-debconf21
Date: 2021-08-23 10:30
Author: Laura Arjona Reina, Jean-Pierre Giraud,
Artist: Lenovo, Infomaniak, Roche, AWS, Google
Tags: debconf21, debconf, sponsors, lenovo, infomaniak, roche, aws, google
Lang: pt-BR
Translator: Thiago Pezzo (Tico), Paulo Henrique de Lima Santana (phls)
Status: published

Estamos muito felizes em anunciar que [**Lenovo**](https://www.lenovo.com),
[**Infomaniak**](https://www.infomaniak.com),
[**Roche**](https://code4life.roche.com/),
[**Amazon Web Services (AWS)**](https://aws.amazon.com)
e [**Google**](https://google.com),
se comprometeram a apoiar a [DebConf21](https://debconf21.debconf.org) como
**patrocinadores(as) Platinum**.

[![lenovologo](|static|/images/lenovo.png)](https://www.lenovo.com)

Como uma líder global em tecnologia, fabricando um amplo portfólio de produtos
conectados, incluindo smartphones, tablets, PCs e workstations, bem como
dispositivos de realidade aumentada/virtual, casas/escritórios inteligentes e
soluções para datacenters, a [**Lenovo**](https://www.lenovo.com) entende quão
críticos são os sistemas abertos e as plataformas para um mundo conectado.

[![infomaniaklogo](|static|/images/infomaniak.png)](https://www.infomaniak.com)

[**Infomaniak**](https://www.infomaniak.com) é a maior empresa de hospedagem
web da Suíça, que também oferece serviços de backup e de armazenamento,
soluções para organizadores(as) de eventos, serviços de transmissão em tempo
real e vídeo sob demanda.
Todos os datacenters e elementos críticos para o funcionamento dos serviços e
produtos oferecidos pela empresa (tanto software quanto hardware) são de
propriedade da Infomaniak.

[![rochelogo](|static|/images/roche.png)](https://code4life.roche.com/)

[**Roche**](https://code4life.roche.com/)
Roche é um grande fornecedor farmacêutico internacional e companhia de
pesquisa, dedicada aos cuidados personalizados com a saúde. Mais de 100.000
funcionários(as) ao redor do mundo trabalham para resolver alguns dos maiores
desafios da humanidade usando ciência e tecnologia. A Roche está envolvida
fortemente em projetos de pesquisa colaborativos financiados publicamente com
outros(as) parceiros(as) industriais e acadêmicos(as), e apoia a DebConf desde
2017.

[![AWSlogo](|static|/images/aws.png)](https://aws.amazon.com)

[**Amazon Web Services (AWS)**](https://aws.amazon.com) é uma das plataformas
em nuvem mais abrangentes e largamente adotadas no mundo,
oferecendo mais de 175 serviços repletos de funcionalidades via datacenters
globais (em 77 zonas de disponibilidade dentro de 24 regiões geográficas).
Consumidores(as) da AWS incluem as startups de mais rápido crescimento, as
maiores empresas e as agências governamentais de liderança.

[![Googlelogo](|static|/images/google.png)](https://www.google.com)

[**Google**](https://google.com/) é uma das maiores empresas de tecnologia do
mundo, fornecendo uma ampla gama de serviços e produtos relacionados à Internet
como tecnologias de publicidade on-line, pesquisa, computação em nuvem,
software e hardware.

O Google tem apoiado o Debian através do patrocínio da DebConf por mais de
dez anos, e é também um parceiro Debian que patrocina partes da
infraestrutura de integração contínua do [Salsa](https://salsa.debian.org)
dentro da Plataforma Google Cloud.

Com esses comprometimentos enquanto patrocinadores(as) Platinum,
Lenovo, Infomaniak, Roche, Amazon Web Services e Google estão contribuindo
para tornar possível nossa conferência anual,
e diretamente apoiando o progresso do Debian e do Software Livre,
ajudando a fortalecer a comunidade que continua a colaborar nos
projetos do Debian durante todo o resto do ano.

Muito obrigado(a) pelo seu apoio à DebConf21!

## Participando da DebConf21 on-line

A 22a Conferência Debian acontecerá on-line, devido ao COVID-19,
de 22 a 28 de agosto de 2021. Serão 8 dias de atividades, acontecendo das
10h à 1h UTC (7h às 21h no horário de Brasília).
Visite o site web da DebConf21 em
[https://debconf21.debconf.org](https://debconf21.debconf.org)
para conhecer a programação completa, assistir as transmissões ao vivo e
juntar-se aos diferentes canais de comunicação para participar da conferência.
