Title: Nya Debianutvecklare och Debian Maintainers (Januari och Februari 2021)
Slug: new-developers-2021-02
Date: 2021-03-23 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: sv
Translator: Andreas Rönnquist
Status: published

Följande bidragslämnare fick sina Debianutvecklarkonton under de senaste två
månaderna:

* Nicholas D. Steeves (sten)
* Nilesh Patra (nilesh)
* David Suárez Rodríguez (deiv)
* Pierre Gruet (pgt)

Följande bidragslämnare adderades som Debian Maintainers under de senaste två
månaderna:

* Antonio Valentino
* Boian Nikolaev Bonev
* Filip Hroch
* Maarten L. Hekkelman
* Xialei Qin
* Xiang Gao

Grattis!
