Title: Arduino vuelve a Debian
Slug: arduino-back-in-debian
Date: 2021-02-01 08:00
Author: Rock Storm
Tags: arduino, announce
Lang: es
Translator: Rock Storm
Status: published

El equipo de Debian Electronics se enorgullece en anunciar que la
última versión de Arduino, la que es probablemente la plataforma para
programar micro-controladores AVR más extendendia, está ya
[empaquetada y subida a Debian Inestable][1].

La última versión que estuvo disponible en Debian fue la 1.0.5, que
data de 2013. Han sido años de pruebas y fracasos pero finalmente,
después de meses de gran esfuerzo por parte de Carsten Schoenert y
Rock Storm, ya tenemos un paquete que funciona con la última versión
de Arduino. Tras más de siete años, los usuarios podremos volver a
instalar la IDE de Arduino con tan sólo *"apt install arduino"*.

*"El propósito de este post no es sólo anunciar esta nueva versión
sino además un llamamiento para que se pruebe"* dijo Rock Storm. *"El
título bien podría ser _SE BUSCA: Beta Testers para Arduino_ (vivos o
muertos :P)."*. El equipo de Debian Electronics agradecería que todo
aquel con las herramientas y los conocimientos necesarios probase el
paquete y nos haga saber si encuentra algún problema.

Con este post queremos agradecer a todo el equipo de Debian
Electronics y a todos los colaboradores su contribución al
paquete. Esta hazaña no hubiera sido posible sin ellos.

[1]: https://packages.debian.org/unstable/arduino
