Title: Nya Debian Maintainers (November och December 2020)
Slug: new-developers-2020-12
Date: 2021-01-22 18:00
Author: Jean-Pierre Giraud
Tags: project
Lang: sv
Translator: Andreas Rönnquist
Status: published

Följande bidragslämnare adderades som Debian Maintainers under de senaste två
månaderna:

* Timo Röhling
* Fabio Augusto De Muzio Tobich
* Arun Kumar Pariyar
* Francis Murtagh
* William Desportes
* Robin Gustafsson
* Nicholas Guriev

Grattis!
