Title: Những nhà Phát triển và Bảo trì Debian mới (Tháng ba và tư 2021)
Slug: new-developers-2021-04
Date: 2021-05-13 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: vi
Translator: Trần Ngọc Quân
Status: published

Trong hai tháng qua, những người đóng góp sau đây nhận được tài khoản Phát triển Debian:

* Jeroen Ploemen (jcfp)
* Mark Hindley (leepen)
* Scarlett Moore (sgmoore)
* Baptiste Beauplat (lyknode)

Trong hai tháng qua, những người đóng góp sau đây được thêm vào với tư cách là người Bảo trì Debian:

* Gunnar Ingemar Hjalmarsson
* Stephan Lachnit

Xin chúc mừng!
