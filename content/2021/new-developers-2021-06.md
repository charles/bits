Title: New Debian Developers and Maintainers (May and June 2021)
Slug: new-developers-2021-06
Date: 2021-07-23 15:00
Author: Jean-Pierre Giraud
Tags: project
Lang: en
Status: published

The following contributors got their Debian Developer accounts in the last two months:

* Timo Röhling (roehling)
* Patrick Franz (deltaone)
* Christian Ehrhardt (paelzer)
* Fabio Augusto De Muzio Tobich (ftobich)
* Taowa (taowa)
* Félix Sipma (felix)
* Étienne Mollier (emollier)
* Daniel Swarbrick (dswarbrick)
* Hanno Wagner (wagner)

The following contributors were added as Debian Maintainers in the last two months:

* Evangelos Ribeiro Tzaras
* Hugh McMaster

Congratulations!
