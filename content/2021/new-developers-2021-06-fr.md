Title: Nouveaux développeurs et mainteneurs de Debian (mai et juin 2021)
Slug: new-developers-2021-06
Date: 2021-07-22 15:30
Author: Jean-Pierre Giraud
Tags: project
Lang: fr
Status: published

Les contributeurs suivants sont devenus développeurs Debian ces deux derniers
mois :

* Timo Röhling (roehling)
* Patrick Franz (deltaone)
* Christian Ehrhardt (paelzer)
* Fabio Augusto De Muzio Tobich (ftobich)
* Taowa (taowa)
* Félix Sipma (felix)
* Étienne Mollier (emollier)
* Daniel Swarbrick (dswarbrick)
* Hanno Wagner (wagner)

et ces contributeurs ont été acceptés comme mainteneurs Debian durant la même
période :

* Evangelos Ribeiro Tzaras
* Hugh McMaster

Félicitations !
