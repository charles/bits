Title: Debian User Forums changes and updates.
Slug: debianuserforums
Date: 2021-08-11 13:00
Author: Donald Norwood
Tags: users, forums,
Status: published

![DebianUserForums](|static|/images/DebianUserForumsIndexpage.png)

Several issues were brought before the Debian Community team regarding
responsiveness, tone, and needed software updates to
[forums.debian.net](https://web.archive.org/web/20210627204458/http://forums.debian.net/).
The question was asked,
[‘who’s in charge?’](https://lists.debian.org/debian-project/2021/03/msg00046.html)

Over the course of the discussion several Debian Developers volunteered to help
by providing a presence on the forums from Debian and to assist with the
necessary changes to keep the service up and  running.

We are happy to announce the following changes to the (NEW!)
[forums.debian.net](https://forums.debian.net/), which have and should address
most of the prior concerns with accountability, tone, use, and reliability;

Debian Developers: Paulo Henrique de Lima Santana (phls), Felix Lechner
(lechner), and Donald Norwood (donald) have been added to the forum's Server
and Administration teams.

The server instance is now running directly within Debian's infrastructure.

The forum software and back-end have been updated to the most recent versions
where applicable.

DNS resolves for both IPv4 and IPv6.

SSL/HTTPS are enabled. (It’s 2021!)

New Captcha and Anti-spam systems are in place to thwart spammers, bots, and
to make it easier for humans to register.

New Administrators and Moderation staff were added to provide additional
coverage across the hours and to combine years of experience with forum
operation and Debian usage.

New viewing styles are available for users to choose from, some of which are
ideal for mobile/tablet viewing.

We inadvertently fixed the time issue that the prior forum had of running 11
minutes fast. :)

We have clarified staff roles and staff visibility.

Responsiveness to users on the forums has increased.

Email addresses for mods/admins have been updated and checked for validity, it
has seen direct use and response.

The guidelines for forum use by users and staff have been
[updated](https://forums.debian.net/viewtopic.php?f=20&t=149781).

The Debian COC has been made into a
[Global Announcement](https://forums.debian.net/viewtopic.php?f=11&t=114009)
as an accompanyist to the newly updated guidelines to give the
moderators/administrators an additional rule-set for unruly or unbecoming
behavior.

Some of the discussion areas have been renamed and refocused, along with the
movement of multiple threads to make indexing and searching of the forums
easier.

Many (New!) features and extensions have been added to the forum for ease of
use and modernization, such as a user thanks system and thread hover previews.

There are some server administrative tasks that were upgraded as well which
don't belong on a public list, but we are backing up regularly and secure. :)

We have a few minor details here and there to attend to and the work is
ongoing.

Many Thanks and Appreciation to the Debian System Administrators (DSA) and
Ganneff who took the time to coordinate and assist with the instance, DNS, and
network and server administration minutiae, our helpful DPL Jonathan Carter,
many thanks to the current and prior forum moderators and administrators: Mez,
sunrat, 4D696B65, arochester, and cds60601 for helping with the modifications
and transition, and to the forum users who participated in lots of the
tweaking. All in all this was a large community task and everyone did a
significant part. Thank you!
