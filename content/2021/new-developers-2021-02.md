Title: New Debian Developers and Maintainers (January and February 2021)
Slug: new-developers-2021-02
Date: 2021-03-23 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: en
Status: published

The following contributors got their Debian Developer accounts in the last two
months:

* Nicholas D. Steeves (sten)
* Nilesh Patra (nilesh)
* David Suárez Rodríguez (deiv)
* Pierre Gruet (pgt)

The following contributors were added as Debian Maintainers in the last two
months:

* Antonio Valentino
* Boian Nikolaev Bonev
* Filip Hroch
* Maarten L. Hekkelman
* Xialei Qin
* Xiang Gao

Congratulations!
