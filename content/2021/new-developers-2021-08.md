Title: New Debian Developers and Maintainers (July and August 2021)
Slug: new-developers-2021-08
Date: 2021-09-13 17:00
Author: Jean-Pierre Giraud
Tags: project
Lang: en
Status: published

The following contributors got their Debian Developer accounts in the last two
months:

* Aloïs Micard (creekorful)
* Sophie Brun (sophieb)

The following contributors were added as Debian Maintainers in the last two
months:

* Douglas Andrew Torrance
* Marcel Fourné
* Marcos Talau
* Sebastian Geiger

Congratulations!
