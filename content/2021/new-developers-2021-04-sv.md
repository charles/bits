Title: Nya Debianutvecklare och Debian Maintainers (Mars och April 2021)
Slug: new-developers-2021-04
Date: 2021-05-13 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: sv
Translator: Andreas Rönnquist
Status: published

Följande bidragslämnare fick sina Debianutvecklarkonton under de senaste två
månaderna:

* Jeroen Ploemen (jcfp)
* Mark Hindley (leepen)
* Scarlett Moore (sgmoore)
* Baptiste Beauplat (lyknode)

Följande bidragslämnare adderades som Debian Maintainers under de senaste två
månaderna:

* Gunnar Ingemar Hjalmarsson
* Stephan Lachnit

Grattis!
