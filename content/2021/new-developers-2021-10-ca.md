Title: Nous desenvolupadors i mantenidors de Debian (setembre i octubre del 2021)
Slug: new-developers-2021-10
Date: 2021-11-19 13:00
Author: Jean-Pierre Giraud
Tags: project
Lang: ca
Status: published

Els següents col·laboradors del projecte han esdevingut Debian Developers en
els darrers dos mesos:

* Bastian Germann (bage)
* Gürkan Myczko (tar)

Els següents col·laboradors del projecte han esdevingut Debian Maintainers en
els darrers dos mesos:

* Clay Stan
* Daniel Milde
* David da Silva Polverari
* Sunday Cletus Nkwuda
* Ma Aiguo
* Sakirnth Nagarasa

Enhorabona a tots!
