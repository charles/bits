Title: Termina a DebConf21 on-line
Date: 2021-09-09 15:00
Tags: debconf21, announce, debconf
Slug: debconf21-closes
Author: Laura Arjona Reina and Donald Norwood
Artist: Judit Foglszinger
Lang: pt-BR
Translator: Thiago Pezzo (Tico), Charles Melara
Status: published

[![Foto em grupo da DebConf21 - clique para aumentar](|static|/images/debconf21_group_small.jpg)](https://wiki.debian.org/DebConf/21/GroupPhoto)

No sábado, 28 de agosto de 2021, a conferência anual dos(as)
desenvolvedores(as) e contribuidores(as) Debian chegou ao seu fim.

A DebConf21 realizou-se on-line pela segunda vez devido à pandemia
do coronavírus (COVID-19).

Todas as sessões foram transmitidas, e várias foram as formas de participação:
via mensagens IRC, documentos de texto colaborativos e on-line,
e salas de reunião para conferência por vídeo.

Com 740 participantes registrados(as) de mais de 15 países e um
total de 70 palestras, sessões de debates,
"desconferências" (Birds of a Feather - BoF) e outras atividades,
a [DebConf21](https://debconf21.debconf.org) foi um grande sucesso.

A estrutura, que havia sido montada para os eventos on-line passados,
envolveram Jitsi, OBS, Voctomix, SReview, nginx, Etherpad, e uma interface web
para o Voctomix que foi melhorada e usada com sucesso na DebConf21.
Todos os componentes da infraestrutura de vídeo são softwares livres e
foram configurados através do repositório público do
[ansible](https://salsa.debian.org/debconf-video-team/ansible)
da equipe de vídeo.

A [grade de programação](https://debconf21.debconf.org/schedule/) da DebConf21
incluiu uma grande variedade de eventos, agrupados em diversas trilhas:

* Introdução ao Software Livre e ao Debian,
* Empacotamento, políticas e infraestrutura Debian,
* Administração de sistemas, automação e orquestração,
* Nuvem e contêineres,
* Segurança,
* Comunidade, diversidade, divulgação local e contextos sociais,
* Internacionalização, localização e acessibilidade,
* Embarcados e kernel,
* Debian blends e distribuições derivadas do Debian,
* Debian nas artes e nas ciências,
* entre outras.

As palestras foram transmitidas usando duas salas, e muitas dessas atividades
foram realizadas em diferentes idiomas: telugu, português, malaiala, canarês,
hindi, marati e inglês, o que permitiu que uma audiência diversa desfrutasse e
participasse.

Entre as atividades, a transmissão de vídeo exibiu os(as) patrocinadores(as)
usuais em loop, mas também exibiu clipes adicionais, incluindo fotos de
DebConfs passadas, fatos divertidos sobre o Debian e vídeos curtos enviados
pelos(as) participantes para se comunicarem com seus amigos(as) do Debian.

A equipe de publicidade do Debian fez sua usual «cobertura ao vivo» para
encorajar a participação, com micronotícias anunciando os diferentes eventos.
A equipe DebConf também forneceu diversas
[opções para seguir a programação em dispositivos móveis](https://debconf21.debconf.org/schedule/mobile/).

Para quem não conseguiu participar, a maioria das palestras e sessões já está
disponível através do
[site web do acervo de eventos do Debian](https://meetings-archive.debian.net/pub/debian-meetings/2021/DebConf21/),
e o restante ficará disponível nos próximos dias.

O site web da [DebConf21](https://debconf21.debconf.org)
permanecerá ativo por razões históricas e continuará a oferecer
os links para as apresentações e vídeos das palestras e eventos.

No próximo ano, a [DebConf22](https://wiki.debian.org/DebConf/22) está
planejada para acontecer em Prizren, Kosovo, em julho de 2022.

A DebConf é comprometida com um ambiente seguro e acolhedor para todos(as)
os(as) participantes. Durante a conferência, diversos times (recepção, equipe
de boas-vindas e equipe de comunidade) estiveram disponíveis para auxiliar, de
modo que os(as) participantes tivessem as melhores experiências na conferência,
e para buscar soluções para quaisquer problemas que pudessem surgir.
Veja a
[página web sobre o código de conduta no site da DebConf21](https://debconf21.debconf.org/about/coc/)
para mais detalhes.

O Debian agradece o compromisso de numerosos(as)
[patrocinadores(as)](https://debconf21.debconf.org/sponsors/)
que apoiaram à DebConf21, particularmente nossos(as) patrocinadores(as)
Platinum:
[**Lenovo**](https://www.lenovo.com),
[**Infomaniak**](https://www.infomaniak.com),
[**Roche**](https://code4life.roche.com),
[**Amazon Web Services (AWS)**](https://aws.amazon.com/)
e [**Google**](https://google.com/).

### Sobre o Debian

O Projeto Debian foi fundado em 1993 por Ian Murdock para ser um verdadeiro
projeto comunitário e livre. Desde então, o projeto cresceu e tornou-se um dos
maiores e mais influentes projetos de código aberto. Milhares de
voluntários(as) de todos os cantos do mundo trabalham juntos(as) para criar e
manter o software Debian. Disponível em 70 idiomas, e com suporte para uma gama
imensa de tipos de computadores, o Debian se autodenomina o
_sistema operacional universal_.

### Sobre a DebConf

A DebConf é a conferência dos(as) desenvolvedores(as) do Projeto Debian. Além
de uma completa programação de palestras técnicas, sociais e políticas, a
DebConf fornece uma oportunidade para desenvolvedores(as), contribuidores(as)
e outras pessoas interessadas se encontrarem presencialmente e trabalharem
mais proximamente. Ela acontece anualmente desde 2000 em locais tão variados
como Escócia, Argentina, Bósnia e Herzegóvina, e Brasil. Mais informações
sobre a DebConf estão disponíveis em
[https://debconf.org/](https://debconf.org).

### Sobre a Lenovo

Como uma líder global em tecnologia, fabricando um amplo portfólio de produtos
conectados, incluindo smartphones, tablets, PCs e workstations, bem como
dispositivos de realidade aumentada/virtual, casas/escritórios inteligentes e
soluções para datacenters, a [**Lenovo**](https://www.lenovo.com) entende quão
críticos são os sistemas abertos e as plataformas para um mundo conectado.

### Sobre a Infomaniak

[**Infomaniak**](https://www.infomaniak.com) é a maior empresa de hospedagem
web da Suíça, que também oferece serviços de backup e de armazenamento,
soluções para organizadores(as) de eventos, serviços de transmissão em tempo
real e vídeo sob demanda. Todos os datacenters e elementos críticos para o
funcionamento dos serviços e produtos oferecidos pela empresa (tanto software
quanto hardware) são de propriedade absoluta da Infomaniak.

### Sobre a Roche

[**Roche**](https://code4life.roche.com) é um grande fornecedor farmacêutico
internacional e companhia de pesquisa, dedicada aos cuidados personalizados
com a saúde. Mais de 100.000 funcionários(as) ao redor do mundo trabalham para
resolver alguns dos maiores desafios da humanidade usando ciência e tecnologia.
A Roche está envolvida fortemente em projetos de pesquisa colaborativos
financiados publicamente com outros(as) parceiros(as) industriais e
acadêmicos(as), e apoia a DebConf desde 2017.

### Sobre a Amazon Web Services (AWS)

[**Amazon Web Services (AWS)**](https://aws.amazon.com) é uma das plataformas
em nuvem mais abrangentes e largamente adotadas no mundo,
oferecendo mais de 175 serviços repletos de funcionalidades via datacenters
globais (em 77 zonas de disponibilidade dentro de 24 regiões geográficas).
Alguns(mas) consumidores(as) da AWS incluem as startups de mais rápido
crescimento, as maiores empresas e as agências governamentais de liderança.

### Sobre a Google

[**Google**](https://google.com) é uma das maiores empresas de tecnologia do
mundo, fornecendo uma ampla gama de serviços e produtos relacionados à
Internet como tecnologias de publicidade on-line, pesquisa, computação em
nuvem, software e hardware.

A Google tem apoiado o Debian através do patrocínio da DebConf por mais de
dez anos, e é também uma parceira Debian patrocinando partes da infraestrutura
de integração contínua do [Salsa](https://salsa.debian.org) dentro da
Plataforma em Nuvem da Google.

### Informações para contato

Para mais informações, por favor visite a página web da DebConf21 em
[https://debconf21.debconf.org](https://debconf21.debconf.org)
ou envie um e-mail em inglês para <press@debian.org>.
