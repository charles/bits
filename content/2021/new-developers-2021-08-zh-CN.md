Title: 新的 Debian 开发者和维护者 (2021年7月 至 8月)
Slug: new-developers-2021-08
Date: 2021-09-13 17:00
Author: Jean-Pierre Giraud
Tags: project
Lang: zh-CN
Translator: Boyuan Yang
Status: published

下列贡献者在过去的两个月中获得了他们的 Debian 开发者帐号：

* Aloïs Micard (creekorful)
* Sophie Brun (sophieb)

下列贡献者在过去的两个月中被添加为 Debian 维护者：

* Douglas Andrew Torrance
* Marcel Fourné
* Marcos Talau
* Sebastian Geiger

祝贺他们！
