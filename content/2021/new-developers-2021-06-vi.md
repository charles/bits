Title: Những nhà Phát triển và Bảo trì Debian mới (Tháng năm và sáu 2021)
Slug: new-developers-2021-06
Date: 2021-07-22 15:30
Author: Jean-Pierre Giraud
Tags: project
Lang: vi
Translator: Trần Ngọc Quân
Status: published

Trong hai tháng qua, những người đóng góp sau đây nhận được tài khoản Phát triển Debian:

* Timo Röhling (roehling)
* Patrick Franz (deltaone)
* Christian Ehrhardt (paelzer)
* Fabio Augusto De Muzio Tobich (ftobich)
* Taowa (taowa)
* Félix Sipma (felix)
* Étienne Mollier (emollier)
* Daniel Swarbrick (dswarbrick)
* Hanno Wagner (wagner)

Trong hai tháng qua, những người đóng góp sau đây được thêm vào với tư cách là người Bảo trì Debian:

* Evangelos Ribeiro Tzaras
* Hugh McMaster

Xin chúc mừng!
