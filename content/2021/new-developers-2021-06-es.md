Title: Nuevos desarrolladores y mantenedores de Debian (mayo y junio del 2021)
Slug: new-developers-2021-06
Date: 2021-07-22 15:30
Author: Jean-Pierre Giraud
Tags: project
Lang: es
Status: published

Los siguientes colaboradores del proyecto se convirtieron en Debian Developers
en los dos últimos meses:

* Timo Röhling (roehling)
* Patrick Franz (deltaone)
* Christian Ehrhardt (paelzer)
* Fabio Augusto De Muzio Tobich (ftobich)
* Taowa (taowa)
* Félix Sipma (felix)
* Étienne Mollier (emollier)
* Daniel Swarbrick (dswarbrick)
* Hanno Wagner (wagner)

Los siguientes colaboradores del proyecto se convirtieron en Debian Maintainers
en los dos últimos meses:

* Evangelos Ribeiro Tzaras
* Hugh McMaster

¡Felicidades a todos!
