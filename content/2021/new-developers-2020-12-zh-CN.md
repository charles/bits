Title: 新的 Debian 维护者 (2020年11月 至 年12月)
Slug: new-developers-2020-12
Date: 2021-01-22 18:00
Author: Jean-Pierre Giraud
Tags: project
Lang: zh-CN
Translator: Boyuan Yang
Status: published

下列贡献者在过去的两个月中被添加为 Debian 维护者：

* Timo Röhling
* Fabio Augusto De Muzio Tobich
* Arun Kumar Pariyar
* Francis Murtagh
* William Desportes
* Robin Gustafsson
* Nicholas Guriev

祝贺他们！
