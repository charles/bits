Title: Nouveaux mainteneurs de Debian (novembre et décembre 2020)
Slug: new-developers-2020-12
Date: 2021-01-22 18:00
Author: Jean-Pierre Giraud
Tags: project
Lang: fr
Status: published

Les contributeurs ont été acceptés comme mainteneurs Debian ces deux derniers
mois :

* Timo Röhling
* Fabio Augusto De Muzio Tobich
* Arun Kumar Pariyar
* Francis Murtagh
* William Desportes
* Robin Gustafsson
* Nicholas Guriev

Félicitations !
