Title: Registration for DebConf21 Online is Open
Date: 2021-06-08 10:00
Tags: debconf, debconf21
Slug: debconf21-open-registration
Author: Stefano Rivera
Lang: en
Status: published

![DebConf21 banner](|static|/images/dc21-logo-horizontal-diversity.png)

The DebConf team is glad to announce that registration for DebConf21 Online is
now open.

The 21st Debian Conference is being held Online, due to COVID-19, from August
22 to August 29, 2021.
It will also sport a DebCamp from August 15 to August 21, 2021 (preceeding the
DebConf).

To register for DebConf21, please visit the DebConf website at
[https://debconf21.debconf.org/register](https://debconf21.debconf.org/register)

Reminder: Creating an account on the site does not register you for the
conference, there's a conference registration form to complete after
signing in.

Participation in DebConf21 is conditional on your respect of our [Code of
Conduct](https://debconf21.debconf.org/about/coc).
We require you to read, understand and abide by this code.

A few notes about the registration process:

- We need to know attendees' locations to better plan the schedule around
  timezones. Please make sure you fill in the "Country I call home" field in
  the registration form accordingly. It's especially important to have this
  data for people who submitted talks, but also for other attendees.

- We are offering limited amounts of financial support for those who
  require it in order to attend. Please refer to the [corresponding
  page](https://debconf21.debconf.org/about/bursaries/) on the website for
  more information.

Any questions about registration should be addressed to
<registration@debconf.org>.

See you online!

DebConf would not be possible without the generous support of all our
sponsors, especially our Platinum Sponsors
[**Lenovo**](https://www.lenovo.com) and
[Infomaniak](https://www.infomaniak.com),
and our Gold Sponsor [**Matanel Foundation**](http://www.matanel.org/).
