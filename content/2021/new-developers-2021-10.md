Title: New Debian Developers and Maintainers (September and October 2021)
Slug: new-developers-2021-10
Date: 2021-11-19 13:00
Author: Jean-Pierre Giraud
Tags: project
Lang: en
Status: published

The following contributors got their Debian Developer accounts in the last two
months:

* Bastian Germann (bage)
* Gürkan Myczko (tar)

The following contributors were added as Debian Maintainers in the last two
months:

* Clay Stan
* Daniel Milde
* David da Silva Polverari
* Sunday Cletus Nkwuda
* Ma Aiguo
* Sakirnth Nagarasa

Congratulations!
