Title: 新的 Debian 开发者和维护者 (2021年1月 至 2月)
Slug: new-developers-2021-02
Date: 2021-03-23 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: zh-CN
Translator: Boyuan Yang
Status: published

下列贡献者在过去的两个月中获得了他们的 Debian 开发者帐号：

* Nicholas D. Steeves (sten)
* Nilesh Patra (nilesh)
* David Suárez Rodríguez (deiv)
* Pierre Gruet (pgt)

下列贡献者在过去的两个月中被添加为 Debian 维护者：

* Antonio Valentino
* Boian Nikolaev Bonev
* Filip Hroch
* Maarten L. Hekkelman
* Xialei Qin
* Xiang Gao

祝贺他们！
