Title: Sobre o Bits do Debian
Slug: about
Lang: pt-BR

*Bits do Debian* é o blog oficial do Projeto Debian. O conteúdo tem curadoria
da [equipe de publicidade][publicity].

Este blog utiliza o [Pelican][pelican]. Você encontra os modelos e o tema usado
neste blog [neste repositório git][gitdo]. Correções, propostas para novos
temas e sugestões construtivas são bem-vindas! Por favor, abra um relatório de
bug no [Sistema de rastreamento de bugs do Debian][bts] contra o [pseudopacote
press][btspress].

Este site está sob a mesma licença e copyright que o site web do
[Debian][debian], veja a [licença das páginas WWW do Debian][wwwlicense].

Se desejar entrar em contato com a gente, por favor envie um e-mail em inglês
para a [lista de discussão debian-publicity][debian-publicity]. Esta é uma
lista publicamente arquivada.

Outras fontes oficiais de divulgação do Projeto Debian são:

* [Micronotícias do Debian](https://micronews.debian.org)
* [Comunicados de imprensa do Debian](https://www.debian.org/News/)
* [Notícias do Projeto Debian](https://www.debian.org/News/weekly/)

[publicity]: https://wiki.debian.org/Teams/Publicity "Equipe de Publicidade"
[pelican]: http://getpelican.com/ "Saiba mais sobre o Pelican"
[debian]: https://www.debian.org "Debian - O Sistema Operacional Universal"
[gitdo]: https://salsa.debian.org/publicity-team/bits
[wwwlicense]: https://www.debian.org/license
[debian-publicity]: https://lists.debian.org/debian-publicity/
[bts]:https://bugs.debian.org/
[btspress]: https://bugs.debian.org/cgi-bin/pkgreport.cgi?pkg=press
