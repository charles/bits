Title: About Bits from Debian
Slug: about

*Bits from Debian* is the official blog of the Debian Project. The content is
curated by the [publicity team][publicity].

This blog is powered by [Pelican][pelican]. You can find the templates and
theme used on this blog [in this git repository][gitdo]. Patches, proposals for
new themes and constructive suggestions are welcome!  Please open a bug report
in the [Debian Bug Tracking System][bts] against the [pseudo-package
press][btspress].

This site is under the same license and copyright as the [Debian][debian]
website, see [Debian WWW pages license][wwwlicense].

If you want to contact us, please send an email to the [debian-publicity
mailing list][debian-publicity]. This is a publicly archived list.

Other official press resources from the Debian Project are:

* [Debian micronews](https://micronews.debian.org)
* [Debian Press Release](https://www.debian.org/News/)
* [Debian Project News](https://www.debian.org/News/weekly/)

[publicity]: https://wiki.debian.org/Teams/Publicity "Publicity Team"
[pelican]: http://getpelican.com/ "Find out about Pelican"
[debian]: https://www.debian.org "Debian - The Universal Operating System"
[gitdo]: https://salsa.debian.org/publicity-team/bits
[wwwlicense]: https://www.debian.org/license
[debian-publicity]: https://lists.debian.org/debian-publicity/
[bts]:https://bugs.debian.org/
[btspress]: https://bugs.debian.org/cgi-bin/pkgreport.cgi?pkg=press
