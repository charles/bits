Title: Acerca de
Slug: about
Lang: es

Este es el blog oficial de [Debian][debian] que sirve de complemento
para otros servicios como:

* [Notas de prensa de Debian](https://www.debian.org/News/)
* [Noticias del Proyecto Debian](https://www.debian.org/News/weekly/)
* [Cuenta de Debian en identi.ca/](https://identi.ca/debian)

Este blog está hecho con [Pelican][pelican]. Podrá encontrar las plantillas
y los temas usados para este blog en [salsa.debian.org][gitdo]. Serán bienvenidos
parches, propuestas de nuevos temas y sugerencias constructivas.

Este sitio tiene la misma licencia y copyright que la web de [Debian][debian].
Vea la [licencia Debian para páginas WWW][wwwlicense].

Si quiere contactar con nosotros, envíe un correo electrónico a la [lista de
distribución de debian-publicity][debian-publicity]. Se trata de un archivo público.

[pelican]: http://getpelican.com/ "Descubra más sobre Pelican"
[debian]: https://www.debian.org "Debian - El sistema operativo universal"
[gitdo]: https://salsa.debian.org/publicity-team/bits
[wwwlicense]: https://www.debian.org/license
[debian-publicity]: https://lists.debian.org/debian-publicity/
