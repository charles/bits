Title: Giới thiệu
Slug: about
Translator: Trần Ngọc Quân
Lang: vi

Đây là nhật ký [Debian][debian] chính thức, bổ sung thêm cho các dịch vụ khác
như là:

* [Phát hành báo chí Debian](https://www.debian.org/News/)
* [Tin tức dự án Debian](https://www.debian.org/News/weekly/)
* [Tài khoản Debian trên identi.ca/](https://identi.ca/debian)

Trang nhật ký này hoạt động dựa vào [Pelican][pelican]. Bạn có thể tìm thấy các
mẫu và chủ đề được sử dụng cho trang này tại [salsa.debian.org][gitdo]. Các miếng
vá, những đề xuất chủ đề mới và các gợi ý có tính xây dựng được hoan nghênh!

Trang này hoạt động dưới cùng bản quyền và giấy phép với trang thông tin điện tử
[Debian][debian], vui lòng xem [giấy phép trang Debian][wwwlicense].

Nếu muốn liên hệ với chúng tôi, vui lòng gửi thư điện tử đến [bó thư
debian-publicity][debian-publicity]. Đây là bó được lưu trữ công khai.

[pelican]: http://getpelican.com/ "Tìm các thông tin giới thiệu về Pelican"
[debian]: https://www.debian.org "Debian - Hệ điều hành toàn cầu"
[gitdo]: https://salsa.debian.org/publicity-team/bits
[wwwlicense]: https://www.debian.org/license
[debian-publicity]: https://lists.debian.org/debian-publicity/
