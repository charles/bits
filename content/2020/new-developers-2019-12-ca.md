Title: Nous desenvolupadors i mantenidors de Debian (novembre i desembre del 2019)
Slug: new-developers-2019-12
Date: 2020-01-26 15:00
Author: Jean-Pierre Giraud
Tags: project
Lang: ca
Status: published

Els següents col·laboradors del projecte han esdevingut Debian Developers en
els darrers dos mesos:

* Louis-Philippe Véronneau (pollo)
* Olek Wojnar (olek)
* Sven Eckelmann (ecsv)
* Utkarsh Gupta (utkarsh)
* Robert Haist (rha)

Els següents col·laboradors del projecte han esdevingut Debian Maintainers en
els darrers dos mesos:

* Denis Danilov
* Joachim Falk
* Thomas Perret
* Richard Laager

Enhorabona a tots!
