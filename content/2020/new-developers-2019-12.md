Title: New Debian Developers and Maintainers (November and December 2019)
Slug: new-developers-2019-12
Date: 2020-01-26 15:00
Author: Jean-Pierre Giraud
Tags: project
Lang: en
Status: published

The following contributors got their Debian Developer accounts in the last two
months:

* Louis-Philippe Véronneau (pollo)
* Olek Wojnar (olek)
* Sven Eckelmann (ecsv)
* Utkarsh Gupta (utkarsh)
* Robert Haist (rha)

The following contributors were added as Debian Maintainers in the last two
months:

* Denis Danilov
* Joachim Falk
* Thomas Perret
* Richard Laager

Congratulations!
