Title: O site web do Debian atualiza sua página principal e se prepara para uma grande renovação
Slug: debian-web-new-homepage
Date: 2020-12-17 13:50
Author: Laura Arjona Reina
Translator: Thiago Pezzo (tico), Paulo Santana (phls)
Lang: pt-BR
Artist:
Tags: web
Status: published

Hoje, o [site web do Debian](https://www.debian.org) apresenta sua nova página
principal.
Desde o mais recente
[sprint](https://bits.debian.org/2019/04/debian-web-team-sprint-2019.html)
da equipe web em março de 2019, temos trabalhado na renovação da estrutura,
conteúdo, layout e scripts que constroem o site.
Tem sido feito esforços principalmente em duas áreas: na remoção ou atualização
de conteúdo obsoleto e na criação de uma nova página principal que seja mais
atrativa para recém-chegados(as), e que também enfatizasse o aspecto social do
projeto Debian além do sistema operacional que desenvolvemos.

[![Site web do Debian: parte da antiga página principal (atrás) e a nova página (frente)](|static|/images/debian_web_old_new.png)](https://www.debian.org)

Embora isto tenha levado mais tempo do que gostaríamos,
e não consideramos que esta seja a página principal final,
acreditamos que é um bom primeiro passo em direção a um site web muito melhor.

A equipe web continuará a trabalhar na reestruturação do site do Debian.
Gostaríamos de convocar a comunidade para nos ajudar e também estamos
considerando receber assistência externa, já que somos um grupo pequeno
cujos(as) participantes também estão envolvidos(as) em outras equipes do Debian.
Alguns dos próximos passos que esperamos dar são o aperfeiçoamento do CSS,
ícones e layout em geral, e a revisão do conteúdo para termos uma estrutura
melhor.

Se você quiser nos ajudar, entre em contato com a gente. Você pode responder
(em inglês) para a versão deste artigo (com alguns detalhes a mais)
[em nossa lista de discussão pública](https://lists.debian.org/debian-www/2020/12/msg00057.html)
ou bater um papo conosco no canal IRC #debian-www
(em irc.debian.org, também em inglês).
