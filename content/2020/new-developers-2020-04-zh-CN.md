Title: 新的 Debian 开发者和维护者 (2020年3月 至 4月)
Slug: new-developers-2020-04
Date: 2020-05-28 18:30
Author: Jean-Pierre Giraud
Tags: project
Lang: zh-CN
Translator: Boyuan Yang
Status: published

下列贡献者在过去的两个月中获得了他们的 Debian 开发者帐号：

* Paride Legovini (paride)
* Ana Custura (acute)
* Felix Lechner (lechner)

下列贡献者在过去的两个月中被添加为 Debian 维护者：

* Sven Geuer
* Håvard Flaget Aasen

祝贺他们！
