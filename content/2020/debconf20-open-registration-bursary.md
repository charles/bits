Title: DebConf20 registration is open!
Date: 2020-05-25 11:30
Tags: debconf, debconf20
Slug: debconf20-open-registration-bursary
Author: Tzafrir Cohen, Jean-Pierre Giraud
Lang: en
Status: published

![DebConf20 banner](|static|/images/dc20-logo_360pxX270px.png)

We are happy to announce that registration for
[DebConf20](https://debconf20.debconf.org)
is now open. The event **will take place from August 23rd to 29th, 2020 at the
University of Haifa, in Israel**, and will be preceded by DebCamp, from August
16th to 22nd.

Although the Covid-19 situation is still rather fluid, as of now, Israel seems
to be on top of the situation. Days with less than 10 new diagnosed infections
are becoming common and businesses and schools are slowly reopening. As such,
we are hoping that, at least as far as regulations go, we will be able to hold
an in-person conference. There is more (and up to date) information at the
[conference's FAQ](https://wiki.debian.org/DebConf/20/Faq). Which means,
barring a second wave, that there is reason to hope that the conference can go
forward.

For that, we need your help. We need to know, assuming health regulations permit
it, how many people intend to attend. This year probably more than ever before,
prompt registration is very important to us. If after months of staying at home
you feel that rubbing elbows with fellow Debian Developers is precisely the
remedy that will salvage 2020, then we ask that you **do register** as soon as
possible.

Sadly, things are still not clear enough for us to make a final commitment to
holding an in-person conference, but knowing how many people intend to attend
will be a great help in making that decision. The deadline for deciding on
postponing, cancelling or changing the format of the conference is June 8th.

To register for DebConf20, please visit our
[website](https://debconf20.debconf.org) and log into the
[registration system](https://debconf20.debconf.org/register/)
and fill out the form. You can always edit or cancel your registration, but
please note that **the last day to confirm or cancel is July 26th, 2020
23:59:59 UTC**. We cannot guarantee availability of accommodation, food and
swag for unconfirmed registrations.

We do suggest that attendees begin making travel arrangements as soon as
possible, of course. Please bear in mind that most air carriers allow
free cancellations and changes.

Any questions about registrations should be addressed to
[registration@debconf.org](mailto:registration@debconf.org).

## Bursary for travel, accomodation and meals

In an effort to widen the diversity of DebConf attendees,
the Debian Project allocates a part of the financial resources obtained through
sponsorships to pay for bursaries (travel, accommodation, and/or meals) for
participants who request this support when they register.

As resources are limited, we will examine the requests and decide who will
receive the bursaries. They will be destined:

* Debian funded bursaries are available to active Debian contributors.
* Debian diversity bursaries are available to newcomers to Debian/DebConf.
  Especially from under-represented communities.

Giving a talk, organizing an event or helping during DebConf20 is taken into
account when deciding upon your bursary, so please mention them in your bursary
application.

For more information about bursaries, please visit
[Applying for a Bursary to DebConf](https://debconf20.debconf.org/about/bursaries)

**Attention:** **deadline to apply for bursaries using the registration form
before May 31st, 2019 23:59:59 UTC**. This deadline is necessary in order to
the organisers to have some time to analyze the requests.

To register for the Conference, either with or without a bursary request,
please visit:
[https://debconf20.debconf.org/register](https://debconf20.debconf.org/register)

Participation to DebConf20 is conditional to your respect of our
[Code of Conduct](https://debconf.org/codeofconduct.shtml). We require you to
read, understand and abide by this code.

DebConf would not be possible without the generous support of all our
sponsors, especially our Platinum Sponsor
[**Lenovo**](https://www.lenovo.com) and
Gold Sponsors [**deepin**](https://www.deepin.com/) and
[**Matanel Foundation**](http://www.matanel.org/).
DebConf20 is still accepting sponsors; if you are interested, or think
you know of others who would be willing to help, [please get in touch]!

[please get in touch]: https://debconf20.debconf.org/sponsors/become-a-sponsor/
