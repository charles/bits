Title: Nous desenvolupadors i mantenidors de Debian (març i abril del 2020)
Slug: new-developers-2020-04
Date: 2020-05-28 18:30
Author: Jean-Pierre Giraud
Tags: project
Lang: ca
Status: published

Els següents col·laboradors del projecte han esdevingut Debian Developers en
els darrers dos mesos:

* Paride Legovini (paride)
* Ana Custura (acute)
* Felix Lechner (lechner)

Els següents col·laboradors del projecte han esdevingut Debian Maintainers en
els darrers dos mesos:

* Sven Geuer
* Håvard Flaget Aasen

Enhorabona a tots!
