Title: Debian accueille ses stagiaires de GSoC 2020
Slug: welcome-gsoc2020-interns
Date: 2020-05-22 02:30
Author: Donald Norwood
Tags:  gsoc, google, announce, development, diversity, software, code, projects
Lang: fr
Translator: Jean-Pierre Giraud
Status: published

![GSoC logo](|static|/images/gsoc.jpg)

Nous sommes heureux de vous annoncer que Debian a sélectionné neuf stagiaires
pour travailler avec nous, sous mentorat, sur différents
[projets](https://wiki.debian.org/SummerOfCode2020/Projects) durant le
[Google Summer of Code](https://summerofcode.withgoogle.com/).

Voici la liste des projets et des stagiaires ainsi que des détails sur les tâches
qu'ils auront à accomplir.

----

Projet : [outils SDK Android dans Debian](https://wiki.debian.org/SummerOfCode2020/ApprovedProjects/Android%20SDK%20Tools%20in%20Debian)

* Stagiaires : Manas Kashyap, Raman Sarda et Samyak-jn

Livrables du projet : rendre disponibles la totalité de la chaîne de compilation
d'Android, de l'infrastructure de développement « Android Target Platform », et des
outils du kit de développement (SDK) dans l'archive Debian.

----

Projet : [empaquetage et assurance qualité des applications concernant le COVID-19](https://wiki.debian.org/SummerOfCode2020/ApprovedProjects/COVID-19)

* Stagiaire : Nilesh

Livrables du projet : assurance qualité, y compris la correction de bogues, les tests
d'intégration continue et la documentation pour toutes les applications de Debian Med
dont la contribution à la lutte contre le COVID-19 est reconnue.

----

Projet : [amélioration de l'écosystème BLAS/LAPACK](https://wiki.debian.org/SummerOfCode2020/ApprovedProjects/BlasLapackEcosys)

* Stagiaire : Mo Zhou

Livrables du projet : amélioration de l'environnement, de la documentation, de la
politique et des vérifications de lintian pour BLAS/LAPACK.

----

Projet : [assurance qualité et intégration continue pour les applications pour les sciences du vivant et la médecine](https://wiki.debian.org/SummerOfCode2020/ApprovedProjects/DebianMedQA)

* Stagiaire : Pranav Ballaney

Livrables du projet : tests d'intégration continue pour toutes les applications de
Debian Med, vérification de l'assurance qualité, et correction de bogues.

----

Projet : [traducteur d'unit Systemd](https://wiki.debian.org/SummerOfCode2020/ApprovedProjects/systemd_unit_translator)

* Stagiaire : K Gopal Krishna

Livrables du projet : un traducteur d'unit systemd vers des scripts d'initialisation
OpenRC. Mise à jour du paquet OpenRC dans Debian Unstable.

----

Project: [prise en charge de la mise à niveau croisée multi-architecture dans Debian](https://wiki.debian.org/SummerOfCode2020/ApprovedProjects/ArchitectureCrossGrade)

* Stagiaire : Kevin Wu

Livrables du projet : évaluation, test et développement d'outils pour évaluer les
vérifications de mise à niveau croisée pour les configurations système et utilisateur.

----

Project: [coopération amont/aval dans Ruby](https://wiki.debian.org/SummerOfCode2020/ApprovedProjects/UpstreamDownstreamCooperationInRuby)

* Stagiaire : utkarsh2102

Livrables du projet : création d'un guide de bonnes pratiques pour rubygems.org à
destination des responsables amont, développement d'un outil capable de détecter
les problèmes et, *si possible*, de corriger automatiquement ces erreurs.
Établissement d'une bonne documentation, conception de l'outil pour qu'il soit
extensible à d'autres langages.

----

Félicitations et bienvenue à tous !

Le programme Google Summer of Code est possible dans Debian grâce à l'effort de
développeurs et de contributeurs de Debian qui consacrent une partie de leur
temps libre à encadrer les stagiaires et aux tâches de diffusion.

Rejoignez-nous pour contribuer à développer Debian ! Vous pouvez suivre les rapports
hebdomadaires des stagiaires sur la [liste de diffusion debian-outreach][debian-outreach-ml],
dialoguer avec nous sur [notre canal IRC][debian-outreach-irc] ou joindre la liste
de diffusion de l'équipe de chacun des projets.

[debian-outreach-ml]: http://lists.debian.org/debian-outreach/ (debian-outreach AT lists.debian.org)
[debian-outreach-irc]: irc://irc.debian.org/debian-outreach (#debian-outreach sur irc.debian.org)
