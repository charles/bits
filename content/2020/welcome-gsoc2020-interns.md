Title: Debian welcomes the 2020 GSOC interns
Slug: welcome-gsoc2020-interns
Date: 2020-05-22 02:30
Author: Donald Norwood
Tags:  gsoc, google, announce, development, diversity, software, mobile, device, code, projects
Status: published

![GSoC logo](|static|/images/gsoc.jpg)

We are very excited to announce that Debian has selected nine interns to work
under mentorship on a variety of
[projects](https://wiki.debian.org/SummerOfCode2020/Projects) with us during
the [Google Summer of Code](https://summerofcode.withgoogle.com/).

Here are the list of the projects, students, and details of the tasks to be
performed.

----

Project: [Android SDK Tools in Debian](https://wiki.debian.org/SummerOfCode2020/ApprovedProjects/Android%20SDK%20Tools%20in%20Debian)

* Student(s): Manas Kashyap, Raman Sarda, and Samyak-jn

Deliverables of the project: Make the entire Android toolchain, Android Target
Platform Framework, and SDK tools available in the Debian archives.

----

Project: [Packaging and Quality assurance of COVID-19 relevant applications](https://wiki.debian.org/SummerOfCode2020/ApprovedProjects/COVID-19)

* Student: Nilesh

Deliverables of the project: Quality assurance including bug fixing, continuous
integration tests and documentation for all Debian Med applications that are
known to be helpful to fight COVID-19

----

Project: [BLAS/LAPACK Ecosystem Enhancement](https://wiki.debian.org/SummerOfCode2020/ApprovedProjects/BlasLapackEcosys)

* Student: Mo Zhou

Deliverables of the project: Better environment, documentation, policy, and
lintian checks for BLAS/LAPACK.

----

Project: [Quality Assurance and Continuous integration for applications in life sciences and medicine](https://wiki.debian.org/SummerOfCode2020/ApprovedProjects/DebianMedQA)

* Student: Pranav Ballaney

Deliverables of the project: Continuous integration tests for all Debian Med
applications, QA review, and bug fixes.

----

Project: [Systemd unit translator](https://wiki.debian.org/SummerOfCode2020/ApprovedProjects/systemd_unit_translator)

* Student: K Gopal Krishna

Deliverables of the project: A systemd unit to OpenRC init script translator.
Updated OpenRC package into Debian Unstable.

----

Project: [Architecture Cross-Grading Support in Debian](https://wiki.debian.org/SummerOfCode2020/ApprovedProjects/ArchitectureCrossGrade)

* Student: Kevin Wu

Deliverables of the project: Evaluate, test, and develop tools to evaluate
cross-grade checks for system and user configuration.

----

Project: [Upstream/Downstream cooperation in Ruby](https://wiki.debian.org/SummerOfCode2020/ApprovedProjects/UpstreamDownstreamCooperationInRuby)

* Student: utkarsh2102

Deliverables of the project: Create guide for rubygems.org on good practices for
upstream maintainers, develop a tool that can detect problems and, *if possible*
fix those errors automatically. Establish good documentation, design the tool to
be extensible for other languages.

----

Congratulations and welcome to all the interns!

The Google Summer of Code program is possible in Debian thanks to the efforts
of Debian Developers and Debian Contributors that dedicate part of their free
time to mentor interns and outreach tasks.

Join us and help extend Debian! You can follow the interns' weekly reports on
the [debian-outreach mailing-list][debian-outreach-ml], chat with us on our
[IRC channel][debian-outreach-irc] or reach out to the individual
projects' team mailing lists.

[debian-outreach-ml]: http://lists.debian.org/debian-outreach/ (debian-outreach AT lists.debian.org)
[debian-outreach-irc]: irc://irc.debian.org/debian-outreach (#debian-outreach sur irc.debian.org)
