Title: Enquête sur les utilisateurs et les contributeurs du suivi à long terme de Debian (LTS)
Slug: lts-survey
Date: 2020-07-13 14:00
Author: Holger Levsen
Tags: project, announce
Lang: fr
Translator: Jean-Pierre Giraud
Status: published

Le 18 juillet, le suivi à long terme de Stretch (Stretch LTS) démarre,
offrant deux années supplémentaires de suivi de sécurité pour la version
Stretch de Debian. Stretch LTS sera la quatrième version de LTS, après
Squeeze LTS qui a été lancé en 2014, Wheezy LTS en 2016 et Jessie LTS
en 2018.

Mais, pour la première fois, nous avons préparé un petit questionnaire sur
les utilisateurs et les contributeurs de LTS, qui ils sont et pourquoi ils
utilisent LTS.

Répondre à ce questionnaire devrait prendre moins d'une dizaine de minutes.
Nous aimerions vraiment que vous
[participiez à cette enquête en ligne](https://surveys.debian.net/index.php?r=survey/index&sid=856794&lang=en) !

Dans deux semaines, le 27 juillet 2020, nous clôturerons l'enquête, aussi,
n'hésitez pas à répondre dès maintenant ! Après cela, nous enverrons un
courriel avec ses résultats.

Vous trouverez davantage d'informations sur LTS sur la page
[https://wiki.debian.org/LTS](https://wiki.debian.org/LTS), y compris des
informations générales de contact.

[Cliquez ici pour répondre au questionnaire maintenant](https://surveys.debian.net/index.php?r=survey/index&sid=856794&lang=en) !
