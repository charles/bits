Title: Những Bảo trì Debian mới (Tháng bảy và tám 2020)
Slug: new-developers-2020-08
Date: 2020-09-19 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: vi
Translator: Trần Ngọc Quân
Status: published

Trong hai tháng qua, những người đóng góp sau đây được thêm vào với tư cách là người Bảo trì Debian:

* Chirayu Desai
* Shayan Doust
* Arnaud Ferraris
* Fritz Reichwald
* Kartik Kulkarni
* François Mazen
* Patrick Franz
* Francisco Vilmar Cardoso Ruviaro
* Octavio Alvarez
* Nick Black

Xin chúc mừng!
