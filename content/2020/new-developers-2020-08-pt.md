Title: Novos(as) mantenedores(as) Debian (julho e agosto de 2020)
Slug: new-developers-2020-08
Date: 2020-09-19 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: pt
Status: published

Os(As) seguintes colaboradores(as) do projeto se tornaram Mantenedores(as)
Debian nos últimos dois meses:

* Chirayu Desai
* Shayan Doust
* Arnaud Ferraris
* Fritz Reichwald
* Kartik Kulkarni
* François Mazen
* Patrick Franz
* Francisco Vilmar Cardoso Ruviaro
* Octavio Alvarez
* Nick Black

Parabéns a todos(as)!
