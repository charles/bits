Title: DebConf20 welcomes its sponsors!
Slug: debconf20-welcomes-sponsors
Date: 2020-08-28 10:30
Author: Laura Arjona Reina
Tags: debconf20, debconf, sponsors
Status: published

![DebConf20 logo](|static|/images/dc20-logo-horizontal-diversity.png)

DebConf20 is taking place online, from 23 August to 29 August 2020.
It is the 21st Debian conference, and organizers and participants are working
hard together at creating interesting and fruitful events.

We would like to warmly welcome the 17 sponsors of DebConf20, and introduce
them to you.

We have four Platinum sponsors.

Our first Platinum sponsor is [**Lenovo**](https://www.lenovo.com).
As a global technology leader manufacturing a wide portfolio of connected
products, including smartphones, tablets, PCs and workstations as well as
AR/VR devices, smart home/office and data center solutions, Lenovo
understands how critical open systems and platforms are to a connected world.

Our next Platinum sponsor is [**Infomaniak**](https://www.infomaniak.com/en).
Infomaniak is Switzerland's largest web-hosting company,
also offering backup and storage services, solutions for event organizers,
live-streaming and video on demand services.
It wholly owns its datacenters and all elements critical
to the functioning of the services and products provided by the company
(both software and hardware).

[**Google**](https://google.com) is our third Platinum sponsor.
Google is one of the largest technology companies in the
world, providing a wide range of Internet-related services and products such
as online advertising technologies, search, cloud computing, software, and
hardware. Google has been supporting Debian by sponsoring DebConf for more than
ten years, and is also a [Debian partner](https://www.debian.org/partners/).

[**Amazon Web Services (AWS)**](https://aws.amazon.com) is our fourth Platinum
sponsor.
Amazon Web Services is one of the world's
most comprehensive and broadly adopted cloud platforms,
offering over 175 fully featured services from data centers globally
(in 77 Availability Zones within 24 geographic regions).
AWS customers include the fastest-growing startups, largest enterprises
and leading government agencies.

Our Gold sponsors are Deepin, the Matanel Foundation, Collabora, and HRT.

[**Deepin**](https://www.deepin.com/) is a Chinese commercial company focusing
on the development and service of Linux-based operating systems. They also lead
research and development of the Deepin Debian derivative.

[**The Matanel Foundation**](http://www.matanel.org) operates in Israel,
as its first concern is to preserve the cohesion of a society and a nation
plagued by divisions. The Matanel Foundation also works in Europe, in Africa
and in South America.

[**Collabora**](https://www.collabora.com/) is a global consultancy delivering
Open Source software solutions to the commercial world.
In addition to offering solutions to clients, Collabora's engineers and
developers actively contribute to many Open Source projects.

[**Hudson-Trading**](https://www.hudsonrivertrading.com)
is a company led by mathematicians, computer scientists, statisticians,
physicists and engineers. They research and develop automated trading
algorithms using advanced mathematical techniques.

Our Silver sponsors are:

[**Linux Professional Institute**](https://www.lpi.org/), the global
certification standard and career support organization for open source
professionals,
[**Civil Infrastructure Platform**](https://www.cip-project.org/),
a collaborative project hosted by the Linux Foundation,
establishing an open source “base layer” of industrial grade software,
[**Ubuntu**](https://www.canonical.com/),
the Operating System delivered by Canonical,
and [**Roche**](https://code4life.roche.com/),
a major international pharmaceutical provider and research company
dedicated to personalized healthcare.

Bronze sponsors:
[**IBM**](https://www.ibm.com/),
[**MySQL**](https://www.mysql.com),
[**Univention**](https://www.univention.com/).

And finally, our Supporter level sponsors,
[**ISG.EE**](https://isg.ee.ethz.ch/) and
[**Pengwin**](https://www.pengwin.dev/).

Thanks to all our sponsors for their support!
Their contributions make it possible for a large number
of Debian contributors from all over the globe to work together,
help and learn from each other in DebConf20.

## Participating in DebConf20 online

The 21st Debian Conference is being held online, due to COVID-19,
from August 23 to 29, 2020. Talks, discussions, panels and other activities
run from 10:00 to 01:00 UTC.
Visit the DebConf20 website at
[https://debconf20.debconf.org](https://debconf20.debconf.org)
to learn about the complete schedule, watch the live streaming and join the
different communication channels for participating in the conference.
