Title: New Debian Developers and Maintainers (May and June 2020)
Slug: new-developers-2020-06
Date: 2020-07-21 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: en
Status: published

The following contributors got their Debian Developer accounts in the last two
months:

* Richard Laager (rlaager)
* Thiago Andrade Marques (andrade)
* Vincent Prat (vivi)
* Michael Robin Crusoe (crusoe)
* Jordan Justen (jljusten)
* Anuradha Weeraman (anuradha)
* Bernelle Verster (indiebio)
* Gabriel F. T. Gomes (gabriel)
* Kurt Kremitzki (kkremitzki)
* Nicolas Mora (babelouest)
* Birger Schacht (birger)
* Sudip Mukherjee (sudip)

The following contributors were added as Debian Maintainers in the last two
months:

* Marco Trevisan
* Dennis Braun
* Stephane Neveu
* Seunghun Han
* Alexander Johan Georg Kjäll
* Friedrich Beckmann
* Diego M. Rodriguez
* Nilesh Patra
* Hiroshi Yokota

Congratulations!
