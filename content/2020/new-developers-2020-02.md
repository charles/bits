Title: New Debian Developers and Maintainers (January and February 2020)
Slug: new-developers-2020-02
Date: 2020-03-23 15:00
Author: Jean-Pierre Giraud
Tags: project
Lang: en
Status: published

The following contributors got their Debian Developer accounts in the last two
months:

* Gard Spreemann (gspr)
* Jonathan Bustillos (jathan)
* Scott Talbert (swt2c)

The following contributors were added as Debian Maintainers in the last two
months:

* Thiago Andrade Marques
* William Grzybowski
* Sudip Mukherjee
* Birger Schacht
* Michael Robin Crusoe
* Lars Tangvald
* Alberto Molina Coballes
* Emmanuel Arias
* Hsieh-Tseng Shen
* Jamie Strandboge

Congratulations!
