Title: Nya Debianutvecklare och Debian Maintainers (November och December 2019)
Slug: new-developers-2019-12
Date: 2020-01-26 15:00
Author: Jean-Pierre Giraud
Tags: project
Lang: sv
Translator: Andreas Rönnquist
Status: published

Följande bidragslämnare fick sina Debianutvecklarkonton under de senaste två
månaderna:

* Louis-Philippe Véronneau (pollo)
* Olek Wojnar (olek)
* Sven Eckelmann (ecsv)
* Utkarsh Gupta (utkarsh)
* Robert Haist (rha)

Följande bidragslämnare adderades som Debian Maintainers under de senaste två
månaderna:

* Denis Danilov
* Joachim Falk
* Thomas Perret
* Richard Laager

Grattis!
