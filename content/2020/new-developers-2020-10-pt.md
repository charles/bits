Title: Novos(as) desenvolvedores(as) e mantenedores(as) Debian (setembro e outubro de 2020)
Slug: new-developers-2020-10
Date: 2020-11-16 20:00
Author: Jean-Pierre Giraud
Tags: project
Lang: pt
Status: published

Os(As) seguintes colaboradores(as) do projeto se tornaram Desenvolvedores(as)
Debian nos últimos dois meses:

* Benda XU (orv)
* Joseph Nahmias (jello)
* Marcos Fouces (marcos)
* Hayashi Kentaro (kenhys)
* James Valleroy (jvalleroy)
* Helge Deller (deller)

Os(As) seguintes colaboradores(as) do projeto se tornaram Mantenedores(as)
Debian nos últimos dois meses:

* Ricardo Ribalda Delgado
* Pierre Gruet
* Henry-Nicolas Tourneur
* Aloïs Micard
* Jérôme Lebleu
* Nis Martensen
* Stephan Lachnit
* Felix Salfelder
* Aleksey Kravchenko
* Étienne Mollier

Parabéns a todos(as)!
