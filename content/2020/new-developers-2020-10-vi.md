Title: Những nhà Phát triển và Bảo trì Debian mới (Tháng chín và mười 2020)
Slug: new-developers-2020-10
Date: 2020-11-16 20:00
Author: Jean-Pierre Giraud
Tags: project
Lang: vi
Translator: Trần Ngọc Quân
Status: published

Trong hai tháng qua, những người đóng góp sau đây nhận được tài khoản Phát triển Debian:

* Benda XU (orv)
* Joseph Nahmias (jello)
* Marcos Fouces (marcos)
* Hayashi Kentaro (kenhys)
* James Valleroy (jvalleroy)
* Helge Deller (deller)

Trong hai tháng qua, những người đóng góp sau đây được thêm vào với tư cách là người Bảo trì Debian:

* Ricardo Ribalda Delgado
* Pierre Gruet
* Henry-Nicolas Tourneur
* Aloïs Micard
* Jérôme Lebleu
* Nis Martensen
* Stephan Lachnit
* Felix Salfelder
* Aleksey Kravchenko
* Étienne Mollier

Xin chúc mừng!
