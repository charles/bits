Title: Những nhà Phát triển và Bảo trì Debian mới (Tháng giêng và hai 2020)
Slug: new-developers-2020-02
Date: 2020-03-23 15:00
Author: Jean-Pierre Giraud
Tags: project
Lang: vi
Translator: Trần Ngọc Quân
Status: published

Trong hai tháng qua, những người đóng góp sau đây nhận được tài khoản Phát triển Debian:

* Gard Spreemann (gspr)
* Jonathan Bustillos (jathan)
* Scott Talbert (swt2c)

Trong hai tháng qua, những người đóng góp sau đây được thêm vào với tư cách là người Bảo trì Debian:

* Thiago Andrade Marques
* William Grzybowski
* Sudip Mukherjee
* Birger Schacht
* Michael Robin Crusoe
* Lars Tangvald
* Alberto Molina Coballes
* Emmanuel Arias
* Hsieh-Tseng Shen
* Jamie Strandboge

Xin chúc mừng!
