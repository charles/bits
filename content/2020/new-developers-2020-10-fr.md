Title: Nouveaux développeurs et mainteneurs de Debian (septembre et octobre 2020)
Slug: new-developers-2020-10
Date: 2020-11-16 20:00
Author: Jean-Pierre Giraud
Tags: project
Lang: fr
Status: published

Les contributeurs suivants sont devenus développeurs Debian ces deux derniers
mois :

* Benda XU (orv)
* Joseph Nahmias (jello)
* Marcos Fouces (marcos)
* Hayashi Kentaro (kenhys)
* James Valleroy (jvalleroy)
* Helge Deller (deller)

et ces contributeurs ont été acceptés comme mainteneurs Debian durant la même
période :

* Ricardo Ribalda Delgado
* Pierre Gruet
* Henry-Nicolas Tourneur
* Aloïs Micard
* Jérôme Lebleu
* Nis Martensen
* Stephan Lachnit
* Felix Salfelder
* Aleksey Kravchenko
* Étienne Mollier

Félicitations !
