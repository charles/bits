Title: Vamos celebrar o DebianDay 2020 por todo o mundo
Date: 2020-07-27 14:05
Tags: debian, birthday
Slug: lets-celebrate-debianday-2020-around-the-world
Author: Paulo Henrique de Lima Santana (phls)
Translator: Rafael Fontenelle
Lang: pt-BR
Status: published

Incentivamos nossa comunidade a celebrar em todo o mundo o 27º aniversário do
Debian organizando eventos como o
[DebianDay](https://wiki.debian.org/DebianDay).
Este ano, devido à pandemia do COVID-19, não podemos organizar eventos
pessoalmente; portanto, solicitamos que colaboradores(as), desenvolvedores(as),
equipes, grupos, mantenedores(as) e usuários(as) promovam o Projeto Debian e as
atividades da Debian on-line no dia 16 (e/ou 15) de agosto.

As comunidades podem organizar uma programação completa de atividades on-line
ao longo do dia. Essas atividades podem incluir palestras, workshops,
participação ativa com contribuições, assistência ou edição de traduções,
debates, BoFs e tudo isso no seu idioma local usando ferramentas como
[Jitsi](https://meet.jit.si) para captura de áudio e vídeo dos(as)
apresentadores(as) para posterior transmissão no YouTube.

Se você não tem conhecimento de nenhuma comunidade local que esteja organizando
um evento completo ou não deseja participar de um, você pode criar sua própria
atividade usando o [OBS](https://obsproject.com) e transmiti-la para o YouTube.
Você pode assistir a um tutorial do OBS
[aqui](https://peertube.debian.social/videos/watch/7f41c0e7-66cc-4234-b929-6b3219d95c14).

Não se esqueça de gravar sua atividade, pois será uma boa ideia enviá-la
para o [Peertube](https://peertube.debian.social) mais tarde.

Por favor, adicione seu evento ou sua atividade na [página wiki do
DebianDay](https://wiki.debian.org/DebianDay/2020), informando e anunciando
no [Debian micronews](https://micronews.debian.org). Para compartilhá-los,
você tem várias opções:

* Siga os passos listados
  [aqui](https://micronews.debian.org/pages/contribute.html)
  para desenvolvedores(as) Debian.
* Nos contate usando IRC no canal `#debian-publicity` na rede OFTC e nos
  pergunte lá.
* Envie um e-mail para
  [debian-publicity@lists.debian.org](https://lists.debian.org/debian-publicity/)
  e peça que seu evento/atividade seja incluído no site do micronews. Esta é
  uma lista pública.

Obs.: A DebConf20 on-line está chegando! O evento será realizado de 23 a 29 de
agosto de 2020. [O registro já está
aberto](https://debconf20.debconf.org/news/2020-07-12-registration-is-open/).
