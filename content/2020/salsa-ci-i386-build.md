Title: Salsa CI now includes i386 build support
Slug: salsa-ci-i386-build
Date: 2020-10-09 20:20
Author: Salsa CI Team
Tags: salsa, i386
Status: published

![Salsa CI pipeline with i386 build support](|static|/images/salsa-ci-i386-build.png)

Salsa CI aims at improving the Debian packaging lifecycle by delivering
Continuous Integration fully compatible with Debian packaging.
The main Salsa CI's project is the
[pipeline](https://salsa.debian.org/salsa-ci-team/pipeline/), that builds
packages and run different tests after every `git push` to Salsa.
The pipeline makes it possible to have a quick and early feedback about any
issues the new changes may have created or solved, without the need to upload
to the archive.

All of the pipeline jobs run on `amd64` architecture, but the Salsa CI Team has
recently added support to build packages also on `i386` architecture.
This work started during the Salsa CI Sprint at DebConf20 after the
["Where is Salsa CI right now" talk](https://debconf20.debconf.org/talks/47-where-is-salsa-ci-right-now/),
and required different changes at the core of pipeline to make it possible.
For more details, this is the related merge request:
[https://salsa.debian.org/salsa-ci-team/pipeline/-/merge_requests/256](https://salsa.debian.org/salsa-ci-team/pipeline/-/merge_requests/256)

If you have any questions, you can contact the Salsa CI Team at the #salsaci
channel on irc.oftc.net
