Title: New Debian Developers and Maintainers (September and October 2020)
Slug: new-developers-2020-10
Date: 2020-11-16 20:00
Author: Jean-Pierre Giraud
Tags: project
Lang: en
Status: published

The following contributors got their Debian Developer accounts in the last two
months:

* Benda XU (orv)
* Joseph Nahmias (jello)
* Marcos Fouces (marcos)
* Hayashi Kentaro (kenhys)
* James Valleroy (jvalleroy)
* Helge Deller (deller)

The following contributors were added as Debian Maintainers in the last two
months:

* Ricardo Ribalda Delgado
* Pierre Gruet
* Henry-Nicolas Tourneur
* Aloïs Micard
* Jérôme Lebleu
* Nis Martensen
* Stephan Lachnit
* Felix Salfelder
* Aleksey Kravchenko
* Étienne Mollier

Congratulations!
