Title: New Debian Maintainers (July and August 2020)
Slug: new-developers-2020-08
Date: 2020-09-19 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: en
Status: published

The following contributors were added as Debian Maintainers in the last two
months:

* Chirayu Desai
* Shayan Doust
* Arnaud Ferraris
* Fritz Reichwald
* Kartik Kulkarni
* François Mazen
* Patrick Franz
* Francisco Vilmar Cardoso Ruviaro
* Octavio Alvarez
* Nick Black

Congratulations!
