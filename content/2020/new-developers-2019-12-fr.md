Title: Nouveaux développeurs et mainteneurs de Debian (novembre et décembre 2019)
Slug: new-developers-2019-12
Date: 2020-01-26 15:00
Author: Jean-Pierre Giraud
Tags: project
Lang: fr
Status: published

Les contributeurs suivants sont devenus développeurs Debian ces deux derniers
mois :

* Louis-Philippe Véronneau (pollo)
* Olek Wojnar (olek)
* Sven Eckelmann (ecsv)
* Utkarsh Gupta (utkarsh)
* Robert Haist (rha)

et ces contributeurs ont été acceptés comme mainteneurs Debian durant la même
période :

* Denis Danilov
* Joachim Falk
* Thomas Perret
* Richard Laager

Félicitations !
