Title: Nous desenvolupadors i mantenidors de Debian (gener i febrer del 2020)
Slug: new-developers-2020-02
Date: 2020-03-23 15:00
Author: Jean-Pierre Giraud
Tags: project
Lang: ca
Status: published

Els següents col·laboradors del projecte han esdevingut Debian Developers en
els darrers dos mesos:

* Gard Spreemann (gspr)
* Jonathan Bustillos (jathan)
* Scott Talbert (swt2c)

Els següents col·laboradors del projecte han esdevingut Debian Maintainers en
els darrers dos mesos:

* Thiago Andrade Marques
* William Grzybowski
* Sudip Mukherjee
* Birger Schacht
* Michael Robin Crusoe
* Lars Tangvald
* Alberto Molina Coballes
* Emmanuel Arias
* Hsieh-Tseng Shen
* Jamie Strandboge

Enhorabona a tots!
