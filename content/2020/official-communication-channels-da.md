Title: Debians officielle kommunikationskanaler
Slug: official-communication-channels
Date: 2020-03-16 14:00
Author: Laura Arjona Reina, Ana Guerrero Lopez and Donald Norwood
Tags: project, announce
Lang: da
Translator: Kåre Thor Olsen
Status: published

Fra tid til anden bliver Debian spurgt om hvad vores officielle
kommunikationskanaler er og hvem der ejer websteder med enslydende navne.

Debians primære websted [www.debian.org](https://www.debian.org)
er vores primære kommunikationsmedie. Dem, der søger oplysninger om vores
aktuelle begivenheder og udviklingsfremdrift i fællesskabet, kan have interesse
i afsnittet [Debian-nyheder](https://www.debian.org/News/) på Debians
websted.  Hvad angår mindre formelle annonceringer, har vi den officielle
Debian-blog [Bits from Debian](https://bits.debian.org) og tjenesten
[Debian micronews](https://micronews.debian.org), til mere
kortfattede nyheder.

Vores officielle nyhedsbrev
[Debians projektnyheder](https://www.debian.org/News/weekly/)
og alle officielle annonceringer af nyheder eller
projektændringer, offentliggøres både på vores websted og udsendes på vores
officielle postlister
[debian-announce](https://lists.debian.org/debian-announce/) eller
[debian-news](https://lists.debian.org/debian-news/). Der er
begrænset adgang til at sende indlæg til disse postlister.

Vi ønsker også at benytte lejligheden til at offentliggøre hvordan
Debian-projektet, eller blot Debian, er struktureret.

Debians struktur reguleres af vores
[vedtægter](https://www.debian.org/devel/constitution).
Tillidsposter og delegater er opremset på vores
[Teams](https://wiki.debian.org/Teams)-side.

Den komplette liste over officielle Debian-medlemmer, finder man på vores
[Nye medlemmer](https://nm.debian.org/members)-side, hvor vores
medlemskab administreres. En bredere liste over Debian-bidragydere, finder man
på vores [Contributors](https://contributors.debian.org)-side.

Hvis du har spørgsmål, kan du på engelsk kontakte presseholdet på
[press@debian.org](mailto:press@debian.org).
