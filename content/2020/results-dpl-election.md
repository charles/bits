Title: DPL elections 2020, congratulations Jonathan Carter!
Date: 2020-04-19 14:25
Tags: dpl, election, leader, vote
Slug: results-dpl-elections-2020
Author: Ana Guerrero López
Status: published

The Debian Project Leader elections just finished and the winner is Jonathan Carter!

His term as project leader starts next Tuesday April 21st and expires on April 20th 2021.

Of a total of 1011 developers, 339 developers voted using the [Condorcet method](http://en.wikipedia.org/wiki/Condorcet_method).
More information about the result is available in the [Debian Project Leader Elections 2020 page](https://www.debian.org/vote/2020/vote_001).

Many thanks to [Jonathan Carter](https://www.debian.org/vote/2020/platforms/jcc),
[Sruthi Chandran](https://www.debian.org/vote/2020/platforms/srud) and
[Brian Gupta](https://www.debian.org/vote/2020/platforms/bgupta) for running.

And special thanks to Sam Hartman for his service as DPL during these last twelve months!
