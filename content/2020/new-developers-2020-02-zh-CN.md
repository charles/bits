Title: 新的 Debian 开发者和维护者 (2020年1月 至 2月)
Slug: new-developers-2020-02
Date: 2020-03-23 15:00
Author: Jean-Pierre Giraud
Tags: project
Lang: zh-CN
Translator: Boyuan Yang
Status: published

下列贡献者在过去的两个月中获得了他们的 Debian 开发者帐号：

* Gard Spreemann (gspr)
* Jonathan Bustillos (jathan)
* Scott Talbert (swt2c)

下列贡献者在过去的两个月中被添加为 Debian 维护者：

* Thiago Andrade Marques
* William Grzybowski
* Sudip Mukherjee
* Birger Schacht
* Michael Robin Crusoe
* Lars Tangvald
* Alberto Molina Coballes
* Emmanuel Arias
* Hsieh-Tseng Shen
* Jamie Strandboge

祝贺他们！
