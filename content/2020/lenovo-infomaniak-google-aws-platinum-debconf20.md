Title: Lenovo, Infomaniak, Google and Amazon Web Services (AWS), Platinum Sponsors of DebConf20
Slug: lenovo-infomaniak-google-aws-platinum-debconf20
Date: 2020-08-20 20:25
Author: Laura Arjona Reina
Artist: Lenovo, Infomaniak, Google, AWS
Tags: debconf20, debconf, sponsors, lenovo, infomaniak, google, aws
Status: published

We are very pleased to announce that [**Lenovo**](https://www.lenovo.com),
[**Infomaniak**](https://www.infomaniak.com),
[**Google**](https://google.com)
and [**Amazon Web Services (AWS)**](https://aws.amazon.com),
have committed to supporting [DebConf20](https://debconf20.debconf.org) as
**Platinum sponsors**.

[![lenovologo](|static|/images/lenovo.png)](https://www.lenovo.com)

As a global technology leader manufacturing a wide portfolio of connected
products, including smartphones, tablets, PCs and workstations as well as
AR/VR devices, smart home/office and data center solutions,
[**Lenovo**](https://www.lenovo.com)
understands how critical open systems and platforms are to a connected world.

[![infomaniaklogo](|static|/images/infomaniak.png)](https://www.infomaniak.com)

[**Infomaniak**](https://www.infomaniak.com) is Switzerland's largest
web-hosting company, also offering backup and storage services, solutions for
event organizers, live-streaming and video on demand services.
It wholly owns its datacenters and all elements critical
to the functioning of the services and products provided by the company
(both software and hardware).

[![Googlelogo](|static|/images/google.png)](https://www.google.com)

[**Google**](https://google.com/) is one of the largest technology companies
in the world, providing a wide range of Internet-related services and products
such as online advertising technologies, search, cloud computing, software,
and hardware.

Google has been supporting Debian by sponsoring DebConf for more than
ten years, and is also a Debian partner sponsoring parts
of [Salsa](https://salsa.debian.org)'s continuous integration infrastructure
within Google Cloud Platform.

[![AWSlogo](|static|/images/aws.png)](https://aws.amazon.com)

[**Amazon Web Services (AWS)**](https://aws.amazon.com) is one of the world's
most comprehensive and broadly adopted cloud platform,
offering over 175 fully featured services from data centers globally
(in 77 Availability Zones within 24 geographic regions).
AWS customers include the fastest-growing startups, largest enterprises
and leading government agencies.

With these commitments as Platinum Sponsors,
Lenovo, Infomaniak, Google and Amazon Web Services are contributing
to make possible our annual conference,
and directly supporting the progress of Debian and Free Software,
helping to strengthen the community that continues to collaborate on
Debian projects throughout the rest of the year.

Thank you very much for your support of DebConf20!

## Participating in DebConf20 online

The 21st Debian Conference is being held Online, due to COVID-19,
from August 23rd to 29th, 2020. There are 7 days of activities, running from
10:00 to 01:00 UTC.
Visit the DebConf20 website at
[https://debconf20.debconf.org](https://debconf20.debconf.org)
to learn about the complete schedule, watch the live streaming and join the
different communication channels for participating in the conference.
