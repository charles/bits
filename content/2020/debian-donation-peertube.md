Title: Debian donation for Peertube development
Slug: debian-donation-peertube
Date: 2020-10-21 12:30
Author: Louis-Philippe Véronneau, Pouhiou, Laura Arjona Reina
Tags: debconf20, donation, fundraising, peertube
Status: published
Lang: en

The Debian project is happy to announce a donation of 10,000 € to help
[Framasoft](https://framasoft.org) reach the fourth stretch-goal of its
[Peertube v3 crowdfunding campaign -- Live Streaming](https://joinpeertube.org/roadmap).

This year's iteration of the Debian annual conference,
[DebConf20](https://debconf20.debconf.org/), had to be held online,
and while being a resounding success, it made clear to the project our need
to have a permanent live streaming infrastructure for small events held by
local Debian groups.
As such, [Peertube](https://joinpeertube.org/), a FLOSS video hosting platform,
seems to be the perfect solution for us.

We hope this unconventional gesture from the Debian project will help us make
this year somewhat less terrible and give us, and thus humanity, better Free
Software tooling to approach the future.

Debian thanks the commitment of numerous Debian donors and DebConf sponsors,
particularly all those that contributed to DebConf20 online's success
(volunteers, speakers and sponsors).
Our project also thanks Framasoft and the PeerTube community for developing
PeerTube as a free and decentralized video platform.

The Framasoft association warmly thanks the Debian Project for
its contribution, from its own funds, towards making PeerTube happen.

This contribution has a twofold impact. Firstly, it's a strong sign of
recognition from an international project - one of the pillars of the Free
Software world - towards a small French association which offers tools to
liberate users from the clutches of the web's giant monopolies.
Secondly, it's a substantial amount of help in these difficult times,
supporting the development of a tool which equally belongs to and is useful
to everyone.

The strength of Debian's gesture proves, once again, that solidarity, mutual
aid and collaboration are values which allow our communities
to create tools to help us strive towards Utopia.
