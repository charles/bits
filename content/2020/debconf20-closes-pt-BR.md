Title: DebConf20 on-line chega ao fim
Date: 2020-08-30 2:40
Tags: debconf20, announce, debconf
Slug: debconf20-closes
Author: Laura Arjona Reina and Donald Norwood
Artist: Aigars Mahinovs
Lang: pt-BR
Translator: Thiago Pezzo (Tico)
Status: published

[![foto de grupo da DebConf20 - clique para aumentar](|static|/images/debconf20_group_small.jpg)](https://wiki.debian.org/DebConf/20/GroupPhoto)

No sábado, 29 de agosto de 2020, a Conferência Anual de
Desenvolvedores(as) e Contribuidores(as) chega a seu fim.

A DebConf20 aconteceu on-line pela primeira vez devido à pandemia
do coronavírus (COVID-19).

Todas as atividades foram transmitidas, com formas variadas de participação:
via mensagens no IRC, textos escritos on-line e colaborativos
e salas para encontros por videoconferência.

Com mais de 850 participantes de 80 países diferentes e um
total de mais 100 apresentações, sessões de debates,
encontros para desconferências (BoF) e outras atividades,
a [DebConf20](https://debconf20.debconf.org) foi um grande sucesso.

Quando ficou claro que a DebConf20 seria um evento exclusivamente
on-line, o time de vídeo da DebConf gastou muito tempo nos últimos meses para
adaptar, melhorar e, em alguns casos, desenvolver do zero, a tecnologia que
seria necessária para tornar possível uma DebConf on-line. Após as lições
aprendidas da MiniDebConfOnline em maio passado, alguns ajustes foram
feitos, e assim eventualmente criamos uma configuração envolvendo Jitsi, OBS,
Voctomix, SReview, nginx, Etherpad e uma interface web desenvolvida
recentemente para o Voctomix, como outros vários elementos do sistema.

Todos os componentes da infraestrutura de vídeo são softwares livres e toda
a estrutura está configurada através de seu repositório público
[ansible](https://salsa.debian.org/debconf-video-team/ansible).

A [programação](https://debconf20.debconf.org/schedule/) da DebConf20 incluiu
duas trilhas em outros idiomas que não o inglês: a MiniConf em língua
espanhola, com oito apresentações em dois dias, e a MiniConf em língua
malaiala, com nove apresentações em três dias. Atividades ad-hoc, apresentadas
por participantes durante o curso de toda a conferência, também foram
possíveis, transmitidas e gravadas. Também aconteceram diversas
reuniões de times como [sprint](https://wiki.debian.org/Sprints/) de certas
áreas de desenvolvimento do Debian.

Entre as apresentações, a transmissão de vídeo exibiu um loop com os(as)
tradicionais patrocinadores(as), e também exibiu algumas vinhetas adicionais
incluindo fotos de DebConfs anteriores, fatos engraçados sobre o Debian e
vídeos curtos do tipo "mande um alô" enviados pelos(as) participantes para
seus(suas) amigos(as) do Debian.

Para aqueles(as) que não conseguiram participar, a maior parte das
apresentações e sessões já estão disponíveis por meio do
[site web do repositório de atividades Debian](https://meetings-archive.debian.net/pub/debian-meetings/2020/DebConf20/),
e as restantes aparecerão lá nos próximos dias.

O site web da [DebConf20](https://debconf20.debconf.org/)
permanecerá ativo para propósitos de arquivamento e continuará a oferecer
links para as apresentações e vídeos das palestras e eventos.

Ano que vem a [DebConf21](https://wiki.debian.org/DebConf/21) está planejada
para acontecer em Haifa, Israel, em agosto ou setembro.

A DebConf é comprometida com um ambiente seguro e acolhedor para todos(as)
os(as) participantes.
Durante a conferência, diversos times (Front Desk, Welcome e Community)
estiveram disponíveis para ajudar, de modo que os(as) participantes tivessem
a melhor experiência na conferência, e estiveram disponíveis para encontrar
soluções para qualquer problema que pudesse aparecer.
Veja a
[página web sobre o Código de Conduta no site webDebConf20](https://debconf20.debconf.org/about/coc/)
para mais detalhes sobre isso.

O Debian agradece o comprometimento de inúmeros(as)
[patrocinadores(as)](https://debconf20.debconf.org/sponsors/)
em apoiar a DebConf20, especialmente nossos(as) patrocinadores(as) Platinum:
[**Lenovo**](https://www.lenovo.com),
[**Infomaniak**](https://www.infomaniak.com),
[**Google**](https://google.com/)
e
[**Amazon Web Services (AWS)**](https://aws.amazon.com/).

### Sobre o Debian

O Projeto Debian foi fundado em 1993 por Ian Murdock para ser um verdadeiro
projeto comunitário e livre. Desde então, o projeto cresceu e tornou-se um dos
maiores e mais influentes projetos de código aberto. Milhares de
voluntários(as) de todos os cantos do mundo trabalham juntos(as) para criar e
manter o software Debian. Disponível em 70 idiomas, e com suporte para uma gama
imensa de tipos de computadores, o Debian se autodenomina o
_sistema operacional universal_.

### Sobre a DebConf

A DebConf é a conferência dos(as) desenvolvedores(as) do Projeto Debian. Além
de uma completa programação de palestras técnicas, sociais e políticas, a
DebConf fornece uma oportunidade para desenvolvedores(as), contribuidores(as)
e outras pessoas interessadas para se encontrarem presencialmente e trabalharem
juntos(as) mais proximamente. Ela acontece
anualmente desde 2000 em locais tão variados como Escócia, Argentina,
Bósnia e Herzegóvina, e Brasil. Mais informações sobre a DebConf estão
disponíveis em [https://debconf.org/](https://debconf.org).

### Sobre a Lenovo

Como uma líder global em tecnologia, fabricando um amplo portfólio de produtos
conectados, incluindo smartphones, tablets, PCs e workstations, bem como
dispositivos de realidade aumentada/virtual, casas/escritórios inteligentes e
soluções para datacenters, a [**Lenovo**](https://www.lenovo.com)
entende quão críticos são os sistemas abertos e as plataformas para um mundo
conectado.

### Sobre a Infomaniak

A [**Infomaniak**](https://www.infomaniak.com) é a maior empresa de hospedagem
web da Suíça, que também oferece serviços de backup e de armazenamento,
soluções para organizadores(as) de eventos, serviços de transmissão em tempo
real e vídeo sob demanda.
Todos os datacenters e elementos críticos para o funcionamento dos serviços e
produtos oferecidos pela empresa (tanto software quanto hardware)
são de propriedade absoluta da Infomaniak.

### Sobre o Google

O [**Google**](https://google.com/) é uma das maiores empresas de tecnologia do
mundo, fornecendo uma ampla gama de serviços e produtos relacionados à Internet
como tecnologias de publicidade on-line, pesquisa, computação em nuvem,
software e hardware.

O Google tem apoiado o Debian através do patrocínio da DebConf por mais de
dez anos, e é também um parceiro Debian que patrocina partes
da infraestrutura de integração contínua do [Salsa](https://salsa.debian.org)
dentro da Plataforma Google Cloud.

### Sobre a Amazon Web Services (AWS)

A [**Amazon Web Services (AWS)**](https://aws.amazon.com) é uma das
plataformas em nuvem mais abrangentes e largamente adotadas no mundo,
oferecendo mais de 175 serviços repletos de funcionalidades via datacenters
globais (em 77 zonas de disponibilidade dentro de 24 regiões geográficas).
Consumidores(as) da AWS incluem as startups de mais rápido crescimento, as
maiores empresas e as agências governamentais de liderança.

### Informações de contato

Para mais informações, por favor visite a página web da DebConf20 em
[https://debconf20.debconf.org/](https://debconf20.debconf.org/)
ou envie um e-mail para <press@debian.org>.
