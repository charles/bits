Title: Novos(as) desenvolvedores(as) e mantenedores(as) Debian (novembro e dezembro de 2019)
Slug: new-developers-2019-12
Date: 2020-01-26 15:00
Author: Jean-Pierre Giraud
Tags: project
Lang: pt
Status: published

Os(As) seguintes colaboradores(as) do projeto se tornaram desenvolvedores(as)
Debian nos últimos dois meses:

* Louis-Philippe Véronneau (pollo)
* Olek Wojnar (olek)
* Sven Eckelmann (ecsv)
* Utkarsh Gupta (utkarsh)
* Robert Haist (rha)

Os(As) seguintes colaboradores(as) do projeto se tornaram mantenedores(as)
Debian nos últimos dois meses:

* Denis Danilov
* Joachim Falk
* Thomas Perret
* Richard Laager

Parabéns a todos(as)!
