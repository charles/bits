Title: Nouveaux développeurs et mainteneurs de Debian (mars et avril 2020)
Slug: new-developers-2020-04
Date: 2020-05-28 18:30
Author: Jean-Pierre Giraud
Tags: project
Lang: fr
Translator: Jean-Pierre Giraud
Status: published

Les contributeurs suivants sont devenus développeurs Debian ces deux derniers
mois :

* Paride Legovini (paride)
* Ana Custura (acute)
* Felix Lechner (lechner)

et ces contributeurs ont été acceptés comme mainteneurs Debian durant la même
période :

* Sven Geuer
* Håvard Flaget Aasen

Félicitations !
