Title: Nuevos desarrolladores y mantenedores de Debian (enero y febrero del 2020)
Slug: new-developers-2020-02
Date: 2020-03-23 15:00
Author: Jean-Pierre Giraud
Tags: project
Lang: es
Status: published

Los siguientes colaboradores del proyecto se convirtieron en Debian Developers
en los dos últimos meses:

* Gard Spreemann (gspr)
* Jonathan Bustillos (jathan)
* Scott Talbert (swt2c)

Los siguientes colaboradores del proyecto se convirtieron en Debian Maintainers
en los dos últimos meses:

* Thiago Andrade Marques
* William Grzybowski
* Sudip Mukherjee
* Birger Schacht
* Michael Robin Crusoe
* Lars Tangvald
* Alberto Molina Coballes
* Emmanuel Arias
* Hsieh-Tseng Shen
* Jamie Strandboge

¡Felicidades a todos!
