Title: Doação do Debian para desenvolvimento do Peerturbe
Slug: debian-donation-peertube
Date: 2020-10-21 12:30
Author: Louis-Philippe Véronneau, Pouhiou, Laura Arjona Reina
Tags: debconf20, donation, fundraising, peertube
Lang: pt-BR
Translator: Thiago Pezzo (tico), Charles Melara
Status: published

O projeto Debian tem o prazer de anunciar a doação de 10.000 euros para ajudar
a [Framasoft](https://framasoft.org) a alcançar a quarta meta adicional de sua
[campanha de financiamento colaborativo do Peertube v3 -- Transmissão ao vivo](https://joinpeertube.org/roadmap).

A edição deste ano da conferência anual do Debian,
a [DebConf20](https://debconf20.debconf.org/), teve que ser realizada on-line e
embora tenha sido um sucesso retumbante, deixou claro ao projeto nossa
necessidade de ter uma infraestrutura permanente de transmissão ao vivo para
pequenos eventos realizados por grupos Debian locais.
O [Peertube](https://joinpeertube.org/), uma plataforma FLOSS de hospedagem de
vídeos, nos parece ser a perfeita solução.

Nós acreditamos que este gesto pouco convencional do projeto Debian nos ajudará
a fazer deste ano um pouco menos terrível e nos dará, como também à humanidade,
melhores ferramentas em Software Livre para lidarmos com o futuro.

O Debian agradece o comprometimento de numerosos(as) financiadores(as) e
patrocinadores(as) da DebConf,
particularmente todas e todos que contribuíram  para o sucesso da DebConf20
on-line (voluntários(as), palestrantes e patrocinadores(as)).
Nosso projeto também agradece à Framasoft e à comunidade PeerTube pelo
desenvolvimento do PeerTube como uma plataforma de vídeo livre e
descentralizada.

A associação Framasoft calorosamente agradece ao Projeto Debian pela
sua contribuição, de seus próprios recursos, para que o PeerTube aconteça.

A contribuição tem duplo impacto. Primeiramente, é um forte sinal de
reconhecimento de um projeto internacional - um dos pilares do mundo do
Software Livre - para uma pequena associação francesa que oferece ferramentas
para livrar os(as) usuários(as) das garras dos gigantes monopólios da web.
Em segundo lugar, é uma ajuda substancial nesses difíceis tempos,
ao suportar o desenvolvimento de uma ferramenta que, de modo igualitário,
pertence e é útil para todos(as).

A força deste gesto do Debian prova, uma vez mais, que
a solidariedade, o apoio recíproco e a colaboração são valores que permitem
que nossas comunidades criem ferramentas que nos ajudem na busca pela Utopia.
