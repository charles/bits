Title: Novos(as) desenvolvedores(as) e mantenedores(as) Debian (maio e junho de 2020)
Slug: new-developers-2020-06
Date: 2020-07-21 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: pt
Translator: Paulo Henrique de Lima Santana
Status: published

Os(As) seguintes colaboradores(as) do projeto se tornaram desenvolvedores(as)
Debian nos últimos dois meses:

* Richard Laager (rlaager)
* Thiago Andrade Marques (andrade)
* Vincent Prat (vivi)
* Michael Robin Crusoe (crusoe)
* Jordan Justen (jljusten)
* Anuradha Weeraman (anuradha)
* Bernelle Verster (indiebio)
* Gabriel F. T. Gomes (gabriel)
* Kurt Kremitzki (kkremitzki)
* Nicolas Mora (babelouest)
* Birger Schacht (birger)
* Sudip Mukherjee (sudip)

Os(As) seguintes colaboradores(as) do projeto se tornaram mantenedores(as)
Debian nos últimos dois meses:

* Marco Trevisan
* Dennis Braun
* Stephane Neveu
* Seunghun Han
* Alexander Johan Georg Kjäll
* Friedrich Beckmann
* Diego M. Rodriguez
* Nilesh Patra
* Hiroshi Yokota

Parabéns a todos(as)!
