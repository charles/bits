Title: Les groupes locaux de Debian à la DebConf20 et au-delà
Slug: debian-local-groups-debconf20
Date: 2020-09-16 19:15
Author: Francisco M. Neto and Laura Arjona Reina
Translator: Jean-Pierre Giraud
Lang: fr
Tags:
Status:  published

Il existe un certain nombre de grands groupes locaux de Debian qui fonctionnent
très bien (Debian France, Debian Brésil et Debian Taïwan, pour en citer
quelques-uns), mais que pouvons-nous faire pour assister de nouveaux groupes
locaux ou pour contribuer à susciter l'intérêt dans d'autres régions du monde ?

Une session sur les
[équipes locales de Debian](https://debconf20.debconf.org/talks/50-local-teams/)
s'est tenue pendant Debconf20 et a provoqué des échanges plutôt constructifs
pendant la diffusion en direct (l'enregistrement est disponible à l'adresse
[https://meetings-archive.debian.net/pub/debian-meetings/2020/DebConf20/](https://meetings-archive.debian.net/pub/debian-meetings/2020/DebConf20/)),
sur [la session Etherpad](https://pad.online.debconf.org/p/50-local-teams)
et sur le canal IRC #debian-localgroups. Cet article est une tentative pour
résumer les éléments essentiels qui ressortent de cette discussion, ainsi que
les projets d'actions futures pour soutenir les groupes locaux Debian nouveaux
ou déjà créés et l'éventualité de monter une équipe d'assistance aux groupes
locaux.

## Situation de pandémie

Durant une pandémie, il peut paraître étrange de discuter de réunions non
connectées, mais en fait c'est le bon moment pour faire des projets d'avenir.
En même temps, la situation actuelle rend encore plus important de favoriser
des interactions locales.

## Des arguments en faveur de groupes locaux

Debian peut paraître intimidante de l'extérieur. Entretenir déjà des relations
avec Debian – en particulier avec des personnes directement impliquées dans le
projet – semble être le chemin que suivent la plupart des contributeurs. Mais
si quelqu'un ne bénéficie pas de ce type de contact, ce n'est pas aussi
facile ; des groupes locaux peuvent faciliter les choses en améliorant la mise
en réseau.

Les groupes locaux sont extrêmement importants pour le succès de Debian dans la
mesure où, souvent, ils aident aux traductions, améliorent notre diversité,
apportent leur soutien, mettent sur pied des chasses aux bogues, forment les
équipes locales des DebConf ainsi que celles des miniDebConf, trouvent des
parrains pour le projet et bien plus encore.

L'existence de groupes locaux pourrait aussi faciliter l'accès aux articles
promotionnels tels que les autocollants et les tasses, parce que les gens n'ont
pas toujours le temps pour dénicher des fournisseurs pour leur fabrication. Le
travail des groupes locaux pourrait faciliter cela en organisant la logistique
associée.

## Comment s'occuper des groupes locaux et comment les définir

Debian rassemble les informations sur les groupes locaux sur la
[page dédiée du wiki](https://wiki.debian.org/LocalGroups) (et ses sous-pages).
D'autres organisations ont aussi leur propre schéma, parfois même affichent un
plan, des blogs ou des règles strictes sur ce qui constitue un groupe local.
Dans le cas de Debian, il n'existe pas d'ensemble de « règles » prédéfinies,
même en ce qui concerne le nom des groupes. Cela convient parfaitement, car
nous partons du principe que certains groupes locaux peuvent être très petits
ou temporaires (créés à un certain moment quand des activités sont programmées,
ils retournent ensuite au silence). Néanmoins, la manière dont les groupes
sont appelés et comment ils sont listés sur la page du wiki définissent ce que
l'on peut en attendre en fonction des types d'activités qu'ils impliquent.

Pour cette raison, nous encourageons tous les groupes locaux de Debian à
vérifier les données qui les concernent dans le
[Debian wiki](https://wiki.debian.org/LocalGroups), à les maintenir à jour
(par exemple en ajoutant une ligne « État : actif – 2020 »), et nous appelons
les groupes informels de contributeurs à Debian qui se réunissent d'une manière
ou d'une autre à créer, eux aussi, une nouvelle entrée sur la page du wiki.

## Quelle assistance de Debian pour les groupes locaux ?

Établir une base de données centralisée des groupes est une bonne chose (si
elle est à jour), mais ce n'est pas suffisant. Nous allons explorer d'autres
voies pour faciliter leur création et augmenter leur visibilité, par exemple en
organisant la logistique pour imprimer et diffuser les articles promotionnels
et faciliter le financement des événements liés à Debian.

## Poursuivre nos efforts

Les efforts consacrés aux groupes locaux vont se poursuivre. Des réunions
régulières sont organisées toutes les deux ou trois semaines ; les personnes
intéressées sont invitées à consulter d'autres communications de DebConf20 sur
le sujet
([présentation de Debian Brésil](https://debconf20.debconf.org/talks/105-introducing-debian-brasil/) ;
[académie Debian : une autre manière pour partager les connaissances sur Debian](https://debconf20.debconf.org/talks/30-debian-academy-another-way-to-share-knowledge-about-debian/),
[une expérience de création de communauté locale dans une petite ville](https://debconf20.debconf.org/talks/55-an-experience-creating-a-local-community-in-a-small-town/)),
des sites web comme [Debian flyers](https://debian.pages.debian.net/debian-flyers/)
(comprenant d'autres matériels comme les cubes et les autocollants),
à visiter la [section événements du site de Debian](https://www.debian.org/events/)
et la page du wiki [Debian Locations](https://wiki.debian.org/DebianLocations)
et enfin à participer au canal IRC #debian-localgroups sur OFTC.
