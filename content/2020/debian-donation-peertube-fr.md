Title: Don de Debian pour le développement de Peertube
Slug: debian-donation-peertube
Date: 2020-10-21 12:30
Author: Louis-Philippe Véronneau, Pouhiou, Laura Arjona Reina
Tags: debconf20, donation, fundraising, peertube
Status: published
Translator: Louis-Philippe Véronneau, Jean-Pierre Giraud
Lang: fr

Le projet Debian a le plaisir d'annoncer qu'il effectue une donation de
10 000 € à [Framasoft](https://framasoft.org) pour l'aider
à atteindre la quatrième étape ambitieuse de sa
[campagne de financement communautaire de Peertube v3 – diffusion en direct](https://joinpeertube.org/roadmap).

En effet, Debian a dû tenir en ligne l'édition 2020 de sa conférence annuelle
[DebConf20](https://debconf20.debconf.org/), et, bien qu'il fut un succès
incontestable, cet événement a démontré la nécessité pour le projet Debian
d'avoir une infrastructure permanente permettant aux groupes locaux de diffuser
en direct des événements ponctuels. En tant que tel,
[Peertube](https://joinpeertube.org/), une plateforme libre d'hébergement de
vidéos en ligne, nous a paru être une solution parfaitement appropriée.

Nous espérons que ce geste inhabituel de la part du projet Debian nous aidera
à traverser cette année terrible et nous donnera, à nous et par conséquent en
l'ensemble du monde, de meilleurs outils libres pour affronter l'avenir.

Debian remercie ses nombreux donatrices et donateurs et les parrains de DebConf
pour leur engagement et en particulier tous ceux qui ont contribué au succès
de DebConf20 en ligne (les bénévoles, les intervenants et les parrains). Le
projet remercie également Framasoft et la communauté PeerTube pour le
développement de la plateforme vidéo libre et décentralisée PeerTube.

De son côté, l'association [Framasoft](https://framasoft.org) remercie
chaudement le projet Debian de contribuer, sur ses fond propres, à faire
exister le logiciel PeerTube.

Cette contribution a une double portée. C'est, d'une part, un signe fort de
reconnaissance de la part d'un projet international et d'un pilier du monde
du libre envers une petite association française qui propose des outils
d'émancipation des monopoles maintenus par les géants du web. Il s'agit,
d'autre part, d'une aide non-négligeable, dans une période difficile, afin
de financer la conception d'un outil qui appartient à tous et peut servir à
chacun.

La force de ce geste de la part du projet Debian prouve, une fois de plus, que
la solidarité, l'entraide et la collaboration sont des valeurs qui permettent
à nos communautés de concrétiser des outils relevant de l'utopie.
