Title: 新的 Debian 开发者和维护者 (2020年5月 至 6月)
Slug: new-developers-2020-06
Date: 2020-07-21 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: zh-CN
Translator: Boyuan Yang
Status: published

下列贡献者在过去的两个月中获得了他们的 Debian 开发者帐号：

* Richard Laager (rlaager)
* Thiago Andrade Marques (andrade)
* Vincent Prat (vivi)
* Michael Robin Crusoe (crusoe)
* Jordan Justen (jljusten)
* Anuradha Weeraman (anuradha)
* Bernelle Verster (indiebio)
* Gabriel F. T. Gomes (gabriel)
* Kurt Kremitzki (kkremitzki)
* Nicolas Mora (babelouest)
* Birger Schacht (birger)
* Sudip Mukherjee (sudip)

下列贡献者在过去的两个月中被添加为 Debian 维护者：

* Marco Trevisan
* Dennis Braun
* Stephane Neveu
* Seunghun Han
* Alexander Johan Georg Kjäll
* Friedrich Beckmann
* Diego M. Rodriguez
* Nilesh Patra
* Hiroshi Yokota

祝贺他们！
