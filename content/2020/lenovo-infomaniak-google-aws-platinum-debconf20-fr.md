Title: Lenovo, Infomaniak, Google et Amazon Web Services (AWS), parrains de platine de DebConf20
Slug: lenovo-infomaniak-google-aws-platinum-debconf20
Date: 2020-08-20 20:25
Author: Laura Arjona Reina
Artist: Lenovo, Infomaniak, Google, AWS
Tags: debconf20, debconf, sponsors, lenovo, infomaniak, google, aws
Lang: fr
Translator: Jean-Pierre Giraud
Status: published

Nous sommes heureux d'annoncer que [**Lenovo**](https://www.lenovo.com),
[**Infomaniak**](https://www.infomaniak.com),
[**Google**](https://google.com)
et [**Amazon Web Services (AWS)**](https://aws.amazon.com),
soutiennent [DebConf20](https://debconf20.debconf.org) en tant que
**parrains de platine**.

[![lenovologo](|static|/images/lenovo.png)](https://www.lenovo.com)

En tant que leader mondial en technologie, produisant une vaste gamme de
produits connectés, comprenant des smartphones, des tablettes, des
machines de bureau et des stations de travail aussi bien que des
périphériques de réalité augmentée et de réalité virtuelle, des solutions
pour la domotique ou le bureau connecté et les centres de données,
[**Lenovo**](https://www.lenovo.com) a compris combien étaient
essentiels les plateformes et systèmes ouverts pour un monde connecté.

[![infomaniaklogo](|static|/images/infomaniak.png)](https://www.infomaniak.com)

[**Infomaniak**](https://www.infomaniak.com) est la plus grande compagnie
suisse d'hébergement web qui offre aussi des services de sauvegarde et de
stockage, des solutions pour les organisateurs d'événement et des
services de diffusion en direct et de vidéo à la demande.
Elle possède l'intégralité de ses centres de données et de tous les éléments
essentiels pour le fonctionnement des services et produits qu'elle fournit
(à la fois sur le plan matériel et logiciel).

[![Googlelogo](|static|/images/google.png)](https://www.google.com)

[**Google**](https://google.com/) est l'une des plus grandes entreprises
technologique du monde qui fournit une large gamme de services relatifs
à Internet et de produits tels que des technologies de publicité en
ligne, des outils de recherche, de l'informatique dans les nuages, des
logiciels et du matériel.

Google apporte son soutien à Debian en parrainant la DebConf depuis plus
de dix ans, et est également un partenaire de Debian en parrainant des
composants de l'infrastructure d'intégration continue de
[Salsa](https://salsa.debian.org) au sein de la plateforme Google Cloud.

[![AWSlogo](|static|/images/aws.png)](https://aws.amazon.com)

[**Amazon Web Services (AWS)**](https://aws.amazon.com) est une des
plateformes de nuage les plus complètes et largement adoptées au monde,
proposant plus de 175 services complets issus de centres de données du monde
entier (dans 77 zones de disponibilité dans 24 régions géographiques).
Parmi les clients d'AWS sont présentes des jeunes pousses à croissance rapide,
de grandes entreprises et des organisations majeures du secteur public.

Grâce à leur engagement comme parrain de platine, Lenovo, Infomaniak,
Google et Amazon Web Services contribuent à rendre possible notre
conférence annuelle, et soutiennent directement les avancées de Debian et du
logiciel libre en aidant à renforcer la communauté qui continue à travailler
ensemble sur les différents aspects du projet Debian tout au long de l'année.

Merci beaucoup pour votre soutien à DebConf20 !

## Participer à DebConf20 en ligne

La vingt-et-unième conférence Debian se tiendra en ligne du 23 au 29 août du
fait de la COVID-19. Il y aura sept journées d'activités de 10h00 à 01h00 UTC.
Visitez le site internet de DebConf20 à l'adresse
[https://debconf20.debconf.org](https://debconf20.debconf.org)
pour en savoir plus sur le programme complet, pour voir les diffusions en
direct et pour rejoindre les différents canaux de communication afin de
participer à la conférence.
