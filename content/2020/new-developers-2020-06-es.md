Title: Nuevos desarrolladores y mantenedores de Debian (mayo y junio del 2020)
Slug: new-developers-2020-06
Date: 2020-07-21 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: es
Status: published

Los siguientes colaboradores del proyecto se convirtieron en Debian Developers
en los dos últimos meses:

* Richard Laager (rlaager)
* Thiago Andrade Marques (andrade)
* Vincent Prat (vivi)
* Michael Robin Crusoe (crusoe)
* Jordan Justen (jljusten)
* Anuradha Weeraman (anuradha)
* Bernelle Verster (indiebio)
* Gabriel F. T. Gomes (gabriel)
* Kurt Kremitzki (kkremitzki)
* Nicolas Mora (babelouest)
* Birger Schacht (birger)
* Sudip Mukherjee (sudip)

Los siguientes colaboradores del proyecto se convirtieron en Debian Maintainers
en los dos últimos meses:

* Marco Trevisan
* Dennis Braun
* Stephane Neveu
* Seunghun Han
* Alexander Johan Georg Kjäll
* Friedrich Beckmann
* Diego M. Rodriguez
* Nilesh Patra
* Hiroshi Yokota

¡Felicidades a todos!
