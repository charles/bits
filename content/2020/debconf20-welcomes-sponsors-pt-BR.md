Title: A DebConf20 dá boas-vindas aos(às) seus(suas) patrocinadores(as)!
Slug: debconf20-welcomes-sponsors
Date: 2020-08-28 10:30
Author: Laura Arjona Reina
Tags: debconf20, debconf, sponsors
Lang: pt-BR
Translator: Thiago Pezzo (Tico)
Status: published

![DebConf20 logo](|static|/images/dc20-logo-horizontal-diversity.png)

A DebConf20 está acontecendo on-line de 23 a 29 de agosto de 2020.
Esta é a 21a. conferência Debian, e organizadores(as) e participantes estão
trabalhando arduamente e unidos(as) para criar eventos interessantes e
frutíferos.

Gostaríamos de calorosamente dar boas vindas aos(às) 17 patrocinadores(as) da
DebConf20 e apresentá-los(as) a você.

Temos quatro patrocinadores(as) Platinum.

Nossa primeira patrocinadora Platinum é a [**Lenovo**](https://www.lenovo.com).
Como uma líder global em tecnologia, fabricando um amplo portfólio de produtos
conectados, incluindo smartphones, tablets, PCs e workstations, bem como
dispositivos de realidade aumentada/virtual, casas/escritórios inteligentes e
soluções para datacenters, a Lenovo entende quão críticos são os sistemas
abertos e as plataformas para um mundo conectado.

Nossa próxima patrocinadora Platinum é a
[**Infomaniak**](https://www.infomaniak.com/en). Infomaniak é a maior empresa
de hospedagem web da Suíça, que também oferece serviços de backup e de
armazenamento, soluções para organizadores(as) de eventos, serviços de
transmissão em tempo real e vídeo sob demanda.
Todos os datacenters e elementos críticos para o funcionamento dos serviços e
produtos oferecidos pela empresa (tanto software quanto hardware) são de
propriedade absoluta da Infomaniak.

O [**Google**](https://google.com) é nossa terceiro patrocinador Platinum.
Google é uma das maiores empresas de tecnologia do mundo, fornecendo uma ampla
gama de serviços e produtos relacionados à Internet como tecnologias de
publicidade on-line, pesquisa, computação em nuvem, software e hardware.
O Google tem apoiado o Debian através do patrocínio da DebConf por mais de
dez anos, e é também um [parceiro Debian](https://www.debian.org/partners/).

A [**Amazon Web Services (AWS)**](https://aws.amazon.com) é nossa quarta
patrocinadora Platinum.
em nuvem mais abrangentes e largamente adotadas no mundo,
oferecendo mais de 175 serviços repletos de funcionalidades via datacenters
globais (em 77 zonas de disponibilidade dentro de 24 regiões geográficas).
Consumidores(as) da AWS incluem as startups de mais rápido crescimento, as
maiores empresas e as agências governamentais de liderança.

Nossos(as) patrocinadores(as) Gold são Deepin, Matanel Foundation, Collabora e
HRT.

A [**Deepin**](https://www.deepin.com/) é uma companhia comercial Chinesa com
foco no desenvolvimento e atendimento de sistemas operacionais baseados em
Linux. Ela também lidera a pesquisa e desenvolvimento do derivativo Debian
Deepin.

[**The Matanel Foundation**](http://www.matanel.org) opera em Israel,
e sua atenção principal está em preservar a coesão da sociedade e da nação,
atormentadas por divisões. The Matanel Foundation também trabalha na Europa,
na África e na América do Sul.

A [**Collabora**](https://www.collabora.com/) é uma consultoria global que
implementa soluções em software de Código Aberto para o mundo comercial.
Além de oferecer soluções para clientes, os(as) engenheiros(as) e
desenvolvedores(as) da Collabora  contribuem ativamente para muitos projetos de
Código Aberto.

A [**Hudson-Trading**](https://www.hudsonrivertrading.com)
é uma companhia liderada por matemáticos(as), cientistas da computação,
estatísticos(as), físicos(as) e engenheiros(as). Eles(as) pesquisam e
desenvolvem algoritmos comerciais automáticos usando avançadas técnicas
matemáticas.

Nossos(as) patrocinadores(as) Silver são:

[**Linux Professional Institute**](https://www.lpi.org/), a organização
padrão de certificação global e de suporte a carreiras para profissionais de
código aberto,
[**Civil Infrastructure Platform**](https://www.cip-project.org/),
um projeto colaborativo hospedado pela Linux Foundation,
estabelecendo uma "camada de base" em código aberto de software com fins
industriais, [**Ubuntu**](https://www.canonical.com/),
o Sistema Operacional fornecido pela Canonical,
e [**Roche**](https://code4life.roche.com/),
uma importante companhia internacional fornecedora e pesquisadora de
farmacêuticos dedicada à assistência à saúde personalizada.

Patrocinadores(as) Bronze:
[**IBM**](https://www.ibm.com/),
[**MySQL**](https://www.mysql.com),
[**Univention**](https://www.univention.com/).

E finalmente, nossos(as) patrocinadores(as) de nível de apoio,
[**ISG.EE**](https://isg.ee.ethz.ch/) e
[**Pengwin**](https://www.pengwin.dev/).

Obrigado(a) a todos(as) os(as) nossos(as) patrocinadores(as) pelo apoio!
Suas contribuições tornaram possível para um grande número
de contribuidores(as) Debian de todo o mundo trabalharem juntos(as),
mutuamente ajudando e aprendendo na DebConf20.

## Participando da DebConf20 on-line

A 21a. Conferência Debian será feita on-line, devido ao COVID-19,
de 23 a 29 de agosto de 2020. Palestras, debates, painéis e outras atividades
acontecem das 10h à 1h UTC (7h às 21h no horário de Brasília)
Visite o site web da DebConf20 em [https://debconf20.debconf.org](https://debconf20.debconf.org)
para conhecer a programação completa, assistir às lives e juntar-se aos
diferentes canais de comunicação para participar da conferência.
