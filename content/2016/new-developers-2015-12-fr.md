Title: Nouveaux développeurs et mainteneurs de Debian (novembre et décembre 2015)
Slug: new-developers-2015-12
Date: 2016-01-12 12:30
Author: Jean-Pierre Giraud
Tags: project
Translator: Jean-Pierre Giraud
Lang: fr
Status: published

Les contributeurs suivants sont devenus développeurs Debian ces deux
derniers mois :

* Stein Magnus Jodal (jodal)
* Prach Pongpanich (prach)
* Markus Koschany (apo)
* Bernhard Schmidt (berni)
* Uwe Kleine-König (ukleinek)
* Timo Weingärtner (tiwe)
* Sebastian Andrzej Siewior (bigeasy)
* Mattia Rizzolo (mattia)
* Alexandre Viau (aviau)
* Lev Lamberov (dogsleg)
* Adam Borowski (kilobyte)
* Chris Boot (bootc)

et ces contributeurs ont été acceptés comme mainteneurs Debian durant la
même période :

* Alf Gaida
* Andrew Ayer
* Marcio de Souza Oliveira
* Alexandre Detiste
* Dave Hibberd
* Andreas Boll
* Punit Agrawal
* Edward Betts
* Shih-Yuan Lee
* Ivan Udovichenko
* Andrew Kelley
* Benda Xu
* Russell Sim
* Paulo Roberto Alves de Oliveira
* Marc Fournier
* Scott Talbert
* Sergio Durigan Junior
* Guillaume Turri
* Michael Lustfield

Félicitations !
