Title: Nouveaux développeurs et mainteneurs de Debian (janvier et février 2016)
Slug: new-developers-2016-02
Date: 2016-03-14 21:30
Author: Jean-Pierre Giraud
Tags: project
Translator: Jean-Pierre Giraud
Lang: fr
Status: published

Les contributeurs suivants sont devenus développeurs Debian ces deux
derniers mois :

* Otto Kekäläinen (otto)
* Dariusz Dwornikowski (darek)
* Daniel Stender (stender)
* Afif Elghraoui (afif)
* Victor Seva (vseva)
* James Cowgill (jcowgill)

et ces contributeurs ont été acceptés comme mainteneurs Debian durant la
même période :

* Giovani Augusto Ferreira
* Ondřej Nový
* Jason Pleau
* Michael Robin Crusoe
* Ferenc Wágner
* Enrico Rossi
* Christian Seiler
* Daniel Echeverry
* Ilias Tsitsimpis
* James Clarke
* Luca Boccassi

Félicitations !
