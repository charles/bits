Title: Nuevos desarrolladores y mantenedores de Debian (noviembre y diciembre del 2015)
Slug: new-developers-2015-12
Date: 2016-01-12 12:30
Author: Jean-Pierre Giraud
Tags: project
Lang: es
Status: published

Los siguientes colaboradores del proyecto se convirtieron en Debian Developers
en los dos últimos meses:

* Stein Magnus Jodal (jodal)
* Prach Pongpanich (prach)
* Markus Koschany (apo)
* Bernhard Schmidt (berni)
* Uwe Kleine-König (ukleinek)
* Timo Weingärtner (tiwe)
* Sebastian Andrzej Siewior (bigeasy)
* Mattia Rizzolo (mattia)
* Alexandre Viau (aviau)
* Lev Lamberov (dogsleg)
* Adam Borowski (kilobyte)
* Chris Boot (bootc)

Los siguientes colaboradores del proyecto se convirtieron en Debian Maintainers
en los dos últimos meses:

* Alf Gaida
* Andrew Ayer
* Marcio de Souza Oliveira
* Alexandre Detiste
* Dave Hibberd
* Andreas Boll
* Punit Agrawal
* Edward Betts
* Shih-Yuan Lee
* Ivan Udovichenko
* Andrew Kelley
* Benda Xu
* Russell Sim
* Paulo Roberto Alves de Oliveira
* Marc Fournier
* Scott Talbert
* Sergio Durigan Junior
* Guillaume Turri
* Michael Lustfield

¡Felicidades a todos!
