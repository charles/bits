Title: Debian chào đón thực tập hè 2016 của mình
Slug: welcome-summer-interns-2016
Date: 2016-04-24 21:00
Author: Nicolas Dandrimont
Tags:  gsoc, outreachy, google, announce, development, diversity, software, code, projects
Translator: Giap Tran
Lang: vi
Status: published

![GSoC 2016 logo](|static|/images/gsoc2016.jpg) ![Outreachy logo](|static|/images/outreachy-logo-300x61.png)

Chúng tôi vui mừng thông báo rằng Debian đã chọn được 29 thực tập sinh làm việc với chúng tôi
trong mùa hè này: 4 trong [Outreachy][1], và 25 trong [Google Summer of Code][2].

[1]: https://wiki.gnome.org/Outreachy/2016/MayAugust#Debian
[2]: https://summerofcode.withgoogle.com/organizations/5023548368224256/#projects

Đây là danh sách các dự án và thực tập sinh, những người sẽ làm việc với chúng:

[Công cụ Android SDK](https://wiki.debian.org/AndroidTools) trên Debian:

+ [Chirayu Desai](https://wiki.debian.org/SummerOfCode2016/StudentApplications/ChirayuDesai)
+ [Kai-Chung Yan](https://wiki.debian.org/SummerOfCode2016/StudentApplications/KaichungYan)
+ [Mouaad Aallam](https://wiki.debian.org/SummerOfCode2016/StudentApplications/MouaadAallam)

APT - Cải tiến dpkg:

+ [David Kalnischkies](https://wiki.debian.org/SummerOfCode2016/StudentApplications/DavidKalnischkies)

Tiếp tục phân tích các gói [Debian-Med](https://www.debian.org/devel/debian-med/):

+ [Canberk Koç](https://wiki.debian.org/SummerOfCode2016/StudentApplications/CanberkKoc)
+ [Tatiana Malygina](https://wiki.gnome.org/Outreachy/2016/MayAugust#Debian)

Mở rộng [Debian Developer Horizon](http://horizon.debian.net/):

+ [Harsh Daftary](http://wiki.debian.org/SummerOfCode2016/StudentApplications/HarshDaftary)

Cải thiện và mở rộng [AppRecommender](https://github.com/tassia/AppRecommender/wiki):

+ [Lucas Albuquerque Medeiros de Moura](https://wiki.debian.org/SummerOfCode2016/StudentApplications/LucasMoura)
+ [Luciano Prestes Cavalcanti](https://wiki.debian.org/SummerOfCode2016/StudentApplications/LucianoPrestes)

Cải thiện giao diện người dùng của [debsources](https://sources.debian.net):

+ [Aaron Delaney](https://wiki.debian.org/SummerOfCode2016/StudentApplications/AaronDelaney)

Cải thiện [voice, video and chat communication](https://wiki.debian.org/SummerOfCode2016/Projects/Voice%20Webcam%20and%20Chat%20Communication) với Free Software:

+ [Alok Anand](https://wiki.debian.org/SummerOfCode2016/StudentApplications/AlokAnand)
+ [Balram P](https://wiki.debian.org/SummerOfCode2016/StudentApplications/BalramP)
+ [Jaminy Prabaharan](https://wiki.debian.org/SummerOfCode2016/StudentApplications/Jaminy)
+ [Keerthana Krishnan](https://wiki.debian.org/SummerOfCode2016/StudentApplications/KeerthanaKrishnan)
+ [Kévin Avignon](https://wiki.debian.org/SummerOfCode2016/StudentApplications/KevinAvignon)
+ [Mateus Bellomo](https://wiki.debian.org/SummerOfCode2016/StudentApplications/MateusBellomo)
+ [Mesut Can Gurle](https://wiki.debian.org/SummerOfCode2016/StudentApplications/MesutCanGurle)
+ [Nicolas Reynaud](https://wiki.debian.org/SummerOfCode2016/StudentApplications/NicolasReynaud)
+ [Nik Vaessen](https://wiki.debian.org/SummerOfCode2016/StudentApplications/NikVaes)
+ [Olivier Grégoire](https://wiki.debian.org/SummerOfCode2016/StudentApplications/OlivierGr%C3%A9goire)
+ [Pranav Jain](https://wiki.debian.org/SummerOfCode2016/StudentApplications/PranavJain)
+ [Simon Désaulniers](https://wiki.debian.org/SummerOfCode2016/StudentApplications/SimonD%C3%A9saulniers)
+ [Udit Raikwar](https://wiki.debian.org/SummerOfCode2016/StudentApplications/UditRaikwar)

Cải tiến nền tảng MIPS và MIPSEL:

+ [Jonathan Jackson](https://wiki.debian.org/SummerOfCode2016/StudentApplications/JonathanJackson)

[Reproducible Builds](https://reproducible-builds.org/) cho Debian và Free Software:

+ [ceridwen](https://wiki.gnome.org/Outreachy/2016/MayAugust#Debian)
+ [Satyam Zode](https://wiki.debian.org/SummerOfCode2016/StudentApplications/SatyamZode)
+ [Scarlett Clark](https://wiki.gnome.org/Outreachy/2016/MayAugust#Debian)
+ [Valerie Young](https://wiki.gnome.org/Outreachy/2016/MayAugust#Debian)

Hỗ trợ cho [KLEE](http://klee.github.io/) trong Debile:

+ [Marko Dimjašević](https://wiki.debian.org/SummerOfCode2016/StudentApplications/MarkoDimjasevic)

Chương trình Google Summer of Code và Outreachy có thể có cho Debian nhờ vào
sự nỗ lực của các nhà phát triển và đóng góp Debian đã dành một phần thời gian
rảnh của mình để cố vấn cho các sinh viên và tiếp cận cộng đồng.

Tham gia cùng chúng tôi và mở rộng Debian! Bạn có thể theo dõi các báo cáo tuần của các sinh viên
trên [bó thư debian-outreach][debian-outreach-ml], trò chuyện với chúng tôi trên
[kênh IRC của chúng ta][debian-soc-irc] hoặc bó thư của mỗi nhóm dự án.

[debian-outreach-ml]: https://lists.debian.org/debian-outreach/ (debian-outreach AT lists.debian.org)
[debian-soc-irc]: irc://irc.debian.org/#debian-soc (#debian-soc on irc.debian.org)

Xin chúc mừng tất cả các bạn trên!
