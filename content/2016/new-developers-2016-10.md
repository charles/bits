Title: New Debian Developers and Maintainers (September and October 2016)
Slug: new-developers-2016-10
Date: 2016-11-03 12:00
Author: Jean-Pierre Giraud
Tags: project
Status: published

The following contributors got their Debian Developer accounts in the last two
months:

* Adriano Rafael Gomes (adrianorg)
* Arturo Borrero González (arturo)
* Sandro Knauß (hefee)

The following contributors were added as Debian Maintainers in the last two
months:

* Abhijith PA
* Mo Zhou
* Víctor Cuadrado Juan
* Zygmunt Bazyli Krynicki
* Robert Haist
* Sunil Mohan Adapa
* Elena Grandi
* Eric Heintzmann
* Dylan Aïssi
* Daniel Shahaf
* Samuel Henrique
* Kai-Chung Yan
* Tino Mettler

Congratulations!
