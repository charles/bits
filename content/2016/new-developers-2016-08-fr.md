Title: Nouveaux développeurs et mainteneurs de Debian (juillet et août 2016)
Slug: new-developers-2016-08
Date: 2016-09-03 12:00
Author: Jean-Pierre Giraud
Tags: project
Lang: fr
Status: published

Les contributeurs suivants sont devenus développeurs Debian ces deux
derniers mois :

* Edward John Betts (edward)
* Holger Wansing (holgerw)
* Timothy Martin Potter (tpot)
* Martijn van Brummelen (mvb)
* Stéphane Blondon (sblondon)
* Bertrand Marc (bmarc)
* Jochen Sprickerhof (jspricke)
* Ben Finney (bignose)
* Breno Leitao (leitao)
* Zlatan Todoric (zlatan)
* Ferenc Wágner (wferi)
* Matthieu Caneill (matthieucan)
* Steven Chamberlain (stevenc)

et ces contributeurs ont été acceptés comme mainteneurs Debian durant la
même période :

* Jonathan Cristopher Carter
* Reiner Herrmann
* Michael Jeanson
* Jens Reyer
* Jerome Benoit
* Frédéric Bonnard
* Olek Wojnar

Félicitations !
