Title: DebConf16: Call for Proposals
Date: 2016-03-24 10:00
Tags: debconf, debconf16, cfp
Slug: debconf16-cfp
Author: Allison Randal
Status: published

The DebConf Content team is pleased to announce the Call for Proposals for the
[DebConf16 conference](https://debconf16.debconf.org/), to be held in
**Cape Town, South Africa** from 2 through 9 July 2016.

## Submitting an Event

In order to submit an event, go to "_Submit a talk_" on
[your profile page in the DebConf16 website](https://debconf16.debconf.org/accounts/login/)
and describe your proposal. Please note, events are not limited to traditional
presentations or informal sessions (BoFs). We welcome submissions of
tutorials, performances, art installations, debates, or any other format of
event that you think would be beneficial to the Debian community.

Please include a short title, suitable for a compact schedule, and an engaging
description of the event. You should use the field "_Notes_" to provide us
information such as additional speakers, scheduling restrictions, or any
special requirements we should consider for your event.

Regular sessions may either be 20 or 45 minutes long (including time for
questions), other kinds of sessions (like workshops) could have different
durations. Please choose the most suitable duration for your event and
explain any special requests.

## Timeline

The first batch of accepted proposals will be announced in **April**. If you
depend on having your proposal accepted in order to attend the conference,
please submit it as soon as possible so that it can be considered during this
first evaluation period.

All proposals must be submitted before **Sunday 1 May 2016** to be evaluated
for the official schedule.

## Topics and Tracks

Though we invite proposals on any Debian or FLOSS related subject, we have
some broad topics on which we encourage people to submit proposals, including:

- Debian Packaging, Policy, and Infrastructure
- Security, Safety, and Hacking
- Debian System Administration, Automation and Orchestration
- Containers and Cloud Computing with Debian
- Debian Success Stories
- Debian in the Social, Ethical, Legal, and Political Context
- Blends, Subprojects, Derivatives, and Projects using Debian
- Embedded Debian and Hardware-Level Systems

## Video Coverage

Providing video of sessions amplifies DebConf achievements and is one of the
conference [goals](<http://debconf.org/goals.shtml). Unless speakers opt-out,
official events will be streamed live over the Internet to promote remote
participation. Recordings will be published later under the
[DebConf license](http://meetings-archive.debian.net/pub/debian-meetings/LICENSE),
as well as presentation slides and papers whenever available.

## Contact and Thanks to Sponsors

DebConf would not be possible without the generous support of all our
sponsors, especially our platinum sponsor
[HPE]. DebConf16 is still accepting sponsors; if you are interested, please [get in touch](https://debconf16.debconf.org/contribute)!

You are welcome to contact the Content Team with any concerns about your
event, or with any ideas or questions about DebConf events in general. You
can reach us at [content@debconf.org](mailto:content@debconf.org).

[HPE]: http://www.hpe.com/engage/opensource

## Registration Reminder

Registration for DebConf is open. Please log into the
DebConf16 website and register from
[your profile page](https://debconf16.debconf.org/accounts/profile/).

To request bursaries (sponsorship) for food, accommodation, or travel, you
must be registered by Sunday, 10 April 2016.

After this date, registrations will still be accepted in any of the basic,
professional, and corporate categories. However, accommodation on the campus
will no longer be guaranteed, and requests for sponsorship will no longer be
accepted.

Even if you are not certain you will be able to attend, we recommend
registering now.  You can always cancel your registration, before the deadline.
We do suggest that attendees begin making travel arrangements as soon as
possible, of course.

We hope to see you all in Cape Town!
