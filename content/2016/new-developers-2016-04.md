Title: New Debian Developers and Maintainers (March and April 2016)
Slug: new-developers-2016-04
Date: 2016-05-17 00:10
Author: Ana Guerrero Lopez
Tags: project
Status: published

The following contributors got their Debian Developer accounts in the last two
months:

* Sven Bartscher (kritzefitz)
* Harlan Lieberman-Berg (hlieberman)

Congratulations!
