Title: DebConf16: Kêu gọi đề xuất
Date: 2016-03-24 10:00
Tags: debconf, debconf16, cfp
Slug: debconf16-cfp
Author: Allison Randal
Translator: Lê Thị Minh
Lang: vi
Status: published

Nhóm Nội dung DebConf vui mừng thông báo kêu gọi các đề xuất cho [hội nghị Debian 2016](https://debconf16.debconf.org/), được tổ chức tại **Cape Town, Cộng Hòa Nam Phi** từ ngày 2 đến ngày 9 tháng 7 năm 2016.

## Đăng ký hoạt động

Để đăng ký hoạt động, vào “_Đăng ký một buổi nói chuyện_” ở [trang cá nhân của bạn nằm trên trang thông tin điện tử DebConf16](https://debconf16.debconf.org/accounts/login/) và mô tả đề xuất của bạn. Xin lưu ý rằng, đối với các bài thuyết trình truyền thống hay các buổi gặp thân mật (BoFs) thì không giới hạn việc đăng ký. Chúng tôi hoan nghênh các đăng ký về hướng dẫn, biểu diễn, sắp đặt nghệ thuật, tranh luận hoặc bất cứ hình thức nào theo bạn là có ích đối với cộng đồng Debian.

Vui lòng thêm phần tiêu đề ngắn ngọn, phù hợp với lịch trình và một đoạn mô tả về hoạt động đó. Bạn nên sử dụng trường "_Notes_" để cung cấp thông tin về các diễn giả bổ sung, giới hạn chương trình, hay bất cứ yêu cầu đặc biệt nào mà chúng tôi cần xem xét.

Các nội dung thường xuyên có thể diễn ra trong vòng 20 hoặc 45 phút (gồm cả thời gian hỏi), các nội dung khác (ví dụ như hội thảo) có thể có thời lượng khác nhau. Vui lòng chọn thời lượng phù hợp nhất cho hoạt động của bạn và nêu rõ nếu có bất cứ các yêu cầu đặc biệt nào.

## Tiến độ

Các đề xuất được chấp thuận trong đợt đầu sẽ được công bố vào **tháng tư**. Nếu bạn phụ thuộc vào việc đề xuất của bạn được chấp nhận thì mới tham dự hội nghị, thì vui lòng gửi càng sớm càng tốt để có thể được xem xét trong giai đoạn đánh giá đầu tiên này.

Tất cả các đề xuất phải nộp trước **chủ nhật, ngày 01 tháng 5 năm 2016** để được đánh giá để đưa vào lịch chính thức.

## Chủ đề và theo dõi

Mặc dù chúng tôi kêu gọi các đề xuất về Debian hoặc chủ đề liên quan đến FLOSS, nhưng có một số chủ đề rộng mà chúng tôi khuyến khích mọi người đăng ký, bao gồm:

- Gói Debian, Chính sách, và Cơ sở hạ tầng
- An ninh, An toàn và Hacking
- Quản trị, Tự động hóa và sắp xếp hệ thống Debian
- Lưu trữ và Điện toán đám mây với Debian
- Các câu chuyện thành công về Debian
- Debian với bối cảnh xã hội, đạo đức, pháp lý và chính trị
- Blends, tiểu dự án, công cụ phái sinh, và các dự án sử dụng Debian
- Debian cho hệ thống nhúng và các hệ thống ở mức phần cứng

## Đưa tin qua Video

Cung cấp các đoạn video để khuếch trương những thành tựu của Hội nghị Debian và đó cũng là trong những [mục tiêu](<http://debconf.org/goals.shtml) của hội nghị. Nếu các diễn giả không hủy đăng ký thì các hoạt động chính thức sẽ được phát trực tiếp qua internet để đẩy mạnh sự tham gia từ xa. Các đoạn ghi hình sau đó sẽ được đăng công khai dưới [giấy phép của DebConf](http://meetings-archive.debian.net/pub/debian-meetings/LICENSE), đồng thời còn có các bài thuyết trình và tài liệu.

## Liên hệ và cảm ơn nhà tài trợ

Hội nghị Debian sẽ không thể tồn tại nếu không có sự hỗ trợ nhiệt tình từ các nhà tài trợ, đặc biệt nhà tài trợ kim cương [HPE]. DebConf16 vẫn còn tiếp nhận tài trợ; nếu bạn quan tâm, vui lòng [liên lạc với chúng tôi](https://debconf16.debconf.org/contribute)!

Hoan nghênh bạn liên hệ với nhóm Nội dung nếu bạn có bất cứ ý kiến hoặc thắc mắc về các sự kiện của Debian nói chung. Bạn có thể liên hệ với chúng tôi qua [content@debconf.org](mailto:content@debconf.org).

[HPE]: http://www.hpe.com/engage/opensource

## Nhắc nhở đăng ký

Việc đăng ký cho DebConf đang mở. Vui lòng đăng nhập vào trang thông tin điện tử
DebConf16 và đăng ký từ [trang hồ sơ của bạn](https://debconf16.debconf.org/accounts/profile/).

Để yêu cầu hỗ trợ tài chính (đỡ đầu) về thức ăn, chỗ ăn ngủ, hay đi lại, bạn phải
đăng ký vào khoảng chủ nhật, ngày 10 tháng 4 năm 2016.

Sau ngày này, đăng ký sẽ vẫn được chấp nhận trong các trường hợp cốt yếu,
người chuyên nghiệp, và công ty. Tuy vậy, việc sắp xếp sân bãi
sẽ không được đảm bảo, và các yêu cầu đỡ đầu sẽ không còn được chấp nhận nữa.

Ngay cả khi bạn không chắc chắn rằng bạn có thể tham dự hay không, chúng tôi vẫn
khuyến khích bạn đăng ký ngay bây giờ. Bạn lúc nào cũng có thể hủy đăng ký, trước khi hết hạn.
Chúng tôi giả thuyết rằng người tham dự bắt đầu lên kế hoạch du hành sớm nhất có
thể, dĩ nhiên là thế rồi.

Mong gặp tất cả các bạn tại Cape Town!
