Title: Free FPGA programming with Debian
Date: 2016-12-22 18:15
Tags: fpga, programming
Slug: fpga-programming-debian
Author: Steffen Möller
Status: published

[FPGA](https://wiki.debian.org/FPGA/) (Field Programmable Gate Array)
are increasingly popular for data acquisition, device control and
application acceleration. Debian now features a completely Free set of
tools to program FPGA in Verilog, prepare the binary and have it
executed on an affordable device.

See [http://wiki.debian.org/FPGA/Lattice](http://wiki.debian.org/FPGA/Lattice)
for details. Readers familiar with
the technology may rightly guess that this refers
to the *yosys* package
together with *berkeley-abc*, *arachne-"Place-and-Route"*
and the *icestorm* tools to communicate with the device.

The packages have been contributed by
the [Debian Science](https://blends.debian.org/science/tasks/) team.

We hope this effort to support the
FPGA community to collect an increasing number of skills to further
smoothen the Open Source experience and lower the entry barriers for this
tantalising technology.
