Title: Nous desenvolupadors i mantenidors de Debian (setembre i octubre del 2016)
Slug: new-developers-2016-10
Date: 2016-11-03 12:00
Author: Jean-Pierre Giraud
Tags: project
Lang: ca
Status: published

Els següents col·laboradors del projecte han esdevingut Debian Developers
en els darrers dos mesos:

* Adriano Rafael Gomes (adrianorg)
* Arturo Borrero González (arturo)
* Sandro Knauß (hefee)

Els següents col·laboradors del projecte han esdevingut Debian Maintainers
en els darrers dos mesos:

* Abhijith PA
* Mo Zhou
* Víctor Cuadrado Juan
* Zygmunt Bazyli Krynicki
* Robert Haist
* Sunil Mohan Adapa
* Elena Grandi
* Eric Heintzmann
* Dylan Aïssi
* Daniel Shahaf
* Samuel Henrique
* Kai-Chung Yan
* Tino Mettler

Enhorabona a tots!
