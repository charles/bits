Title: Nuevos desarrolladores y mantenedores de Debian (septiembre y octubre del 2016)
Slug: new-developers-2016-10
Date: 2016-11-03 12:00
Author: Jean-Pierre Giraud
Tags: project
Lang: es
Status: published

Los siguientes colaboradores del proyecto se convirtieron en Debian Developers
en los dos últimos meses:

* Adriano Rafael Gomes (adrianorg)
* Arturo Borrero González (arturo)
* Sandro Knauß (hefee)

Los siguientes colaboradores del proyecto se convirtieron en Debian Maintainers
en los dos últimos meses:

* Abhijith PA
* Mo Zhou
* Víctor Cuadrado Juan
* Zygmunt Bazyli Krynicki
* Robert Haist
* Sunil Mohan Adapa
* Elena Grandi
* Eric Heintzmann
* Dylan Aïssi
* Daniel Shahaf
* Samuel Henrique
* Kai-Chung Yan
* Tino Mettler

¡Felicidades a todos!
