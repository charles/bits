Title: Looking for the artwork for the next Debian release
Slug: looking-for-the-artwork-for-the-next-debian-release
Date: 2016-07-29 19:15:00
Author: Ana Guerrero Lopez
Tags: stretch, artwork
Status: published

Each release of Debian has a shiny new theme, which is visible on the boot
screen, the login screen and, most prominently, on the desktop wallpaper.

Debian plans to release Stretch next year. As ever, we need your help in
creating its theme!
You have the opportunity to design a theme that will inspire thousands of
people while working in their Debian systems.

They might be people working in exciting NASA missions:

[![Debian Squeeze Space Fun Spotted during the Juno Orbital Insertion live stream](|static|/images/debian_squeeze_space_fun_juno_coverage.png)][squeeze-theme-img]

[squeeze-theme-img]: http://imgur.com/BeqwJiI

Or DYI users who decided to make a matching keyboard:

[![Keyboard matching Debian Lenny Theme](|static|/images/debian_lenny_matching_keyboard.jpg)][lenny-theme-img]

[lenny-theme-img]: http://i.imgur.com/6O15V4A.jpg

If you're interested, please take a look at
[https://wiki.debian.org/DebianDesktop/Artwork/Stretch](https://wiki.debian.org/DebianDesktop/Artwork/Stretch)
