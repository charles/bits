Title: New Debian Developers and Maintainers (May and June 2016)
Slug: new-developers-2016-06
Date: 2016-07-10 17:30
Author: Ana Guerrero Lopez
Tags: project
Status: published

The following contributors got their Debian Developer accounts in the last two
months:

* Josué Ortega (josue)
* Mathias Behrle (mbehrle)
* Sascha Steinbiss (satta)
* Lucas Kanashiro (kanashiro)
* Vasudev Sathish Kamath (vasudev)
* Dima Kogan (dkogan)
* Rafael Laboissière (rafael)
* David Kalnischkies (donkult)
* Marcin Kulisz (kula)
* David Steele (steele
* Herbert Parentes Fortes Neto (hpfn)
* Ondřej Nový (onovy)
* Donald Norwood (donald)
* Neutron Soutmun (neutrons)
* Steve Kemp (skx)

The following contributors were added as Debian Maintainers in the last two
months:

* Sean Whitton
* Tiago Ilieve
* Jean Baptiste Favre
* Adrian Vondendriesch
* Alkis Georgopoulos
* Michael Hudson-Doyle
* Roger Shimizu
* SZ Lin
* Leo Singer
* Peter Colberg

Congratulations!
