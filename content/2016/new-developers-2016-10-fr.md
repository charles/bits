Title: Nouveaux développeurs et mainteneurs de Debian (septembre et octobre 2016)
Slug: new-developers-2016-10
Date: 2016-11-03 12:00
Author: Jean-Pierre Giraud
Tags: project
Lang: fr
Status: published

Les contributeurs suivants sont devenus développeurs Debian ces deux
derniers mois :

* Adriano Rafael Gomes (adrianorg)
* Arturo Borrero González (arturo)
* Sandro Knauß (hefee)

et ces contributeurs ont été acceptés comme mainteneurs Debian durant la
même période :

* Abhijith PA
* Mo Zhou
* Víctor Cuadrado Juan
* Zygmunt Bazyli Krynicki
* Robert Haist
* Sunil Mohan Adapa
* Elena Grandi
* Eric Heintzmann
* Dylan Aïssi
* Daniel Shahaf
* Samuel Henrique
* Kai-Chung Yan
* Tino Mettler

Félicitations !
