Title: New Debian Developers and Maintainers (January and February 2016)
Slug: new-developers-2016-02
Date: 2016-03-14 21:30
Author: Jean-Pierre Giraud
Tags: project
Status: published

The following contributors got their Debian Developer accounts in the last two
months:

* Otto Kekäläinen (otto)
* Dariusz Dwornikowski (darek)
* Daniel Stender (stender)
* Afif Elghraoui (afif)
* Victor Seva (vseva)
* James Cowgill (jcowgill)

The following contributors were added as Debian Maintainers in the last two
months:

* Giovani Augusto Ferreira
* Ondřej Nový
* Jason Pleau
* Michael Robin Crusoe
* Ferenc Wágner
* Enrico Rossi
* Christian Seiler
* Daniel Echeverry
* Ilias Tsitsimpis
* James Clarke
* Luca Boccassi

Congratulations!
