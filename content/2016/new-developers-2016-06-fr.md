Title: Nouveaux développeurs et mainteneurs de Debian (mai et juin 2016)
Slug: new-developers-2016-06
Date: 2016-07-10 17:30
Author: Ana Guerrero Lopez
Tags: project
Lang: fr
Status: published

Les contributeurs suivants sont devenus développeurs Debian ces deux
derniers mois :

* Josué Ortega (josue)
* Mathias Behrle (mbehrle)
* Sascha Steinbiss (satta)
* Lucas Kanashiro (kanashiro)
* Vasudev Sathish Kamath (vasudev)
* Dima Kogan (dkogan)
* Rafael Laboissière (rafael)
* David Kalnischkies (donkult)
* Marcin Kulisz (kula)
* David Steele (steele
* Herbert Parentes Fortes Neto (hpfn)
* Ondřej Nový (onovy)
* Donald Norwood (donald)
* Neutron Soutmun (neutrons)
* Steve Kemp (skx)

et ces contributeurs ont été acceptés comme mainteneurs Debian durant la
même période :

* Sean Whitton
* Tiago Ilieve
* Jean Baptiste Favre
* Adrian Vondendriesch
* Alkis Georgopoulos
* Michael Hudson-Doyle
* Roger Shimizu
* SZ Lin
* Leo Singer
* Peter Colberg

Félicitations !
