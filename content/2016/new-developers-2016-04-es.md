Title: Nuevos desarrolladores y mantenedores de Debian (marzo y abril del 2016)
Slug: new-developers-2016-04
Date: 2016-05-17 00:10
Author: Ana Guerrero Lopez
Tags: project
Lang: es
Status: published

Los siguientes colaboradores del proyecto se convirtieron en Debian Developers
en los dos últimos meses:

* Sven Bartscher (kritzefitz)
* Harlan Lieberman-Berg (hlieberman)

¡Felicidades!
