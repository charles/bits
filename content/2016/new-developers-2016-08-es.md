Title: Nuevos desarrolladores y mantenedores de Debian (julio y agosto del 2016)
Slug: new-developers-2016-08
Date: 2016-09-03 12:00
Author: Jean-Pierre Giraud
Tags: project
Lang: es
Status: published

Los siguientes colaboradores del proyecto se convirtieron en Debian Developers
en los dos últimos meses:

* Edward John Betts (edward)
* Holger Wansing (holgerw)
* Timothy Martin Potter (tpot)
* Martijn van Brummelen (mvb)
* Stéphane Blondon (sblondon)
* Bertrand Marc (bmarc)
* Jochen Sprickerhof (jspricke)
* Ben Finney (bignose)
* Breno Leitao (leitao)
* Zlatan Todoric (zlatan)
* Ferenc Wágner (wferi)
* Matthieu Caneill (matthieucan)
* Steven Chamberlain (stevenc)

Los siguientes colaboradores del proyecto se convirtieron en Debian Maintainers
en los dos últimos meses:

* Jonathan Cristopher Carter
* Reiner Herrmann
* Michael Jeanson
* Jens Reyer
* Jerome Benoit
* Frédéric Bonnard
* Olek Wojnar

¡Felicidades a todos!
