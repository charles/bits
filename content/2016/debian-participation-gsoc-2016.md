Title: Debian selected to participate in the Google Summer of Code
Slug: debian-participation-gsoc-2016
Date: 2016-03-13 16:00
Author: Nicolas Dandrimont
Tags: gsoc, google, announce, development, diversity, software, code, projects
Status: published

![GSoC 2016 logo](|static|/images/gsoc2016.jpg)

For the tenth time running, Debian has been selected as a mentoring
organization for the [Google Summer of Code][1] ([Debian-specific program
page][2]), an internship program open to university students aged 18 and up.

[1]: https://summerofcode.withgoogle.com/
[2]: https://wiki.debian.org/SummerOfCode2016

Our team of amazing mentors has cooked up an [exciting list of projects][3]
this year, and we would be glad to have you on board with Debian for one of
those summer internships. The student application period will open on March 14
(and close on March 25), but feel free to [subscribe to our mailing list][4]
and get in touch with our mentors. You can also catch us on our IRC channel
[#debian-soc](irc://irc.debian.org/#debian-soc).

[3]: https://wiki.debian.org/SummerOfCode2016/Projects
[4]: https://lists.debian.org/debian-outreach/ (debian-outreach AT lists.debian.org)
