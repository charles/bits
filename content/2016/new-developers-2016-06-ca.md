Title: Nous desenvolupadors i mantenidors de Debian (maig i juny del 2016)
Slug: new-developers-2016-06
Date: 2016-07-10 17:30
Author: Ana Guerrero Lopez
Tags: project
Lang: ca
Status: published

Els següents col·laboradors del projecte han esdevingut Debian Developers
en els darrers dos mesos:

* Josué Ortega (josue)
* Mathias Behrle (mbehrle)
* Sascha Steinbiss (satta)
* Lucas Kanashiro (kanashiro)
* Vasudev Sathish Kamath (vasudev)
* Dima Kogan (dkogan)
* Rafael Laboissière (rafael)
* David Kalnischkies (donkult)
* Marcin Kulisz (kula)
* David Steele (steele
* Herbert Parentes Fortes Neto (hpfn)
* Ondřej Nový (onovy)
* Donald Norwood (donald)
* Neutron Soutmun (neutrons)
* Steve Kemp (skx)

Els següents col·laboradors del projecte han esdevingut Debian Maintainers
en els darrers dos mesos:

* Sean Whitton
* Tiago Ilieve
* Jean Baptiste Favre
* Adrian Vondendriesch
* Alkis Georgopoulos
* Michael Hudson-Doyle
* Roger Shimizu
* SZ Lin
* Leo Singer
* Peter Colberg

Enhorabona a tots!
