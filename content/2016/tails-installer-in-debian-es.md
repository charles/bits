Title: El instalador de Tails ya está en Debian
Slug: tails-installer-in-debian
Date: 2016-02-11 14:30
Author: u
Lang: es
Translator: Adrià García-Alzórriz
Tags: tails, privacy, anonymity, announce
Status: published

[Tails][tails-link] (The amnesic incognito live system) es un sistema
operativo en vido («Live») basado en Debian GNU/Linux el cual cuida de la
privacidad de sus usuarios y de su anonimato, haciendo uso de internet
de forma anónima y sorteando la censura. Se instala en un dispositivo
USB y se configura para no dejar ningún rastro en el ordenador en el
que se ha usado a menos que se indique explícitamente.

[![Tails Logo](|static|/images/tails-logo-flat.png)][tails-link]

A partir de hoy, las personas que necesiten seguridad digital no
necesitarán ser expertas en informática. Es necesario que para adoptar
una nueva tecnología, iniciarse en ella sea sencillo incluso en
entornos estresantes y de alto riesgo. Es por ello que hemos querido
que instalar Tails fuera más rápido, más sencillo y más seguro para los
nuevos usuarios.

Uno de los componentes de Tails, el [instalador de Tails][tails-instaler] ha sido
incluido en Debian gracias al [Equipo de mantenedores de las herramientas de
privacidad de Debian][team-link].

El instalador de Tails es una [herramienta gráfica][screenshot] para
instalar o actualizar Tails en un dispositivo USB desde una imagen ISO. Su
finalidad es tener [Tails funcionando y ejecutándose][tails-installdoc] de una forma más rápida y fácil.

El proceso anterior para iniciarse con Tails era muy complejo y
problemático para los usuarios menos técnicos. Se necesitaba iniciar
Tails tres veces y copiar la imagen ISO completa al dispositivo USB dos veces
antes de conseguir Tails plenamente funcional en un dispositivo USB con la
persistencia habilitada.

Ahora esto se puede hacer de forma más simple instalando el instalador
de Tails en su actual sistema Debian, usando Sid, Stretch o
jessie-backports, conectando un dispositivo USB y eligiendo si se desea
actualizar el dispositivo USB o instalar Tails usando una imagen ISO
previamente descargada.

El instalador de Tails también ayuda a los usuarios de Tails a crear un
almacenamiento cifrado persistente para datos personales y
configuraciones en el resto del espacio disponible.

[tails-link]: https://tails.boum.org
[tails-instaler]: https://tracker.debian.org/pkg/tails-installer
[screenshot]: https://screenshots.debian.net/package/tails-installer
[team-link]: https://qa.debian.org/developer.php?email=pkg-privacy-maintainers%40lists.alioth.debian.org
[tails-installdoc]: https://tails.boum.org/install
