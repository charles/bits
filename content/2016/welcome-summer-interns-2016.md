Title: Debian welcomes its 2016 summer interns
Slug: welcome-summer-interns-2016
Date: 2016-04-24 21:00
Author: Nicolas Dandrimont
Tags:  gsoc, outreachy, google, announce, development, diversity, software, code, projects
Status: published

![GSoC 2016 logo](|static|/images/gsoc2016.jpg) ![Outreachy logo](|static|/images/outreachy-logo-300x61.png)

We're excited to announce that Debian has selected 29 interns to work with us
this summer: 4 in [Outreachy][1], and 25 in the [Google Summer of Code][2].

[1]: https://wiki.gnome.org/Outreachy/2016/MayAugust#Debian
[2]: https://summerofcode.withgoogle.com/organizations/5023548368224256/#projects

Here is the list of projects and the interns who will work on them:

[Android SDK tools](https://wiki.debian.org/AndroidTools) in Debian:

+ [Chirayu Desai](https://wiki.debian.org/SummerOfCode2016/StudentApplications/ChirayuDesai)
+ [Kai-Chung Yan](https://wiki.debian.org/SummerOfCode2016/StudentApplications/KaichungYan)
+ [Mouaad Aallam](https://wiki.debian.org/SummerOfCode2016/StudentApplications/MouaadAallam)

APT - dpkg communications rework:

+ [David Kalnischkies](https://wiki.debian.org/SummerOfCode2016/StudentApplications/DavidKalnischkies)

Continuous Integration for [Debian-Med](https://www.debian.org/devel/debian-med/) packages:

+ [Canberk Koç](https://wiki.debian.org/SummerOfCode2016/StudentApplications/CanberkKoc)
+ [Tatiana Malygina](https://wiki.gnome.org/Outreachy/2016/MayAugust#Debian)

Extending the [Debian Developer Horizon](http://horizon.debian.net/):

+ [Harsh Daftary](http://wiki.debian.org/SummerOfCode2016/StudentApplications/HarshDaftary)

Improving and extending [AppRecommender](https://github.com/tassia/AppRecommender/wiki):

+ [Lucas Albuquerque Medeiros de Moura](https://wiki.debian.org/SummerOfCode2016/StudentApplications/LucasMoura)
+ [Luciano Prestes Cavalcanti](https://wiki.debian.org/SummerOfCode2016/StudentApplications/LucianoPrestes)

Improving the [debsources](https://sources.debian.net) frontend:

+ [Aaron Delaney](https://wiki.debian.org/SummerOfCode2016/StudentApplications/AaronDelaney)

Improving [voice, video and chat communication](https://wiki.debian.org/SummerOfCode2016/Projects/Voice%20Webcam%20and%20Chat%20Communication) with Free Software:

+ [Alok Anand](https://wiki.debian.org/SummerOfCode2016/StudentApplications/AlokAnand)
+ [Balram P](https://wiki.debian.org/SummerOfCode2016/StudentApplications/BalramP)
+ [Jaminy Prabaharan](https://wiki.debian.org/SummerOfCode2016/StudentApplications/Jaminy)
+ [Keerthana Krishnan](https://wiki.debian.org/SummerOfCode2016/StudentApplications/KeerthanaKrishnan)
+ [Kévin Avignon](https://wiki.debian.org/SummerOfCode2016/StudentApplications/KevinAvignon)
+ [Mateus Bellomo](https://wiki.debian.org/SummerOfCode2016/StudentApplications/MateusBellomo)
+ [Mesut Can Gurle](https://wiki.debian.org/SummerOfCode2016/StudentApplications/MesutCanGurle)
+ [Nicolas Reynaud](https://wiki.debian.org/SummerOfCode2016/StudentApplications/NicolasReynaud)
+ [Nik Vaessen](https://wiki.debian.org/SummerOfCode2016/StudentApplications/NikVaes)
+ [Olivier Grégoire](https://wiki.debian.org/SummerOfCode2016/StudentApplications/OlivierGr%C3%A9goire)
+ [Pranav Jain](https://wiki.debian.org/SummerOfCode2016/StudentApplications/PranavJain)
+ [Simon Désaulniers](https://wiki.debian.org/SummerOfCode2016/StudentApplications/SimonD%C3%A9saulniers)
+ [Udit Raikwar](https://wiki.debian.org/SummerOfCode2016/StudentApplications/UditRaikwar)

MIPS and MIPSEL ports improvements:

+ [Jonathan Jackson](https://wiki.debian.org/SummerOfCode2016/StudentApplications/JonathanJackson)

[Reproducible Builds](https://reproducible-builds.org/) for Debian and Free Software:

+ [ceridwen](https://wiki.gnome.org/Outreachy/2016/MayAugust#Debian)
+ [Satyam Zode](https://wiki.debian.org/SummerOfCode2016/StudentApplications/SatyamZode)
+ [Scarlett Clark](https://wiki.gnome.org/Outreachy/2016/MayAugust#Debian)
+ [Valerie Young](https://wiki.gnome.org/Outreachy/2016/MayAugust#Debian)

Support for [KLEE](http://klee.github.io/) in Debile:

+ [Marko Dimjašević](https://wiki.debian.org/SummerOfCode2016/StudentApplications/MarkoDimjasevic)

The Google Summer of Code and Outreachy programs are possible in Debian thanks
to the effort of Debian developers and contributors that dedicate
part of their free time to mentor students and outreach tasks.

Join us and help extend Debian! You can follow the students weekly reports on the
[debian-outreach mailing-list][debian-outreach-ml], chat with us on
[our IRC channel][debian-soc-irc] or on each project's team mailing lists.

[debian-outreach-ml]: https://lists.debian.org/debian-outreach/ (debian-outreach AT lists.debian.org)
[debian-soc-irc]: irc://irc.debian.org/#debian-soc (#debian-soc on irc.debian.org)

Congratulations to all of them!
