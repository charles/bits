Title: Debian is participating in the next round of Outreachy!
Slug: debian-is-participating-in-the-next-round-of-outreachy
Date: 2016-10-09 19:50
Author: Nicolas Dandrimont
Tags: announce, outreachy
Status: published

Following the success of the last round of Outreachy, we are glad to announce
that Debian will take part in the program for the next round, with internships
lasting from the 6th of December 2016 to the 6th of March 2017.

From [the official website](https://www.gnome.org/outreachy): *Outreachy helps
people from groups underrepresented in free and open source software get
involved. We provide a supportive community for beginning to contribute any
time throughout the year and offer focused internship opportunities twice a
year with a number of free software organizations.*

Currently, internships are open internationally to women (cis and trans),
trans men, and genderqueer people. Additionally, they are open to residents
and nationals of the United States of any gender who are Black/African
American, Hispanic/Latin@, American Indian, Alaska Native, Native Hawaiian, or
Pacific Islander.

If you want to apply to an internship in [Debian](https://www.debian.org), you
should take a look at [the wiki page](https://wiki.debian.org/Outreachy), and
contact the mentors for the projects listed, or seek more information on the
(public)
[debian-outreach mailing-list](https://lists.debian.org/debian-outreach/). You
can also contact the [Outreach Team](https://wiki.debian.org/Teams/Outreach)
directly. If you have a project idea and are willing to mentor an intern, you
can submit a project idea on the
[Outreachy wiki page](https://wiki.debian.org/Outreachy#projects).

Here's a few words on what the interns for the last round achieved within
Outreachy:

- [Tatiana Malygina][2] worked on Continuous Integration for Bioinformatics
  applications; She has pushed more than a hundred commits to the Debian Med
  SVN repository over the last months, and has been sponsored for more than 20
  package uploads.

[2]: https://contributors.debian.org/contributor/latticetower-guest@alioth/

- [Valerie Young][3] worked on Reproducible Builds infrastructure, driving a
  complete overhaul of the database and software behind the
  tests.reproducible-builds.org website. Her blog contains regular updates
  throughout the program.

[3]: http://www.spectranaut.cc/

- [ceridwen][4] worked on creating reprotest, an all-in-one tool allowing
  anyone to check whether a build is reproducible or not, replacing the string
  of ad-hoc scripts the reproducible builds team used so far. She posted
  regular updates on the Reproducible Builds team blog.

[4]: https://reproducible.alioth.debian.org/blog/posts/people/ceridwen/

- While [Scarlett Clark][5] did not complete the internship (as she found a
  full-time job by the mid-term evaluation!), she spent the four weeks she
  participated in the program providing patches for reproducible builds in
  Debian and KDE upstream.

[5]: http://scarlettgatelyclark.com/

Debian would not be able to participate in Outreachy without the help of the
[Software Freedom Conservancy](https://sfconservancy.org/), who provides
administrative support for Outreachy, as well as the continued support of
Debian's donors, who provide funding for the internships. If you want to
donate, please
[get in touch with one of our trusted organizations](https://www.debian.org/donations).

Debian is looking forward to welcoming new interns for the next few months,
come join us!
