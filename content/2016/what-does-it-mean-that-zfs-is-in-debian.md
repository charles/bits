Title: What does it mean that ZFS is included in Debian?
Slug: what-does-it-mean-that-zfs-is-in-debian
Date: 2016-05-15 22:55
Author: Ana Guerrero Lopez
Tags: zfs, contrib
Status: published

Petter Reinholdtsen recently blogged about [ZFS availability in Debian][1].
Many people have worked hard on getting ZFS support available in Debian and we
would like to thank everyone involved in getting to this point and explain
what ZFS in Debian means.

The landing of ZFS in the Debian archive was blocked for years due to
licensing problems. Finally,
[the inclusion of ZFS was announced slightly more than a year ago, on April 2015][2]
by the DPL at the time, Lucas Nussbaum who wrote *"We received legal advice
from Software Freedom Law Center about the inclusion of libdvdcss and ZFS in
Debian, which should unblock the situation in both cases and enable us to ship
them in Debian soon."*. In January this year, the following DPL, Neil McGovern
blogged with a lot of more details about the legal situation behind this and
summarized it as *["TLDR: It’s going in contrib, as a source only dkms
module."][3]*

ZFS is not available exactly in Debian, since Debian is only what's included
in the *"main"* section archive. What people really meant here is that ZFS
code is now in included in *"contrib"* and it's available for users using DKMS.

Many people also mixed this with Ubuntu now including ZFS. However, Debian and
Ubuntu are not doing the same, Ubuntu is shipping directly pre-built kernel
modules, something that is considered to be a GPL violation. As the Software
Freedom Conservancy wrote *"[while licensed under an acceptable license  for
Debian's Free Software Guidelines, also has a default use that can  cause
licensing problems for downstream Debian users"][4]*.

[1]: http://people.skolelinux.org/pere/blog/Debian_now_with_ZFS_on_Linux_included.html
[2]: https://lists.debian.org/debian-devel-announce/2015/04/msg00006.html
[3]: http://blog.halon.org.uk/2016/01/on-zfs-in-debian/
[4]: https://sfconservancy.org/blog/2016/feb/25/zfs-and-linux/
