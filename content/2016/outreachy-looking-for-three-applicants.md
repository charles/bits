Title: Debian is looking for three interns in the Outreachy Program
Slug: outreachy-looking-for-three-applicants
Date: 2016-03-12 20:10
Author: Nicolas Dandrimont
Tags:  outreachy, announce, development, diversity, women, software, code, projects
announce, outreachy
Status: published

![Outreachy logo](|static|/images/outreachy-logo-300x61.png)

As part of its diversity outreach initiatives, Debian will be participating in
the upcoming 12th round (May - August 2016) of [Outreachy][1], an internship
program open worldwide to women (cis and trans), trans men and genderqueer
people, as well as nationals and residents of the United States of any gender
who are Black/African American, Hispanic/Latin@, American Indian, Alaska
Native, Native Hawaiian, or Pacific Islander.

[1]: https://www.gnome.org/outreachy/

Thanks to the generosity of our donors, and specifically of our sponsor [Intel](http://www.intel.com)
who has given us funds specifically for one intern, Debian will be able to
welcome three interns this round.

Applications for the program are open until March 22nd, so don't wait up!
Debian has [a lot of interesting internship opportunities this year][2]. More
info about the program is available on the [Debian specific program page][3],
as well as on the [official website][4]. Feel free to contact the outreach team
and mentors on our [mailing list][5] or IRC channel #debian-soc in irc.oftc.net

[2]: https://wiki.debian.org/SummerOfCode2016/Projects (the list is common with GSoC)
[3]: https://wiki.debian.org/Outreachy/Round12
[4]: https://wiki.gnome.org/Outreachy
[5]: https://lists.debian.org/debian-outreach/ (debian-outreach AT lists.debian.org)

If you want Debian to keep participating in such programs, and expand its
outreach efforts, you can [donate to one of the organizations supporting the
Debian project][7], or volunteer some time by participating in discussions on our
mailing list.

[7]: https://www.debian.org/donations
