Title: Nous desenvolupadors i mantenidors de Debian (març i abril del 2016)
Slug: new-developers-2016-04
Date: 2016-05-17 00:10
Author: Ana Guerrero Lopez
Tags: project
Lang: ca
Status: published

Els següents col·laboradors del projecte han esdevingut Debian Developers
en els darrers dos mesos:

* Sven Bartscher (kritzefitz)
* Harlan Lieberman-Berg (hlieberman)

Enhorabona a tots!
