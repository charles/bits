Title: Debian Celebrates 31 years!
Slug: debian-turns-31
Date: 2024-08-16 12:00
Author: Donald Norwood, Paul Wise, Justin B Rye, Debian Publicity Team
tags: debian, birthday, anniversary, debianday
Artist: Daniel Lenharo de Souza
status: published

[![Debian 31 years by Daniel
Lenharo](|static|/images/debian-31th_DLenharo_300x350.png)](|static|/images/debian-31th_DLenharo_full.png)

As the expression goes, "Time flies when you are having fun", meaning you do
not normally account for the passage of time when you are distracted and
enjoying yourself. The expression is a well established English idiom, though
today for a moment the Debian Project pauses to reflect on that expression.

It has been 31 years now that we have been around.

It has been 31 amazing years of fun and amazement in watching the world around
us grow and ourselves grow into the world.

Let us tell you, we have had a great time in doing so.

We have been invited to nearly every continent and country for over 25 Debian
Developer Conferences, we have contributed to the sciences with our [Debian Pure
Blends][1]; we have not given up on or discounted aged hardware with [Long Term
Support (LTS)][2]; we have encouraged and sponsored diversity with our [Outreach
Programs][3]. We have contributed to exploration of this lovely planet and the
[vast vacuum of space][4] (where no one hears Developers scream).

There is more to what we have done but from a cursory glance, we seem to have
done it all.

But *we* never noticed it.

Time does fly or ["escape irretrievably"][5] when having a good time and making
progress, though our pause at this moment is that we have also had a few
moments of honest self-evaluation and  reflection. Over the years the project
has lost some significant loved ones who were dear to us - you may have called
them Developers while we called them Friends, we called them Mentors, we hurt,
we grieved, and in their memories we keep moving forward.

The course of the project has seen a few tragedies, has seen heated discourse
in the public domain, has addressed and weathered concerns, and has still
continually grown.

And we did that in the public sphere, because at the core this is an open
project. Our code is public, our bugs and failings are public, our
communications are public, our meetings are public, and our love of FLOSS is
most definitely public.

And now more than ever the Debian Project realizes that the "we" that is
sprinkled throughout this letter is just another way of saying: "you".  You,
the user, contributor, sponsor, developer, maintainer, bug squasher; all of you
make the WE that is Debian. So what are WE waiting for? Lets celebrate!

Join the worldwide celebration or find an event local to you by visiting our
[DebianDay events page][6] - see you there!

[1]: https://www.debian.org/blends
[2]: https://wiki.debian.org/LTS
[3]: https://wiki.debian.org/Teams/Outreach
[4]: https://bits.debian.org/2017/04/unknown-parallel-universe-uses-debian.html
[5]: https://en.wikipedia.org/wiki/Tempus_fugit
[6]: https://wiki.debian.org/DebianDay/2024
