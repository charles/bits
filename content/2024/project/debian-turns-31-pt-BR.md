Title: O Debian comemora seus 31 anos!
Slug: debian-turns-31
Date: 2024-08-16 12:00
Author: Donald Norwood, Paul Wise, Justin B Rye, Debian Publicity Team
Translator: Paulo Henrique de Lima Santana (phls)
Lang: pt-BR
tags: debian, birthday, anniversary, debianday
Artist: Daniel Lenharo de Souza
status: published

[![Debian 31 years by Daniel
Lenharo](|static|/images/debian-31th_DLenharo_300x350.png)](|static|/images/debian-31th_DLenharo_full.png)

Como diz a expressão, "o tempo voa quando você está se divertindo", significando
que você normalmente não considera a passagem do tempo quando está se distraindo
ou se divertindo. A frase é uma expressão idiomática inglesa bem estabelecida,
embora hoje o Projeto Debian pare por um momento para refletir sobre essa
expressão.

Já faz 31 anos que estamos por aqui.

Foram 31 anos incríveis de diversão e espanto ao ver o mundo ao nosso redor
crescer, e nós mesmos crescermos no mundo.

Deixe-nos dizer, nós nos divertimos muito fazendo isso.

Fomos convidados para quase todos os continentes e países, para mais de 25
conferências de desenvolvedores(as) Debian; contribuímos para as ciências com
nossos [Debian Pure Blends][1]; não desistimos ou descartamos hardware antigos
com o [Suporte de Longo Prazo (LTS - Long Term Support)][2]; encorajamos e
patrocinamos a diversidade com nossos [Programas Outreach][3]. Contribuímos
para a exploração deste adorável planeta e do [vasto vácuo do espaço][4] (onde
ninguém ouve os(as) desenvolvedores(as) gritarem).

Existem mais coisas do que fizemos, mas em um rápido olhar, parece que
fizemos tudo.

Mas *nós* nunca percebemos.

O tempo voa ou ["escapa irrevogavelmente"][5] quando nos divertimos e
progredimos, embora nossa pausa neste momento seja porque também tivemos alguns
momentos honestos de autoavaliação e reflexão. Ao longo dos anos, o projeto
perdeu algumas pessoas amadas importantes que eram queridos(as) por nós -
você pode tê-los(las) chamado de desenvolvedores(as) enquanto nós os(as)
chamávamos de amigos(as), nós os(as) chamávamos de mentores(as), nós sofremos,
nós lamentamos, e em suas memórias continuamos seguindo em frente.

O decorrer do projeto viu algumas tragédias, viu discursos acalorados no espaço
público, abordou e resistiu a preocupações, e ainda cresceu continuamente.

E fizemos isso na esfera pública, porque no fundo este é um projeto aberto.
Nosso código é público, nossos bugs e falhas são públicos, nossas comunicações
são públicas, nossas reuniões são públicas, e nosso amor pelo FLOSS é
definitivamente público.

E agora mais do que nunca o Projeto Debian percebe que o "nós" que está
espalhado por esta carta é apenas outra maneira de dizer: "você". Você, o(a)
usuário(a), contribuidor(a), patrocinador(a), desenvolvedor(a), mantenedor(a),
eliminador(a) de bugs; todos vocês fazem o NÓS que é o Debian. Então, o que
NÓS estamos esperando? Vamos comemorar!

Participe da celebração mundial ou encontre um evento local para você, visitando
nossa [página de eventos do Debian Day][6] - nos vemos lá!

Obs: a logo deste ano para comemorar os 31 anos do Debian foi criada pelo
brasileiro Daniel Lenharo :-)

[1]: https://www.debian.org/blends
[2]: https://wiki.debian.org/LTS
[3]: https://wiki.debian.org/Teams/Outreach
[4]: https://bits.debian.org/2017/04/unknown-parallel-universe-uses-debian.html
[5]: https://en.wikipedia.org/wiki/Tempus_fugit
[6]: https://wiki.debian.org/DebianDay/2024
