Title: Looking for the artwork for Trixie the next Debian release
Slug: trixie-artwork-cfp
Date: 2024-06-21 12:00:00
Author: Jonathan Carter
Tags: trixie, artwork
Status: published

Each release of Debian has a shiny new theme, which is visible on the boot
screen, the login screen and, most prominently, on the desktop wallpaper.
Debian plans to release Trixie, the next release, next year. As ever, we
need your help in creating its theme! You have the opportunity to design a
theme that will inspire thousands of people while working in their Debian
systems.

For the most up to date details, please refer to the
[wiki](https://wiki.debian.org/DebianDesktop/Artwork/Trixie).

We would also like to take this opportunity to thank Juliette
Taka Belin for doing the
[Emerald theme for bookworm](https://wiki.debian.org/DebianArt/Themes/Emerald).

The deadlines for submissions is: 2024-09-19

The artwork is usually picked based on which themes look the most:

* ''Debian'': admittedly not the most defined concept, since everyone has their
  own take on what Debian means to them.
* ''plausible to integrate without patching core software'':
  as much as we love some of the insanely hot looking themes, some
  would require heavy GTK+ theming and patching GDM/GNOME.
* ''clean / well designed'': without becoming something that gets annoying to
  look at a year down the road.  Examples of good themes include Joy,
  Lines, Softwaves and futurePrototype.

If you'd like more information or details, please post to the
[Debian Desktop mailing list](https://lists.debian.org/debian-desktop/).
