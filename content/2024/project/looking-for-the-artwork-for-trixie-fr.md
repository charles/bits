Title: Recherche d'un thème pour Trixie, la prochaine version de Debian
Slug: trixie-artwork-cfp
Date: 2024-06-21 12:00:00
Author: Jonathan Carter
Translator: Jean-Pierre Giraud
Tags: trixie, artwork
Lang: fr
Status: published

Chaque édition de Debian bénéficie d'un nouveau thème brillant visible sur
l'écran d'amorçage, l'écran de connexion et, de la façon la plus évidente,
sur le fond d'écran. Debian prévoit de publier Trixie, sa prochaine version,
durant l'année à venir. Et, comme toujours, nous avons besoin de vous pour
créer son thème ! Vous avez l'occasion de concevoir un thème qui inspirera
des milliers de personnes quand ils travaillent sur leur machine Debian.

Pour disposer des détails les plus récents, veuillez vous référer à la page du
[wiki](https://wiki.debian.org/DebianDesktop/Artwork/Trixie).

Nous voudrions profiter de cette occasion pour remercier Juliette
Taka Belin pour avoir créé le
[thème Emerald pour Bookworm](https://wiki.debian.org/DebianArt/Themes/Emerald).

La date limite pour les propositions est le 19 septembre 2024.

Le thème est habituellement choisi en se basant sur ce qui paraît le plus :

* « Debian » : il est vrai que ce n'est pas le concept le mieux défini, dans la
  mesure où chacun a son sentiment sur ce que représente Debian pour eux ;
* « possible à intégrer sans corriger le logiciel de base » :
  autant nous aimons les thèmes follement excitants, certains nécessiteraient
  un gros travail d'adaptation des thèmes GTK+ et la correction de GDM/GNOME ;
* « clair et bien conçu » : sans être quelque chose qui devient ennuyeux à
  regarder au bout d'un an. Parmi les bons thèmes, on peut citer Joy, Lines,
  Softwaves et futurePrototype.

Si vous souhaitez disposer plus d'informations, veuillez utiliser la
[liste de diffusion Debian Desktop](https://lists.debian.org/debian-desktop/).
