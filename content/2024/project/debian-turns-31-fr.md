Title: Debian fête ses 31 ans !
Slug: debian-turns-31
Date: 2024-08-16 12:00
Author: Donald Norwood, Paul Wise, Justin B Rye, Debian Publicity Team
Translator: Jean-Pierre Giraud (jipege)
Lang: fr
tags: debian, birthday, anniversary, debianday
Artist: Daniel Lenharo de Souza
status: published

[![Debian a 31 ans par Daniel Lenharo](|static|/images/debian-31th_DLenharo_300x350.png)](|static|/images/debian-31th_DLenharo_full.png)

Comme le dit l'expression, « le temps passe vite quand on s'amuse », ce qui
signifie que vous ne percevez pas normalement le temps qui passe quand vous
vous distrayiez et que vous vous amusez. Cette phrase est un dicton anglais
bien établi, cependant aujourd’hui le projet Debian s'arrête un moment pour
réfléchir à cette expression.

Cela fait maintenant 31 ans que nous existons.

Cela fait 31 années incroyables de plaisir et d'émerveillement à regarder le
monde grandir autour de nous et nous-mêmes nous grandissons dans le monde.

Permettez-nous de vous le dire, nous avons passé un très bon moment à le
faire.

Nous avons été invités dans presque tous les continents et pays pour plus de
25 conférences des développeurs et développeuses Debian ; nous avons contribué
à la science avec nos [Debian Pure Blends – mélanges exclusifs de Debian][1] ;
nous n'avons pas abandonné ni rejeté les vieux logiciels avec la
[prise en charge à long terme (LTS - Long Term Support)][2] ; nous
avons encouragé et parrainé la diversité avec nos [Programmes Outreach][3].
Nous avons contribué à l'exploration de cette belle planète et de
[l'immensité de l'espace][4] (où jamais personne n'a entendu crier un
développeur ou une développeuse).

Il y a plus à faire que ce que nous avons fait, mais d'un rapide coup d'œil,
il semble que nous ayons tout fait.

Mais *nous* ne l'avons jamais remarqué.

Le temps passe ou [« s'échappe irrémédiablement"][5] quand nous passons de
bons moments ou quand nous progressons, cependant, cette pause que nous
faisons à ce moment c'est aussi parce que nous avons eu aussi quelques moments
d'auto-évaluation et de réflexion. Durant ces années, le projet a perdu
quelques proches qui nous étaient chers – on les a peut-être appelés
développeurs ou développeuses, mais nous, nous les appelions amis, nous les
appelions mentors, nous avons souffert, nous les avons pleurés, et à leur
mémoire, nous continuons d'avancer.

Le projet dans son parcours a vu quelques tragédies, a entendu des discours
enflammés dans l'espace public, a affronté des difficultés et les a
surmontées, et a continué à croître sans cesse.

Tout cela, nous l'avons fait dans la sphère publique, parce qu'être un projet
ouvert est au cœur de Debian. Notre code est public, nos bogues et nos échecs
sont publics, nos communications sont publiques, nos réunions sont publiques
et notre attachement au logiciel libre et à code source ouvert est public de
façon définitive.

Et aujourd'hui plus que jamais, le projet Debian se rend compte que le
« nous » qui émaille ce message est une juste autre manière de dire « vous ».
Vous les utilisateurs, les contributeurs, les parrains, les développeurs, les
responsables de paquet, les chasseur de bogues, vous tous vous constituez
le NOUS qui est Debian. Célébrons cela !

Participez à la célébration mondiale ou trouvez un événement près de chez
vous en regardant la [page des événements de la journée Debian][6] - nous
nous verrons là-bas !

[1]: https://www.debian.org/blends
[2]: https://wiki.debian.org/fr/LTS
[3]: https://wiki.debian.org/Teams/Outreach
[4]: https://bits.debian.org/2017/04/unknown-parallel-universe-uses-debian.html
[5]: https://fr.wikipedia.org/wiki/Tempus_fugit
[6]: https://wiki.debian.org/DebianDay/2024
