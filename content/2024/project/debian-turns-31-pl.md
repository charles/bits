Title: Debian świętuje 31 lat!
Slug: debian-turns-31
Date: 2024-08-16 12:00
Author: Donald Norwood, Paul Wise, Justin B Rye, Debian Publicity Team
Translator: Grzegorz Szymaszek
Tags: debian, birthday, anniversary, debianday
Artist: Daniel Lenharo de Souza
Lang: pl
Status: published

[![31 lat Debiana autorstwa Daniela
Lenharo](|static|/images/debian-31th_DLenharo_300x350.png)](|static|/images/debian-31th_DLenharo_full.png)

Jak mówi stare powiedzenie, "czas ucieka gdy się dobrze bawisz", co
oznacza, że normalnie nie zwracacie uwagi na mijający czas gdy jesteście
rozproszeni i miło go spędzacie.
Jest to znane powiedzenie w języku angielskim i polskim, ale dziś projekt Debian
na chwilę się zatrzymuje i nad nim pochyla.

Minęło 31 lat odkąd istniejemy.

To było 31 fantastycznych lat zabawy i fascynacji w oglądaniu świata
dojrzewającego wokół nas i nas dojrzewających razem z nim.

Powiemy Wam, dobrze się bawiliśmy przy tym wszystkim.

Byliśmy zapraszani na prawie każdy kontynent i do wielu krajów w trakcie ponad
25 Konferencji Deweloperów Debiana, mieliśmy swój wkład naukowy w naszych
dedykowanych [odmianach dystrybucji][1]; nie poddaliśmy się we wspieraniu
starszego sprzętu z naszym [wydłużonym wsparciem][2]; zachęcaliśmy
i sponsorowaliśmy różnorodność w naszych [programach wychodzenia naprzeciw][3].
Mamy swój wkład w eksploracji naszej ukochanej planety i nieprzemierzonych
[przestrzeni kosmosu][4] _(gdzie nikt nie słyszy krzyków Deweloperów)_.

Można mówić więcej o tym co zrobiliśmy, ale patrząc powierzchownie, można
odnieść wrażenie, że zrobiliśmy wszystko.

Ale *my* nigdy tego nie zauważyliśmy.

Czas płynie lub [ucieka][5] gdy miło spędzamy czas i robimy postępy, chociaż
mamy pauzę w tym momencie, mieliśmy także kilka chwil szczerych przemyśleń.
W czasie istnienia projektu utraciliśmy kilka bardzo ważnych i drogich dla nas
osób — mogliście nazywać ich Deweloperami, podczas gdy my nazywaliśmy ich
Przyjaciółmi, czy Mentorami, sprawiło nam to ból i smutek, cały czas ich
pamiętając nadal działamy i idziemy naprzód.

W swoim istnieniu projekt widział parę tragedii, trudnych dyskusji w przestrzeni
publicznej, w których musiał zająć stanowisko i pogodzić zwaśnione strony,
i nadal ciągle rósł.

A robiliśmy to w przestrzeni publicznej, ponieważ jest to całkowicie otwarty
projekt.
Nasz kod jest publiczny, nasze błędy i pomyłki są publiczne, nasza komunikacja
jest publiczna, nasze spotkania są publiczne, nasza miłość do wolnego
oprogramowania jest najlepiej widoczna.

I teraz bardziej niż kiedykolwiek Projekt Debian uświadamia sobie, że „my”,
które wyskakuje w wielu miejscach tego listu jest po prostu innym sposobem
powiedzenia „Wy”.
Wy: użytkownicy, współtwórcy, sponsorzy, deweloperzy, opiekunowie, łowcy błędów;
Wy wszyscy jesteście tym **Wy**, którym jest Debian.
Więc na co **Wy** czekacie?
Świętujmy!

Dołączcie do międzynarodowego świętowania lub znajdźcie lokalne wydarzenia
odwiedzając naszą stronę [DebianDay][6] — do zobaczenia!

[1]: https://www.debian.org/blends
[2]: https://wiki.debian.org/LTS
[3]: https://wiki.debian.org/Teams/Outreach
[4]: https://bits.debian.org/2017/04/unknown-parallel-universe-uses-debian.html
[5]: https://en.wikipedia.org/wiki/Tempus_fugit
[6]: https://wiki.debian.org/DebianDay/2024
