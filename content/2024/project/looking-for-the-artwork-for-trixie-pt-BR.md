Title: Chamada para o tema padrão da Trixie, a próxima versão do Debian
Slug: trixie-artwork-cfp
Date: 2024-06-21 12:00:00
Author: Jonathan Carter
Translator: Carlos Henrique Lima Melara (charles)
Lang: pt-BR
Tags: trixie, artwork
Status: published

Toda nova versão do Debian estreia um novo tema, o qual é visível na tela de
inicialização, na tela de login e, eminentemente, no fundo da área de trabalho.
A próxima versão do Debian será a Trixie, que está planejada para ser lançada ano
que vem. E, como sempre, precisamos da sua ajuda para criar o tema! Você tem a
oportunidade de criar um tema que inspirará milhares de pessoas que utilizam
seus sistemas Debian.

Para os detalhes mais recentes, por favor, veja a
[wiki](https://wiki.debian.org/DebianDesktop/Artwork/Trixie).

Também gostaríamos de aproveitar a oportunidade para agradecer
Juliette Taka Belin por criar o
[tema Emerald para o bookworm](https://wiki.debian.org/DebianArt/Themes/Emerald).

O limite para submissão é: 19/09/2024

O trabalho artístico é normalmente escolhido com base em qual tema é mais:

* ''Debian'': admissivelmente um conceito vago, já que cada um tem sua própria
  definição sobre o significado do Debian.
* ''plausível para integração sem alterar a base do sistema'':
  apesar de amarmos alguns dos maravilhosos temas submetidos, eles
  necessitariam de muita customização no GTK+ e a alteração do GDM/GNOME.
* ''minimalista / bem elaborado'': sem que se torne algo enjoativo após anos
  de uso. Exemplos de bons temas incluem Joy, Lines, Softwaves e
  futurePrototype.

Se você gostaria de mais informações ou detalhes, por favor, mande um e-mail
(em inglês) para a
[lista de e-mails Debian Desktop](https://lists.debian.org/debian-desktop/).
