Title: Debian welcomes the 2024 GSOC contributors/students
Slug: welcome-gsoc2024-contributors
Date: 2024-05-01 23:56
Author: Nilesh Patra
Tags:  gsoc, google, announce, development, diversity, software, code, projects
Status: published

![GSoC logo](|static|/images/gsoc.jpg)

We are very excited to announce that Debian has selected seven contributors to work
under mentorship on a variety of
[projects](https://wiki.debian.org/SummerOfCode2024/Projects) with us during the
[Google Summer of Code](https://summerofcode.withgoogle.com/).

Here are the list of the projects, students, and details of the tasks to be performed.

----

Project: [Android SDK Tools in Debian](https://wiki.debian.org/SummerOfCode2024/ApprovedProjects/AndroidSDKToolsInDebian)

* Student: anuragxone

Deliverables of the project: Make the entire Android toolchain, Android Target
Platform Framework, and SDK tools available in the Debian archives.

----

Project: [Benchmarking Parallel Performance of Numerical MPI Packages](https://wiki.debian.org/SummerOfCode2024/ApprovedProjects/BenchmarkingParallelPerformanceMPIPackages)

* Student: Nikolaos

Deliverables of the project: Deliver an automated method for Debian maintainers
to test selected numerical Debian packages for their parallel performance in
clusters, in particular to catch performance regressions from updates, and to
verify expected performance gains, such as Amdahl’s and Gufstafson’s law, from
increased cluster resources.

----

Project: [Debian MobCom](https://wiki.debian.org/SummerOfCode2024/ApprovedProjects/DebianOsmoCom)

* Student: Nathan D

Deliverables of the project: Update the outdated mobile packages and recreate aged
packages due to new dependencies. Bring in more mobile communication tools by
adding about 5 new packages.

----

Project: [Improve support of the Rust coreutils in Debian](https://wiki.debian.org/SummerOfCode2024/ApprovedProjects/DebianRustCoreutils)

* Student: Sreehari Prasad TM

Deliverables of the project: Make uutils behave more like GNU’s coreutils by
improving compatibility with GNU coreutils test suit.

----

Project: [Improve support of the Rust findutils in Debian](https://wiki.debian.org/SummerOfCode2024/ApprovedProjects/DebianRustFindutils)

* Student: hanbings

Deliverables of the project: A safer and more performant implementation of the
GNU suite's xargs, find, locate and updatedb tools in rust.

----

Project: [Expanding ROCm support within Debian and derivatives](https://wiki.debian.org/SummerOfCode2024/ApprovedProjects/ExpandingROCmSupportWithinDebianAndDerivatives)

* Student: xuantengh

Deliverables of the project: Building, packaging, and uploading missing ROCm
software into Debian repositories, starting with simple tools and progressing to
high-level applications like PyTorch, with the final deliverables comprising a
series of ROCm packages meeting community quality assurance standards.

----

Project: [procps: Development of System Monitoring, Statistics and Information
Tools in
Rust](https://wiki.debian.org/SummerOfCode2024/ApprovedProjects/uutils%3A%20reimplement%20some%20procps%20tools%20in%20Rust)

* Student: Krysztal Huang

Deliverables of the project: Improve the usability of the entire Rust-based
implementation of the procps utility on Linux.

----

Congratulations and welcome to all the contributors!

The Google Summer of Code program is possible in Debian thanks to the efforts of
Debian Developers and Debian Contributors that dedicate part of their free time to
mentor contributors and outreach tasks.

Join us and help extend Debian! You can follow the contributors' weekly reports
on the [debian-outreach mailing-list][debian-outreach-ml], chat with us on our
[IRC channel][debian-outreach-irc] or reach out to the individual projects'
team mailing lists.

[debian-outreach-ml]: http://lists.debian.org/debian-outreach/ (debian-outreach
AT lists.debian.org)
[debian-outreach-irc]: irc://irc.debian.org/debian-outreach (#debian-outreach
on irc.debian.org)
