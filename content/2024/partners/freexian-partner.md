Title: Debian welcomes Freexian as our newest partner!
Slug: freexianpartner
Date: 2024-10-4 03:17
Author: Donald Norwood
Tags: partners, support, freexian
Lang: en
Translator:
Image: /images/openlogo-100.png
Status: published

[![Freexian logo](|static|/images/freexian.png)](https://www.freexian.com/)

We are excited to announce and welcome [Freexian](https://www.freexian.com)
into [Debian Partners](https://www.debian.org/partners/).

Freexian specializes in Free Software with a particular focus on Debian
GNU/Linux. Freexian can assist with consulting, training, technical support,
packaging, or software development on projects involving use or development of Free
software.

All of Freexian's employees and partners are well-known contributors in the Free
Software community, a choice that is integral to Freexian's business model.

#### About the Debian Partners Program

[The Debian Partners Program](https://www.debian.org/partners/2024/partners)
was created to recognize companies and organizations that help and provide
continuous support to the project with services, finances, equipment, vendor
support, and a slew of other technical and non-technical services.

Partners provide critical assistance, help, and support which has advanced and
continues to further our work in providing the 'Universal Operating System' to
the world.

Thank you Freexian!
