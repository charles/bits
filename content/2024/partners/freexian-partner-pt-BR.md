Title: O Debian dá as boas-vindas à Freexian como nossa mais nova parceira!
Slug: freexianpartner
Date: 2024-10-04 03:17
Author: Donald Norwood
Tags: partners, support, freexian
Translator: Paulo Henrique de Lima Santana (phls)
Lang: pt-BR
Image: /images/openlogo-100.png
Status: published

[![Freexian logo](|static|/images/freexian.png)](https://www.freexian.com/)

Estamos felizes em anunciar e dar as boas-vindas à
[Freexian](https://www.freexian.com) entre os(as)
[parceiros(as) do Debian](https://www.debian.org/partners/).

A Freexian é especializada em Software Livre com foco particular em Debian
GNU/Linux. A Freexian pode auxiliar com consultoria, treinamento, suporte
técnico, empacotamento ou desenvolvimento de software em projetos que envolvam
uso ou desenvolvimento de Software Livre.

Todos(as) os(as) funcionários(as) e parceiros(as) da Freexian são
colaboradores(as) conhecidos(as) na comunidade de Software Livre, uma escolha
que é essencial para o modelo de negócios da Freexian.

#### Sobre o programa de parceiros(as) do Debian

[O programa de parceiros(as) do
Debian](https://www.debian.org/partners/2024/partners)
foi criado para reconhecer empresas e organizações que ajudam e fornecem
suporte contínuo ao projeto com serviços, finanças, equipamentos, suporte de
fornecedores e uma série de outros serviços técnicos e não técnicos.

Os(As) parceiros(as) fornecem assistência, ajuda e suporte essenciais que
avançaram e continuam a promover nosso trabalho de fornecer o
"Sistema Operacional Universal" ao mundo.

Obrigado, Freexian!
