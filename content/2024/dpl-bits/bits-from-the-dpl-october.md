Title: Bits from the DPL
Date: 2024-10-07
Tags: dpl, lintian, SPDX tools, network stack, trixie, teams
Slug: bits-from-the-dpl-Octobber
Author: Andreas Tille
Status: published

Dear Debian community,

this are my bits from DPL for September.

New lintian maintainer
----------------------

I'm pleased to welcome Louis-Philippe Véronneau as a new Lintian
maintainer. He humorously acknowledged his new role, stating,
"[Apparently I'm a Lintian maintainer now][li1]".  I remain confident that
we can, and should, continue modernizing our policy checker, and I see
this as one important step toward that goal.

[li1]: https://lists.debian.org/debian-devel/2024/09/msg00014.html

SPDX name / license tools
-------------------------

There was a discussion about deprecating the unique names for DEP-5 and
migrating to fully [compliant SPDX names][sp1].

Simon McVittie [wrote][sp2]: "Perhaps our Debian-specific names *are*
better, but the relevant question is whether they are *sufficiently*
better to outweigh the benefit of sharing effort and specifications with
the rest of the world (and I don't think they are)."  Also Charles
Plessy sees the value of deprecating the Debian ones and align on
[SPDX][sp3].

The thread on debian-devel list contains several practical hints for
writing debian/copyright files.

[sp1]: https://lists.debian.org/debian-devel/2024/09/msg00140.html
[sp2]: https://lists.debian.org/debian-devel/2024/09/msg00150.html
[sp3]: https://lists.debian.org/debian-devel/2024/09/msg00163.html

proposal: Hybrid network stack for Trixie
-----------------------------------------

There was a very long discussion on debian-devel list about the network
stack on Trixie that started in [July][nw1] and was continued in end of
[August / beginning of September][nw2].  The discussion was also covered
on [LWN][nw3].  It continued in a
"[proposal: Hybrid network stack for Trixie][nw4]" by Lukas Märdian.

[nw1]: <https://lists.debian.org/debian-devel/2024/07/msg00098.html>
[nw2]: <https://lists.debian.org/debian-devel/2024/08/msg00317.html>
[nw3]: <https://lwn.net/Articles/989055/>
[nw4]: <https://lists.debian.org/debian-devel/2024/09/msg00240.html>

Contacting teams
----------------

I continued reaching out to teams in September. One common pattern I've
noticed is that most teams lack a clear strategy for attracting new
contributors. Here's an example snippet from one of my outreach emails,
which is representative of the typical approach:

 Q: Do you have some strategy to gather new contributors for your team?
 A: No.
 Q: Can I do anything for you?
 A: Everything that can help to have more than 3 guys :-D

Well, only the first answer, "No," is typical. To help the JavaScript
team, I'd like to invite anyone with JavaScript experience to join the
team's mailing list and offer to learn and contribute. While I've only
built a JavaScript package once, I know this team has developed
excellent tools that are widely adopted by others. It's an active and
efficient team, making it a great starting point for those looking to
get involved in Debian.  You might also want to check out the
"[Little tutorial for JS-Team beginners][ct2]".

Given the lack of a strategy to actively recruit new contributors--a
common theme in the responses I've received--I recommend reviewing my
talk from DebConf23 [about teams][ct3]. The Debian Med team would have
struggled significantly in my absence (I've paused almost all work with
the team since becoming DPL) if I hadn't consistently focused on
bringing in new members. I'm genuinely proud of how the team has managed
to keep up with the workload (thank you, Debian Med team!). Of course,
onboarding newcomers takes time, and there's no guarantee of long-term
success, but if you don't make the effort, you'll never find out.

[ct1]: <https://lists.debian.org/debian-js/>
[ct2]: <https://wiki.debian.org/Javascript/Tutorial>
[ct3]: <https://debconf23.debconf.org/talks/32-teams-newcomers-and-numbers/>

OS underpaid
------------

The Register, in its article titled
"[Open Source Maintainers Underpaid, Swamped by Security, Going Gray][os1]",
summarizes the 2024 State of the
Open Source Maintainer Report. I find this to be an interesting read,
both in general and in connection with the challenges mentioned in the
previous paragraph about finding new team members.

[os1]: <https://www.theregister.com/2024/09/18/open_source_maintainers_underpaid/>

Kind regards
    Andreas.
