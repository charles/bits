Title: Bits from the DPL
Date: 2024-11-07
Tags: dpl, ada lovelace day, Cambridge, ftpmaster, teams, programming languages, open oource AI
Slug: bits-from-the-dpl-november
Author: Andreas Tille
Status: published

Dear Debian community,

this is Bits from DPL for October.  In addition to a summary of my
recent activities, I aim to include newsworthy developments within
Debian that might be of interest to the broader community. I believe
this provides valuable insights and foster a sense of connection across
our diverse projects. Also, I welcome your feedback on the format and
focus of these Bits, as community input helps shape their value.

Ada Lovelace Day 2024
=====================

As outlined in my platform, I'm committed to increasing the diversity of
Debian developers. I hope the recent article celebrating Ada Lovelace
Day 2024–featuring interviews with [women in Debian][al1]–will serve as an
inspiring motivation for more women to join our community.

[al1]: https://lists.debian.org/debian-women/2024/10/msg00000.html

MiniDebConf Cambridge
=====================

This was my first time attending the [MiniDebConf in Cambridge][md1],
hosted at the ARM building. I thoroughly enjoyed the welcoming
atmosphere of both MiniDebCamp and MiniDebConf. It was wonderful to
reconnect with people who hadn't made it to the last two DebConfs, and,
as always, there was plenty of hacking, insightful discussions, and
valuable learning.

If you missed the recent MiniDebConf, there's a great opportunity to
attend the next one in [Toulouse][md2]. It was recently decided to
include a MiniDebCamp beforehand as well.

[md1]: https://wiki.debian.org/DebianEvents/gb/2024/MiniDebConfCambridge
[md2]: https://wiki.debian.org/DebianEvents/fr/2024/Toulouse

FTPmaster accepts MRs for DAK
=============================

At the recent MiniDebConf in Cambridge, I discussed potential
enhancements for DAK to make life easier for both FTP Team members and
developers. For those interested, the document "[Hacking on DAK][dak1]"
provides guidance on setting up a local DAK instance and developing
patches, which can be submitted as MRs.

As a perfectly random example of such improvements some older MR,
"[Add commands to accept/reject updates from a policy queue][dak2]" might give
you some inspiration.

At MiniDebConf, we compiled an initial list of features that could
benefit both the FTP Team and the developer community. While I had
preliminary discussions with the FTP Team about these items, not all
ideas had consensus.  I aim to open a detailed, public discussion to
gather broader feedback and reach a consensus on which features to
prioritize.

* Accept+Bug report

Sometimes, packages are rejected not because of DFSG-incompatible licenses
but due to other issues that could be resolved within an existing package
(as discussed in my DebConf23 BoF, "[Chatting with ftpmasters][dak3]"[1]).
During the "[Meet the ftpteam][dak4]" BoF
([Log/transcription of the BoF can be found here][dak5]), for the moment
until the MR gets accepted, a new option was proposed for FTP Team members
reviewing packages in NEW:
*<p align =center>Accept + Bug Report</p>*
This option would allow a package to enter Debian (in unstable or
experimental) with an automatically filed RC bug report. The RC bug would
prevent the package from migrating to testing until the issues are addressed.
To ensure compatibility with the BTS, which only accepts bug reports for
existing packages, a delayed job (24 hours post-acceptance) would file the
bug.

* Binary name changes - for instance if done to experimental not via new

When binary package names change, currently the package must go through the
NEW queue, which can delay the availability of updated libraries.  Allowing
such packages to bypass the queue could expedite this process.  A
configuration option to enable this bypass specifically for uploads to
experimental may be useful, as it avoids requiring additional technical
review for experimental uploads.

Previously, I believed the requirement for binary name changes to pass
through NEW was due to a missing feature in DAK, possibly addressable via an
MR. However, in discussions with the FTP Team, I learned this is a matter of
team policy rather than technical limitation. I haven't found this policy
documented, so it may be worth having a community discussion to clarify and
reach consensus on how we want to handle binary name changes to get the MR
sensibly designed.

* Remove dependency tree

When a developer requests the removal of a package – whether entirely or for
specific architectures – RM bugs must be filed for the package itself as well
as for each package depending on it. It would be beneficial if the dependency
tree could be automatically resolved, allowing either:

    a) the DAK removal tooling to remove the entire dependency tree
       after prompting the bug report author for confirmation, or

    b) the system to auto-generate corresponding bug reports for all
       packages in the dependency tree.

The latter option might be better suited for implementation in an MR for
reportbug. However, given the possibility of large-scale removals (for
example, targeting specific architectures), having appropriate tooling for
this would be very beneficial.

In my opinion the proposed DAK enhancements aim to support both FTP Team
members and uploading developers. I'd be very pleased if these ideas
spark constructive discussion and inspire volunteers to start working on
them--possibly even preparing to join the FTP Team.

On the topic of ftpmasters: an ongoing discussion with SPI lawyers is
currently reviewing the [non-US agreement established 22 years ago][dak5].
Ideally, this review will lead to a streamlined workflow for ftpmasters,
removing certain hurdles that were originally put in place due to legal
requirements, which were updated in 2021.

[dak1]: https://salsa.debian.org/ftp-team/dak/-/blob/master/docs/development.rst?ref_type=heads
[dak2]: https://salsa.debian.org/ftp-team/dak/-/merge_requests/270
[dak3]: https://debconf23.debconf.org/talks/31-chatting-with-ftpmasters/
[dak4]: https://debconf24.debconf.org/talks/154-meet-the-ftpteam/
[dak5]: https://salsa.debian.org/tille/dc24/-/blob/ftpmaster_bof_log/etherpad/txt/154-meet-the-ftpteam.txt?ref_type=heads
[dak6]: https://lists.debian.org/debian-devel-announce/2002/03/msg00019.html

Contacting teams
================

My outreach efforts to Debian teams have slowed somewhat recently.
However, I want to emphasize that anyone from a packaging team is more
than welcome to reach out to me directly. My outreach emails aren't
following any specific orders--just my own somewhat naïve view of Debian,
which I'm eager to make more informed.

Recently, I received two very informative responses: one from the Qt/KDE
Team, which thoughtfully compiled input from several team members into a
[shared document][ct1]. The other was from the Rust Team, where I
received three quick, helpful replies–one of which included an invitation
to their upcoming team meeting.

[ct1]: https://mensuel.framapad.org/p/4dp9h91jgh-aa34?lang=fr

Interesting readings on our mailing lists
=========================================

I consider the following threads on our mailing list some interesting
reading and would like to add some comments.

Sensible languages for younger contributors
-------------------------------------------

Though the discussion on [debian-devel about programming languages][la1] took
place in September, I recently caught up with it. I strongly
believe Debian must continue evolving to stay relevant for the future.

 "Everything must change, so that everything can stay the same."
   -- Giuseppe Tomasi di Lampedusa, The Leopard

I encourage constructive discussions on integrating programming
languages in our toolchain that support this evolution.

[la1]: https://lists.debian.org/debian-devel/2024/09/msg00268.html

Concerns regarding the "Open Source AI Definition"
--------------------------------------------------

A recent thread on the debian-project list discussed the
"[Open Source AI Definition][ai1]". This topic will impact Debian in the
future, and we need to reach an informed decision. I'd be glad to see more
perspectives in the discussions−particularly on finding a sensible consensus,
understanding how FTP Team members view their delegated role, and
considering whether their delegation might need adjustments for clarity
on this issue.

[ai1]: https://lists.debian.org/debian-project/2024/10/msg00005.html

Kind regards
    Andreas.
