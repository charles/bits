Title: Debian Project Leader Election 2024, Andreas Tille elected.
Slug: dpl-elections-2024
Date: 2024-04-22 14:00
Author: Donald Norwood
Tags: dpl, election, leader, vote
Status: published

The voting period for the Debian Project Leader election has ended. Please join
us in congratulating Andreas Tille as the new Debian Project Leader.

The new term for the project leader started on 2024-04-21.

369 of 1,010 Debian Developers voted using the [Condorcet
method](http://en.wikipedia.org/wiki/Condorcet_method).

More information about the results of the voting are available on the [Debian
Project Leader Elections 2024](https://www.debian.org/vote/2024/vote_001) page.

Many thanks all of our Developers for voting.
