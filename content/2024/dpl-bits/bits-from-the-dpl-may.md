Title: Bits from the DPL
Date: 2024-05-02 03:00
Tags: dpl, bits from the dpl, community, debian
Slug: bits-from-the-dpl-may2024
Author: Andreas Tille
Status: published

Hi,

Keeping my promise for monthly bits, here's a quick snapshot of my first
ten days as DPL.

Special thanks to Jonathan for an insightful introduction that left less
room for questions. His introduction covered my first tasks like expense
approval and CTTE member appointments thoroughly. Although I made a
visible oversight by forgetting to exclude Simon McVittie &lt;smcv&gt; from
the list, whose term has
[ended](https://lists.debian.org/debian-devel-announce/2024/04/msg00010.html)
, I'm committed to learning from this mistake.  In future I'll prioritize
thorough proofreading to ensure accuracy.

Part of my "work" was learning what channels I need to subscribe and
adjust my .procmailrc and .muttrc took some time.

Recently I had my first press interview. I had to answer a couple of prepared
questions for [Business IT News](https://deb.li/ZktM). It seems journalists are
always on the lookout for unique angles. When asked if humility is a new trait
for DPLs, my response would be a resounding "No."  In my experience, humility
is a common quality among DPLs I've encountered, including Jonathan.

One of my top priorities is reaching out to all our dedicated and
appointed teams, including those managing critical infrastructure. I've
begun with the CTTE, Salsa Admins and Debian Snapshot.  Everything
appears to be in order with the CTTE team.  I'm waiting for response
from Salsa and Snapshot, which is fine given the recent contact.

I was pointed out to the fact that lintian is in an unfortunate state as
Axel Beckert confirmed on the lintian maintainers
[list](https://lists.debian.org/debian-lint-maint/2024/04/msg00010.html).
It turns out that bug #1069745 of magics-python should not have been undetected
for a long time if lintian [bug #677078](https://bugs.debian.org/677078) would
have been fixed.  It seems obvious to me that lintian needs more work to
fulfill its role as reliably policy checker to ensure our high level of
packaging quality.

In any case thanks a lot to Axel who is doing his best but it seems
urgent to me to find some more person-power for this task.  Any volunteer
to lend some helping hand in the lintian maintainers team?

On 2024-04-30 I gave my first talk "Bits from greenhorn DPL" online
at MiniDebConf Brasil in Belo Horizonte.  The Q&A afterwards stired
some flavours of the question: "What can Debian Brasil do better?"
My answer was always in a way: Given your great activity in now
organising the fifth MiniDebConf you are doing pretty well and I have
no additional hints for the moment.

Kind regards
   Andreas.
