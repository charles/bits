Title: Bits from the DPL
Date: 2024-12-03
Tags: dpl, minidebconf, newcomers, reproducible builds, bug squashing
Slug: bits-from-the-dpl-december
Author: Andreas Tille
Status: published

This is bits from DPL for November.

MiniDebConf Toulouse
====================

I had the pleasure of attending the MiniDebConf in Toulouse, which
featured a range of [engaging talks][mt1], complementing those from the
recent MiniDebConf in Cambridge. Both events were preceded by a DebCamp,
which provided a valuable opportunity for focused work and
collaboration.

DebCamp
-------

During these events, I participated in numerous technical discussions on
topics such as maintaining long-neglected packages, team-based
maintenance, FTP master policies, Debusine, and strategies for
separating maintainer script dependencies from runtime dependencies,
among others. I was also fortunate that members of the Publicity Team
attended the MiniDebCamp, giving us the opportunity to meet in person
and collaborate face-to-face.

Independent of the ongoing lengthy discussion on the
[Debian Devel mailing list][mt2], I encountered the perspective that unifying
Git workflows might be more critical than ensuring all packages are managed
in Git. While I'm uncertain whether these two questions--adopting Git as
a universal development tool and agreeing on a common workflow for its
use--can be fully separated, I believe it's worth raising this topic for
further consideration.

Attracting newcomers
--------------------

In my own [talk][mt3], I regret not leaving enough time for questions--my
apologies for this. However, I want to revisit the sole question raised,
which essentially asked: Is the documentation for newcomers sufficient
to attract new contributors? My immediate response was that this
question is best directed to new contributors themselves, as they are in
the best position to identify gaps and suggest improvements that could
make the documentation more helpful.

That said, I'm personally convinced that our challenges extend beyond
just documentation. I don't get the impression that newcomers are lining
up to join Debian only to be deterred by inadequate documentation. The
issue might be more about fostering interest and engagement in the first
place.

My personal impression is that we sometimes fail to convey that Debian
is not just a product to download for free but also a technical
challenge that warmly invites participation. Everyone who respects our
Code of Conduct will find that Debian is a highly diverse community,
where joining the project offers not only opportunities for technical
contributions but also meaningful social interactions that can make the
effort and time truly rewarding.

In several of my previous talks (you can find them on my [talks page][mt4]
–just search for "team," and don't be deterred if you see
"Debian Med" in the title; it's simply an example), I emphasized that
the interaction between a mentor and a mentee often plays a far more
significant role than the documentation the mentee has to read. The key
to success has always been finding a way to spark the mentee's interest
in a specific topic that resonates with their own passions.

Bug of the Day
--------------

In [my presentation][mt3], I provided a brief overview of the Bug of the
Day initiative, which was launched with the aim of demonstrating how to
fix bugs as an entry point for learning about packaging. While the
current level of interest from newcomers seems limited, the initiative
has brought several additional benefits.

I must admit that I'm learning quite a bit about Debian myself. I often
compare it to exploring a house's cellar with a flashlight –you uncover
everything from hidden marvels to things you might prefer to discard.
I've also come across traces of incredibly diligent people who have
invested their spare time polishing these hidden treasures (what we call
NMUs). The janitor, a service in Salsa that automatically updates
packages, fits perfectly into this cellar metaphor, symbolizing the
ongoing care and maintenance that keep everything in order. I hadn't
realized the immense amount of silent work being done behind the
scenes--thank you all so much for your invaluable QA efforts.

Reproducible builds
-------------------

It might be unfair to single out a specific talk from Toulouse, but I'd
like to highlight the one on [reproducible builds][mt5]. Beyond its
technical focus, the talk also addressed the recent [loss of Lunar][mt6], whom
we mourn deeply. It served as a tribute to Lunar's contributions
and legacy. Personally, I've encountered packages maintained by Lunar
and bugs he had filed. I believe that taking over his packages and
addressing the bugs he reported is a meaningful way to honor his memory
and acknowledge the value of his work.

[mt1]: https://toulouse2024.mini.debconf.org/schedule/
[mt2]: https://lists.debian.org/debian-devel/2024/11/msg00459.html
[mt3]: https://toulouse2024.mini.debconf.org/talks/10-bits-from-dpl/
[mt4]: https://people.debian.org/~tille/talks/
[mt5]: https://toulouse2024.mini.debconf.org/talks/4-reproducible-builds-rebuilding-what-is-distributed-from-ftpdebianorg/
[mt6]: https://www.debian.org/News/2024/20241119

Advent calendar bug squashing
=============================

I’d like to promote an idea originally introduced by Thorsten Alteholz,
who in [2011][ac1] proposed a Bug Squashing Advent Calendar for the Debian
Med team. (For those unfamiliar with the concept of an Advent Calendar,
you can find an explanation on [Wikipedia][ac2].) While the original
version included a fun graphical element —which we’ve had to set aside
due to time constraints (volunteers, anyone?)— we’ve kept the tradition
alive by tackling one bug per day from December 1st to 24th each year.
This initiative helps clean up issues that have accumulated over the
year.

Regardless of whether you celebrate the concept of Advent, I warmly
recommend this approach as a form of continuous bug-squashing party for
every team. Not only does it contribute to the release readiness of your
team’s packages, but it’s also an enjoyable and bonding activity for
team members.

[ac1]: https://lists.debian.org/debian-med/2011/11/msg00124.html
[ac2]: https://en.wikipedia.org/wiki/Advent_calendar

Best wishes for a cheerful and productive December

    Andreas.
