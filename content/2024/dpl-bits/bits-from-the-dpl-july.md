Title: Bits from the DPL
Date: 2024-07-02 19:00
Tags: dpl, statement, community, tag2upload, usrmerge, teams, help, DebConf, history
Slug: bits-from-the-dpl-july
Author: Andreas Tille
Status: published

Dear Debian community,

Statement on Daniel Pocock
--------------------------

The Debian project has successfully taken action to secure its
trademarks and interests worldwide, as detailed in our press
[statement](https://www.debian.org/News/2024/20240606). I would like
to personally thank everyone in the community who was involved in this
process. I would have loved for you all to have spent your volunteer
time on more fruitful things.

Debian Boot team might need help
--------------------------------

I think I've identified the issue that finally motivated me to contact
our teams: for a long time, I have had the impression that Debian is
driven by several "one-person teams" (to varying extents of individual
influence and susceptibility to burnout). As DPL, I see it as my task to
find ways to address this issue and provide support.

I received private responses from Debian Boot team members, which
motivated me to kindly invite volunteers to some prominent and highly
visible fields of work that you might find personally challenging. I
recommend subscribing to the
[Debian Boot mailing list](https://lists.debian.org/debian-boot/) to see
where you might be able to provide assistance.

/usrmerge
----------

Helmut Grohne [confirmed](https://lists.debian.org/debian-devel/2024/06/msg00034.html)
that the last remaining packages shipping aliased files inside the package set
relevant to debootstrap were uploaded. Thanks a lot for Helmut and all
contributors that helped to implement
[DEP17](https://dep-team.pages.debian.net/deps/dep17/).

Contacting more teams
---------------------

I'd like to repeat that I've registered a BoF for DebConf24 in Busan
with the following description:

  This BoF is an attempt to gather as much as possible teams inside
  Debian to exchange experiences, discuss workflows inside teams, share
  their ways to attract newcomers etc.

  Each participant team should prepare a short description of their work
  and what team roles (“openings”) they have for new contributors. Even
  for delegated teams (membership is less fluid), it would be good to
  present the team, explain what it takes to be a team member, and what
  steps people usually go to end up being invited to participate. Some
  other teams can easily absorb contributions from salsa MRs, and at some
  point people get commit access. Anyway, the point is that we work on the
  idea that the pathway to become a team member becomes more clear from an
  outsider point-of-view.

I'm lagging a bit behind my team contacting schedule and will not manage
to contact every team before DebConf. As a (short) summary, I can draw
some positive conclusions about my efforts to reach out to teams. I was
able to identify some issues that were new to me and which I am now
working on. Examples include limitations in Salsa and Salsa CI. I
consider both essential parts of our infrastructure and will support
both teams in enhancing their services.

Some teams confirmed that they are basically using some common
infrastructure (Salsa team space, mailing lists, IRC channels) but that
the individual members of the team work on their own problems without
sharing any common work. I have also not read about convincing
strategies to attract newcomers to the team, as we have established, for
instance, in the Debian Med team.

DebConf attendance
------------------

The amount of money needed to fly people to South Korea was higher than
usual, so the DebConf bursary team had to make some difficult decisions
about who could be reimbursed for travel expenses. I extended the budget
for diversity and newcomers, which enabled us to invite some additional
contributors. We hope that those who were not able to come this year can
make it next year to Brest or to MiniDebConf
[Cambridge](https://lists.debian.org/debian-devel-announce/2024/06/msg00002.html)
or
[Toulouse](https://lists.debian.org/debian-devel-announce/2024/05/msg00001.html)

tag2upload
----------

On June 12, Sean Whitton requested comments on the debian-vote list
regarding a General Resolution (GR) about
[tag2upload](https://lists.debian.org/debian-vote/2024/06/msg00000.html).
The discussion began with technical details but unfortunately, as often
happens in long threads, it drifted into abrasive language, prompting
the community team to address the behavior of an opponent of the GR
supporters. After 560 emails covering technical details, including a
detailed [security review](https://www.eyrie.org/~eagle/notes/debian/tag2upload.html)
by Russ Allbery, Sean finally proposed the
[GR](https://lists.debian.org/debian-vote/2024/06/msg00561.html)
on June 27, 2024 (two weeks after requesting comments).

Firstly, I would like to thank the drivers of this GR and acknowledge
the technical work behind it, including the security review. I am
positively convinced that Debian can benefit from modernizing its
infrastructure, particularly through stronger integration of Git into
packaging workflows.

Sam Hartman provided some historical context
[[1]](https://lists.debian.org/debian-vote/2024/06/msg00581.html),
[[2]](https://lists.debian.org/debian-devel/2019/07/msg00501.html),
[[3]](https://lists.debian.org/debian-devel/2019/08/msg00407.html),
[[4]](https://bugs.debian.org/932753),
noting that this discussion originally took place five years ago with no
results from several similarly lengthy threads. My favorite
[summary](https://lists.debian.org/debian-vote/2024/06/msg00583.html) of the
entire thread was given by Gregor Herrmann, which reflects the same gut
feeling I have and highlights a structural problem within Debian that
hinders technical changes. Addressing this issue is definitely a matter
for the Debian Project Leader, and I will try to address it during my
term.

At the time of writing these bits, a
[proposal](https://lists.debian.org/debian-vote/2024/06/msg00602.html)
from ftpmaster, which is being continuously discussed, might lead to a
solution. I was also asked to extend the
[GR discussion periods](https://lists.debian.org/debian-vote/2024/07/msg00006.html)
which I will do in separate mail.

Talk: Debian GNU/Linux for Scientific Research
----------------------------------------------

I was invited to have a
[talk](https://people.debian.org/~tille/talks/20240620_using_debian_in_science/index_en.html)
in the Systems-Facing Track of University of British Columbia (who is
sponsoring rack space for several Debian servers).  I admit it felt a bit
strange to me after working more than 20 years for establishing Debian in
scientific environments to be invited to such a talk "because I'm DPL". 😉

Kind regards
   Andreas.
