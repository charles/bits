Title: apt install dpl-candidate: Sruthi Chandran
Slug: dpl-interview-SruthiChandran
Date: 2024-04-05 20:36
Author: Yashraj Moghe with The Debian Publicity Team
Tags: interviews, dpl, vote, meetDDs
Status: published

The Debian Project Developers will shortly vote for a new Debian Project Leader
known as the DPL.

The DPL is the official representative of representative of The Debian Project
tasked with managing the overall project, its vision, direction, and finances.

The DPL is also responsible for the selection of Delegates, defining areas of
responsibility within the project, the coordination of Developers, and making
decisions required for the project.

Our outgoing and present DPL Jonathan Carter served 4 terms, from 2020
through 2024. Jonathan shared his last [Bits from the
DPL](https://bits.debian.org/2024/04/bits-from-the-dpl-april.html)
post to Debian recently and his hopes for the future of Debian.

Recently, we sat with the two present candidates for the DPL position asking
questions to find out  who they really are in a series of interviews about
their platforms, visions for Debian, lives, and even their favorite text
editors. The interviews were conducted by disaster2life (Yashraj Moghe) and
made available from video and audio transcriptions:

* Andreas Tille
  [[Interview](https://bits.debian.org/2024/04/dpl-interview-AndresTille.html)]
* Sruthi Chandran [this document]

Voting for the position starts on April 6, 2024.

_Editors' note:
 This is our official return to Debian interviews, readers should stay tuned
 for more upcoming interviews with Developers and other important figures in
 Debian as part of our "Meet your Debian Developer" series. We used the
 following tools and services: [Turboscribe.ai](https://turboscribe.ai) for the
 transcription from the audio and video files, [IRC:
 Oftc.net](https://www.oftc.net) for communication, [Jitsi
 meet](https://meet.jit.si/) for interviews, and [Open Broadcaster Software
 (OBS)](https://obsproject.com/) for editing and video. While we encountered
 many technical difficulties in the return to this process, we are still able
 and proud to present the transcripts of the interviews edited only in a few
 areas for readability._

**2024 Debian Project Leader Candidate: Sruthi Chandran**

## Sruthi's interview

**Hi Sruthi, so for the first question, who are you and could you tell us a little
bit about yourself?**

[Sruthi]:
> I usually talk about me whenever I am talking about answering the question who
am I, I usually say like I am a librarian turned free software enthusiast and
a Debian Developer. So I had no technical background and I learned, I was
introduced to free software through my husband and then I learned Debian
packaging, and eventually I became a Debian Developer. So I always give my
example to people who say I am not technically inclined, I don't have technical
background so I can't contribute to free software.
>
> So yeah, that's what I refer to myself.

**For the next question, could you tell me what do you do in Debian, and could
you mention your story up until here today?**

[Sruthi]:
> Okay, so let me start from my initial days in Debian. I started contributing to
Debian, my first contribution was a Tibetan font. We went to a Tibetan place
and they were saying they didn't have a font in Linux.
>
> So that's how I started contributing. Then I moved on to Ruby packages, then I
have some JavaScript and Go packages, all dependencies of GitLab. So I was
involved with maintaining GitLab for some time, now I'm not very active there.
>
> But yeah, so GitLab was the main package I was contributing to since I
contributed since 2016 to maybe like 2020 or something. Later I have come
[over to] packaging. Now I am part of some of the teams, delegated teams, like
community team and outreach team, as well as the Debconf committee. And the
biggest, I think, my activity in Debian, I would say is organizing Debconf
> 2023. So it was a great experience and yeah, so that's my story in Debian.

**So what are three key terms about you and your candidacy?**

[Sruthi]:
> Okay, let me first think about it. For candidacy, I can start with diversity is
one point I started expressing from the first time I contested for DPL. But to
be honest, that's the main point I want to bring.

[Yashraj]:
> So for diversity, if you could break down your thoughts on diversity and make
them, [about] your three points including diversity.

[Sruthi]:
> So in addition to, eventually  when starting it was just diversity. Now I have
like a bit more ideas, like community, like I want to be a leader for the
Debian community. More than, I don't know, maybe people may not agree, but I
would say I want to be a leader of Debian community rather than a Debian
operating system.
>
> I connect to community more and third point I would say.

**The term of a DPL lasts for an year. So what do you think during, what would you
try to do during that, that you can't do from your position now?**

[Sruthi]:
> Okay. So I, like, I am very happy with the structure of Debian and how things
work in Debian. Like you can do almost a lot of things, like almost all things
without being a DPL.
>
> Whatever change you want to bring about or whatever you want to do, you can do
without being a DPL. Anyone, like every DD has the same rights. Only things I
feel [the] DPL has hold on are mainly the budget or the funding part, which like,
that's where they do the decision making part.
>
> And then comes like, and one advantage of DPL driving some idea is that somehow
people tend to listen to that with more, like, tend to give more attention to
what DPL is saying rather than a normal DD. So I wanted to, like, I have
answered some of the questions on how to, how I plan to do the financial
budgeting part, how I want to handle, like, and the other thing is using the
extra attention that I get as a DPL, I would like to obviously start with the
diversity aspect in Debian. And yeah, like, I, what I want to do is not, like,
be a leader and say, like, take Debian to one direction where I want to go, but
I would rather take suggestions and inputs from the whole community and go about
with that.
>
> So yes, that's what I would say.

**And taking a less serious question now, what is your preferred text editor?**

[Sruthi]:
> Vim.

[Yashraj]:
> Vim, wholeheartedly team Vim?

[Sruthi]:
> Yes.

[Yashraj]:
> Great. Well, this was made in Vim, all the text for this.

[Sruthi]:
> So, like, since you mentioned extra data, I'll give my example, like, it's just
a fun note, when I started contributing to Debian, as I mentioned, I didn't have
any knowledge about free software, like Debian, and I was not used to even
using Linux. So, and I didn't have experience with these text editors. So, when
I started contributing, I used to do the editing part using gedit.
>
> So, that's how I started. Eventually, I moved to Nano, and once I reached Vim,
I didn't move on.

**Team Vim. Next question. What, what do you think is the importance of
the Debian project in the world today? And where would you like to see it in
10 years, like 10 years into the future?**

[Sruthi]:
> Okay. So, Debian, as we all know, is referred to as the universal operating
system without, like, it is said for a reason. We have hundreds and hundreds of
operating systems, like Linux, distributions based on Debian.
>
> So, I believe Debian, like even now, Debian has good influence on the, at least
on the Linux or Linux ecosystem. So, what we implement in Debian has, like, is
going to affect quite a lot of, like, a very good percentage of people using
Linux. So, yes.
>
> So, I think Debian is one of the leading Linux distributions. And I think in
10 years, we should be able to reach a position, like, where we are not, like,
even now, like, even these many years after having Linux, we face a lot of
problems in newer and newer hardware coming up and installing on them is a big
problem. Like, firmwares and all those things are getting more and more
complicated.
>
> Like, it should be getting simpler, but it's getting more and more complicated.
So, I, one thing I would imagine, like, I don't know if we will ever reach
there, but I would imagine that eventually with the Debian, we should be able
to have some, at least a few of the hardware developers or hardware producers
have Debian pre-installed and those kind of things. Like, not, like, become,
I'm not saying it's all, it's also available right now.
>
> What I'm saying is that it becomes prominent enough to be opted as, like, default
distro.

**What part of Debian has made you And what part of the project has kept you going
all through these years?**

[Sruthi]:
> Okay. So, I started to contribute in 2016, and I was part of the team doing
GitLab packaging, and we did have a lot of training workshops and those kind of
things within India. And I was, like, I had interacted with some of the Indian
DDs, but I never got, like, even through chat or mail.
>
> I didn't have a lot of interaction with the rest of the world, DDs. And the 2019
Debconf changed my whole perspective about Debian. Before that, I wasn't, like,
even, I was interested in free software.
>
> I was doing the technical stuff and all. But after DebConf, my whole idea has
been, like, my focus changed to the community. Debian community is a very
welcoming, very interesting community to be with.
>
> And so, I believe that, like, 2019 DebConf was a for me. And that kept, from
2019, my focus has been to how to support, like, how, I moved to the community
part of Debian from there. Then in 2020 I became part of the community team,
and, like, I started being part of other teams.
>
> So, these, I would say, the Debian community is the one, like, aspect of Debian
that keeps me whole, keeps me held on to the Debian ecosystem as a whole.

**Continuing to speak about Debian, what do you think, what is the first thing that
comes to your mind when you think of Debian, like, the word, the community,
what's the first thing?**

[Sruthi]:
> I think I may sound like a broken record or something.

[Yashraj]:
> No, no.

[Sruthi]:
> Again, I would say the Debian community, like, it's the people who makes
Debian, that makes Debian special.
>
> Like, apart from that, if I say, I would say I'm very, like, one part of Debian
that makes me very happy is the, how the governing system of Debian works, the
Debian constitution and all those things, like, it's a very unique thing for
Debian. And, and it's like, when people say you can't work without a proper,
like, establishment or even somebody deciding everything for you, it's
difficult. When people say, like, we have been, Debian has been proving
it for quite a long time now, that it's possible.
>
> So, so that's one thing I believe, like, that's one unique point. And I am very
proud about that.

**What areas do you think Debian is failing in, how can it (that standing) be
improved?**

[Sruthi]:
> So, I think where Debian is failing now is getting new people into Debian.
Like, I don't remember, like, exactly the answer. But I remember hearing
someone mention, like, the average age of a Debian Developer is, like, above 40
or 45 or something, like, exact age, I don't remember.
>
> But it's like, Debian is getting old. Like, the people in Debian are getting old
and we are not getting enough of new people into Debian. And that's very
important to have people, like, new people coming up.
>
> Otherwise, eventually, like, after a few years, nobody, like, we won't have
enough people to take the project forward. So, yeah, I believe that is where we
need to work on. We are doing some efforts, like, being part of GSOC or
outreachy and having maybe other events, like, local events. Like, we used to
have a lot of Debian packaging workshops in India. And those kind of, I think,
in Brazil and all, they all have, like, local communities are doing. But we are
not very successful in retaining the people who maybe come and try out things.
>
> But we are not very good at retaining the people, like, retaining people who
come. So, we need to work on those things. Right now, I don't have a solid
answer for that.
>
> But one thing, like, I was thinking about is, like, having a Debian specific
outreach project, wherein the focus will be about the Debian, like, starting
will be more on, like, usually what happens in GSOC and outreach is that people
come, have the, do the contributions, and they go back. Like, they don't have
that connection with the Debian, like, Debian community or Debian project. So,
what I envision with these, the Debian outreach, the Debian specific outreach
is that we have some part of the internship, like, even before starting the
internship, we have some sessions and, like, with the people in Debian having,
like, getting them introduced to the Debian philosophy and Debian community and
Debian, how Debian works.
>
> And those things, we focus on that. And then we move on to the technical
internship parts. So, I believe this could do some good in having, like, when
you have people you can connect to, you tend to stay back in a project mode.
>
> When you feel something more than, like, right now, we have so many technical
stuff to do, like, the choice for a college student is endless. So, if they
want, if they stay back for something, like, maybe for Debian, I would say, we
need to have them connected to the Debian project before we go into technical
parts. Like, technical parts, like, there are other things as well, where they
can go and do the technical part, but, like, they can come here, like, yeah.
>
> So, that's what I was saying. Focused outreach projects is one thing. That's
just one.
>
> That's not enough. We need more of, like, more ideas to have more new people
come up. And I'm very happy with, like, the DebConf thing. We tend to get more
and more people from the places where we have a DebConf. Brazil is an example.
After the Debconf, they have quite a good improvement on Debian contributors.
>
> And I think in India also, it did give a good result. Like, we have more people
contributing and staying back and those things. So, yeah.
>
> So, these were the things I would say, like, we can do to improve.

**For the final question, what field in free software do you, what
field in free software generally do you think requires the most work to be
put into it? What do you think is Debian's part in that field?**

[Sruthi]:
> Okay. Like, right now, what comes to my mind is the free software licenses
parts. Like, we have a lot of free software licenses, and there are non-free
software licenses.
>
> But currently, I feel free software is having a big problem in enforcing these
licenses. Like, there are, there may be big corporations or like some people who
take up the whole, the code and may not follow the whole, for example, the GPL
licenses. Like, we don't know how much of those, how much of the free softwares
are used in the bigger things.
>
> Yeah, I agree. There are a lot of corporations who are afraid to touch free
software. But there would be good amount of free software, free work that
converts into property, things violating the free software licenses and those
things.
>
> And we do not have the kind of like, we have SFLC, SFC, etc. But still, we do
not have the ability to go behind and trace and implement the licenses. So,
enforce those licenses and bring people who are violating the licenses forward
and those kind of things is challenging because one thing is it takes time,
like, and most importantly, money is required for the legal stuff.
>
> And not always people who like people who make small software, or maybe big,
but they may not have the kind of time and money to have these things enforced.
So, that's a big challenge free software is facing, especially in our current
scenario. I feel we are having those, like, we need to find ways how we can get
it sorted.
>
> I don't have an answer right now what to do. But this is a challenge I felt
like and Debian's part in that. Yeah, as I said, I don't have a solution for
that.
>
> But the Debian, so DFSG and Debian sticking on to the free software licenses is
a good support, I think.

**So, that was the final question, Do you have anything else you want to mention
for anyone watching this?**

[Sruthi]:
> Not really, like, I am happy, like, I think I was able to answer the questions.
And yeah, I would say who is watching. I won't say like, I'm the best DPL
candidate, you can't have a better one or something.
>
> I stand for a reason. And if you believe in that, or the Debian community and
Debian diversity, and those kinds of things, if you believe it, I hope you
would be interested, like, you would want to vote for me. That's it.
>
> Like, I'm not, I'll make it very clear. I'm not doing a technical leadership
part here. So, those, I can't convince people who want technical leadership to
vote for me.
>
> But I would say people who connect with me, I hope they vote for me.
</br>
</br>
</br>
