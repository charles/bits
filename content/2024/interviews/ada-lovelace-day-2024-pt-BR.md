Title: Ada Lovelace Day 2024 - Entrevista com algumas mulheres do Debian
Slug: ada-lovelace-day-2024-interview-with-some-women-in-debian
Date: 2024-10-21 00:01
Author: Anupa Ann Joseph
Tags: debian, interviews, women, ada lovelace day, debian women, diversity, outreach
Lang: pt-BR
Status: published
Translator: Paulo Henrique de Lima Santana (phls)

![Alt Ada Lovelace portrait](|static|/images/ada_lovelace.png)

*O [Ada Lovelace Day](https://en.wikipedia.org/wiki/Ada_Lovelace_Day) foi
comemorado no dia 8 de outubro de 2024, e nesta ocasião, para celebrar e
conscientizar sobre as contribuições das mulheres para os campos de Ciência,
Tecnologia, Engenharia e Matemática (STEM - Science, Technology,
Engineering, and Mathematics), entrevistamos algumas mulheres do Debian.*

*Aqui compartilhamos seus pensamentos, comentários e preocupações com a
esperança de inspirar mais mulheres a se tornarem parte das Ciências e, claro,
a trabalhar dentro do Debian.*

Este artigo foi enviado simultaneamente para a lista de discussão
[debian-women](https://lists.debian.org/debian-women/2024/10/msg00000.html)

### *Beatrice Torracca*

**1. Quem é você?**

Eu sou Beatrice, sou italiana. Tecnologia da Internet e tudo relacionado a
computadores é apenas um hobby para mim, não minha linha de trabalho ou o
assunto dos meus estudos acadêmicos. Tenho muitos interesses e muito pouco
tempo. Gostaria de fazer muitas coisas e, ao mesmo tempo, sou muito Oblomoviana
para fazer qualquer uma.

**2. Como você foi apresentada ao Debian?**

Como usuária, comecei a usar newsgroups quando tive minha primeira conexão
discada e sempre se falava sobre essa coisa estranha chamada
[Linux](https://www.linux.org). Como migrar do DOS para o Windows foi um choque
para mim, me senti como se tivesse perdido o controle da minha máquina, tentei o
Linux com [Debian Potato](https://www.debian.org/releases/potato/) e nunca mais
fiquei longe do Debian desde então para meu equipamento pessoal.

**3. Há quanto tempo você está no Debian?**

Defina "no". Como usuária... desde o Potato, são muitos anos para contar. Como
colaboradora, um tempo similar, desde o início de 2000, eu acho. Meu primeiro
e-mail arquivado sobre contribuir para a tradução da descrição dos pacotes
Debian data de 2001.

**4. Você usa Debian no seu dia a dia? Se sim, como?**

Sim!! Eu uso o testing. Eu o tenho no meu PC desktop em casa e no meu laptop.
O desktop é onde eu tenho um servidor IMAP local que busca todos os e-mails das
minhas contas de e-mail e onde eu sincronizo e faço backup de todos os meus
dados. Em ambos eu faço coisas do dia a dia (de e-mail a banco on-line, de
compras a impostos), todas as formas de entretenimento, um pouco de trabalho se
eu tiver que trabalhar de casa ([GNU R](https://www.r-project.org/) para
estatística, [LibreOffice](https://www.libreoffice.org/)... os suspeitos de
sempre). No trabalho, sou obrigada a ter outro sistema operacional,
infelizmente, mas estou trabalhando na configuração de um sistema
[Debian Live](https://www.debian.org/devel/debian-live) para usar lá também.
Além disso, se no trabalho começarmos a fazer bioinformática, pode haver uma
máquina Linux em nosso futuro... Claro que vou sugerir e torcer por um sistema
Debian.

**5. Você tem alguma sugestão para melhorar a participação das mulheres no Debian?**

Essa é difícil. Não tenho certeza. Talvez, mais visibilidade para as mulheres
que já estão no Projeto Debian e fazer com que as recém-chegadas se sintam
vistas, valorizadas e bem-vindas. Um ambiente respeitoso e seguro também é
essencial, é claro, mas acho que o Debian fez um grande progresso nesse aspecto
com o [Código de Conduta](https://www.debian.org/code_of_conduct). Sou uma
grande fã de promover diversidade e inclusão; sempre há espaço para melhorias.

### *Ileana Dumitrescu (ildumi)*

**1. Quem é você?**

Sou apenas uma garota no mundo que gosta de gatos e de empacotar
[Software Livre](https://www.gnu.org/philosophy/free-sw.html).

**2. Como você foi apresentada ao Debian?**

Eu estava mexendo em um computador rodando Debian alguns anos atrás, e decidi
aprender mais sobre Software Livre. Depois de uma ou duas buscas, encontrei
o grupo [Debian Women](https://www.debian.org/women).

**3. Há quanto tempo você está no Debian?**

Comecei a pensar em contribuir para o Debian em 2021. Depois de entrar em
contato com o grupo Debian Women, recebi muitas informações e conselhos úteis
sobre diferentes maneiras de contribuir, e decidi que a manutenção de pacotes
era a melhor opção para mim. Acabei me tornando uma Mantenedora do Debian em
2023, e continuo mantendo alguns pacotes no meu tempo livre.

**4. Você usa Debian no seu dia a dia? Se sim, como?**

Sim, é meu sistema operacional GNU/Linux favorito! Eu o uso para e-mail,
bate-papo, navegação, empacotamento, etc.

**5. Você tem alguma sugestão para melhorar a participação das mulheres no Debian?**

A [lista de discussão para o grupo Debian Women](https://lists.debian.org/debian-women)
pode atrair mais participação se for mais utilizada. Foi onde comecei, e
imagino que a participação aumentaria se fosse mais engajada.

### *Kathara Sasikumar (kathara)*

**1. Quem é você?**

Eu sou Kathara Sasikumar, tenho 22 anos e sou uma usuária recente do Debian que
virou Mantenedora na Índia. Eu tento me tornar uma pessoa criativa por meio de
esboços de desenhos ou tocando acordes de guitarra, mas não funciona! xD

**2. Como você foi apresentada ao Debian?**

Quando comecei a faculdade, eu era aquela aluna excessivamente entusiasmada que
se inscrevia em todos os clubes e se voluntariava para tudo que cruzava meu
caminho, assim como qualquer outro(a) calouro(a).

Mas então, a pandemia chegou e, como muitos(as), cheguei a um ponto baixo. A
depressão da COVID era real e eu estava me sentindo bem para baixo. Nessa época,
o [Clube FOSS](https://fossnss.org) da minha faculdade de repente se tornou
mais ativo. Meus/Minhas amigos(as), sabendo que eu tinha um amor por software
livre, me incentivaram a entrar para o Clube. Eles(as) achavam que isso poderia
me ajudar a levantar meu ânimo e sair da crise em que eu estava.

No começo, eu me juntei apenas por pressão dos(as) colegas(as), mas quando me
envolvi, o Clube realmente decolou. O Clube FOSS se tornou mais e mais ativo
durante a pandemia, e eu me vi passando cada vez mais tempo nele.

Um ano depois, tivemos a oportunidade de sediar uma
[MiniDebConf](https://in2022.mini.debconf.org/) em nossa faculdade. Onde conheci
muitos(as) Desenvolvedores(as) e Mantenedores(as) do Debian, participar de suas
palestras e conversar com eles(as) me deu uma perspectiva mais ampla sobre o
Debian, e eu amei a filosofia do Debian.

Naquela época, eu estava pulando de distro, mas nunca me acomodei. Eu
ocasionalmente usava o Debian, mas nunca fiquei por perto. No entanto, depois
da MiniDebConf, eu me vi usando o Debian de forma mais consistente, e ele
realmente se conectou comigo. A comunidade era incrivelmente calorosa e
acolhedora, o que fez toda a diferença.

**3. Há quanto tempo você está no Debian?**

Agora, tenho usado o Debian como meu guia diário por cerca de um ano.

**4. Você usa Debian no seu dia a dia? Se sim, como?**

Tornou-se minha distro principal, e eu a uso todos os dias para aprendizado
contínuo e trabalho em vários projetos de software com ferramentas livres e de
código aberto. Além disso, recentemente me tornei uma Mantenedora Debian (DM) e
assumi a responsabilidade de manter alguns pacotes. Estou ansiosa para
contribuir mais para a comunidade Debian 🙂

### *Rhonda D'Vine (rhonda)*

**1. Quem é você?**

Meu nome é Rhonda, meus pronomes são ela/dela, elu/delu.
Tenho 51 anos, trabalho com TI.

**2. Como você foi apresentada ao Debian?**

Eu já estava pesquisando sobre Linux por causa da universidade, primeiro foi o
[SuSE](https://www.suse.com). E as pessoas brincavam com gtk. Mas quando eles(as)
empacotaram o [GNOME](https://www.gnome.org/) e ele simplesmente nem instalou,
eu procurei alternativas. Um colega de trabalho da época me deu um CD do Debian.
Embora eu não pudesse instalar a partir dele porque o
[Slink](https://www.debian.org/releases/slink/) não reconhecia a unidade pcmcia.
Eu tive que instalá-lo por meio de disquetes, mas, fora isso, foi muito bem
feito. E o GNOME inicial estava funcionando, então nunca mais olhei para
trás. 🙂

**3. Há quanto tempo você está no Debian?**

Mesmo antes de eu estar mais envolvida, um colega me perguntou se eu poderia
ajudar a traduzir a documentação de lançamento. Essa foi minha primeira
contribuição para o Debian, para o lançamento do Slink no início de 1999. E eu
estava usando alguns outros softwares antes em meus sistemas SuSE, e eu queria
continuar a usá-los no Debian, obviamente. Então foi assim que me envolvi com o
empacotamento no Debian. Mas eu continuei ajudando com o trabalho de tradução, e
por um longo período de tempo eu era quase a única pessoa ativa na parte
alemã do site web.

**4. Você usa Debian no seu dia a dia? Se sim, como?**

Estar envolvida com o Debian foi uma grande parte da razão pela qual comecei
meus trabalhos há muito tempo. Sempre trabalhei com manutenção de sistemas
Debian (ou [Ubuntu](https://ubuntu.com/)). Particularmente, eu executo o Debian
no meu laptop, ocasionalmente trocando para o Windows no dual boot quando
(raramente) necessário.

**5. Você tem alguma sugestão para melhorar a participação das mulheres no Debian?**

Há fatores que não podemos influenciar, como o fato de muitas mulheres serem
empurradas para o trabalho de cuidado porque as estruturas patriarcais funcionam
dessa forma, e não têm tempo nem energia para investir muito em outras coisas.
Mas poderíamos aprender a apreciar melhor as contribuições menores, e não focar
tanto na quantidade de contribuições. Quando olhamos para discussões mais
longas em listas de discussão, aqueles(as) que escrevem mais e-mails na verdade
não contribuem mais para a discussão, eles(as) geralmente se repetem sem
adicionar mais substância. Ao trabalhar em nossos próprios padrões de discussão,
isso poderia criar um ambiente mais acolhedor para muitas pessoas.

### *Sophie Brun (sophieb)*

**1. Quem é você?**

Sou uma mulher francesa de 44 anos. Sou casada e tenho 2 filhos.

**2. Como você foi apresentada ao Debian?**

Em 2004, meu namorado (agora meu marido) instalou o Debian no meu computador
pessoal para me apresentar ao Debian. Eu não sabia quase nada sobre Código
Aberto. Durante meus estudos de engenharia, um professor mencionou a existência
do Linux, [Red Hat](https://www.redhat.com) em particular, mas sem dar detalhes.

Eu aprendi Debian usando e lendo (com antecedência) o [Manual do(a)
Administrador(a) Debian](https://debian-handbook.info/browse/pt-BR/stable/).

**3. Há quanto tempo você está no Debian?**

Sou usuária desde 2004. Mas só comecei a contribuir para o Debian em 2015: eu
tinha largado meu emprego e queria trabalhar em algo mais significativo. É por
isso que me juntei ao meu marido na [Freexian](https://www.freexian.com/), sua
empresa. Ao contrário da maioria das pessoas, eu acho, comecei a contribuir para
o Debian para o meu trabalho. Só me tornei Desenvolvedora Debian (DD) em 2021
sob uma leve pressão social e quando me senti confiante o suficiente.

**4. Você usa Debian no seu dia a dia? Se sim, como?**

Claro que eu uso o Debian na minha vida profissional para quase todas as
tarefas: de tarefas administrativas a empacotamento no Debian.

Eu também uso o Debian na minha vida pessoal. Eu tenho necessidades muito
básicas: [Firefox](https://www.mozilla.org/firefox),
[LibreOffice](https://www.libreoffice.org/), [GnuCash](https://www.gnucash.org/)
e [Rhythmbox](https://wiki.gnome.org/Apps/Rhythmbox) são os principais
aplicativos que eu preciso.

### *Sruthi Chandran (srud)*

**1. Quem é você?**

Feminista, bibliotecária que virou defensora do Software Livre e Desenvolvedora
Debian. Faço parte da equipe do
[Debian Outreach](https://wiki.debian.org/Outreachy) e do Comitê da
[DebConf](https://www.debconf.org/).

**2. Como você foi apresentada ao Debian?**

Fui apresentada ao mundo do Software Livre e ao Debian por meio do meu marido.
Participei de muitos eventos do Debian com ele. Durante um desses eventos, por
curiosidade, participei de uma oficina de empacotamento no Debian. Logo depois,
visitei uma comunidade tibetana na Índia e eles mencionaram que não havia uma
fonte tibetana adequada no GNU/Linux. A fonte tibetana foi meu primeiro pacote
no Debian.

**3. Há quanto tempo você está no Debian?**

Contribuo para o Debian desde 2016 e sou Desenvolvedora Debian desde 2019.

**4. Você usa Debian no seu dia a dia? Se sim, como?**

Não usei nenhuma outra distribuição no meu laptop desde que conheci o Debian.

**5. Você tem alguma sugestão para melhorar a participação das mulheres no Debian?**

Eu estava envolvida ativamente na mentoria de novatos(as) no Debian desde que
comecei a contribuir. Eu trabalho especialmente para reduzir a lacuna de gênero
dentro da comunidade Debian e do Software Livre em geral. Na minha experiência,
acredito que a visibilidade de mulheres já existentes na comunidade encorajará
mais mulheres a participar. Também acho que deveríamos reintroduzir a mentoria
por meio do grupo debian-women.

### *Tássia Camões Araújo (tassia)*

**1. Quem é você?**

Tássia Camões Araújo, uma brasileira que mora no Canadá. Sou uma aprendiz
apaixonada que tenta sair da minha zona de conforto e sempre encontrar algo
novo para aprender. Também adoro orientar as pessoas em sua jornada de
aprendizado. Mas não me considero uma geek típica. Meu desafio sempre foi não
me distrair com o próximo projeto antes de terminar o que tenho em mãos. Dito
isso, adoro fazer parte de uma comunidade de geeks e me sinto empoderada por
isso. Adoro o Debian por sua excelência técnica, e é sempre reconfortante saber
que alguém está cuidando das coisas que não gosto ou não consigo fazer. Quando
não estou perto de computadores, uma das minhas coisas favoritas é sentir o
vento no meu rosto, geralmente enquanto ando de skate ou de bicicleta; também
adoro música, e estou sempre cantando uma melodia na minha cabeça.

**2. Como você foi apresentada ao Debian?**

Como estudante, tive o privilégio de ser apresentada ao FLOSS ao mesmo tempo em
que fui apresentada à programação de computadores. Minha universidade não
tinha condições de ter laboratórios no modelo de software proprietário usual, e
o que parecia uma limitação na época acabou sendo uma grande oportunidade de
aprendizado para mim e meus/minhas colegas. Entrei para essa iniciativa
liderada por estudantes para "liberar" nossos servidores e construir
laboratórios baseados em LTSP - onde um único computador poderoso poderia
alimentar algumas dezenas de thin clients sem disco. Quão revolucionário isso
foi na época! E que conquista! De estudantes para estudantes, todos(as) usando
Debian. A maioria desse grupo se tornou amigos(as) próximos(as); me casei com
um deles, e alguns(mas) deles(as) também encontraram seu caminho para o Debian.

**3. Há quanto tempo você está no Debian?**

Usei o Debian pela primeira vez em 2001, mas minha primeira conexão real com a
comunidade foi participando da DebConf 2004. Desde então, ir à DebConfs se
tornou um hábito. É aquele momento do ano em que me reconecto com a comunidade
global e minha motivação para contribuir é impulsionada. E você sabe, em 20
anos, vi pessoas se tornarem pais/mães, avôs/avós, filhos(as) crescerem;
tivemos nosso próprio filho e tivemos o prazer de apresentá-lo à comunidade;
lamentamos a perda de amigos(as) e nos curamos juntos. Eu diria que o Debian é
como uma família, mas não do tipo que você ganha aleatoriamente quando nasce, o
Debian é minha família por escolha.

**4. Você usa Debian no seu dia a dia? Se sim, como?**

Hoje em dia, leciono no Vanier College em Montreal. Meu curso favorito para
lecionar é UNIX, que tenho o prazer de lecionar principalmente usando Debian.
Tento inspirar meus/minhas alunos(as) a descobrir o Debian e outros projetos
FLOSS, e estamos felizes em administrar um clube FLOSS com a participação de
alunos(as), funcionários(as) e ex-alunos(as). Adoro ver essas mentes jovens e
curiosas colocadas a serviço do FLOSS. É como recrutar soldados(as) para uma boa
batalha, e uma que pode mudar suas vidas, como certamente mudou a minha.

**5. Você tem alguma sugestão para melhorar a participação das mulheres no Debian?**

Acho que a maneira mais eficaz de inspirar outras mulheres é dar visibilidade a
mulheres ativas em nossa comunidade. Falar em conferências, publicar conteúdo,
ser vocal sobre o que fazemos para que outras mulheres possam nos ver e se ver
nessas posições no futuro. Não é fácil, e não gosto de estar sob os holofotes.
Levei muito tempo para me sentir confortável em falar em público, então posso
entender a luta daquelas que não querem se expor. Mas acredito que esse espaço
de vulnerabilidade pode abrir caminho para novas conexões. Pode inspirar
confiança e, finalmente, motivar nossa próxima geração. É com isso em mente que
publico estas linhas.

Outro ponto que não podemos negligenciar é que no Debian trabalhamos como
voluntários(as), e isso por si só nos coloca em grande desvantagem. Em nossas
sociedades, as mulheres geralmente assumem uma carga mais pesada do que seus
parceiros em termos de cuidados e outras tarefas invisíveis, então é difícil
conseguir o tempo livre necessário para ser voluntária. Esta é uma das razões
pelas quais levo meu filho para as conferências que participo, e até agora
recebi todo o apoio que preciso para ir às DebConfs com ele. É uma maneira de
compartilhar o fardo de cuidar com nossa comunidade - é preciso uma aldeia
para criar uma criança. Além de nos permitir participar, também serve para
mostrar a outras mulheres (e homens) que você pode ter uma,vida familiar e ainda
contribuir para o Debian.

Minha sensação é que não estamos indo muito bem em termos de diversidade no
Debian no momento, mas isso não deve nos desencorajar nem um pouco. É assim que
é agora, mas isso não significa que sempre será assim. Sinto que passamos por
ciclos. Lembro de momentos em que tínhamos muito mais colaboradoras mulheres
ativas, e estou confiante de que podemos melhorar nossa proporção novamente no
futuro. Enquanto isso, apenas tento continuar, fazer minha parte, atrair
aquelas que posso, tranquilizar aquelas que estão com muito medo de se
aproximar. O Debian é uma comunidade maravilhosa, é uma família, e é claro que
uma família não pode viver sem nós, as mulheres.

*Estas entrevistas foram conduzidas por meio de trocas de e-mail em outubro
de 2024. Obrigada a todas as mulheres maravilhosas que participaram desta
entrevista. Nós realmente apreciamos suas contribuições no Debian e no
Software Livre.*
