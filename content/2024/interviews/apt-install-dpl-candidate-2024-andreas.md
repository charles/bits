Title: apt install dpl-candidate: Andreas Tille
Slug: dpl-interview-AndresTille
Date: 2024-04-05 20:36
Author: Yashraj Moghe with The Debian Publicity Team
Tags: interviews, dpl, vote, meetDDs
Status: published

The Debian Project Developers will shortly vote for a new Debian Project Leader
known as the DPL.

The Project Leader is the official representative of The Debian Project tasked with
managing the overall project, its vision, direction, and finances.

The DPL is also responsible for the selection of Delegates, defining areas of
responsibility within the project, the coordination of Developers, and making
decisions required for the project.

Our outgoing and present DPL Jonathan Carter served 4 terms, from 2020
through 2024. Jonathan shared his last [Bits from the DPL](https://bits.debian.org/2024/04/bits-from-the-dpl-april.html)
post to Debian recently and his hopes for the future of Debian.

Recently, we sat with the two present candidates for the DPL position asking
questions to find out  who they really are in a series of interviews about
their platforms, visions for Debian, lives, and even their favorite text
editors. The interviews were conducted by disaster2life (Yashraj Moghe) and
made available from video and audio transcriptions:

* Andreas Tille [this document]
* Sruthi Chandran [[Interview]](https://bits.debian.org/2024/04/dpl-interview-SruthiChandran.html)

Voting for the position starts on April 6, 2024.

_Editors' note:
 This is our official return to Debian interviews, readers should stay tuned
 for more upcoming interviews with Developers and other important figures in
 Debian as part of our "Meet your Debian Developer" series. We used the
 following tools and services: [Turboscribe.ai](https://turboscribe.ai) for the
 transcription from the audio and video files, [IRC:
 Oftc.net](https://www.oftc.net) for communication, [Jitsi
 meet](https://meet.jit.si/) for interviews,  and [Open Broadcaster Software
 (OBS)](https://obsproject.com/) for editing and video. While we encountered
 many technical difficulties in the return to this process, we are still able
 and proud to present the transcripts of the
interviews edited only in a few areas for readability._

**2024 Debian Project Leader Candidate: Andrea Tille**

## Andreas' Interview

**Who are you? Tell us a little about yourself.**

[Andreas]:
> How am I? Well, I'm, as I wrote in my platform, I'm a proud grandfather doing a
lot of free software stuff, doing a lot of sports, have some goals in mind which
I like to do and hopefully for the best of Debian.

**And How are you today?**

[Andreas]:
> How I'm doing today? Well, actually I have some headaches but it's fine for the
interview.
>
> So, usually I feel very good. Spring was coming here and today it's raining and
I plan to do a bicycle tour tomorrow and hope that I do not get really sick but
yeah, for the interview it's fine.

**What do you do in Debian? Could you mention your story here?**

[Andreas]:
> Yeah, well, I started with Debian kind of an accident because I wanted to have
some package salvaged which is called WordNet. It's a monolingual dictionary and
I did not really plan to do more than maybe 10 packages or so. I had some kind
of training with xTeddy which is totally unimportant, a cute teddy you can put
on your desktop.
>
> So, and then well, more or less I thought how can I make Debian attractive for
my employer which is a medical institute and so on.  It could make sense to
package bioinformatics and medicine software and it somehow evolved in a
direction I did neither expect it nor wanted to do, that I'm currently the most
busy uploader in Debian, created several teams around it.
>
> DebianMate is very well known from me. I created the Blends team to create teams
and techniques around what we are doing which was Debian TIS, Debian Edu, Debian
Science and so on and I also created the packaging team for R, for the
statistics package R which is technically based and not topic based. All these
blends are covering a certain topic and R is just needed by lots of these
blends.
>
> So, yeah, and to cope with all this I have written a script which is routing an
update to manage all these uploads more or less automatically. So, I think I had
one day where I uploaded 21 new packages but it's just automatically generated,
right? So, it's on one day more than I ever planned to do.

**What is the first thing you think of when you think of Debian?**

_Editors' note: The question was misunderstood as the “worst thing you think of
when you think of Debian”_

[Andreas]:
> The worst thing I think about Debian, it's complicated. I think today on Debian
board I was asked about the technical progress I want to make and in my opinion
we need to standardize things inside Debian. For instance, bringing all the
packages to salsa, follow some common standards, some common workflow which is
extremely helpful.
>
> As I said, if I'm that productive with my own packages we can adopt this in
general, at least in most cases I think. I made a lot of good experience by the
support of well-formed teams. Well-formed teams are those teams where people
support each other, help each other.
>
> For instance, how to say, I'm a physicist by profession so I'm not an IT expert.
I can tell apart what works and what not but I'm not an expert in those
packages. I do and the amount of packages is so high that I do not even
understand all the techniques they are covering like Go, Rust and something like
this.
>
> And I also don't speak Java and I had a problem once in the middle of the night
and I've sent the email to the list and was a Java problem and I woke up in the
morning and it was solved. This is what I call a team. I don't call a team some
common repository that is used by random people for different packages also but
it's working together, don't hesitate to solve other people's problems and
permit people to get active.
>
> This is what I call a team and this is also something I observed in, it's hard
to give a percentage, in a lot of other teams but we have other people who do
not even understand the concept of the team. Why is working together make some
advantage and this is also a tough thing. I [would] like to tackle in my term if
I get elected to form solid teams using the common workflow. This is one thing.
>
> The other thing is that we have a lot of good people in our infrastructure like
FTP masters, DSA and so on. I have the feeling they have a lot of work and are
working more or less on their limits, and I like to talk to them [to ask] what
kind of change we could do to move that limits or move their personal health to
the better side.

**The DPL term lasts for a year, What would you do during that you couldn't do now?**

[Andreas]:
> Yeah, well this is basically what I said are my main issues. I need to admit I
have no really clear imagination what kind of tasks will come to me as a DPL
because all these financial issues and law issues possible and issues [that]
people who are not really friendly to Debian might create. I'm afraid these
things might occupy a lot of time and I can't say much about this because I
simply don't know.

**What are three key terms about you and your candidacy?**

[Andreas]:
> As I said, I like to work on standards, I’d like to make Debian try [to get
it right so] that people don't get overworked, this third key point is be
inviting to newcomers, to everybody who wants to come. Yeah, I also mentioned in
my term this diversity issue, geographical and from gender point of view. This
may be the three points I consider most important.

**Preferred text editor?**

[Andreas]:
> Yeah, my preferred one? Ah, well, I have no preferred text editor. I'm using the
Midnight Commander very frequently which has an internal editor which is
convenient for small text. For other things, I usually use VI but I also use
Emacs from time to time. So, no, I have not preferred text editor. Whatever
works nicely for me.

**What is the importance of the community in the Debian Project? How would
like to see it evolving over the next few years?**

[Andreas]:
> Yeah, I think the community is extremely important. So, I was on a lot of
DebConfs. I think it's not really 20 but 17 or 18 DebCons and I really enjoyed
these events every year because I met so many friends and met so many
interesting people that it's really enriching my life and those who I never met
in person but have read interesting things and yeah, Debian community makes
really a part of my life.

**And how do you think it should evolve specifically?**

[Andreas]:
> Yeah, for instance, last year in Kochi, it became even clearer to me that the
geographical diversity is a really strong point. Just discussing with some women
from India who is afraid about not coming next year to Busan because there's a
problem with Shanghai and so on. I'm not really sure how we can solve this but I
think this is a problem at least I wish to tackle and yeah, this is an
interesting point, the geographical diversity and I'm running the so-called
mentoring of the month.
>
> This is a small project to attract newcomers for the Debian Med team which has
the focus on medical packages and I learned that we had always men applying for
this and so I said, okay, I dropped the constraint of medical packages.
>
> Any topic is fine, I teach you packaging but it must be someone who does not
consider himself a man. I got only two applicants, no, actually, I got one
applicant and one response which was kind of strange if I'm hunting for women or
so.
>
> I did not understand but I got one response and interestingly, it was for me one
of the least expected counters. It was from Iran and I met a very nice woman,
very open, very skilled and gifted and did a good job or have even lose contact
today and maybe we need more actively approach groups that are underrepresented.
I don't know if what's a good means which I did but at least I tried and so I
try to think about these kind of things.

**What part of Debian has made you smile? What part of the project has kept
you going all through the years?**

[Andreas]:
> Well, the card game which is called Mao on the DebConf made me smile all the
time. I admit I joined only two or three times even if I really love this kind
of games but I was occupied by other stuff so this made me really smile. I also
think the first online DebConf in 2020 made me smile because we had this kind of
short video sequences and I tried to make a funny video sequence about every
DebConf I attended before. This is really funny moments but yeah, it's not only
smile but yeah.
>
> One thing maybe it's totally unconnected to Debian but I learned personally
something in Debian that we have a do-ocracy and you can do things which you
think that are right if not going in between someone else, right? So respect
everybody else but otherwise you can do so.
>
> And in 2020 I also started to take trees which are growing widely in my garden
and plant them into the woods because in our woods a lot of trees are dying and
so I just do something because I can. I have the resource to do something, take
the small tree and bring it into the woods because it does not harm anybody. I
asked the forester if it is okay, yes, yes, okay. So everybody can do so but I
think the idea to do something like this came also because of the free software
idea. You have the resources, you have the computer, you can do something and
you do something productive, right? And when thinking about this I think it was
also my Debian work.
>
> Meanwhile I have planted more than 3,000 trees so it's not a small number but
yeah, I enjoy this.

**What part of Debian would you have some criticisms for?**

[Andreas]:
> Yeah, it's basically the same as I said before. We need more standards to work
together. I do not want to repeat this but this is what I think, yeah.

**What field in Free Software generally do you think requires the most work
to be put into it? What do you think is Debian's part in the field?**

[Andreas]:
> It's also in general, the thing is the fact that I'm maintaining packages which
are usually as modern software is maintained in Git, which is fine but we have
some software which is at Sourceport, we have software laying around somewhere,
we have software where Debian somehow became Upstream because nobody is caring
anymore and free software is very different in several things, ways and well, I
in principle like freedom of choice which is the basic of all our work.
>
> Sometimes this freedom goes in the way of productivity because everybody is free
to re-implement. You asked me for the most favorite editor. In principle one
really good working editor would be great to have and would work and we have
maybe 500 in Debian or so, I don't know.
>
> I could imagine if people would concentrate and say five instead of 500 editors,
we could get more productive, right? But I know this will not happen, right? But
I think this is one thing which goes in the way of making things smooth and
productive and we could have more manpower to replace one person who's [having]
children, doing some other stuff and can't continue working on something and
maybe this is a problem I will not solve, definitely not, but which I see.

**What do you think is Debian's part in the field?**

[Andreas]:
> Yeah, well, okay, we can bring together different Upstreams, so we are building
some packages and have some general overview about similar things and can say,
oh, you are doing this and some other person is doing more or less the same, do
you want to join each other or so, but this is kind of a channel we have to our
Upstreams which is probably not very successful.
>
> It starts with code copies of some libraries which are changed a little bit,
which is fine license-wise, but not so helpful for different things and so I've
tried to convince those Upstreams to forward their patches to the original one,
but for this and I think we could do some kind of, yeah, [find] someone who
brings Upstream together or to make them stop their forking stuff, but it costs
a lot of energy and we probably don't have this and it's also not realistic that
we can really help with this problem.

**Do you have any questions for me?**

[Andreas]:
> I enjoyed the interview, I enjoyed seeing you again after half a year or so.
Yeah, actually I've seen you in the eating room or cheese and wine party or so,
I do not remember we had to really talk together, but yeah, people around, yeah,
for sure. Yeah.
</br>
</br>
</br>
