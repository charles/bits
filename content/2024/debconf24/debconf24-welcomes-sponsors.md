Title: DebConf24 welcomes its sponsors!
Slug: debconf24-welcomes-sponsors
Date: 2024-07-27 23:45
Author: The Debian Publicity Team
Tags: debconf, debconf24, sponsors
Lang: en
Image: /images/logo_win_dc24_korea.png
Status: published

![DebConf24 logo](|static|/images/logo_win_dc24_korea.png)

[DebConf24](https://debconf24.debconf.org/), the 25th edition of the Debian
conference is taking place in Pukyong National University at Busan, Republic of
Korea.
Thanks to the hard work of its organizers, it again will be an interesting and
fruitful event for attendees.

We would like to warmly welcome the sponsors of DebConf24, and introduce them to
you.

We have three Platinum sponsors.

- [**Proxmox**](https://www.proxmox.com/) is the first Platinum sponsor.
  Proxmox provides powerful and user-friendly Open Source server software.
  Enterprises of all sizes and industries use Proxmox solutions to deploy
  efficient and simplified <abbr>IT</abbr> infrastructures, minimize total cost
  of ownership, and avoid vendor lock-in.
  Proxmox also offers commercial support, training services, and an extensive
  partner ecosystem to ensure business continuity for its customers.
  Proxmox Server Solutions <abbr lang="de">GmbH</abbr> was established in 2005
  and is headquartered in Vienna, Austria.
  Proxmox builds its product offerings on top of the Debian operating system.

- Our second Platinum sponsor is [**Infomaniak**](https://www.infomaniak.com/).
  Infomaniak is an independent cloud service provider recognised throughout
  Europe for its commitment to privacy, the local economy and the environment.
  Recording growth of 18% in 2023, the company is developing a suite of online
  collaborative tools and cloud hosting, streaming, marketing and events
  solutions.
  Infomaniak uses exclusively renewable energy, builds its own data centers and
  develops its solutions in Switzerland, without relocating.
  The company powers the website of the Belgian radio and <abbr>TV</abbr>
  service (<abbr>RTBF</abbr>) and provides streaming for more than 3,000
  <abbr>TV</abbr> and radio stations in Europe.

- [**Wind River**](https://www.windriver.com/) is our third Platinum sponsor.
  For nearly 20 years, Wind River has led in commercial Open Source Linux
  solutions for mission-critical enterprise edge computing.
  With expertise across aerospace, automotive, industrial, telecom, and more,
  the company is committed to Open Source through initiatives like eLxr, Yocto,
  Zephyr, and StarlingX.

Our Gold sponsors are:

- [**Ubuntu**](https://www.canonical.com/),
  the Operating System delivered by Canonical.

- [**Freexian**](https://www.freexian.com/),
  a services company specialized in Free Software and in particular Debian
  GNU/Linux, covering consulting, custom developments, support and training.
  Freexian has a recognized Debian expertise thanks to the participation of
  Debian developers.

- [**Lenovo**](https://www.lenovo.com/),
  a global technology leader manufacturing a wide portfolio of connected
  products including smartphones, tablets, PCs and workstations as well as
  AR/VR devices, smart home/office and data center solutions.

- [**Korea Tourism Organization**](https://knto.or.kr/eng/index),
  which purpose is to advance tourism as a key driver for national economic
  growth and enhancement of national welfare and intends to be a public
  organization that makes the Korean people happier; it promotes national wealth
  through tourism.

- [**Busan <abbr>IT</abbr> Industry Promotion Agency**](http://www.busanit.or.kr/),
  an industry promotion organization that contributes to the innovation of the
  digital economy with the power of <abbr>IT</abbr> and <abbr>CT</abbr> and
  supports the ecosystem for innovative local startups and companies to grow.

- [**Microsoft**](https://www.microsoft.com/),
  who enables digital transformation for the era of an intelligent cloud and an
  intelligent edge.
  Its mission is to empower every person and every organization on the planet to
  achieve more.

- [**doubleO**](https://doubleo.co.kr/en/main/),
  a company that specializes in consulting and developing empirical services
  using big data analysis and artificial intelligence.
  doubleO provides a variety of data-centered services together with small and
  medium-sized businesses in Busan/Gyeongnam.

Our Silver sponsors are:

- [**Roche**](https://www.roche.com/), a major international pharmaceutical
  provider and research company dedicated to personalized healthcare.
- [**Two Sigma**](https://www.twosigma.com/), rigorous inquiry, technology
  data science, and invention to bring science to finance and help solve the
  toughest challenges across financial services.
- [**Arm**](https://www.arm.com/): leading technology provider of processor
  <abbr>IP</abbr>, Arm powered solutions have been supporting innovation for
  more than 30 years and are deployed in over 280 billion chips to date.
- The [**Bern University of Applied Sciences**](https://www.bfh.ch/) with around
  [7,800](https://www.bfh.ch/en/about-bfh/facts-and-figures/facts-figures/)
  students enrolled, located in the Swiss capital.
- [**Google**](https://google.com/), one of the largest technology companies in
  the world, providing a wide range of Internet-related services and products
  such as online advertising technologies, search, cloud computing, software,
  and hardware.
- [**<abbr>FSIJ</abbr>**](https://www.fsij.org/), the Free Software Initiative
  of Japan, a non-profit organization dedicated to supporting Free Software
  growth and development.
- [**Busan Tourism Organisation**](https://www.visitbusan.net/en/index.do):
  leading public corporation that generates social and economic values in Busan
  tourism industry, developing tourism resources in accordance with government
  policies and invigorate tourism industry.
- [**Civil Infrastructure Platform**](https://www.cip-project.org/),
  a collaborative project hosted by the Linux Foundation,
  establishing an open source “base layer” of industrial grade software.
- [**Collabora**](https://www.collabora.com/), a global consultancy delivering
  Open Source software solutions to the commercial world.
- [**Matanel Foundation**](http://www.matanel.org/), which operates in Israel,
  as its first concern is to preserve the cohesion of a society and a nation
  plagued by divisions.

Bronze sponsors:

- [**Altus Metrum**](https://altusmetrum.org/),
- [**Loongson**](https://www.loongson.cn/EN),
- [**NIPA**](https://www.nipa.kr/eng/index),
- [**IPA**](https://koipa.or.kr/),

And finally, our Supporter level sponsors:

- [**evolix**](https://evolix.com/),
- [**ISG**](https://isg.ee.ethz.ch/),
- [**Linux Professional Institute**](https://www.lpi.org/).

A special thanks to the
[**Pukyong National University**](https://www.pknu.ac.kr/main), our Venue
Partner and our Network Partners [**KOREN**](https://www.koren.kr/eng/index.asp)
and [**KREONET**](https://www.kreonet.net)!

Thanks to all our sponsors for their support!
Their contributions make it possible for a large number of Debian contributors
from all over the globe to work together, help and learn from each other in
DebConf24.
