Title: DebConf24 starts today in Busan on Sunday, July 28, 2024
Slug: debconf24-starts-today
Date: 2024-07-27 23:50
Author: The Debian Publicity Team
Tags: debconf, debconf24
Lang: en
Artwork: Debian
Status: published

[DebConf24](https://debconf24.debconf.org/), the 25th annual
[Debian Developer Conference](https://www.debconf.org/), is taking place in
Busan, Republic of Korea from July 28th to August 4th, 2024.
Debian contributors from all over the world have come together at Pukyong
National University, Busan, to participate and work in a conference exclusively
ran by volunteers.

Today the main conference starts with around 340 expected attendants and over 100
scheduled activities, including 45-minute and 20-minute talks, Bird of a Feather
("<abbr>BoF</abbr>") team meetings, workshops, a job fair, as well as a variety
of other events.
The full [schedule](https://debconf24.debconf.org/schedule/) is updated each
day, including activities planned ad-hoc by attendees over the course of the
conference.

If you would like to engage remotely, you can follow the **video streams**
available from the [DebConf24 website](https://debconf24.debconf.org/) for the
events happening in the three talk rooms: _Bada_, _Somin_ and _Pado_.
Or you can join the conversations happening inside the talk rooms via the
[OFTC IRC network](https://www.oftc.net/) in the
[#debconf-bada](irc://irc.debian.org/debconf-bada),
[#debconf-somin](irc://irc.debian.org/debconf-somin), and
[#debconf-pado](irc://irc.debian.org/debconf-pado) channels.
Please also join us in the [#debconf](irc://irc.debian.org/debconf) channel for
common discussions related to DebConf.

You can also follow the live coverage of news about DebConf24 provided by our
[micronews service](https://micronews.debian.org/) or the @debian profile on
your favorite social network.

DebConf is committed to a safe and welcoming environment for all participants.
Please see our [Code of Conduct page](https://debconf24.debconf.org/about/coc/)
for more information on this.

Debian thanks the commitment of numerous sponsors to support DebConf24,
particularly our Platinum Sponsors: **Proxmox**, **Infomaniak** and
**Wind River**.

![DebConf24 logo](|static|/images/debconf24-sponsors-banner_1_800x450.png)
