Title: Proxmox Platinum Sponsor of DebConf24
Slug: proxmox-platinum-debconf24
Date: 2024-04-04 01:17
Author: Sahil Dhiman
Artist: Proxmox
Tags: debconf24, debconf, sponsors, proxmox
Image: /images/proxmox.png
Status: published

[![proxmoxlogo](|static|/images/proxmox.png)](https://www.proxmox.com/)

We are pleased to announce that [**Proxmox**](https://www.proxmox.com/)
has committed to sponsor [DebConf24](https://debconf24.debconf.org) as a
**Platinum Sponsor**.

Proxmox  provides powerful and user-friendly open-source server software.
Enterprises of all sizes and industries use Proxmox solutions to deploy
efficient and simplified IT infrastructures, minimize total cost of ownership,
and avoid vendor lock-in. Proxmox also offers commercial support, training
services, and an extensive partner ecosystem to ensure business continuity
for its customers. Proxmox Server Solutions GmbH was established in 2005 and is
headquartered in Vienna, Austria.

_Proxmox builds its product offerings on top of the Debian operating system._

With this commitment as Platinum Sponsor, Proxmox is contributing to make
possible our annual conference, and directly supporting the progress of Debian
and Free Software, helping to strengthen the community that continues to
collaborate on Debian projects throughout the rest of the year.

Thank you very much, Proxmox, for your support of DebConf24!

## Become a sponsor too!

DebConf24 will take place from 28th July to 4th August 2024 in Busan,
South Korea, and will be preceded by DebCamp, from 21st to 27th July 2024.

DebConf24 is accepting sponsors! Interested companies and organizations may
contact the DebConf team through
[sponsors@debconf.org](mailto:sponsors@debconf.org), or visit the [Become a
DebConf Sponsor](https://debconf24.debconf.org/sponsors/become-a-sponsor)
website.
