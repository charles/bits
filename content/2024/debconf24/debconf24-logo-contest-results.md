Title: DebConf24 Logo Contest Results
Slug: debconf24-logo-contest-results
Date: 2024-02-08 06:00
Author: Donald Norwood
Tags: debconf24, logos, contests, south korea, busan, debconf, artwork
Lang: en
Image: /images/logo_win_dc24_skorea.png
Artist: Woohee Yang
Status: published

Earlier this month the DebConf team announced the [DebConf24 Logo
Contest](https://lists.debian.org/debconf-discuss/2024/01/msg00000.html)
asking aspiring artists, designers, and contributors to submit an image that
would represent the host city of Busan, the host nation of South Korea, and
promote the next [Debian Developer Conference](https://www.debconf.org).

The logo contest for [DebConf24](https://debconf24.debconf.org) received 10
submissions and garnered 354 responses with 3 proposals in particular getting
very close to first place. The winning logo received 88 votes, the 2nd favored
logo received 87 votes, and the 3rd most favored received 86
[votes](https://app.formbricks.com/share/73jH5p5Ffkt3BmOBUqaj/summary).

Thank you to Woohee Yang and Junsang Moon for sharing their artistic visions.

A very special Thank You to everyone who took the time to vote for our beautiful
new logo!

The DebConf24 Team is proud to
[share](https://lists.debian.org/debconf-team/2024/02/msg00005.html) for
**preview only** the winning logo for the 24th Debian Developer Conference:

![[DebConf24 Logo Contest Winner]](|static|/images/logo_win_dc24_skorea.png)

'_sun-seagull-sea_' by Woohee Yang

This is a preview copy, other revisions will occur for sizing, print, and
media...  but we had to share it with you all now. :).

Looking forward to seeing you all at #debconf24 in #Busan, South Korea 2024!
