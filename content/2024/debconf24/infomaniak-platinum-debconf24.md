Title: Infomaniak Platinum Sponsor of DebConf24
Slug: infomaniak-platinum-debconf24
Date: 2024-05-01 12:08
Author: Sahil Dhiman
Artist: Infomaniak
Tags: debconf24, debconf, sponsors, infomaniak
Image: /images/infomaniak.png
Status: published

[![infomaniaklogo](|static|/images/infomaniak.png)](https://www.infomaniak.com/)

We are pleased to announce that
**[Infomaniak](https://www.infomaniak.com)** has committed to sponsor
[DebConf24](https://debconf24.debconf.org/) as a **Platinum Sponsor**.

Infomaniak is an independent cloud service provider recognised
throughout Europe for its commitment to privacy, the local economy and
the environment. Recording growth of 18% in 2023, the company is
developing a suite of online collaborative tools and cloud hosting,
streaming, marketing and events solutions.

Infomaniak uses exclusively renewable energy, builds its own data
centers and develops its solutions in Switzerland at the heart of
Europe, without relocating. The company powers the website of the
Belgian radio and TV service (RTBF) and provides streaming for more than
3,000 TV and radio stations in Europe.

With this commitment as Platinum Sponsor, Infomaniak is contributing to
the Debian annual Developers' conference, directly supporting the
progress of Debian and Free Software. Infomaniak contributes to
strengthen the community that collaborates on Debian projects from all
around the world throughout all of the year.

Thank you very much, Infomaniak, for your support of DebConf24!

## Become a sponsor too!

[DebConf24](https://debconf24.debconf.org/) will take place from 28th
July to 4th August 2024 in Busan, South Korea, and will be preceded by
DebCamp, from 21st to 27th July 2024.

DebConf24 is accepting sponsors! Interested companies and organizations should
contact the DebConf team through
[sponsors@debconf.org](mailto:sponsors@debconf.org), or viisit the DebConf24
website at
[https://debconf24.debconf.org/sponsors/become-a-sponsor/](https://debconf24.debconf.org/sponsors/become-a-sponsor/).
