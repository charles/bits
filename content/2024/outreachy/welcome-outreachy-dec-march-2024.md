Title: Debian welcomes its new Outreachy interns
Slug: welcome-outreachy-interns-2024
Date: 2024-11-29 10:00
Author: Nilesh Patra
Tags: announce, outreachy
Artist: Outreachy
Image: outreachy-logo-300x61.png
Status: published

![Outreachy logo](|static|/images/outreachy-logo-300x61.png)

Debian continues participating in Outreachy, and we're excited to announce that
Debian has selected two interns for the Outreachy December 2024 - March 2025 round.

[Patrick Noblet Appiah](https://noblet.hashnode.dev/) will work on Automatic Indi-3rd-party driver update,
mentored by Thorsten Alteholz.

[Divine Attah-Ohiemi](https://github.com/Death-thekidd) will work on Making the Debian main website more attractive by switching to HuGo as site generator,
mentored by Carsten Schoenert, Subin Siby and Thomas Lange.

----

Congratulations and welcome Patrick Noblet Appiah and Divine Attah-Ohiemi!

From [the official website](https://www.outreachy.org/): *Outreachy provides
three-month internships for people from groups traditionally underrepresented
in tech. Interns work remotely with mentors from Free and Open Source Software
(FOSS) communities on projects ranging from programming, user experience,
documentation, illustration and graphical design, to data science.*

The Outreachy programme is possible in Debian thanks to the efforts of Debian
developers and contributors who dedicate their free time to mentor students and
outreach tasks, and the
[Software Freedom Conservancy](https://sfconservancy.org/)'s administrative
support, as well as the continued support of Debian's donors, who provide
funding for the internships.

Join us and help extend Debian! You can follow the work of the Outreachy
interns reading their blogs (they are syndicated in [Planet Debian][planet]),
and chat with us in the #debian-outreach IRC channel and
[mailing list](https://lists.debian.org/debian-outreach/).

[planet]: https://planet.debian.org
