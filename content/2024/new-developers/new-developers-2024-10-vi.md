Title: Những nhà Phát triển và Bảo trì Debian mới (Tháng chín và mười 2024)
Slug: new-developers-2024-10
Date: 2024-11-28 18:00
Author: Jean-Pierre Giraud
Tags: project
Lang: vi
Translator: Trần Ngọc Quân
Status: published

Trong hai tháng qua, những người đóng góp sau đây nhận được tài khoản Phát
triển Debian:

* Joachim Bauch (fancycode)
* Alexander Kjäll (capitol)
* Jan Mojžíš (janmojzis)
* Xiao Sheng Wen (atzlinux)

Trong hai tháng qua, những người đóng góp sau đây được thêm vào với tư cách là
người Bảo trì Debian:

* Alberto Bertogli
* Alexis Murzeau
* David Heilderberg
* Xiyue Deng
* Kathara Sasikumar
* Philippe Swartvagher

Xin chúc mừng!
