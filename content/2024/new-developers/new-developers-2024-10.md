Title: New Debian Developers and Maintainers (September and October 2024)
Slug: new-developers-2024-10
Date: 2024-11-28 18:00
Author: Jean-Pierre Giraud
Tags: project
Lang: en
Status: published

The following contributors got their Debian Developer accounts in the last two
months:

* Joachim Bauch (fancycode)
* Alexander Kjäll (capitol)
* Jan Mojžíš (janmojzis)
* Xiao Sheng Wen (atzlinux)

The following contributors were added as Debian Maintainers in the last two
months:

* Alberto Bertogli
* Alexis Murzeau
* David Heilderberg
* Xiyue Deng
* Kathara Sasikumar
* Philippe Swartvagher

Congratulations!
