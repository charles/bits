Title: नए डेबियन डेवलपर्स और मेंटेनर्स (मार्च और अप्रैल 2024)
Slug: new-developers-2024-04
Date: 2024-05-31 18:00
Author: Jean-Pierre Giraud
Tags: project
Lang: hi-IN
Translator: Yashraj Moghe
Status: published

पिछले दो महीनों में निम्नलिखित सहायकों को उनका डेबियन डेवलपर अकाउंट मिला:

* Patrick Winnertz (winnie)
* Fabian Gruenbichler (fabiang)

और निम्नलिखित को डेबियन मेंटेनर का स्थान दिया गया हैं:

* Juri Grabowski
* Tobias Heider
* Jean Charles Delépine
* Guilherme Puida Moreira
* Antoine Le Gonidec
* Arthur Barbosa Diniz

इन्हें बधाईयाँ!
