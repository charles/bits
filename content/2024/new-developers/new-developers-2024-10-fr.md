Title: Nouveaux développeurs et mainteneurs de Debian (septembre et octobre 2024)
Slug: new-developers-2024-10
Date: 2024-11-28 18:00
Author: Jean-Pierre Giraud
Tags: project
Lang: fr
Status: published

Les contributeurs suivants sont devenus développeurs Debian ces deux derniers
mois :

* Joachim Bauch (fancycode)
* Alexander Kjäll (capitol)
* Jan Mojžíš (janmojzis)
* Xiao Sheng Wen (atzlinux)

et ces contributeurs ont été acceptés comme mainteneurs Debian durant la même
période :

* Alberto Bertogli
* Alexis Murzeau
* David Heilderberg
* Xiyue Deng
* Kathara Sasikumar
* Philippe Swartvagher

Félicitations !
