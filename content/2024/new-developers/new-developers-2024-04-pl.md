Title: Nowi Deweloperzy i Opiekunowie Debiana (marzec i kwiecień 2024)
Slug: new-developers-2024-04
Date: 2024-05-31 18:00
Author: Jean-Pierre Giraud
Tags: project
Lang: pl
Translator: Grzegorz Szymaszek
Status: published

Następujący współtwórcy otrzymali konta Deweloperów Debiana w&nbsp;ciągu
ostatnich dwóch miesięcy:

* <span lang="de">Patrick Winnertz</span> (winnie)
* <span lang="de">Fabian Gruenbichler</span> (fabiang)

Następujący współtwórcy dołączyli do Opiekunów Debiana w&nbsp;ciągu ostatnich
dwóch miesięcy:

* <span lang="de">Juri Grabowski</span>
* <span lang="de">Tobias Heider</span>
* <span lang="fr">Jean Charles Delépine</span>
* <span lang="pt">Guilherme Puida Moreira</span>
* <span lang="fr">Antoine Le Gonidec</span>
* <span lang="pt">Arthur Barbosa Diniz</span>

Gratulacje!
