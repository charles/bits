Title: Những nhà Phát triển và Bảo trì Debian mới (Tháng ba và tư 2024)
Slug: new-developers-2024-04
Date: 2024-05-31 18:00
Author: Jean-Pierre Giraud
Tags: project
Lang: vi
Translator: Trần Ngọc Quân
Status: published

Trong hai tháng qua, những người đóng góp sau đây nhận được tài khoản Phát
triển Debian:

* Patrick Winnertz (winnie)
* Fabian Gruenbichler (fabiang)

Trong hai tháng qua, những người đóng góp sau đây được thêm vào với tư cách là
người Bảo trì Debian:

* Juri Grabowski
* Tobias Heider
* Jean Charles Delépine
* Guilherme Puida Moreira
* Antoine Le Gonidec
* Arthur Barbosa Diniz

Xin chúc mừng!
