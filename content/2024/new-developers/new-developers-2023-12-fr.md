Title: Nouveaux développeurs et mainteneurs de Debian (novembre et décembre 2023)
Slug: new-developers-2023-12
Date: 2024-01-31 16:00
Author: Donald Norwood
Tags: project
Lang: fr
Translator:
Status: published

Les contributeurs suivants sont devenus développeurs Debian ces deux derniers
mois :

* Alexandre Detiste (tchet)
* Amin Bandali (bandali)
* Jean-Pierre Giraud (jipege)
* Timthy Pearson (tpearson)

et ce contributeur a été accepté comme mainteneur Debian durant la même
période :

* Safir Secerovic

Félicitations !
