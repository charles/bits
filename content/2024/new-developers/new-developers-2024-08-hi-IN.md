Title: नए डेबियन डेवलपर्स और मेंटेनर्स (जुलाई और अगस्त 2024)
Slug: new-developers-2024-08
Date: 2024-09-30 16:30
Author: Jean-Pierre Giraud
Tags: project
Lang: hi-IN
Translator: Yashraj Moghe
Status: published

पिछले दो महीनों में निम्नलिखित सहायकों को उनका डेबियन डेवलपर अकाउंट मिला:

* Carlos Henrique Lima Melara (charles)
* Joenio Marques da Costa (joenio)
* Blair Noctis (ncts)

और निम्नलिखित को डेबियन मेंटेनर का स्थान दिया गया हैं:

* Taihsiang Ho

इन्हें बधाईयाँ!
