Title: New Debian Developers and Maintainers (July and August 2024)
Slug: new-developers-2024-08
Date: 2024-09-30 16:30
Author: Jean-Pierre Giraud
Tags: project
Lang: en
Status: published

The following contributors got their Debian Developer accounts in the last two
months:

* Carlos Henrique Lima Melara (charles)
* Joenio Marques da Costa (joenio)
* Blair Noctis (ncts)

The following contributors were added as Debian Maintainers in the last two
months:

* Taihsiang Ho

Congratulations!
