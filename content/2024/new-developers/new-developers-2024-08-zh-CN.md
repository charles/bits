Title: 新的 Debian 开发者和维护者 (2024年7月 至 8月)
Slug: new-developers-2024-08
Date: 2024-09-30 16:30
Author: Jean-Pierre Giraud
Tags: project
Lang: zh-CN
Translator: Boyuan Yang
Status: published

下列贡献者在过去的两个月中获得了他们的 Debian 开发者帐号：

* Carlos Henrique Lima Melara (charles)
* Joenio Marques da Costa (joenio)
* Blair Noctis (ncts)

下列贡献者在过去的两个月中被添加为 Debian 维护者：

* Taihsiang Ho

祝贺他们！
