Title: New Debian Developers and Maintainers (March, April, and May 2024)
Slug: new-developers-2024-05
Date: 2024-05-20 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: en
Status: draft

The following contributors got their Debian Developer accounts in the last two
months:

* DD
* DD

The following contributors were added as Debian Maintainers in the last two
months:

* DC
* DC

The following Debian Devlopers celebrated anniversaries in Debian:

* Nilesh Patra 5 years in Debian (May 2024)
* Oscar Davies 8 years in Debian (May 2024)
* Wouter Verhelst 23 years in Debian (JaJanuray 2024)
* Amar Tufo 1 year in Debian (January 2024)
* Michael Miller 10 years in Debian (December 2023)
* Samsul Ma'arif 5 years in Debian (December 2023)
* Jeroen van Wolffelaar 19 years in Debian (December 2023)
* Baozi Chen 9 years in Debian (December 2023)
* Julien Viard de Galbert 13 years in Debian (November 2023)

Congratulations!
