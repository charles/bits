Title: Nuevos desarrolladores y mantenedores de Debian (marzo y abril del 2024)
Slug: new-developers-2024-04
Date: 2024-05-31 18:00
Author: Jean-Pierre Giraud
Tags: project
Lang: es
Translator:
Status: published

Los siguientes colaboradores del proyecto se convirtieron en Debian Developers
en los dos últimos meses:

* Patrick Winnertz (winnie)
* Fabian Gruenbichler (fabiang)

Los siguientes colaboradores del proyecto se convirtieron en Debian Maintainers
en los dos últimos meses:

* Juri Grabowski
* Tobias Heider
* Jean Charles Delépine
* Guilherme Puida Moreira
* Antoine Le Gonidec
* Arthur Barbosa Diniz

¡Felicidades a todos!
