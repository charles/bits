Title: Nya Debianutvecklare och Debian Maintainers (Juli och Augusti 2024)
Slug: new-developers-2024-08
Date: 2024-09-30 16:30
Author: Jean-Pierre Giraud
Tags: project
Lang: sv
Translator: Andreas Rönnquist
Status: published

Följande bidragslämnare fick sina Debianutvecklarkonton under de senaste två
månaderna:

* Carlos Henrique Lima Melara (charles)
* Joenio Marques da Costa (joenio)
* Blair Noctis (ncts)

Följande bidragslämnare adderades som Debian Maintainers under de senaste två
månaderna:

* Taihsiang Ho

Grattis!
