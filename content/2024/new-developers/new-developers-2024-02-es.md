Title: Nuevos desarrolladores y mantenedores de Debian (enero y febrero del 2024)
Slug: new-developers-2024-02
Date: 2024-03-23 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: es
Translator:
Status: published

Los siguientes colaboradores del proyecto se convirtieron en Debian Developers
en los dos últimos meses:

* Carles Pina i Estany (cpina)
* Dave Hibberd (hibby)
* Soren Stoutner (soren)
* Daniel Gröber (dxld)
* Jeremy Sowden (azazel)
* Ricardo Ribalda Delgado (ribalda)

Los siguientes colaboradores del proyecto se convirtieron en Debian Maintainers
en los dos últimos meses:

* Joachim Bauch
* Ananthu C V
* Francesco Ballarin
* Yogeswaran Umasankar
* Kienan Stewart

¡Felicidades a todos!
