Title: متطوري ومشرفي دبيان الجدد (أيّار وحزيران 2024)
Slug: new-developers-2024-06
Date: 2024-07-19 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: ar
Translator: Maytham Alsudany
Status: published

حصلوا المساهمون التاليون على حسابات متطوري دبيان في الشهرين الماضيين:

* Dennis van Dok (dvandok)
* Peter Wienemann (wiene)
* Quentin Lejard (valde)
* Sven Geuer (sge)
* Taavi Väänänen (taavi)
* Hilmar Preusse (hille42)
* Matthias Geiger (werdahias)
* Yogeswaran Umasankar (yogu)

تمت إضافة المساهمون التاليون كمشرفي دبيان في الشهرين الماضيين:

* Bernhard Miklautz
* Felix Moessbauer
* Maytham Alsudany
* Aquila Macedo
* David Lamparter
* Tim Theisen
* Stefano Brivio
* Shengqi Chen

تهانينا!
