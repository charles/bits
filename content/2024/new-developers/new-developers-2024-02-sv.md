Title: Nya Debianutvecklare och Debian Maintainers (Januari och Februari 2024)
Slug: new-developers-2024-02
Date: 2024-03-23 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: sv
Translator: Andreas Rönnquist
Status: published

Följande bidragslämnare fick sina Debianutvecklarkonton under de senaste två
månaderna:

* Carles Pina i Estany (cpina)
* Dave Hibberd (hibby)
* Soren Stoutner (soren)
* Daniel Gröber (dxld)
* Jeremy Sowden (azazel)
* Ricardo Ribalda Delgado (ribalda)

Följande bidragslämnare adderades som Debian Maintainers under de senaste två
månaderna:

* Joachim Bauch
* Ananthu C V
* Francesco Ballarin
* Yogeswaran Umasankar
* Kienan Stewart

Grattis!
