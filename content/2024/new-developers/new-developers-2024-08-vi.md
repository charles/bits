Title: Những nhà Phát triển và Bảo trì Debian mới (Tháng bảy và tám 2024)
Slug: new-developers-2024-08
Date: 2024-09-30 16:30
Author: Jean-Pierre Giraud
Tags: project
Lang: vi
Translator: Trần Ngọc Quân
Status: published

Trong hai tháng qua, những người đóng góp sau đây nhận được tài khoản Phát
triển Debian:

* Carlos Henrique Lima Melara (charles)
* Joenio Marques da Costa (joenio)
* Blair Noctis (ncts)

Trong hai tháng qua, những người đóng góp sau đây được thêm vào với tư cách là
người Bảo trì Debian:

* Taihsiang Ho

Xin chúc mừng!
