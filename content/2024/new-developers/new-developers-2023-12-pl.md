Title: Nowi Deweloperzy i Opiekunowie Debiana (listopad i grudzień 2023)
Slug: new-developers-2023-12
Date: 2024-01-31 16:00
Author: Donald Norwood
Tags: project
Lang: pl
Translator: Grzegorz Szymaszek
Status: published

Następujący współtwórcy otrzymali konta Deweloperów Debiana w&nbsp;ciągu
ostatnich dwóch miesięcy:

* Alexandre Detiste (tchet)
* Amin Bandali (bandali)
* Jean&shy;&#8209;Pierre Giraud (jipege)
* Timthy Pearson (tpearson)

Następujący współautor dołączył do Opiekunów Debiana w&nbsp;ciągu ostatnich
dwóch miesięcy:

* Safir Secerovic

Gratulacje!
