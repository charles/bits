Title: Nouveaux développeurs et mainteneurs de Debian (juillet et août 2024)
Slug: new-developers-2024-08
Date: 2024-09-30 16:30
Author: Jean-Pierre Giraud
Tags: project
Lang: fr
Translator:
Status: published

Les contributeurs suivants sont devenus développeurs Debian ces deux derniers
mois :

* Carlos Henrique Lima Melara (charles)
* Joenio Marques da Costa (joenio)
* Blair Noctis (ncts)

et ces contributeurs ont été acceptés comme mainteneurs Debian durant la même
période :

* Taihsiang Ho

Félicitations !
