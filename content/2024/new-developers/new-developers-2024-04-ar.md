Title: متطوري ومشرفي دبيان الجدد (آذار ونيسان 2024)
Slug: new-developers-2024-04
Date: 2024-05-31 18:00
Author: Jean-Pierre Giraud
Tags: project
Lang: ar
Translator: Maytham Alsudany
Status: published

حصلوا المساهمون التاليون على حسابات متطوري دبيان في الشهرين الماضيين:

* Patrick Winnertz (winnie)
* Fabian Gruenbichler (fabiang)

تمت إضافة المساهمون التاليون كمشرفي دبيان في الشهرين الماضيين:

* Juri Grabowski
* Tobias Heider
* Jean Charles Delépine
* Guilherme Puida Moreira
* Antoine Le Gonidec
* Arthur Barbosa Diniz

تهانينا!
