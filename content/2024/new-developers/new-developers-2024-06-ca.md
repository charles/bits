Title: Nous desenvolupadors i mantenidors de Debian (maig i juny del 2024)
Slug: new-developers-2024-06
Date: 2024-07-19 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: ca
Translator:
Status: published

Els següents col·laboradors del projecte han esdevingut Debian Developers en
els darrers dos mesos:

* Dennis van Dok (dvandok)
* Peter Wienemann (wiene)
* Quentin Lejard (valde)
* Sven Geuer (sge)
* Taavi Väänänen (taavi)
* Hilmar Preusse (hille42)
* Matthias Geiger (werdahias)
* Yogeswaran Umasankar (yogu)

Els següents col·laboradors del projecte han esdevingut Debian Maintainers en
els darrers dos mesos:

* Bernhard Miklautz
* Felix Moessbauer
* Maytham Alsudany
* Aquila Macedo
* David Lamparter
* Tim Theisen
* Stefano Brivio
* Shengqi Chen

Enhorabona a tots!
