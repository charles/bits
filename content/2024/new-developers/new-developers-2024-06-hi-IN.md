Title: नए डेबियन डेवलपर्स और मेंटेनर्स (मई और जून 2024)
Slug: new-developers-2024-06
Date: 2024-07-19 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: hi-IN
Translator: Yashraj Moghe
Status: published

पिछले दो महीनों में निम्नलिखित सहायकों को उनका डेबियन डेवलपर अकाउंट मिला:

* Dennis van Dok (dvandok)
* Peter Wienemann (wiene)
* Quentin Lejard (valde)
* Sven Geuer (sge)
* Taavi Väänänen (taavi)
* Hilmar Preusse (hille42)
* Matthias Geiger (werdahias)
* Yogeswaran Umasankar (yogu)

और निम्नलिखित को डेबियन मेंटेनर का स्थान दिया गया हैं:

* Bernhard Miklautz
* Felix Moessbauer
* Maytham Alsudany
* Aquila Macedo
* David Lamparter
* Tim Theisen
* Stefano Brivio
* Shengqi Chen

इन्हें बधाईयाँ!
