Title: متطوري ومشرفي دبيان الجدد (تَمُّوز و آب 2024)
Slug: new-developers-2024-08
Date: 2024-09-30 16:30
Author: Jean-Pierre Giraud
Tags: project
Lang: ar
Translator: Maytham Alsudany
Status: published

حصلوا المساهمون التاليون على حسابات متطوري دبيان في الشهرين الماضيين:

* Carlos Henrique Lima Melara (charles)
* Joenio Marques da Costa (joenio)
* Blair Noctis (ncts)

تمت إضافة المساهمون التاليون كمشرفي دبيان في الشهرين الماضيين:

* Taihsiang Ho

تهانينا!
