Title: Nya Debianutvecklare och Debian Maintainers (Mars och April 2024)
Slug: new-developers-2024-04
Date: 2024-05-31 18:00
Author: Jean-Pierre Giraud
Tags: project
Lang: sv
Translator: Andreas Rönnquist
Status: published

Följande bidragslämnare fick sina Debianutvecklarkonton under de senaste två
månaderna:

* Patrick Winnertz (winnie)
* Fabian Gruenbichler (fabiang)

Följande bidragslämnare adderades som Debian Maintainers under de senaste två
månaderna:

* Juri Grabowski
* Tobias Heider
* Jean Charles Delépine
* Guilherme Puida Moreira
* Antoine Le Gonidec
* Arthur Barbosa Diniz

Grattis!
