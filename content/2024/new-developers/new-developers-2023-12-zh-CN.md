Title: 新的 Debian 开发者和维护者 (2023年11月 至 年12月)
Slug: new-developers-2023-12
Date: 2024-01-31 16:00
Author: Donald Norwood
Tags: project
Lang: zh-CN
Translator: Boyuan Yang
Status: published

下列贡献者在过去的两个月中获得了他们的 Debian 开发者帐号：

* Alexandre Detiste (tchet)
* Amin Bandali (bandali)
* Jean-Pierre Giraud (jipege)
* Timthy Pearson (tpearson)

下列贡献者在过去的两个月中被添加为 Debian 维护者：

* Safir Secerovic

祝贺他们！
