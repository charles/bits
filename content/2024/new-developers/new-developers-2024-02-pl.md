Title: Nowi Deweloperzy i Opiekunowie Debiana (styczeń i luty 2024)
Slug: new-developers-2024-02
Date: 2024-03-23 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: pl
Translator: Grzegorz Szymaszek
Status: published

Następujący współtwórcy otrzymali konta Deweloperów Debiana w&nbsp;ciągu
ostatnich dwóch miesięcy:

* <span lang="ca">Carles Pina i Estany</span> (cpina)
* <span lang="en">Dave Hibberd</span> (hibby)
* <span lang="en">Soren Stoutner</span> (soren)
* <span lang="de">Daniel Gröber</span> (dxld)
* <span lang="en">Jeremy Sowden</span> (azazel)
* <span lang="es">Ricardo Ribalda Delgado</span> (ribalda)

Następujący współtwórcy dołączyli do Opiekunów Debiana w&nbsp;ciągu ostatnich
dwóch miesięcy:

* <span lang="de">Joachim Bauch</span>
* <span lang="en">Ananthu C V</span>
* <span lang="it">Francesco Ballarin</span>
* <span lang="en">Yogeswaran Umasankar</span>
* <span lang="en">Kienan Stewart</span>

Gratulacje!
