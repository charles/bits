Title: Novos(as) desenvolvedores(as) e mantenedores(as) Debian (setembro e outubro de 2024)
Slug: new-developers-2024-10
Date: 2024-11-28 18:00
Author: Jean-Pierre Giraud
Tags: project
Lang: pt
Status: published

Os(As) seguintes colaboradores(as) do projeto se tornaram Desenvolvedores(as)
Debian nos últimos dois meses:

* Joachim Bauch (fancycode)
* Alexander Kjäll (capitol)
* Jan Mojžíš (janmojzis)
* Xiao Sheng Wen (atzlinux)

Os(As) seguintes colaboradores(as) do projeto se tornaram Mantenedores(as)
Debian nos últimos dois meses:

* Alberto Bertogli
* Alexis Murzeau
* David Heilderberg
* Xiyue Deng
* Kathara Sasikumar
* Philippe Swartvagher

Parabéns a todos!
