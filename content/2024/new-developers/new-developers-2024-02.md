Title: New Debian Developers and Maintainers (January and February 2024)
Slug: new-developers-2024-02
Date: 2024-03-23 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: en
Status: published

The following contributors got their Debian Developer accounts in the last two
months:

* Carles Pina i Estany (cpina)
* Dave Hibberd (hibby)
* Soren Stoutner (soren)
* Daniel Gröber (dxld)
* Jeremy Sowden (azazel)
* Ricardo Ribalda Delgado (ribalda)

The following contributors were added as Debian Maintainers in the last two
months:

* Joachim Bauch
* Ananthu C V
* Francesco Ballarin
* Yogeswaran Umasankar
* Kienan Stewart

Congratulations!
