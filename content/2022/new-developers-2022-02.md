Title: New Debian Developers and Maintainers (January and February 2022)
Slug: new-developers-2022-02
Date: 2022-03-21 17:00
Author: Jean-Pierre Giraud
Tags: project
Lang: en
Status: published

The following contributor got his Debian Developer account in the last two
months:

* Francisco Vilmar Cardoso Ruviaro (vilmar)

The following contributors were added as Debian Maintainers in the last two
months:

* Lu YaNing
* Mathias Gibbens
* Markus Blatt
* Peter Blackman
* David da Silva Polverari

Congratulations!
