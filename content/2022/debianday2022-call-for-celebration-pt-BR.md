Title: Debian Day 2022 - chamada para celebração
Slug: debianday2022-call-for-celebration
Date: 2022-08-08 17:00
Author: The Debian Publicity Team
Tags: debian, project, anniversary, DebianDay
Translator: Paulo Henrique de Lima Santana (phls)
Lang: pt-BR
Status: published

Todos os anos no dia 16 de agosto acontece o aniversário do Projeto Debian. E
várias comunidades ao redor do mundo celebram essa data organizando encontros
locais em um evento chamado "Debian Day" (Dia do Debian).

Então que tal comemorar o 29º aniversário do Projeto Debian em 2022 na sua
cidade?

Convidamos você e sua comunidade local para organizarem o Debian Day promovendo
um evento com palestras, oficinas,
[bug squashing party](https://wiki.debian.org/BSP) (festa de caça à bugs),
[assinaturas de chaves OpenPGP](https://wiki.debian.org/Keysigning), etc.
Ou simplesmente realizando um encontro entre pessoas que gostam do Debian em
um bar/pizzaria/cafeteria/restaurante para comemorar. Ou seja, todo tipo de
encontro é válido!

Mas lembrem-se que a pandemia de COVID-19 ainda não acabou, então tome todas as
medidas necessárias para a proteção das pessoas.

Como o dia 16 de agosto cai em ma terça-feira, se vocês acharem melhor
organizar durante o final de semana, não tem problema. O importante é acontecer
a celebração do Projeto Debian.

Lembre-se de adicionar a sua cidade na
[página wiki do Debian Day](https://wiki.debian.org/DebianDay/2022)

Existe uma [lista de Grupos Locais](https://wiki.debian.org/LocalGroups) do
Debian em todo o mundo. Se sua cidade estiver listada, converse com o pessoal
para vocês organizarem o Debian Day juntos.

Vamos fazer fotos e publicar nas mídias sociais usando as
hashtags: #DebianDay #DebianDay2022
