Title: Những nhà Phát triển và Bảo trì Debian mới (Tháng chín và mười 2022)
Slug: new-developers-2022-10
Date: 2022-11-30 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: vi
Translator: Trần Ngọc Quân
Status: published

Trong hai tháng qua, những người đóng góp sau đây nhận được tài khoản Phát triển Debian:

* Abraham Raji (abraham)
* Phil Morrell (emorrp1)
* Anupa Ann Joseph (anupa)
* Mathias Gibbens (gibmat)
* Arun Kumar Pariyar (arun)
* Tino Didriksen (tinodidriksen)

Trong hai tháng qua, những người đóng góp sau đây được thêm vào với tư cách là người Bảo trì Debian:

* Gavin Lai
* Martin Dosch
* Taavi Väänänen
* Daichi Fukui
* Daniel Gröber
* Vivek K J
* William Wilson
* Ruben Pollan

Xin chúc mừng!
