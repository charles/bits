Title: Nous desenvolupadors i mantenidors de Debian (gener i febrer del 2022)
Slug: new-developers-2022-02
Date: 2022-03-21 17:00
Author: Jean-Pierre Giraud
Tags: project
Lang: ca
Status: published

El següent col·laborador del projecte ha esdevingut Debian Developer en els
darrers dos mesos:

* Francisco Vilmar Cardoso Ruviaro (vilmar)

Els següents col·laboradors del projecte han esdevingut Debian Maintainers en
els darrers dos mesos:

* Lu YaNing
* Mathias Gibbens
* Markus Blatt
* Peter Blackman
* David da Silva Polverari

Enhorabona a tots!
