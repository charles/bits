Title: Nouveaux développeurs et mainteneurs de Debian (mai et juin 2022)
Slug: new-developers-2022-06
Date: 2022-07-29 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: fr
Status: published

Les contributeurs suivants sont devenus développeurs Debian ces deux derniers
mois :

* Geoffroy Berret (kaliko)
* Arnaud Ferraris (aferraris)

et ces contributeurs ont été acceptés comme mainteneurs Debian durant la même
période :

* Alec Leanas
* Christopher Michael Obbard
* Lance Lin
* Stefan Kropp
* Matteo Bini
* Tino Didriksen

Félicitations !
