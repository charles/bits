Title: Nouveaux développeurs et mainteneurs de Debian (novembre et décembre 2021)
Slug: new-developers-2021-12
Date: 2022-01-13 17:00
Author: Jean-Pierre Giraud
Tags: project
Lang: fr
Status: published

Les contributeurs suivants sont devenus développeurs Debian ces deux derniers
mois :

* Douglas Andrew Torrance (dtorrance)
* Mark Lee Garrett (lee)

et ces contributeurs ont été acceptés comme mainteneurs Debian durant la même
période :

* Lukas Matthias Märdian
* Paulo Roberto Alves de Oliveira
* Sergio Almeida Cipriano Junior
* Julien Lamy
* Kristian Nielsen
* Jeremy Paul Arnold Sowden
* Jussi Tapio Pakkanen
* Marius Gripsgard
* Martin Budaj
* Peymaneh
* Tommi Petteri Höynälänmaa

Félicitations !
