Title: Những nhà Phát triển và Bảo trì Debian mới (Tháng năm và sáu 2022)
Slug: new-developers-2022-06
Date: 2022-07-29 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: vi
Translator: Trần Ngọc Quân
Status: published

Trong hai tháng qua, những người đóng góp sau đây nhận được tài khoản Phát triển Debian:

* Geoffroy Berret (kaliko)
* Arnaud Ferraris (aferraris)

Trong hai tháng qua, những người đóng góp sau đây được thêm vào với tư cách là người Bảo trì Debian:

* Alec Leanas
* Christopher Michael Obbard
* Lance Lin
* Stefan Kropp
* Matteo Bini
* Tino Didriksen

Xin chúc mừng!
