Title: Clôture de DebConf22 à Prizren et annonce des dates de DebConf23
Date: 2022-07-25 10:30
Tags: debconf22, debconf23, announce, debconf
Slug: debconf22-closes
Author: Debian Publicity Team
Artist: Aigars Mahinovs
Lang: fr
Translator: Jean-Pierre Giraud
Image: /images/debconf22_group_small.jpg
Status: published

[![Photo de groupe de DebConf22 – cliquer pour l'agrandir](|static|/images/debconf22_group_small.jpg)](https://wiki.debian.org/DebConf/22/Photos?action=AttachFile&do=view&target=debconf23_group_photo.jpg)

Dimanche 24 juillet 2022, la conférence annuelle des développeurs et
contributeurs Debian s'est achevée. Accueillant 260 participants venus de
38 pays différents, et plus de 91 événements combinant communications,
séances de discussion ou sessions spécialisées (BoF), ateliers et activités,
[DebConf22](https://debconf22.debconf.org) a été un énorme succès.

La conférence a été précédée par le DebCamp annuel, du 10 au 16 juillet
qui a mis l'accent sur des travaux individuels et des rencontres d'équipes
pour une collaboration directe au développement de Debian. En
particulier, cette année, il y a eu des rencontre pour faire avancer le
développement de Mobian/Debian sur téléphone portable, des constructions
reproductibles et Python dans Debian, et une formation intensive pour
les nouveaux venus pour les introduire à Debian et pour qu'ils
acquièrent une expérience pratique de son utilisation et de contribution
à la communauté.

La conférence annuelle des développeurs Debian proprement dite a débuté
samedi 17 juillet 2022. Conjointement à des séances plénières telles que les
traditionnelles « brèves du chef du projet », une séance permanente
de signature de clés, les présentations éclair et l'annonce de la conférence de
l'année prochaine, ([DebConf23](https://wiki.debian.org/DebConf/23) qui se
tiendra à Kochi, en Inde), diverses séances se sont tenues liées aux équipes
de langages de programmation comme Python, Perl et Ruby, ainsi
que des nouvelles informations de plusieurs projets et d'équipes internes
de Debian, des sessions de discussions (BoFs) de plusieurs équipes techniques
(Long Term Support, outils Android, Distributions dérivées de Debian,
Installateur Debian et équipe Images, Debian Science...) et des
communautés locales (Debian Brésil, Debian Inde, les Équipes locale de
Debian), ainsi que de nombreux
autres événements d'intérêt concernant Debian et le logiciel libre.

Le [programme](https://debconf22.debconf.org/schedule/)
a été mis à jour chaque jour avec des activités planifiées et improvisées
introduites par les participants pendant toute la durée de la conférence.
Plusieurs activités qui n'avaient pu être organisées les années précédentes du
fait de la pandémie de COVID ont fait leur retour dans le programme de
la conférence : une bourse d'emploi, un soirée poésie et scène ouverte,
la fête traditionnelle du fromage et du vin, les photos de groupe et la
journée d'excursion.

Pour tous ceux qui n'ont pu participer, la plupart des
communications et des sessions ont été enregistrées pour une diffusion
en direct avec des vidéos rendues disponibles sur le
[site web des réunions Debian](https://meetings-archive.debian.net/pub/debian-meetings/2022/DebConf22/).
Presque toutes les sessions ont facilité la participation à distance par
des applications de messagerie IRC ou un éditeur de texte collaboratif
en ligne.

Le site web de [DebConf22](https://debconf22.debconf.org/)
restera actif à fin d'archive et continuera à offrir des liens vers les
présentations et vidéos des communications et des événements.

L'an prochain, [DebConf23](https://wiki.debian.org/DebConf/23) se tiendra
à Kochi en Inde du 10 au 16 septembre 2023. Comme à l'accoutumée, les
organisateurs débuteront les travaux en Inde, avant la DebConf, par un
DebCamp (du 3 au 9 septembre 2023), avec un accent particulier mis sur le
travail individuel ou en équipe pour améliorer la distribution.

DebConf s'est engagée à offrir un environnement sûr et accueillant pour
tous les participants.
Voir
[la page web à propos du Code de conduite sur le site de DebConf22](https://debconf22.debconf.org/about/coc/)
pour plus de détails à ce sujet.

Debian remercie les nombreux
[parrains](https://debconf22.debconf.org/sponsors/)
pour leur engagement dans leur soutien à DebConf22, et en particulier nos
parrains de platine :
[**Lenovo**](https://www.lenovo.com),
[**Infomaniak**](https://www.infomaniak.com),
[**ITP Prizren**](https://itp-prizren.com/)
et [**Google**](https://google.com/).

### À propos de Debian

Le projet Debian a été fondé en 1993 par Ian Murdock pour être un projet
communautaire réellement libre. Depuis cette date, le projet Debian est
devenu l'un des plus importants et des plus influents projets à code source
ouvert. Des milliers de volontaires du monde entier travaillent ensemble
pour créer et maintenir les logiciels Debian. Traduite en soixante-dix
langues et gérant un grand nombre de types d'ordinateurs, la
distribution Debian est conçue pour être le _système d'exploitation universel_.

### À propos de DebConf

DebConf est la conférence des développeurs du projet Debian. En plus
d'un programme complet de présentations techniques, sociales ou
organisationnelles, DebConf fournit aux développeurs, aux contributeurs et
à toutes personnes intéressées, une occasion de rencontre et de travail
collaboratif interactif. DebConf a eu lieu depuis 2000 en des endroits du
monde aussi variés que l'Écosse, l'Argentine et la Bosnie-Herzegovine. Plus
de renseignements sont disponibles sur
[https://debconf.org](https://debconf.org/).

### À propos de Lenovo

En tant que leader mondial en technologie, produisant une vaste gamme de
produits connectés, comprenant des smartphones, des tablettes, des
machines de bureau et des stations de travail aussi bien que des
périphériques de réalité augmentée et de réalité virtuelle, des
solutions pour la domotique ou le bureau connecté et les centres de
données, [**Lenovo**](https://www.lenovo.com) comprend combien sont
essentiels les plateformes et systèmes ouverts pour un monde connecté.

### À propos d'Infomaniak

[**Infomaniak**](https://www.infomaniak.com) est la plus grande compagnie
suisse d'hébergement web qui offre aussi des services de sauvegarde et
de stockage, des solutions pour les organisateurs d'événement et de
services de diffusion en direct et de vidéo à la demande. Elle possède
l'intégralité de ses centres de données et de tous les éléments
essentiels pour le fonctionnement des services et produits qu'elle
fournit (à la fois sur le plan matériel et logiciel).

### À propos d'ITP Prizren

L'[**Innovation and Training Park Prizren**](https://itp-prizren.com/)
ambitionne d'être un facteur du changement et de stimulation dans le
domaine des technologies de l'information et de la communication,
l'agroalimentaire et les industries créatives au moyen de la création
et de la gestion d'un environnement favorable et de services efficaces
pour les PME, en exploitant différents types d'innovations qui peuvent
contribuer à améliorer le développement de l'industrie et de la
recherche du Kosovo, apportant des bénéfices à l'économie et à la
société du pays dans son ensemble.

### À propos de Google

[**Google**](https://google.com/) est notre quatrième parrain de platine.
Google est l'une des plus grandes entreprises technologiques du monde
qui fournit une large gamme de services relatifs à Internet et de
produits tels que des technologies de publicité en ligne, des outils de
recherche, de l'informatique dans les nuages, des logiciels et du
matériel.

Google apporte son soutien à Debian en parrainant la DebConf
depuis plus de dix ans, et est également un partenaire Debian parrainant
des éléments de l'infrastructure d'intégration continue de
[Salsa](https://salsa.debian.org) au sein de la plateforme Google Cloud.

### Plus d'informations

Pour plus d'informations, veuillez consulter la page internet de
DebConf 22 à l'adresse
[https://debconf22.debconf.org/](https://debconf22.debconf.org/)
ou écrire à <press@debian.org>.
