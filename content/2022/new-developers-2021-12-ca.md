Title: Nous desenvolupadors i mantenidors de Debian (novembre i desembre del 2021)
Slug: new-developers-2021-12
Date: 2022-01-13 17:00
Author: Jean-Pierre Giraud
Tags: project
Lang: ca
Status: published

Els següents col·laboradors del projecte han esdevingut Debian Developers en
els darrers dos mesos:

* Douglas Andrew Torrance (dtorrance)
* Mark Lee Garrett (lee)

Els següents col·laboradors del projecte han esdevingut Debian Maintainers en
els darrers dos mesos:

* Lukas Matthias Märdian
* Paulo Roberto Alves de Oliveira
* Sergio Almeida Cipriano Junior
* Julien Lamy
* Kristian Nielsen
* Jeremy Paul Arnold Sowden
* Jussi Tapio Pakkanen
* Marius Gripsgard
* Martin Budaj
* Peymaneh
* Tommi Petteri Höynälänmaa

Enhorabona a tots!
