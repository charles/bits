Title: DebConf22 startar idag i Prizren
Slug: debconf22-starts
Date: 2022-07-17 09:00
Author: The Debian Publicity Team
Tags: debconf, debconf22
Lang: sv
Translator: Andreas Rönnquist
Image: /images/debconf22-banner-700px.png
Status: published

[DebConf22](https://debconf22.debconf.org/), den 23:e årliga
Debiankonferensen, tar plats i Prizren, Kosovo
från 17 juli till 24 juli, 2022.

Debianbidragslämnare från hela världen har samlats i
[Innovation and Training Park (ITP)](https://debconf22.debconf.org/about/venue)
i Prizren, Kosovo, för att delta och arbeta i en konferens som uteslutande
drivs av frivilliga.

Idag börjar huvudkonferensen med mer än 270 förväntade deltagare och
82 schemalagda aktiviteter,
inklusive 45-minuters och 20-minuters föreläsningar och gruppmöten ("BoF"),
workshops, och en jobbmässa, så väl som en mängd olika evenemang.

Det kompletta schemat på
[https://debconf2.debconf.org/schedule/](https://debconf22.debconf.org/schedule/)
uppdateras varje dag, och inkluderar aktiviteter skapade i stunden av
deltagare under hela konferensen.

Om du vill engagera dig på distans, kan du följa
[**videoströmmen** som finns tillgänglig från DebConf22-webbplatsen](https://debconf22.debconf.org/)
av evenemangen som sker i de tre föreläsningssalarna:
_Drini_, _Lumbardhi_ och _Ereniku_.
Eller så kan du delta i konversationen om vad som händer i
föreläsningssalarna
  [**#debconf-drini**](https://webchat.oftc.net/?channels=#debconf-drini),
  [**#debconf-lumbardhi**](https://webchat.oftc.net/?channels=#debconf-lumbardhi) och
  [**#debconf-ereniku**](https://webchat.oftc.net/?channels=#debconf-ereniku)
(alla dessa kanaler hittar dfu i OFTC IRC-nätverket).

Du kan även följa direktsändningen av nyheter om DebConf22 på
[https://micronews.debian.org](https://micronews.debian.org) eller @debian-profilen
i dina sociala nätverk.

DebConf är engagerat i en säker och välkomnande miljö för alla deltagare.
Se [webbsidan om uppförandekoden på DebConf22's webbplats](https://debconf22.debconf.org/about/coc/)
för ytterligare detaljer om detta.

Debian tackar för engagemanget från flera
[sponsorer](https://debconf22.debconf.org/sponsors/)
för deras stöd för DebConf22, speciellt våra Platinasponsorer:
[**Lenovo**](https://www.lenovo.com),
[**Infomaniak**](https://www.infomaniak.com),
[**ITP Prizren**](https://itp-prizren.com)
och [**Google**](https://google.com).

![DebConf22 banner öppen registrering](|static|/images/debconf22-banner-700px.png)
