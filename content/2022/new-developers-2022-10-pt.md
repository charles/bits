Title: Novos(as) desenvolvedores(as) e mantenedores(as) Debian (setembro e outubro de 2022)
Slug: new-developers-2022-10
Date: 2022-11-30 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: pt
Status: published

Os(As) seguintes colaboradores(as) do projeto se tornaram Desenvolvedores(as)
Debian nos últimos dois meses:

* Abraham Raji (abraham)
* Phil Morrell (emorrp1)
* Anupa Ann Joseph (anupa)
* Mathias Gibbens (gibmat)
* Arun Kumar Pariyar (arun)
* Tino Didriksen (tinodidriksen)

Os(As) seguintes colaboradores(as) do projeto se tornaram Mantenedores(as)
Debian nos últimos dois meses:

* Gavin Lai
* Martin Dosch
* Taavi Väänänen
* Daichi Fukui
* Daniel Gröber
* Vivek K J
* William Wilson
* Ruben Pollan

Parabéns a todos!
