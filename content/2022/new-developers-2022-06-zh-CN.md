Title: 新的 Debian 开发者和维护者 (2022年5月 至 6月)
Slug: new-developers-2022-06
Date: 2022-07-29 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: zh-CN
Translator: Boyuan Yang
Status: published

下列贡献者在过去的两个月中获得了他们的 Debian 开发者帐号：

* Geoffroy Berret (kaliko)
* Arnaud Ferraris (aferraris)

下列贡献者在过去的两个月中被添加为 Debian 维护者：

* Alec Leanas
* Christopher Michael Obbard
* Lance Lin
* Stefan Kropp
* Matteo Bini
* Tino Didriksen

祝贺他们！
