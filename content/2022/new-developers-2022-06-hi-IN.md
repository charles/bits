Title: नए डेबियन डेवलपर्स और मेंटेनर्स (मई और जून 2022)
Slug: new-developers-2022-06
Date: 2022-07-29 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: hi-IN
Translator: Yashraj Moghe
Status: published

आखरी 2 महीनो मैं इन सहायको को उनका डेबियन डेवलपर अकाउंट मिला

* Geoffroy Berret (kaliko)
* Arnaud Ferraris (aferraris)

और इन सहायको को डेबियन मेंटेनर का स्थान दिया गया हैं

* Alec Leanas
* Christopher Michael Obbard
* Lance Lin
* Stefan Kropp
* Matteo Bini
* Tino Didriksen

इन्हे बधाइयाँ!
