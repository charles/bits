Title: DebConf22 bursary applications and call for papers are closing in less than 72 hours!
Slug: debconf22-bursaries-and-cfp-last-call
Date: 2022-04-28 9:30
Author: The Debian Publicity Team
Tags: debconf, debconf22
Lang: en
Image: /images/debconf22-banner-700px.png
Status: published

If you intend to apply for a
[DebConf22 bursary](https://debconf22.debconf.org/about/bursaries/) and/or
submit an [event proposal](https://debconf22.debconf.org/cfp/) and have not yet
done so, please proceed as soon as possible!

**Bursary applications for DebConf22 will be accepted until May 1st at 23:59 UTC**.
Applications submitted after this deadline will not be considered.

You can apply for a bursary when you [register](https://debconf22.debconf.org/register)
for the conference.

Remember that giving a talk or organising an event is considered towards your bursary;
if you have a submission to make, submit it even if it is only sketched-out.
You will be able to detail it later. DebCamp plans can be entered in the usual
[Sprints page at the Debian wiki](https://wiki.debian.org/Sprints).

Please make sure to double-check your accommodation choices (dates and venue).
Details about accommodation arrangements can be found on the
[accommodation page](https://debconf22.debconf.org/about/accommodation/).

**Event proposals will be accepted until May 1st at 23:59 UTC too**.

Events are not limited to traditional presentations or informal sessions (BoFs):
we welcome submissions of tutorials, performances, art installations, debates,
or any other format of event that you think would be of interest to the Debian
community.

Regular sessions may either be 20 or 45 minutes long (including time for
questions), other kinds of sessions (workshops, demos, lightning talks, and so
on) could have different durations. Please choose the most suitable duration for
your event and explain any special requests. You can submit it
[here](https://debconf22.debconf.org/talks/new).

The the 23rd edition of [DebConf](https://www.debconf.org) **will take place
from July 17th to 24th, 2022 at the
[Innovation and Training Park (ITP)](https://itp-prizren.com) in Prizren,
Kosovo**, and will be preceded by DebCamp, from July 10th to 16th.

See you in [Prizren](https://debconf22.debconf.org/about/prizren/)!

![DebConf22 banner open registration](|static|/images/debconf22-banner-700px.png)
