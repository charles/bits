Title: DebConf22 closes in Prizren and DebConf23 dates announced
Date: 2022-07-25 10:30
Tags: debconf22, debconf23, announce, debconf
Slug: debconf22-closes
Author: Debian Publicity Team
Artist: Aigars Mahinovs
Image: /images/debconf22_group_small.jpg
Status: published

[![DebConf22 group photo - click to enlarge](|static|/images/debconf22_group_small.jpg)](https://wiki.debian.org/DebConf/22/Photos?action=AttachFile&do=view&target=debconf23_group_photo.jpg)

On Sunday 24 July 2022, the annual Debian Developers and
Contributors Conference came to a close.
Hosting 260 attendees
from 38 different countries over a combined 91 event talks, discussion
sessions, Birds of a Feather (BoF) gatherings, workshops, and
activities, [DebConf22](https://debconf22.debconf.org) was a large
success.

The conference was preceded by the annual DebCamp held 10 July to 16
July which focused on individual work and team sprints for in-person
collaboration towards developing Debian. In particular, this year there
have been sprints to advance development of Mobian/Debian on mobile,
reproducible builds and Python in Debian, and a BootCamp for newcomers,
to get introduced to Debian and have some hands-on experience with using
it and contributing to the community.

The actual Debian Developers Conference started on Sunday 17 July 2022.
Together with activities such as the traditional 'Bits from the DPL'
talk, the continuous key-signing party, lightning talks
and the announcement of next year's DebConf
([DebConf23](https://wiki.debian.org/DebConf/23) in Kochi, India),
there were several sessions related to programming language teams such as
Python, Perl and Ruby, as well as news updates on several projects and
internal Debian teams, discussion sessions (BoFs) from many technical
teams (Long Term Support, Android tools, Debian Derivatives, Debian
Installer and Images team, Debian Science\...) and local communities
(Debian Brasil, Debian India, the Debian Local Teams),
along with many other events of interest regarding Debian and free software.

The [schedule](https://debconf22.debconf.org/schedule/)
was updated each day with planned and ad-hoc activities introduced by attendees
over the course of the entire conference. Several activities that couldn\'t be
organized in past years due to the COVID pandemic returned to the
conference\'s schedule: a job fair, open-mic and poetry night, the
traditional Cheese and Wine party, the group photos and the Day Trip.

For those who were not able to attend, most of the talks and sessions were
recorded for live streams with videos made,
available through the
[Debian meetings archive website](https://meetings-archive.debian.net/pub/debian-meetings/2022/DebConf22/).
Almost all of the sessions facilitated remote participation via IRC messaging
apps or online collaborative text documents.

The [DebConf22 website](https://debconf22.debconf.org/)
will remain active for archival purposes and will continue to offer
links to the presentations and videos of talks and events.

Next year, [DebConf23](https://wiki.debian.org/DebConf/23) will be held in
Kochi, India, from September 10 to September 16, 2023.
As tradition follows before the next DebConf the local organizers in India
will start the conference activites with DebCamp (September 03 to September
09, 2023), with particular focus on individual and team work towards improving
the distribution.

DebConf is committed to a safe and welcome environment for all
participants.
See the
[web page about the Code of Conduct in DebConf22 website](https://debconf22.debconf.org/about/coc/)
for more details on this.

Debian thanks the commitment of numerous
[sponsors](https://debconf22.debconf.org/sponsors/)
to support DebConf22, particularly our Platinum Sponsors:
[**Lenovo**](https://www.lenovo.com),
[**Infomaniak**](https://www.infomaniak.com),
[**ITP Prizren**](https://itp-prizren.com/)
and [**Google**](https://google.com/).

### About Debian

The Debian Project was founded in 1993 by Ian Murdock to be a truly free
community project. Since then the project has grown to be one of the
largest and most influential open source projects. Thousands of
volunteers from all over the world work together to create and maintain
Debian software. Available in 70 languages, and supporting a huge range
of computer types, Debian calls itself the _universal operating system_.

### About DebConf

DebConf is the Debian Project's developer conference. In addition to a
full schedule of technical, social and policy talks, DebConf provides an
opportunity for developers, contributors and other interested people to
meet in person and work together more closely. It has taken place
annually since 2000 in locations as varied as Scotland, Argentina, and
Bosnia and Herzegovina. More information about DebConf is available from
[https://debconf.org/](https://debconf.org).

### About Lenovo

As a global technology leader manufacturing a wide portfolio of
connected products, including smartphones, tablets, PCs and workstations
as well as AR/VR devices, smart home/office and data center solutions,
[**Lenovo**](https://www.lenovo.com) understands how critical open systems
and platforms are to a connected world.

### About Infomaniak

[**Infomaniak**](https://www.infomaniak.com) is Switzerland\'s largest
web-hosting company, also offering backup and storage services,
solutions for event organizers, live-streaming and video on demand
services. It wholly owns its datacenters and all elements critical to
the functioning of the services and products provided by the company
(both software and hardware).

### About ITP Prizren

[**Innovation and Training Park Prizren**](https://itp-prizren.com/) intends
to be a changing and boosting element in the area of ICT, agro-food and
creatives industries, through the creation and management of a
favourable environment and efficient services for SMEs, exploiting
different kinds of innovations that can contribute to Kosovo to improve
its level of development in industry and research, bringing benefits to
the economy and society of the country as a whole.

### About Google

[**Google**](https://google.com/) is one of the largest technology companies
in the world, providing a wide range of Internet-related services and
products such as online advertising technologies, search, cloud
computing, software, and hardware.

Google has been supporting Debian by sponsoring DebConf for more than
ten years, and is also a Debian partner sponsoring parts of
[Salsa](https://salsa.debian.org)'s continuous integration
infrastructure within Google Cloud Platform.

### Contact Information

For further information, please visit the DebConf22 web page at
[https://debconf22.debconf.org/](https://debconf22.debconf.org/) or send mail
to <press@debian.org>.
