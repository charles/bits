Title: DebConf22 démarre aujourd'hui à Prizren
Slug: debconf22-starts
Date: 2022-07-17 06:00
Author: L'équipe en charge de la publicité de Debian
Tags: debconf, debconf22
Lang: fr
Translator: Jean-Pierre Giraud
Image: /images/debconf22-banner-700px.png
Status: published

[DebConf22](https://debconf22.debconf.org/), la vingt-troisième conférence
annuelle de Debian, se tient à Prizren, au Kosovo du 17 au 24 juillet 2022.

Des contributeurs de Debian venant du monde entier se sont rassemblés à
l'[Innovation and Training Park (ITP)](https://debconf22.debconf.org/about/venue)
à Prizren, Kosovo, pour participer à une conférence exclusivement gérée par des
bénévoles et y travailler.

Aujourd'hui débute la conférence principale avec plus de 270 participants
attendus et 82 activités programmées, dont des conférences de 45 et 20 minutes
et des réunions d'équipe (« BoF »), des ateliers et une bourse d'emploi ainsi
qu'une grande variété d'autres événements.

Le programme complet que vous trouverez ici
[https://debconf2.debconf.org/schedule/](https://debconf22.debconf.org/schedule/)
est mis à jour chaque jour, comprenant des activités prévues spécialement par les
participants durant toute la conférence.

Si vous souhaitez vous impliquer à distance, vous vous pouvez suivre les
[**flux vidéos** disponibles à partir du site web de la DebConf22](https://debconf22.debconf.org/)
des événements ayant lieu dans les trois salles de conférences :
_Drini_, _Lumbardhi_ et _Ereniku_.
Vous pouvez aussi vous joindre aux conversations sur ce qui se déroule dans les
salles de conférence:
  [**#debconf-drini**](https://webchat.oftc.net/?channels=#debconf-drini),
  [**#debconf-lumbardhi**](https://webchat.oftc.net/?channels=#debconf-lumbardhi) et
  [**#debconf-ereniku**](https://webchat.oftc.net/?channels=#debconf-ereniku)
(tous ces canaux se trouvent sur le canal IRC OFTC).

Vous pouvez aussi suivre la couverture en directe de nouvelles concernant la
DebConf22 sur
[https://micronews.debian.org](https://micronews.debian.org) ou sur le profil
@debian de votre réseau social favori.

La DebConf s'est engagée à assurer un environnement sûr et accueillant pour tous
les participants. Consultez la
[page web concernant le Code de Conduite sur le site web de la DebConf22](https://debconf22.debconf.org/about/coc/)
pour plus de détails à ce sujet.

Debian remercie les nombreux [parrains](https://debconf22.debconf.org/sponsors/)
pour leur engagement à soutenir la DebConf22, en particulier nos parrains de
Platine :
[**Lenovo**](https://www.lenovo.com),
[**Infomaniak**](https://www.infomaniak.com),
[**ITP Prizren**](https://itp-prizren.com)
et [**Google**](https://google.com).

![Logo d'inscription de DebConf22](|static|/images/debconf22-banner-700px.png)
