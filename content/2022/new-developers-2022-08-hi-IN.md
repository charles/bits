Title: नए डेबियन डेवलपर्स और मेंटेनर्स (जुलाई और अगस्त 2022)
Slug: new-developers-2022-08
Date: 2022-09-26 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: hi-IN
Translator: Yashraj Moghe
Status: published

पिछले दो महीनों में निम्नलिखित सहायकों को उनका डेबियन डेवलपर अकाउंट मिला:

* Sakirnth Nagarasa (sakirnth)
* Philip Rinn (rinni)
* Arnaud Rebillout (arnaudr)
* Marcos Talau (talau)

और निम्नलिखित को डेबियन मेंटेनर का स्थान दिया गया हैं:

* Xiao Sheng Wen
* Andrea Pappacoda
* Robin Jarry
* Ben Westover
* Michel Alexandre Salim

इन्हें बधाईयाँ!
