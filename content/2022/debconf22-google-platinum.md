Title: Google Platinum Sponsor of DebConf22
Slug: debconf22-google-platinum
Date: 2022-05-05 10:00
Author: The Debian Publicity Team
Artist: Google
Tags: debconf22, debconf, sponsors, google
Image: /images/debconf22-banner-700px.png
Status: published

[![Googlelogo](|static|/images/google.png)](https://www.google.com)

We are very pleased to announce that [**Google**](https://www.google.com)
has committed to support [DebConf22](https://debconf22.debconf.org) as a
**Platinum sponsor**. This is the third year in a row that Google is
sponsoring The Debian Conference with the higher tier!

Google is one of the largest technology companies in the
world, providing a wide range of Internet-related services and products
as online advertising technologies, search, cloud computing, software, and
hardware.

Google has been supporting Debian by sponsoring DebConf since more than
ten years, and is also a Debian partner sponsoring parts
of [Salsa](https://salsa.debian.org)'s continuous integration infrastructure
within Google Cloud Platform.

With this additional commitment as Platinum Sponsor for DebConf22,
Google contributes to make possible our annual conference,
and directly supports the progress of Debian and Free Software
helping to strengthen the community that continues to collaborate on
Debian projects throughout the rest of the year.

Thank you very much Google, for your support of DebConf22!

## Become a sponsor too!

[DebConf22](https://debconf22.debconf.org) **will take place from July 17th to
24th, 2022 at the [Innovation and Training Park (ITP)](https://itp-prizren.com)
in Prizren, Kosovo**, and will be preceded by DebCamp, from July 10th to 16th.

And DebConf22 is still accepting sponsors!
Interested companies and organizations may contact the DebConf team
through [sponsors@debconf.org](mailto:sponsors@debconf.org), and
visit the DebConf22 website at
[https://debconf22.debconf.org/sponsors/become-a-sponsor](https://debconf22.debconf.org/sponsors/become-a-sponsor).

![DebConf22 banner open registration](|static|/images/debconf22-banner-700px.png)
