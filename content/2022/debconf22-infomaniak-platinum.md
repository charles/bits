Title: Infomaniak Platinum Sponsor of DebConf22
Slug: debconf22-infomaniak-platinum
Date: 2022-04-06 12:30
Author: The Debian Publicity Team
Artist: Infomaniak
Tags: debconf22, debconf, sponsors, infomaniak
Image: /images/debconf22-banner-700px.png
Status: published

[![infomaniaklogo](|static|/images/infomaniak.png)](https://www.infomaniak.com)

We are very pleased to announce that
[**Infomaniak**](https://www.infomaniak.com)
has committed to support [DebConf22](https://debconf22.debconf.org) as a
**Platinum sponsor**. This is the fourth year in a row that Infomaniak is
sponsoring The Debian Conference with the higher tier!

Infomaniak is Switzerland's largest web-hosting company,
also offering backup and storage services, solutions for event organizers,
live-streaming and video on demand services.
It wholly owns its datacenters and all elements critical
to the functioning of the services and products provided by the company
(both software and hardware).

With this commitment as Platinum Sponsor,
Infomaniak contributes to make possible our annual conference,
and directly supports the progress of Debian and Free Software
helping to strengthen the community that continues to collaborate on
Debian projects throughout the rest of the year.

Thank you very much Infomaniak, for your support of DebConf22!

## Become a sponsor too!

[DebConf22](https://debconf22.debconf.org) **will take place from July 17th to
24th, 2022 at the [Innovation and Training Park (ITP)](https://itp-prizren.com)
in Prizren, Kosovo**, and will be preceded by DebCamp, from July 10th to 16th.

And DebConf22 is still accepting sponsors!
Interested companies and organizations may contact the DebConf team
through [sponsors@debconf.org](mailto:sponsors@debconf.org), and
visit the DebConf22 website at
[https://debconf22.debconf.org/sponsors/become-a-sponsor](https://debconf22.debconf.org/sponsors/become-a-sponsor).

![DebConf22 banner open registration](|static|/images/debconf22-banner-700px.png)
