Title: Nya Debianutvecklare och Debian Maintainers (Januari och Februari 2022)
Slug: new-developers-2022-02
Date: 2022-03-21 17:00
Author: Jean-Pierre Giraud
Tags: project
Lang: sv
Translator: Andreas Rönnquist
Status: published

Följande bidragslämnaren fick sina Debianutvecklarkonton under de senaste två
månaderna:

* Francisco Vilmar Cardoso Ruviaro (vilmar)

Följande bidragslämnare adderades som Debian Maintainers under de senaste två
månaderna:

* Lu YaNing
* Mathias Gibbens
* Markus Blatt
* Peter Blackman
* David da Silva Polverari

Grattis!
