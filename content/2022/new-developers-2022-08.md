Title: New Debian Developers and Maintainers (July and August 2022)
Slug: new-developers-2022-08
Date: 2022-09-26 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: en
Status: published

The following contributors got their Debian Developer accounts in the last two
months:

* Sakirnth Nagarasa (sakirnth)
* Philip Rinn (rinni)
* Arnaud Rebillout (arnaudr)
* Marcos Talau (talau)

The following contributors were added as Debian Maintainers in the last two
months:

* Xiao Sheng Wen
* Andrea Pappacoda
* Robin Jarry
* Ben Westover
* Michel Alexandre Salim

Congratulations!
