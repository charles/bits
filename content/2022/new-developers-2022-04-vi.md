Title: Những nhà Phát triển và Bảo trì Debian mới (Tháng ba và tư 2022)
Slug: new-developers-2022-04
Date: 2022-05-13 17:00
Author: Jean-Pierre Giraud
Tags: project
Lang: vi
Translator: Trần Ngọc Quân
Status: published

Trong hai tháng qua, những người đóng góp sau đây nhận được tài khoản Phát triển Debian:

* Henry-Nicolas Tourneur (hntourne)
* Nick Black (dank)

Trong hai tháng qua, những người đóng góp sau đây được thêm vào với tư cách là người Bảo trì Debian:

* Jan Mojžíš
* Philip Wyett
* Thomas Ward
* Fabio Fantoni
* Mohammed Bilal
* Guilherme de Paula Xavier Segundo

Xin chúc mừng!
