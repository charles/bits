Title: Nouveaux développeurs et mainteneurs de Debian (janvier et février 2022)
Slug: new-developers-2022-02
Date: 2022-03-21 17:00
Author: Jean-Pierre Giraud
Tags: project
Lang: fr
Status: published

Le contributeur suivant est devenu développeur Debian ces deux derniers mois :

* Francisco Vilmar Cardoso Ruviaro (vilmar)

et ces contributeurs ont été acceptés comme mainteneurs Debian durant la même
période :

* Lu YaNing
* Mathias Gibbens
* Markus Blatt
* Peter Blackman
* David da Silva Polverari

Félicitations !
