Title: Những nhà Phát triển và Bảo trì Debian mới (Tháng mười một và mười hai 2021)
Slug: new-developers-2021-12
Date: 2022-01-13 17:00
Author: Jean-Pierre Giraud
Tags: project
Lang: vi
Translator: Trần Ngọc Quân
Status: published

Trong hai tháng qua, những người đóng góp sau đây nhận được tài khoản Phát triển Debian:

* Douglas Andrew Torrance (dtorrance)
* Mark Lee Garrett (lee)

Trong hai tháng qua, những người đóng góp sau đây được thêm vào với tư cách là người Bảo trì Debian:

* Lukas Matthias Märdian
* Paulo Roberto Alves de Oliveira
* Sergio Almeida Cipriano Junior
* Julien Lamy
* Kristian Nielsen
* Jeremy Paul Arnold Sowden
* Jussi Tapio Pakkanen
* Marius Gripsgard
* Martin Budaj
* Peymaneh
* Tommi Petteri Höynälänmaa

Xin chúc mừng!
