Title: Novos(as) desenvolvedores(as) e mantenedores(as) Debian (março e abril de 2022)
Slug: new-developers-2022-04
Date: 2022-05-13 17:00
Author: Jean-Pierre Giraud
Tags: project
Lang: pt
Status: published

Os(As) seguintes colaboradores(as) do projeto se tornaram Desenvolvedores(as)
Debian nos últimos dois meses:

* Henry-Nicolas Tourneur (hntourne)
* Nick Black (dank)

Os(As) seguintes colaboradores(as) do projeto se tornaram Mantenedores(as)
Debian nos últimos dois meses:

* Jan Mojžíš
* Philip Wyett
* Thomas Ward
* Fabio Fantoni
* Mohammed Bilal
* Guilherme de Paula Xavier Segundo

Parabéns a todos!
