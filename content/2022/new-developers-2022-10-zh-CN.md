Title: 新的 Debian 开发者和维护者 (2022年9月 至 年10月)
Slug: new-developers-2022-10
Date: 2022-11-30 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: zh-CN
Translator: Boyuan Yang
Status: published

下列贡献者在过去的两个月中获得了他们的 Debian 开发者帐号：

* Abraham Raji (abraham)
* Phil Morrell (emorrp1)
* Anupa Ann Joseph (anupa)
* Mathias Gibbens (gibmat)
* Arun Kumar Pariyar (arun)
* Tino Didriksen (tinodidriksen)

下列贡献者在过去的两个月中被添加为 Debian 维护者：

* Gavin Lai
* Martin Dosch
* Taavi Väänänen
* Daichi Fukui
* Daniel Gröber
* Vivek K J
* William Wilson
* Ruben Pollan

祝贺他们！
