Title: New Debian Developers and Maintainers (September and October 2022)
Slug: new-developers-2022-10
Date: 2022-11-30 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: en
Status: published

The following contributors got their Debian Developer accounts in the last two
months:

* Abraham Raji (abraham)
* Phil Morrell (emorrp1)
* Anupa Ann Joseph (anupa)
* Mathias Gibbens (gibmat)
* Arun Kumar Pariyar (arun)
* Tino Didriksen (tinodidriksen)

The following contributors were added as Debian Maintainers in the last two
months:

* Gavin Lai
* Martin Dosch
* Taavi Väänänen
* Daichi Fukui
* Daniel Gröber
* Vivek K J
* William Wilson
* Ruben Pollan

Congratulations!
