Title: Nuevos desarrolladores y mantenedores de Debian (julio y agosto del 2022)
Slug: new-developers-2022-08
Date: 2022-09-26 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: es
Status: published

Los siguientes colaboradores del proyecto se convirtieron en Debian Developers
en los dos últimos meses:

* Sakirnth Nagarasa (sakirnth)
* Philip Rinn (rinni)
* Arnaud Rebillout (arnaudr)
* Marcos Talau (talau)

Los siguientes colaboradores del proyecto se convirtieron en Debian Maintainers
en los dos últimos meses:

* Xiao Sheng Wen
* Andrea Pappacoda
* Robin Jarry
* Ben Westover
* Michel Alexandre Salim

¡Felicidades a todos!
