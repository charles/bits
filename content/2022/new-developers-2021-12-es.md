Title: Nuevos desarrolladores y mantenedores de Debian (noviembre y diciembre del 2021)
Slug: new-developers-2021-12
Date: 2022-01-13 17:00
Author: Jean-Pierre Giraud
Tags: project
Lang: es
Status: published

Los siguientes colaboradores del proyecto se convirtieron en Debian Developers
en los dos últimos meses:

* Douglas Andrew Torrance (dtorrance)
* Mark Lee Garrett (lee)

Los siguientes colaboradores del proyecto se convirtieron en Debian Maintainers
en los dos últimos meses:

* Lukas Matthias Märdian
* Paulo Roberto Alves de Oliveira
* Sergio Almeida Cipriano Junior
* Julien Lamy
* Kristian Nielsen
* Jeremy Paul Arnold Sowden
* Jussi Tapio Pakkanen
* Marius Gripsgard
* Martin Budaj
* Peymaneh
* Tommi Petteri Höynälänmaa

¡Felicidades a todos!
