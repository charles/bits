Title: DebConf22 welcomes its sponsors!
Slug: debconf22-welcomes-sponsors
Date: 2022-07-18 09:00
Author: The Debian Publicity Team
Tags: debconf, debconf22, sponsors
Lang: en
Image: /images/debconf22-official-logo.png
Status: published

[DebConf22](https://debconf22.debconf.org) is taking place in Prizren, Kosovo,
from 17th to 24th July, 2022.
It is the 23rd edition of the Debian conference and organizers are working hard
to create another interesting and fruitful event for attendees.

We would like to warmly welcome the sponsors of DebConf22, and
introduce you to them.

We have four Platinum sponsors.

Our first Platinum sponsor is [**Lenovo**](https://www.lenovo.com).
As a global technology leader manufacturing a wide portfolio of connected
products, including smartphones, tablets, PCs and workstations as well as
AR/VR devices, smart home/office and data center solutions, Lenovo
understands how critical open systems and platforms are to a connected world.

[**Infomaniak**](https://www.infomaniak.com/en) is our second Platinum sponsor.
Infomaniak is Switzerland's largest web-hosting company,
also offering backup and storage services, solutions for event organizers,
live-streaming and video on demand services.
It wholly owns its datacenters and all elements critical
to the functioning of the services and products provided by the company
(both software and hardware).

The [**ITP Prizren**](https://itp-prizren.com) is our third Platinum sponsor.
ITP Prizren intends to be a changing and
boosting element in the area of ICT, agro-food and creatives industries,
through the creation and management of a favourable environment and efficient
services for SMEs, exploiting different kinds of innovations that can
contribute to Kosovo to improve its level of development in industry and
research, bringing benefits to the economy and society of the country as a
whole.

[**Google**](https://google.com) is our fourth  Platinum sponsor.
Google is one of the largest technology companies in the
world, providing a wide range of Internet-related services and products such
as online advertising technologies, search, cloud computing, software, and
hardware. Google has been supporting Debian by sponsoring DebConf for more than
ten years, and is also a Debian partner sponsoring parts
of [Salsa](https://salsa.debian.org)'s continuous integration infrastructure
within Google Cloud Platform.

Our Gold sponsors are:

[**Roche**](https://code4life.roche.com),
a major international pharmaceutical provider and research company dedicated to
personalized healthcare.

[**Microsoft**](https://www.microsoft.com),
enables digital transformation for the era of an intelligent cloud and an
intelligent edge. Its mission is to empower every person and every organization
on the planet to achieve more.

[**Ipko Telecommunications**](https://www.ipko.com),
provides telecommunication services and it is the first and the most
dominant mobile operator which offers fast-speed mobile internet – 3G and 4G
networks in Kosovo.

[**Ubuntu**](https://www.canonical.com),
the Operating System delivered by Canonical.

[**U.S. Agency for International Development**](https://www.usaid.gov/kosovo),
leads international development and humanitarian efforts to save lives, reduce
poverty, strengthen democratic governance and help people progress beyond
assistance.

Our Silver sponsors are:

[**Pexip**](https://www.pexip.com), is the video communications platform that
solves the needs of large organizations.
[**Deepin**](https://deepin.org/) is a Chinese commercial company focusing on
the development and service of Linux-based operating systems.
[**Hudson River Trading**](https://www.hudsonrivertrading.com),
a company researching and developing automated trading algorithms
using advanced mathematical techniques.
[**Amazon Web Services (AWS)**](https://aws.amazon.com),
is one of the world's most comprehensive and broadly adopted cloud platforms,
offering over 175 fully featured services from data centers globally.
The [**Bern University of Applied Sciences**](https://www.bfh.ch)
with near [7,800](https://www.bfh.ch/en/bfh/facts_figures.html) students
enrolled, located in the Swiss capital.
[**credativ**](http://www.credativ.de),
a service-oriented company focusing on open-source software and also a
[Debian development partner](https://www.debian.org/partners).
[**Collabora**](https://www.collabora.com),
a global consultancy delivering Open Source software solutions to the
commercial world.
[**Arm**](https://www.arm.com): with the world’s Best SoC Design Portfolio,
Arm powered solutions have been supporting innovation for more than 30 years
and are deployed in over 225 billion chips to date.
[**GitLab**](https://about.gitlab.com/solutions/open-source),
an open source end-to-end software development platform with built-in version
control, issue tracking, code review, CI/CD, and more.
[**Two Sigma**](https://www.twosigma.com),
rigorous inquiry, data analysis, and invention to help solve the toughest
challenges across financial services.
[**Starlabs**](https://www.starlabs.dev), builds software experiences and focus
on building teams that deliver creative Tech Solutions for our clients.
[**Solaborate**](https://www.solaborate.com), has the world’s most integrated
and powerful virtual care delivery platform.
[**Civil Infrastructure Platform**](https://www.cip-project.org),
a collaborative project hosted by the Linux Foundation,
establishing an open source “base layer” of industrial grade software.
[**Matanel Foundation**](http://www.matanel.org), operates in Israel,
as its first concern is to preserve the cohesion of a society and a nation
plagued by divisions.

Bronze sponsors:

[**bevuta IT**](https://www.bevuta.com/en),
[**Kutia**](https://kutia.net),
[**Univention**](https://www.univention.com),
[**Freexian**](https://www.freexian.com/services/debian-lts.html).

And finally, our Supporter level sponsors:

[**Altus Metrum**](https://altusmetrum.org),
[**Linux Professional Institute**](https://www.lpi.org),
[**Olimex**](https://www.olimex.com),
[**Trembelat**](https://trembelat.com),
[**Makerspace IC Prizren**](https://makerspaceprizren.com),
[**Cloud68.co**](https://cloud68.co),
[**Gandi.net**](https://www.gandi.net/en-US),
[**ISG.EE**](https://isg.ee.ethz.ch),
[**IPKO Foundation**](https://ipkofoundation.org),
[**The Deutsche Gesellschaft für Internationale Zusammenarbeit (GIZ) GmbH**](https://www.giz.de/en/worldwide/298.html).

Thanks to all our sponsors for their support!
Their contributions make it possible for a large number
of Debian contributors from all over the globe to work together,
help and learn from each other in DebConf22.

![DebConf22 logo](|static|/images/debconf22-official-logo.png)
