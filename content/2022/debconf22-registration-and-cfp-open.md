Title: DebConf22 registration and call for proposals are open!
Date: 2022-03-18 21:10
Slug: debconf22-registration-and-call-for-proposals-are-open
Author: The Debian Publicity Team
Tags: debconf, debconf22
Lang: en
Image: /images/debconf22-banner-700px.png
Status: published

![DebConf22 banner open registration](|static|/images/debconf22-banner-700px.png)

Registration for [DebConf22](https://debconf22.debconf.org) is now open.
The the 23rd edition of [DebConf](https://www.debconf.org) **will take place
from July 17th to 24th, 2022 at the
[Innovation and Training Park (ITP)](https://itp-prizren.com) in Prizren,
Kosovo**, and will be preceded by DebCamp, from July 10th to 16th.

Along with the registration, the DebConf content team announced the
[call for proposals](https://debconf22.debconf.org/cfp). **Deadline to submit a
proposal to be considered in the main schedule is April 15th, 2022 23:59:59 UTC
(Friday).**

[DebConf](https://www.debconf.org) is an event open to everyone, no matter how
you identify yourself or how others perceive you. We want to increase visibility
of our diversity and work towards inclusion at Debian Project, drawing our
attendees from people just starting their Debian journey, to seasoned Debian
Developers or active contributors in different areas like packaging,
translation, documentation, artwork, testing, specialized derivatives, user
support and many other. In other words, all are welcome.

To register for the event, log into the
[registration system](https://debconf22.debconf.org/register)
and fill out the form.
You will be able to edit and update your registration at any point. However, in
order to help the organizers have a better estimate of how many people will
attend the event, we would appreciate if you could access the system and confirm
(or cancel) your participation in the conference as soon as you know if you will
be able to come. **The last day to confirm or cancel is July 1st, 2022 23:59:59
UTC**. If you don't confirm or you register after this date, you can come to
the DebConf22 but we cannot guarantee availability of accommodation, food and
swag (t-shirt, bag, and so on).

For more information about registration, please visit
[registration information](https://debconf22.debconf.org/about/registration).

## Submitting an event

You can now submit an [event proposal](https://debconf22.debconf.org/talks/new).
Events are not limited to traditional presentations or informal sessions (BoFs):
we welcome submissions of tutorials, performances, art installations, debates,
or any other format of event that you think would be of interest to the Debian
community.

Regular sessions may either be 20 or 45 minutes long (including time for
questions), other kinds of sessions (workshops, demos, lightning talks, and so
on) could have different durations. Please choose the most suitable duration for
your event and explain any special requests.

In order to submit a talk, you will need to create an account on the website.
We suggest that Debian Salsa account holders (including DDs and DMs) use
their [Salsa](https://salsa.debian.org) login when creating an account. However,
this isn't required, as you can sign up with an e-mail address and password.

## Bursary for travel, accommodation and meals

In an effort to widen the diversity of DebConf attendees, the Debian Project
allocates a part of the financial resources obtained through sponsorships to pay
for bursaries (travel, accommodation, and/or meals) for participants who request
this support when they register.

As resources are limited, we will examine the requests and decide who will
receive the bursaries. They will be destined:

* To active Debian contributors.
* To promote diversity: newcomers to Debian and/or DebConf, especially from
  under-represented communities.

Giving a talk, organizing an event or helping during DebConf22 is taken intoa
account when deciding upon your bursary, so please mention them in your bursary
application.

For more information about bursaries, please visit
[applying for a bursary to DebConf](https://debconf22.debconf.org/about/bursaries).

**Attention:** the registration for DebConf22 will be open until the conference
starts, but the **deadline to apply for bursaries using the registration form
before May 1st, 2022 23:59:59 UTC**.
This deadline is necessary in order to the organizers use time to analyze the
requests, and for successful applicants to prepare for the conference.

DebConf would not be possible without the generous support of all our sponsors,
especially our Platinum Sponsors [**Lenovo**](https://www.lenovo.com) and
[**Infomaniak**](https://www.infomaniak.com).

DebConf22 is accepting sponsors; if you are interested, or think
you know of others who would be willing to help,
[please get in touch](https://debconf22.debconf.org//sponsors/become-a-sponsor)!
