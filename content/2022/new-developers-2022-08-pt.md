Title: Novos(as) desenvolvedores(as) e mantenedores(as) Debian (julho e agosto de 2022)
Slug: new-developers-2022-08
Date: 2022-09-26 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: pt
Status: published

Os(As) seguintes colaboradores(as) do projeto se tornaram Desenvolvedores(as)
Debian nos últimos dois meses:

* Sakirnth Nagarasa (sakirnth)
* Philip Rinn (rinni)
* Arnaud Rebillout (arnaudr)
* Marcos Talau (talau)

Os(As) seguintes colaboradores(as) do projeto se tornaram Mantenedores(as)
Debian nos últimos dois meses:

* Xiao Sheng Wen
* Andrea Pappacoda
* Robin Jarry
* Ben Westover
* Michel Alexandre Salim

Parabéns a todos!
