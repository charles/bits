Title: Debian Project Leader election 2022, Jonathan Carter re-elected
Slug: dpl-elections-2022
Date: 2022-04-21 19:30
Author: Laura Arjona Reina
Tags: dpl, election, leader, vote
Status: published

The voting period and tally of votes for the Debian Project Leader election
has just concluded, and the winner is Jonathan Carter, who has been elected
for third time. Congratulations! The new term for the project leader starts on
2022-04-21.

354 of 1,023 Developers voted using the
[Condorcet method](http://en.wikipedia.org/wiki/Condorcet_method).

More information about the results of the voting are available on the
[Debian Project Leader Elections 2022](https://www.debian.org/vote/2022/vote_002)
page.

Many thanks to Felix Lechner, Jonathan Carter and Hideki Yamane for their
campaigns, and to our Developers for voting.
