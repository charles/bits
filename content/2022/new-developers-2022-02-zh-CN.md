Title: 新的 Debian 开发者和维护者 (2022年1月 至 2月)
Slug: new-developers-2022-02
Date: 2022-03-21 17:00
Author: Jean-Pierre Giraud
Tags: project
Lang: zh-CN
Translator: Boyuan Yang
Status: published

下列贡献者在过去的两个月中获得了他的 Debian 开发者帐号：

* Francisco Vilmar Cardoso Ruviaro (vilmar)

下列贡献者在过去的两个月中被添加为 Debian 维护者：

* Lu YaNing
* Mathias Gibbens
* Markus Blatt
* Peter Blackman
* David da Silva Polverari

祝贺他们！
