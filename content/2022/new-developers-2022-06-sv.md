Title: Nya Debianutvecklare och Debian Maintainers (Maj och Juni 2022)
Slug: new-developers-2022-06
Date: 2022-07-29 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: sv
Translator: Andreas Rönnquist
Status: published

Följande bidragslämnare fick sina Debianutvecklarkonton under de senaste två
månaderna:

* Geoffroy Berret (kaliko)
* Arnaud Ferraris (aferraris)

Följande bidragslämnare adderades som Debian Maintainers under de senaste två
månaderna:

* Alec Leanas
* Christopher Michael Obbard
* Lance Lin
* Stefan Kropp
* Matteo Bini
* Tino Didriksen

Grattis!
