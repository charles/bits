Title: Novos(as) desenvolvedores(as) e mantenedores(as) Debian (janeiro e fevereiro de 2022)
Slug: new-developers-2022-02
Date: 2022-03-21 17:00
Author: Jean-Pierre Giraud
Tags: project
Lang: pt
Status: published

O seguinte colaborador do projeto se tornou Desenvolvedor Debian nos últimos
dois meses:

* Francisco Vilmar Cardoso Ruviaro (vilmar)

Os(As) seguintes colaboradores(as) do projeto se tornaram Mantenedores(as)
Debian nos últimos dois meses:

* Lu YaNing
* Mathias Gibbens
* Markus Blatt
* Peter Blackman
* David da Silva Polverari

Parabéns a todos(as)!
