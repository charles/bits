Title: DebConf22 schließt in Prizren, Datum der DebConf23 bekanntgegeben
Date: 2022-07-25 10:30
Tags: debconf22, debconf23, announce, debconf
Slug: debconf22-closes
Author: Debian Publicity Team
Artist: Aigars Mahinovs
Lang: de
Translator: Erik Pfannenstein
Image: /images/debconf22_group_small.jpg
Status: published

[![DebConf22 group photo - click to enlarge](|static|/images/debconf22_group_small.jpg)](https://wiki.debian.org/DebConf/22/Photos?action=AttachFile&do=view&target=debconf23_group_photo.jpg)

Am Sonntag, dem 24. Juli 2022, kam die jährliche
Debian-Entwickler- und -Unterstützerkonferenz zu ihrem Abschluss. 210
Teilnehmerinnen und Teilnehmer aus 38 Ländern trafen sich zu insgesamt
91 Vorträgen, Diskussionsrunden, Birds-of-a-Feather-Versammlungen (BoF),
Workshops und Unternehmungen; somit war die
[DebConf22](https://debconf22.debconf.org) ein großer Erfolg.

Der Konferenz ging vom 10. bis 16. Juli das alljährliche DebCamp voran,
das seinen Fokus auf Einzelarbeiten und Team-Sprints, in denen die
Entwicklung von Debian in persönlichem Kontakt vorangebracht wurde,
richtete. Dieses Jahr gab es Sprints vor allem zu den Themen
Mobian/Debian auf Mobilgeräten, reproduzierbares Bauen und Python in
Debian. Ein BootCamp machte neue Mitglieder mit Debian vertraut und
ermöglichte es ihnen, das System und die Teilnahme an der Gemeinschaft
interaktiv zu erkunden.

Die eigentliche Debian-Entwicklerkonferenz begann dann am Sonntag, dem 17.
Juli 2022. Neben Aktivitäten wie dem traditionellen 'Bits from the
DPL'-Vortrag, der nie endenden Schlüssel-Signierungs-Party,
Lightning-Vorträgen und der Bekanntgabe der nächsten DebConf
([DebConf23](https://wiki.debian.org/DebConf/23) in Kochi, Indien)
wurden mehrere Sitzungen veranstaltet, die sich um
Programmiersprachen-Teams wie Python, Perl und Ruby drehten, darüber
hinaus gab es Neuigkeiten von verschiedenen Projekten und
Debian-internen Teams, Diskussionsrunden (BoFs) zahlreicher
Technik-Teams (Langzeitunterstützung (LTS), Android-Werkzeuge,
Debian-Derivate, Debian-Installer und -Abbilder, Debian Science usw.)
sowie lokaler Gemeinschaften (Debian Brasilien, Debian Indien, das
Debian Local Team) und viele andere Veranstaltungen zu Debian und Freier
Software.

Der [Zeitplan](https://debconf22.debconf.org/schedule/) wurde täglich
mit geplanten und spontanen Unternehmungen ergänzt, die von den
Anwesenden im Verlauf der DebConf angestoßen wurden. Mehrere
Aktivitäten, die in den letzten Jahren wegen der COVID-Pandemie nicht
durchgeführt werden konnten, kehrten wieder auf den Konferenzzeitplan
zurück: eine Jobbörse, eine Nacht der offenen Bühne und Gedichte, die
klassische Käse-und-Wein-Party, Grupppenfotos und der Tagesausflug.

Für diejenigen, die nicht dabei sein konnten, wurden die meisten
Vorträge und Sitzungen live ausgestrahlt und aufgezeichnet; die
Aufzeichnungen sind auf der
[Debian-Meetings-Archiv-Website](https://meetings-archive.debian.net/pub/debian-meetings/2022/DebConf22/)
zu finden. Fast alle Sitzungen ermöglichten eine Fernteilnahme per IRC
und gemeinsam online bearbeitete Textdokumente.

Die [DebConf22-Website](https://debconf22.debconf.org/) bleibt zu
Archivzwecken erreichbar und wird auch weiterhin Links zu den Vorträgen
und Videos vorhalten.

Die nächste DebConf, die
[DebConf23](https://wiki.debian.org/DebConf/23), wird von 10. bis 16.
September 2022 in Kochi in Indien stattfinden. Der Tradition folgend
wird der Konferenz ein DebCamp (03. bis 09. September 2023) vorausgehen,
das sich auf die Einzel- und Teambeiträge zur Distribution konzentrieren
wird.

Der DebConf ist eine sichere und angenehme Umgebung für alle Anwesenden
wichtig. Siehe den
[Verhaltenskodex auf der DebConf22-Website](https://debconf22.debconf.org/about/coc/)
für weitere Einzelheiten zu diesem Thema.

Debian dankt den vielen [Sponsoren](https://debconf22.debconf.org/sponsors/),
welche mit ihrem Einsatz die DebConf22 unterstützt haben, vor allem unsere
Platin-Sponsoren:
[**Lenovo**](https://www.lenovo.com),
[**Infomaniak**](https://www.infomaniak.com),
[**ITP Prizren**](https://itp-prizren.com/)
und [**Google**](https://google.com/).

### Über Debian

Das Debian-Projekt wurde 1993 von Ian Murdock gegründet und sollte ein
vollständig freies Gemeinschaftsprojekt sein. Seitdem ist es zu einem
der größten und einflussreichsten Open-Source-Projekte angewachsen.
Tausende von Freiwilligen aus aller Welt helfen zusammen, um
Debian-Software zu erschaffen und zu pflegen. Wegen seiner Verfügbarkeit
in 70 Sprachen und der Unterstützung für viele verschiedene
Computer-Typen bezeichnet sich Debian als das "universelle
Betriebssystem".

### Über die DebConf

Die DebConf ist Debians Entwicklerkonferenz. Neben einem Zeitplan voller
technischer, sozialer und politischer Vorträge bietet die DebConf eine
Möglichkeit für Entwickler, Unterstützer und interessierte Personen,
sich persönlich zu treffen und enger zusammenzuarbeiten. Sie findet seit
dem Jahr 2000 an verschiedenen Orten statt, unter anderem in Schottland,
Argentinien und Bosnien-Herzegowina. Weitere Informationen über die
DebConf sind auf [https://debconf.org](https://debconf.org/) zu finden.

### Über Lenovo

Als globaler Technikvorreiter, der eine große Auswahl an vernetzten
Geräten, einschließlich Smartphones, Tablets, PCs und Worksations sowie
AR/VR-Geräte, Smart Home/Office und Datencenter-Lösungen herstellt, ist
sich [**Lenovo**](https://www.lenovo.com) bewusst, wie kritisch offene
Systeme und Plattformen für eine vernetzte Welt sind.

### Über Infomaniak

[**Infomaniak**](https://www.infomaniak.com) ist der größte Webhoster der
Schweiz, der auch Backup- und Speicherdienste, Lösungen für Veranstalter
sowie Live-Streaming und Videos auf Abruf anbietet. Er befindet sich im
Vollbesitz seiner Datencenter sowie sämtlicher kritischer Bestandteile,
die für das Funktionieren der angebotenen Dienste und Produkte
unerlässlich sind (sowohl Software als auch Hardware).

### Über ITP Prizren

Der [**Innovations- und Ausbildungspark Prizren**](https://itp-prizren.com/)
hat sich zum Ziel gesetzt, eine treibende Kraft für Änderungen und
Stärkungen im Bereich der ICT, Agrar- und Künstlerindustrie zu sein,
indem er eine optimale Umgebung und effiziente Dienste für KMU
bereitstellt. Dabei nutzt er verschiedene Arten von Innovationen, die
dem Kosovo dabei helfen können, seinen Stand in Industrie und Forschung
zu verbessern und somit der Wirtschaft und der Gesellschaft im Land als
Ganzes Vorteile zu verschaffen.

### Über Google

[**Google**](https://google.com/) ist eines der größten Internetunternehmen
der Welt und bietet ein umfangreiches Sortiment an Internetbezogenen
Diensten und Produkten, unter anderem Online-Werbung, Suche, Cloud
Computing, Software und Hardware.

Google unterstützt Debian seit mehr als zehn Jahren durch Sponsern der
DebConf und als Debian-Partner betreibt es auch Teile der
Continuous-Integration-Infrastruktur von
[Salsa](https://salsa.debian.org) innerhalb der Google-Cloud-Plattform.

### Kontaktinformationen

Für weitere Informationen besuchen Sie bitte die DebConf22-Website unter
[https://debconf22.debconf.org/](https://debconf22.debconf.org/)
oder senden Sie eine E-Mail (auf Englisch) an <press@debian.org>.
