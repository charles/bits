Title: Novos(as) desenvolvedores(as) e mantenedores(as) Debian (maio e junho de 2022)
Slug: new-developers-2022-06
Date: 2022-07-29 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: pt
Status: published

Os(As) seguintes colaboradores(as) do projeto se tornaram Desenvolvedores(as)
Debian nos últimos dois meses:

* Geoffroy Berret (kaliko)
* Arnaud Ferraris (aferraris)

Os(As) seguintes colaboradores(as) do projeto se tornaram Mantenedores(as)
Debian nos últimos dois meses:

* Alec Leanas
* Christopher Michael Obbard
* Lance Lin
* Stefan Kropp
* Matteo Bini
* Tino Didriksen

Parabéns a todos!
