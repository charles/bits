Title: DPL Activity logs for April/May 2020
Date: 2020-06-03 19:11
Tags: dpl
Slug: dpl-updates-jcc-t01-m01
Author: Jonathan Carter
Status: published

### First month as DPL

I survived my first month as DPL! I agree with previous DPLs who have described
it as starting a whole new job. Fortunately it wasn't very stressful, but it
certainly was *very* time consuming. On the very first day my inbox exploded
with requests. I dealt with this by deferring anything that wasn't important
right away and just started working through it. Fortunately the initial swell
subsided as the month progressed. The bulk of my remaining e-mail backlog are a
few media outlets who wants to do interviews. I'll catch up with those during
this month.

Towards the end of the month, most of my focus was on helping to prepare for an
[online
MiniDebConf](https://wiki.debian.org/DebianEvents/internet/2020/MiniDebConfOnline)
that we hosted over the last weekend in May. We had lots of fun and we had some
great speakers sharing their knowledge and expertise during the weekend.

### Activity log

As I do on my own blog for free software activities, I'll attempt to keep a log
of DPL activities on this blog. Here's the log for the period 2020-04-21 to
2020-05-21:

**2020-04-19:** Handover session with Sam, our outgoing DPL. We covered a lot
of questions I had and main areas that the DPL works in. Thanks to Sam for
having taken the time to do this.

**2020-04-21:** First day of term! Thank you to everybody who showed support
and have offered to help!

**2020-04-21:** Request feedback from the trademark team on an ongoing
trademark dispute.

**2020-04-21**: Join the GNOME Advisory Board as a representative from Debian.

**2020-04-21:** Reply on an ongoing community conflict issue.

**2020-04-21:** Update Debian project summary for SPI annual report.

**2020-04-21:** Received a great e-mail introduction from Debian France and
followed up on that.

**2020-04-21:** Posted "[Bits from the new
DPL](https://lists.debian.org/debian-devel-announce/2020/04/msg00013.html)" to
debian-devel-announce.

**2020-04-22:** Became Debian's OSI Affilliate representative.

**2020-04-22:** Reply to a bunch of media inquiries for interviews, will do
them later when initial priorities are on track.

**2020-04-23:** Resign as Debian FTP team trainee and mailing list moderator.
In both these areas there are enough people taking care of it and I intend to
maximise available time for DPL and technical areas in the project.

**2020-04-25:** Review outgoing mail for trademark team.

**2020-04-25:** Answer some questions in preparation for DAM/Front Desk
delegation updates.

**2020-04-26:** Initiated wiki documentation for delegation [updates
process](https://wiki.debian.org/Teams/DPL/HowTo/DelegationUpdates).

**2020-04-27:** Update delegation for the [Debian Front Desk
team](https://lists.debian.org/debian-devel-announce/2020/04/msg00014.html).

**2020-04-29:** Schedule video call with Debian treasurer team.

**2020-04-29:** OSI affiliate call. Learned about some Open Source projects
including [OpenDev](https://opendev.org/),
[OpenSourceMatters](https://www.opensourcematters.org/organisation.html), [FOSS
Responders](https://fossresponders.com/) and [Democracy
Labs](https://www.democracylab.org/index/?section=Home).

**2020-05-04:** Delivered my first talk session as DPL titled ["Mixed Bag of
Debian"](https://peertube.debian.social/videos/watch/cdfe5b24-e3ad-4422-a6f2-15ca4fffd895)
at "Fique Em Casa Use Debian" (Stay at home and use Debian!), organised by
[Debian
Brazil](https://peertube.debian.social/accounts/debianbrazilteam/video-channels),
where they had a different talk every evening during the month of May. Great
initiative I hope other local groups consider copying their ideas!

**2020-05-05:** Had a 2 hour long call with the treasurer team. Feeling
optimistic for the future of Debian's financing although it will take some time
and a lot of work to get where we want to be.

**2020-05-17:** Respond to cloud delegation update.
